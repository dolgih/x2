/*
    js/editor.js
    Java Scrips services of post editor; x1 phorum application

    This file is a part of x1 project.
    http://x1-regonda.rhcloud.com

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

var editor=document.getElementById("content");
editor.init=function(){
  this.toolBar=document.createElement('div');
  this.parentNode.insertBefore(this.toolBar, this);
  this.toolBar.init = function(){
    var btnInit=function(bbcTag){
      this.bbcTag=bbcTag;
      this.title=bbcTag.title;
      this.src=media+"/bbcode/"+bbcTag.name+".gif";
      this.style.border="2px solid gray"; this.style.background='white';
      this.onmouseover=function(){this.style.border="2px solid OrangeRed";}
      this.onmouseout=function(){this.style.border="2px solid gray";}
      this.processInsertion=function(selected){
        var b=editor.selectionStart,e=editor.selectionEnd;
        var sel=editor.value.substring(b,e);
        var name=this.bbcTag.name,
          insertion=this.bbcTag.insertion||("["+name+(selected?"="+selected:"")+"]");
        editor.value=editor.value.substring(0,b)
            +insertion+sel+(this.bbcTag.single?"":"[/"+name+"]")
            + editor.value.substring(e,editor.value.length);
        editor.setSelectionRange(b+insertion.length,e+insertion.length);
        editor.focus();
      }
      this.onclick=function(){
        var values=this.bbcTag.values;
        if(values){
          var s=document.createElement("select"); s.bbcButton=this;
          s.style.position="absolute";
          s.style.left=this.offsetLeft.toString()+"px";
          s.style.top=this.offsetTop.toString()+"px";
          s.onmouseleave=function(){this.parentNode.removeChild(this);}
          s.onchange=function(){this.bbcButton.processInsertion(this.value);this.onmouseleave();}
          s.size=this.bbcTag.name=="size"?6:12;
          this.parentNode.appendChild(s);
          for(var i=0;i<values.length;i++){
              var opt=document.createElement("option");
              opt.value=values[i];
              if(this.bbcTag.name=="color")
                opt.style.color=values[i];
              if(this.bbcTag.name=="size")
                opt.style.fontSize=values[i];
              opt.text=values[i];
              s.appendChild(opt);
          }
        } else
          this.processInsertion();
      }
    }
    var tags=bbcodeData.tags;
    for(var category=1;category<bbcodeData.categoryBound;category++){
      var lastCat=0;
      for (var name in tags){
        if (tags.hasOwnProperty(name)&&tags[name].category==category){
          var btn=new Image(); btn.init=btnInit; btn.init(tags[name]);
          this.appendChild(btn);
          lastCat=category;
        }
      }
      if(lastCat)
        this.appendChild(document.createTextNode(" "));
    }
    // smiles
    var smilesInit=function(){
      this.title="Smiles";
      this.src=media+"/bbcode/subjectsmiley.gif";
      this.style.border="2px solid gray"; this.style.background='white';
      this.onmouseover=function(){this.style.border="2px solid Magenta";}
      this.onmouseout=function(){this.style.border="2px solid gray";}
      this.onclick=function(){
        var s=document.createElement("div");
        s.style.position="absolute";
        s.style.left=this.offsetLeft.toString()+"px";
        s.style.top=this.offsetTop.toString()+"px";
        s.style.width="7em";
        s.style.background="White";
        s.style.border="2px solid gray";
        s.onmouseleave=function(){this.parentNode.removeChild(this);}
        var smileInit=function(smile){
          this.smile=smile;
          this.title=smile.title;
          this.src=smile.path;
          this.style.width=this.style.height="1.5em";
          this.style.border="2px solid white"; this.style.background='white';
          this.onmouseover=function(){this.style.border="2px solid OrangeRed";}
          this.onmouseout=function(){this.style.border="2px solid white";}
          this.processInsertion=function(selected){
            var b=editor.selectionStart,e=editor.selectionEnd;
            var sel=editor.value.substring(b,e);
            var name=this.bbcTag.name,
              insertion=this.bbcTag.insertion||("["+name+(selected?"="+selected:"")+"]");
            editor.value=editor.value.substring(0,b)
                +insertion+sel+(this.bbcTag.single?"":"[/"+name+"]")
                + editor.value.substring(e,editor.value.length);
            editor.setSelectionRange(b+insertion.length,e+insertion.length);
            editor.focus();
          }
          this.onclick=function(){
            var b=editor.selectionStart,e=editor.selectionEnd;
            var insertion=" "+this.smile.code;
            editor.value=editor.value.substring(0,b)+
              insertion+editor.value.substring(e,editor.value.length);
            editor.setSelectionRange(b,b+insertion.length);
            editor.focus();
            this.allSmiles.onmouseleave();
          }
        }
        for(var code in this.smiles){
          if (this.smiles.hasOwnProperty(code)){
            var smile=new Image; smile.init=smileInit; smile.init(this.smiles[code]);
            smile.allSmiles=s;
            s.appendChild(smile);
          }
        }
        this.parentNode.appendChild(s);
      }
    }
    var btn=new Image(); btn.smiles=bbcodeData.smiles;
    btn.init=smilesInit; btn.init();
    this.appendChild(btn);
    // end of smiles
  }
  this.toolBar.init();
  // format
  this.format=document.getElementById("format");
  this.format.init=function(){
    this.onchange=function(){editor.toolBar.hidden=this.value!="bbcode";};
  }
  this.format.init();
  this.format.onchange();
  // abstract
  this.abstract=document.getElementById("abstract");
  this.abstract.init=function(){this.hidden=!this.value;}
  this.abstract.toggle=function(){
    this.hidden=!this.hidden;
    if(this.hidden)editor.focus();
    else this.focus();
  }
  this.abstract.init();
  // quote
  this.quote=Object.create(null);
  this.quote.selection=function(q){
    function isIn(node){
      while(node)
        if(node.classList&&(node.classList.contains("postright")
          ||node.classList.contains("page")))return true;
        else node=node.parentNode;
      return false;
    }
    var selected=window.getSelection();
    for(var i=0;i<selected.rangeCount;i++){
      var s=selected.getRangeAt(i);
      if(!isIn(s.commonAncestorContainer))return"";
    }
    return selected.toString();
  }
  this.quote.click=function(event,q,postId){
    var s=this.selection(q);
    if(s){
      event.preventDefault?event.preventDefault():(event.returnValue = false);
      editor.value+="[quote="+postId.toString()+"]"; var b=editor.value.length;
      editor.value+=s; var e=editor.value.length;
      editor.value+="[/quote]";
      editor.setSelectionRange(b,e);
      editor.focus();
    }
  }
}
editor.init();
