X2F=-q=f -n -cx -v -b -H -sl \
  -xl=.git/* \
  -xl=bin/nano -xl=src/x2 -xl=src/doc/html \
  -xl=*.log -xl=*.o -xl=*.report -xl=*.gch -xl=*.bak \
  -xl=src/bbcode/test -xl=src/bbcode/test.htm -xl=src/bbcode/libx2BbCode.so \
  -xl=src/mail/test -xl=src/mail/libx2mail.so \
  -xl=src/postgres/test -xl=src/postgres/libx2sql.so \
  -xl=src/db/x2.sql -xl=src/db/x2db.html \
  x2f
X2FG=-q=f -cx -v -sl x2f .git/config

.PHONY:	cmp
cmp:
	ad1 $(X2F)

.PHONY:	save
save:
	ad1 -u $(X2F)
	ad1 -u $(X2FG)
