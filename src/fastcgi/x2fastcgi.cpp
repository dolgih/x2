/**
    \file x2fastcgi.cpp
    \brief another fastcgi implementation

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/


#include "x2.h"
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/un.h>

namespace x2 {

static Str errstr( const char *msg ) {
  char buf[ 1024 ];
  return Str( msg ) + ": " + strerror_r( errno, buf, sizeof buf );
}

// FCGX_Request

// OutputCallback

void FCGX_Request::Socket::operator() ( void const *buf, size_t count ) {
  if( -1 == write( fd, buf, count ))
    throw std::runtime_error( errstr( "OutputCallback: write() failed" ));
}

// end of OutputCallback

int FCGX_Request::openSocket( const char *path, int backlog ) {
  if( ! path || ! *path ) {
    LOG_FATAL( "path is empty" );
    return -1;
  }

  const char *colon = strchr( path, ':' );
  int domain = colon ? AF_INET : AF_UNIX;

  socketId = ::socket( domain, SOCK_STREAM, 0 );

  if( socketId == -1 ) return LOG_ERROR( errstr( "Failed to create socket" )), -1;

  int optVal = 1;
  if( setsockopt( socketId, SOL_SOCKET, SO_REUSEADDR, &optVal, sizeof optVal ) == -1 )
    return LOG_ERROR( errstr( "setsockopt(SO_REUSEADDR) failed" )), -1;

  union { sockaddr_in in; sockaddr_un un; } addr; bzero( &addr, sizeof addr );
  socklen_t addrLen = sizeof addr.in;
  if( domain == AF_INET ) {
    addr.in.sin_family = AF_INET;
    addr.in.sin_port = htons( atoi( colon + 1 ));
    Str host( path, colon - path );
    if( host.empty()) addr.in.sin_addr.s_addr = INADDR_ANY;
    else addr.in.sin_addr.s_addr = inet_addr( host.data());
    if( ! addr.in.sin_port || INADDR_NONE == addr.in.sin_addr.s_addr )
      return LOG_FATAL( x2::utl::f( "bad address '{1}'", path )), -1;
  } else { // unix socket
    addr.un.sun_family = AF_UNIX;
    strncpy( addr.un.sun_path, path, sizeof addr.un.sun_path - 1 );
    unlink( path );
    addrLen = sizeof addr.un;
  }

  if( bind( socketId, reinterpret_cast<struct sockaddr *>( &addr ), addrLen ) == -1 )
    return LOG_ERROR( errstr( "Failed to get socket to bind" )), -1;

  if( listen( socketId, backlog ) == -1 )
    return LOG_ERROR( errstr( "Failed to get socket to listen" )), -1;

  return socketId;
}

int FCGX_Request::accept() try {
  finish();

  socket.set( ::accept( socketId, 0, 0 ));
  if( -1 == socket.get()) return LOG_ERROR( errstr( "accept()" )), -1;

  // Read input from socket and put it into the protocol driver
  // for processing.
  do {
    char buf[ 4 * 1024 ];
    ssize_t rc = read( socket.get(), buf, sizeof buf );
    if( rc < 0 ) return LOG_ERROR( errstr( "read() failed" )), -1;
    process_input( buf, rc );
    if( request == 0 )
      request = get_request();
    else
      if( request->stdin_stream.size() > x2::cfg.server.maxPostSize ) {
        LOG_WARN( "the request is too big" );
        break;
      }
  } while( ! request || ! request->stdin_eof );

  // Make sure we don't get a keep-connection request.
  if( request->keep_connection ) {
    request->end_request( 1, FCGIRequest::CANT_MPX_CONN ), request = 0;
    LOG_ERROR( "X2 program can't handle KEEP_CONNECTION" );
    return -1;
  }

  return socket.get();
}
catch( const std::exception &e ) {
  finish();
  LOG_ERROR( e.what());
  return -1;
}

void FCGX_Request::finish() {
  // Terminate the request.
  try {
    if( request ) request->end_request( 0, FCGIRequest::REQUEST_COMPLETE ), request = 0;
  }
  catch( const std::exception &e ) {
    LOG_ERROR( e.what());
    terminate_request( request->id );
  }
  socket.close();
}

void FCGX_Request::getStr( Str &str ) const {
  str = request->stdin_stream;
  request->stdin_stream = Str();
}

int FCGX_Request::putStr( const Str &str ) {
  try {
    request->write( str.data(), str.size());
  } catch( const std::runtime_error &e ) {
    LOG_ERROR( e.what());
    return -1;
  }
  return str.size();
}

const Str &FCGX_Request::param( const Str &name ) const {
  auto found = request->params.find( name );
  static const Str emptyString;
  if( found == request->params.end()) return emptyString;
  return found->second;
}


int FCGX_Request::socketId = 0;

// end of FCGX_Request

} // x2

