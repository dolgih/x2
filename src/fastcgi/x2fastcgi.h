/**
    \file x2fastcgi.cpp
    \brief another fastcgi implementation

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 {

struct FCGX_Request : FCGIProtocolDriver {
  FCGX_Request() : FCGIProtocolDriver( socket ) { }
  static int openSocket( const char *path, int backlog );
  int initRequest( int, int ) { return 0; }
  int accept();
  void finish();

  void getStr( Str &str ) const;
  int putStr( const Str &str );
  const Str &param( const Str &name ) const;
  const std::map<Str, Str> &params() const { return request->params; }

  bool isResponder() const { return request->role == FCGIRequest::RESPONDER; }
  size_t contentLength() const { return request->stdin_stream.size(); }

private:
  struct Socket : OutputCallback {
    ~Socket() override { close(); }
    void operator() ( void const *buf, size_t count ) override;
    void close() { if( fd != -1 ) ::close( fd ), fd = -1; }
    int get() const { return fd; }
    void set( int newFd ) { close(); fd = newFd; }

  private:
    int fd = -1;
  } socket;
  FCGIRequest *request = 0;

  static int socketId;
};

inline int FCGX_Init( void ) { return 0; }
inline int FCGX_OpenSocket( const char *path, int backlog ) { return FCGX_Request::openSocket( path, backlog ); }
inline int FCGX_InitRequest( FCGX_Request *request, int sock, int flags ) { return request->initRequest( sock, flags ); }
inline int FCGX_Accept_r( FCGX_Request *request ) { return request->accept(); }
inline void FCGX_Finish_r( FCGX_Request *request ) { request->finish(); }
inline void FCGX_ShutdownPending( void ) { }
inline void OS_LibShutdown( void ) { }

} // x2

