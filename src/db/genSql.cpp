/*
    genSql.cpp
    database descriptor - class for various manipulations
    with database (creating, documenting, verifying and so on)

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dbDescriptor.h"
extern "C" {
  #include <mkdio.h>
}

using namespace std;


#define checkIO(var, name) var(name); \
        if(!var.good()) { \
            cerr << "genSql error: can not open file: " << name << endl; \
            return 1; \
        }

int main(int argc, char *argv[]) {
    if(argc < 2) {
usage:
        cout << "Usage: genSql {MYSQL} | {POSTGRES} | {SQLITE} [NODATA]\n";
        return 2;
    }
    x2::dbDescriptor::DbType type;
    Str req = argv[1];
    if(req == "MYSQL") type = x2::dbDescriptor::MYSQL;
    else if(req == "POSTGRES") type = x2::dbDescriptor::POSTGRES;
    else if(req == "SQLITE") type = x2::dbDescriptor::SQLITE;
    else {
        cerr << "error: invalid argument: " << argv[1] << "\n";
        return 3;
    }
    bool noData = false;
    if(argc > 2) {
        if(Str(argv[2]) == "NODATA")
            noData = true;
        else
            goto usage;
    }
    if(argc > 3)
        goto usage;

    x2::dbDescriptor::Database database(type, noData);

    ofstream checkIO(sql, "x2.sql");
    ifstream checkIO(synopsis, "synopsis.md");
    std::ostringstream out; out  << "(" << req << " version)\n\n" << synopsis.rdbuf()
      <<  database.str(true) + "\n";
    sql << "-- " << req  << " version\n\n" << database.str();

    MMIOT* mkd = mkd_string(out.str().data(), out.str().size(), MKD_AUTOLINK|MKD_TOC );
    if( !mkd ) {
        std::cerr << "mkd_string failed\n";
        return 4;
    }
    mkd_compile(mkd, MKD_AUTOLINK|MKD_TOC );
    char *ptr = 0;
    int size = 0;
    size = mkd_document(mkd,&ptr);
    Str content(ptr,size);
    size = mkd_toc(mkd, &ptr);
    Str toc(ptr, size);
    free(ptr); mkd_cleanup(mkd);
    //text << endl; // markdown bug
    ofstream checkIO(html, "x2db.html");
    html << "<html><head>\n"
      "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
      "<title>X2 Project Database.</title>\n</head>\n<body>\n" << toc <<  content
      << "\n</body>\n</html>\n";
    return 0;
}
