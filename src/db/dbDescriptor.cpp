/*
    dbDescriptor.cpp
    database descriptor - class for various manipulations
    with database (creating, documenting, verifying and so on)

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include <assert.h>
#include <sstream>
#include "dbDescriptor.h"

using namespace std;

namespace x2 { namespace dbDescriptor {

Database::Database(DbType type, bool noData) : Db(type, noData) {

  prep("DROP TABLE IF EXISTS topics_log");
  prep("DROP TABLE IF EXISTS posts_log");

/*******
  users
********/
  Table users("users", "all authorized x2 users"); users
  +Column("id", Column::SERIAL | Column::PRIMARY | Column::NOTNULL)
  +Column("name", Column::VARCHAR | Column::UNIQUE | Column::NOTNULL, 32)
  +Column("password", Column::CHAR | Column::NOTNULL, 40)
  +Column("email", Column::VARCHAR | Column::UNIQUE| Column::NOTNULL, 128)
  +Column("phone", Column::VARCHAR | Column::NOTNULL | Column::DEFAULTESTR, 16)
  +Column("properties", Column::TEXT | Column::NOTNULL | Column::DEFAULTESTR)
  +Column("ip", Column::VARCHAR | Column::NOTNULL | Column::DEFAULTESTR, 50)
  +Column("posts_number", Column::INTEGER | Column::NOTNULL | Column::DEFAULT0)
  +Column("last_activity", Column::DATETIME | Column::NOTNULL).def("'1999-01-01 00:00:00'")
  +Column("created", Column::TIMESTAMP | Column::NOTNULL)
  +Column("modified", Column::TIMESTAMP | Column::NOTNULL);
  users.serialId = "users_id_seq"; users.serial = 101;

  users.data(
R"(INSERT INTO users(id,name,password,email,properties) VALUES(-1,'guest','',
  'guest@volga.unaux.com','{"avatar":"/media/stdAvatars/gone.jpg"}');
INSERT INTO users(id,name,password,email,phone,properties)
  VALUES(1,'admin','dd94709528bb1c83d08f3088d4043f4742891f4f','admin@volga.unaux.com',
  '(+78453)35-15-68', '{"human_name":"Admin","avatar":"/media/stdAvatars/poison.png",
  "web_site":"https://bitbucket.org/dolgih/","phone2":"+7-917-0230-917"}');
INSERT INTO users(id,name,password,email,properties) VALUES(0,'anonymous',
  '','','{"human_name":"noname"}');
INSERT INTO users(id,name,password,email,properties) VALUES(2,'system','','x2engine@volga.unaux.com',
  '{"avatar":"/media/stdAvatars/system.gif"}');
INSERT INTO users(id,name,password,email,phone,properties)
  VALUES(21,'ad1','6540a156c57aa691202710d83b2a716f28633939',
  'dolguikh@outlook.com','(+78453)35-15-68','{"human_name":"anatoliDolguikh",
  "avatar":"/media/stdAvatars/neroWolfe.jpg","web_site":"https://bitbucket.org/dolgih/","phone2":"+7-917-0230-917"}'))"
);

  users.addenda(
R"(-- Note: in password saved sha1 of password+name;
-- Note: userNames 'guest' 'Admin', 'anonymous','system' and ids -1,0,1,2 are predefined;
-- Note: last_activity < 2000-01-01 interpreted as empty, last_activity < 1985-01-01 prevents data storage;
-- Note: SEQUENCE users_id_seq_anonymous for anonymouses;
DROP SEQUENCE IF EXISTS users_id_seq_anonymous;
CREATE SEQUENCE users_id_seq_anonymous INCREMENT -1 MINVALUE -2000000000 MAXVALUE -1000 START -1000 CYCLE)"
);
  table(users);

/********
  groups
*********/
  Table groups("groups", "groups of users"); groups
  +Column("id", Column::SERIAL | Column::PRIMARY | Column::NOTNULL)
  +Column("name", Column::VARCHAR | Column::UNIQUE | Column::NOTNULL, 32)
  +Column("modified", Column::TIMESTAMP | Column::NOTNULL);
  groups.data(R"(INSERT INTO groups(id,name) VALUES(-3,'banned');
INSERT INTO groups(id,name) VALUES(-2,'notApproved');
INSERT INTO groups(id,name) VALUES(-1,'notActivated');
INSERT INTO groups(id,name) VALUES(1,'admins');
INSERT INTO groups(id,name) VALUES(0,'anonymous');
INSERT INTO groups(id,name) VALUES(3,'moderators');
INSERT INTO groups(id,name) VALUES(100,'users');
INSERT INTO groups(id,name) VALUES(200,'staff');
INSERT INTO groups(id,name) VALUES(130,'announcement-writers');
INSERT INTO groups(id,name) VALUES(140,'news-writers'))");
  groups.addenda("-- Note: groupNames 'banned', 'notApproved', notActivated', admins', 'anonymous', 'moderators' and ids -3,-2,-1,0,1,3 are predefined");
  groups.serialId = "groups_id_seq"; groups.serial = 141;
  table(groups);

/**************
  users2groups
***************/
  Table users2groups("users2groups", "users in groups"); users2groups
  +Column("user_id", Column::INTEGER | Column::NOTNULL)
  +Column("group_id", Column::INTEGER | Column::NOTNULL)
  +Column("modified", Column::TIMESTAMP | Column::NOTNULL);
  users2groups.data(R"(INSERT INTO users2groups(user_id,group_id) VALUES(1,1);
INSERT INTO users2groups(user_id,group_id) VALUES(21,1))");
   users2groups.constraint(R"(PRIMARY KEY(user_id, group_id),
    FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE,
    FOREIGN KEY(group_id) REFERENCES groups(id))");
  users2groups.addenda("CREATE INDEX users2groups_group_id_user_id ON users2groups (group_id, user_id)");
  table(users2groups);

/************
  restricted
*************/
  Table restricted("restricted", "users with restricted access rights"); restricted
  +Column("user_id", Column::INTEGER | Column::NOTNULL)
  +Column("group_id", Column::INTEGER | Column::NOTNULL)
  +Column("description", Column::VARCHAR, 128)
  +Column("before", Column::TIMESTAMP | Column::NOTNULL)
  +Column("modified", Column::TIMESTAMP | Column::NOTNULL);
  restricted.constraint(R"(PRIMARY KEY(user_id, group_id),
    FOREIGN KEY(user_id, group_id) REFERENCES users2groups(user_id, group_id) ON UPDATE CASCADE)");
  table(restricted);

/*******
  parts
********/
  Table parts("parts", "parts of the site"); parts
  +Column("id", Column::SERIAL | Column::PRIMARY | Column::NOTNULL)
  +Column("name", Column::VARCHAR | Column::UNIQUE | Column::NOTNULL, 96)
  +Column("description", Column::VARCHAR | Column::UNIQUE | Column::NOTNULL, 128)
  +Column("weight", Column::INTEGER | Column::NOTNULL).def("500")
  +Column("topics_number", Column::INTEGER | Column::NOTNULL | Column::DEFAULT0)
  +Column("posts_number", Column::INTEGER | Column::NOTNULL | Column::DEFAULT0)
  +Column("last_message", Column::INTEGER)
  +Column("author", Column::INTEGER | Column::NOTNULL)
  +Column("modified", Column::TIMESTAMP | Column::NOTNULL);
  parts.constraint(R"(CHECK (weight > 0 AND weight < 1000),
    FOREIGN KEY(author) REFERENCES users(id) ON UPDATE CASCADE)");
  parts.data(R"(INSERT INTO parts(id,name,description,weight,author) VALUES(200,'Staff','Special part for the staff, hidden from users.',700,1);
INSERT INTO parts(id,name,description,weight,topics_number,posts_number,last_message,author) VALUES(300,'Announcements','Read this forum first to find out the latest information.',5,1,1,501,1);
INSERT INTO parts(id,name,description,weight,author) VALUES(400,'News','Site News.',20,1);
INSERT INTO parts(id,name,description,weight,topics_number,posts_number,last_message,author) VALUES(500,'Pages','Site Pages.',30,3,3,300,1);
INSERT INTO parts(id,name,description,weight,author) VALUES(600,'Feedback','Complaints, questions, suggestions, appeals to the administration.',900,1);
INSERT INTO parts(id,name,description,weight,author) VALUES(700,'Bin','Deleted, trash and other questionable posts.',980,1);
INSERT INTO parts(id,name,description,topics_number,posts_number,last_message,author) VALUES(1000,'Common Part','Feel free to talk about anything and everything in this board.',1,1,701,1);
INSERT INTO parts(id,name,description,author) VALUES(800,'X2 Development','Talking about X2 Project. Developing, Using, Errors, usw.',1))");
  parts.serialId = "parts_id_seq"; parts.serial = 1101;
  parts.addenda("-- Note: partNames 'Announcements', 'News', 'Pages', 'Feedback', 'Bin' and ids 300,400,500,600,700 are predefined");
  table(parts);

/*************
  access_list
**************/
  Table accessList("access_list", "access list by parts"); accessList
  +Column("part_id", Column::INTEGER | Column::NOTNULL)
  +Column("group_id", Column::INTEGER | Column::NOTNULL)
  +Column("r", Column::CHAR | Column::NOTNULL, 1).def("'-'")
  +Column("w", Column::CHAR | Column::NOTNULL, 1).def("'-'")
  +Column("c", Column::CHAR | Column::NOTNULL, 1).def("'-'")
  +Column("o", Column::CHAR | Column::NOTNULL, 1).def("'-'")
  +Column("m", Column::CHAR | Column::NOTNULL, 1).def("'-'")
  +Column("h", Column::CHAR | Column::NOTNULL, 1).def("'-'")
  +Column("modified", Column::TIMESTAMP | Column::NOTNULL);
  accessList.constraint(R"(CHECK (r in ('r','-') AND w in ('w','-') AND c in ('c','-') AND o in ('o','-') AND m in ('m','-') AND h in ('h','-')),
    PRIMARY KEY(part_id, group_id),
    FOREIGN KEY(part_id) REFERENCES parts(id),
    FOREIGN KEY(group_id) REFERENCES groups(id))")
  .addenda("CREATE INDEX access_list_group_id_part_id ON access_list (group_id, part_id)")
            /* staff */
  .addenda("-- staff")
  .data("INSERT INTO access_list(part_id,group_id,c) VALUES(200,200,'c')")
            /* Announcements */
  .addenda("-- Announcements")
  .data("INSERT INTO access_list(part_id,group_id,c) VALUES(300,130,'c')")
  .data("INSERT INTO access_list(part_id,group_id,r) VALUES(300,0,'r')")
            /* News */
  .addenda("-- News")
  .data("INSERT INTO access_list(part_id,group_id,c) VALUES(400,140,'c')")
  .data("INSERT INTO access_list(part_id,group_id,r) VALUES(400,0,'r')")
            /* Pages */
  .addenda("-- Pages")
  .data("INSERT INTO access_list(part_id,group_id,r) VALUES(500,0,'r')")
            /* Feedback */
  .addenda("-- Feedback")
  .data("INSERT INTO access_list(part_id,group_id,c) VALUES(600,0,'c')")
            /* Bin */
  .addenda("-- Bin")
  .data("INSERT INTO access_list(part_id,group_id,w) VALUES(700,0,'w')")
            /* Common Part */
  .addenda("-- Common Part")
  .data("INSERT INTO access_list(part_id,group_id,o) VALUES(1000,100,'o')")
  .data("INSERT INTO access_list(part_id,group_id,r) VALUES(1000,0,'r')")
            /* X2 Development */
  .addenda("-- X2 Development")
  .data("INSERT INTO access_list(part_id,group_id,o) VALUES(800,100,'o')")
  .data("INSERT INTO access_list(part_id,group_id,r) VALUES(800,0,'r')");
  table(accessList);

/********
  topics
*********/
  Table topics("topics", "topics (threads)"); topics
  +Column("id", Column::SERIAL | Column::PRIMARY | Column::NOTNULL)
  +Column("part_id", Column::INTEGER | Column::NOTNULL)
  +Column("pinned", Column::CHAR, 1)
  +Column("last_post", Column::INTEGER)
  +Column("closed", Column::CHAR | Column::NOTNULL | Column::DEFAULTESTR, 1 )
  +Column("visits_number", Column::INTEGER | Column::NOTNULL | Column::DEFAULT0)
  +Column("posts_number", Column::INTEGER | Column::NOTNULL | Column::DEFAULT0)
  +Column("modified", Column::TIMESTAMP | Column::NOTNULL);
  topics.constraint(R"(CHECK (pinned >= 'A' AND pinned <= 'Z'),
    CHECK (closed = 'C' OR closed = ' '),
    FOREIGN KEY(part_id) REFERENCES parts(id))");
  topics.data(R"(INSERT INTO topics(id,part_id,posts_number) VALUES(1,500,1);
INSERT INTO topics(id,part_id,posts_number) VALUES(200,500,1);
INSERT INTO topics(id,part_id,posts_number) VALUES(300,500,1);
INSERT INTO topics(id,part_id,posts_number) VALUES(400,500,1);
INSERT INTO topics(id,part_id,posts_number) VALUES(501,300,1);
INSERT INTO topics(id,part_id,posts_number) VALUES(701,1000,1))");
  topics.addenda(R"(-- Note: topics 'Main', 'About', 'Contacts' with ids 1,200,300,400 are predefined;

-- Функция предназначена для организации порядка вывода тем в разделе,
-- чтобы сначала выводились бы прикрепленные темы, а потом начиная с темы,
-- в которой наиболее поздний последний пост
CREATE OR REPLACE FUNCTION topics_order
(pinned CHAR(1), last_post topics.last_post%TYPE)
  RETURNS BIGINT AS $$
  BEGIN
    RETURN (ASCII(COALESCE(pinned,'z'))::BIGINT*1E12::BIGINT-COALESCE(last_post,0));
  END; $$ LANGUAGE plpgsql IMMUTABLE;

CREATE INDEX topics_part_id_topics_order ON topics (part_id, topics_order(pinned, last_post));

-- Функция возвращает страницу тем (pg) раздела (part), если задан ненулевой
-- идентификатор темы (topic), то страница и раздел вычисляются для этой темы.
-- Страницы нумеруются с нуля.
CREATE OR REPLACE FUNCTION page_of_topics
(pg_size INT, topic topics.id%TYPE, part parts.id%TYPE, pg INT)
  RETURNS TABLE(id topics.id%TYPE, part_id topics.part_id%TYPE, pinned topics.pinned%TYPE,
    last_post topics.last_post%TYPE,  closed topics.closed%TYPE,
    visits_number topics.visits_number%TYPE, posts_number topics.posts_number%TYPE,
    modified topics.modified%TYPE, page INT) AS $$
  DECLARE
    pnd topics.pinned%TYPE; lst_pst topics.last_post%TYPE;
    cnt INT; tpcs_nmb INT;
  BEGIN
    IF topic <> 0 THEN
      SELECT topics.part_id, topics.pinned, topics.last_post INTO part, pnd,
        lst_pst FROM topics WHERE topics.id=topic;
      IF NOT FOUND THEN
        RAISE WARNING 'topic not found: %', topic;
        RETURN;
      END IF;
      RAISE NOTICE 'found topic is % % %', part, pnd, lst_pst;
      SELECT topics_number INTO tpcs_nmb FROM parts WHERE parts.id=part
        FOR UPDATE OF parts NOWAIT; -- lock the record
      SELECT COUNT(*) INTO cnt FROM topics WHERE topics.part_id=part AND
        topics_order(topics.pinned,topics.last_post)<=topics_order(pnd,lst_pst);
      IF cnt = 0 THEN
        RAISE WARNING 'topic not found: part=% pinned=% last_post=%', part, pnd, lst_pst;
        RETURN;
      END IF;
      pg := (cnt - 1) / pg_size;
      RAISE NOTICE 'calculated page = %', pg;
      IF (tpcs_nmb - 1) / pg_size < pg THEN -- TODO связь с программой для сброса
        -- кэша и др.
        RAISE WARNING 'recalc of bad topics_number in parts: part=% topics_number=% page=%',
          part, tpcs_nmb, pg;
        PERFORM recalc_parts(ARRAY[part]);
      END IF;
    ELSE
      PERFORM topics_number FROM parts WHERE parts.id=part
        FOR UPDATE OF parts NOWAIT; -- lock the record
    END IF;
    RAISE NOTICE 'page_of_topics: part=% page=% pg_size=%', part, pg, pg_size;
    RETURN QUERY SELECT topics.*, pg FROM topics WHERE topics.part_id=part
      ORDER BY topics_order(topics.pinned,topics.last_post) LIMIT pg_size OFFSET pg_size*pg
      FOR UPDATE OF topics NOWAIT;
  END; $$ LANGUAGE plpgsql;

CREATE TABLE topics_log (LIKE topics);
ALTER TABLE topics_log ADD villain INT NOT NULL;
ALTER TABLE topics_log ADD deleted TIMESTAMPTZ NOT NULL DEFAULT NOW())");
  table(topics);

/*******
  posts
********/
  Table posts("posts", "posts (articles)"); posts
  .pres("DROP TYPE IF EXISTS post_format")
  .pres("CREATE TYPE post_format AS ENUM ('text','html','bbcode','markdown','template')")
  +Column("id", Column::INTEGER | Column::PRIMARY | Column::NOTNULL).def("nextval('topics_id_seq')")
  +Column("topic_id", Column::INTEGER | Column::NOTNULL)
  +Column("author", Column::INTEGER | Column::NOTNULL)
  +Column("title", Column::VARCHAR | Column::NOTNULL, 256)
  +Column("abstract", Column::TEXT | Column::NOTNULL)
  +Column("content", Column::TEXT | Column::NOTNULL)
  +Column("format", "post_format").def("'text'")
  +Column("properties", Column::TEXT | Column::NOTNULL | Column::DEFAULTESTR)
  +Column("ip", Column::VARCHAR | Column::NOTNULL | Column::DEFAULTESTR, 50)
  +Column("author2", Column::INTEGER | Column::NOTNULL)
  +Column("created", Column::TIMESTAMP | Column::NOTNULL)
  +Column("modified", Column::TIMESTAMP | Column::NOTNULL);
  posts.constraint(R"(FOREIGN KEY(topic_id) REFERENCES topics(id),
    FOREIGN KEY(author) REFERENCES users(id) ON UPDATE CASCADE,
    FOREIGN KEY(author2) REFERENCES users(id) ON UPDATE CASCADE)");
  posts.data(
R"(INSERT INTO posts(id,topic_id,author,title,abstract,content,author2) VALUES(1,1,1,'Main Page','X2 Main Page.','This is the main page of the X2 Site.',1);
INSERT INTO posts(id,topic_id,author,title,abstract,content,author2) VALUES(200,200,1,'About Page','X2 About Page.','This is the about page of the X2 Site.',1);
INSERT INTO posts(id,topic_id,author,title,abstract,content,author2) VALUES(300,300,1,'Contacts Page','X2 Contacts Page.','This is the contacts page of the X2 Site.',1);
INSERT INTO posts(id,topic_id,author,title,abstract,content,format,author2) VALUES(400,400,21,'Source','X2 Source Page.','Here you can see the doxy)" "\r\n" R"([url]https://dolgih.bitbucket.io/index.html[/url],)" "\r\n\r\n" R"(here the source)" "\r\n" R"([url]https://bitbucket.org/dolgih/x2/src/[/url],)" "\r\n\r\n" R"(and here the database structure description.)" "\r\n" R"([url]/src/db/x2db.html[/url]','bbcode',21);
INSERT INTO posts(id,topic_id,author,title,abstract,content,author2) VALUES(501,501,1,'The Phorum is now installed','The Phorum is now installed.','Congratulations! The X2 Phorum is now installed.',1);
INSERT INTO posts(id,topic_id,author,title,abstract,content,author2) VALUES(701,701,1,'Test Message','This is a test message.','This is a test message. You can delete it after installation using the moderation tools. These tools will be visible in this screen if you log in as the administrator user that you created during install.)" "\r\n\r\nX2 Team',1)"
);
  posts.addenda(
R"(CREATE INDEX posts_topic_id_id ON posts (topic_id, id);
ALTER TABLE topics ADD CONSTRAINT topics_last_post_fkey FOREIGN KEY (last_post) REFERENCES posts(id);
ALTER TABLE parts ADD CONSTRAINT parts_last_message_fkey FOREIGN KEY (last_message) REFERENCES posts(id);
-- Note: posts 'Main', 'About', 'Contacts' with ids 1,200,300 are predefined;

CREATE OR REPLACE FUNCTION recalc_parts ( ids INT[] = ARRAY[]::INT[] )
  RETURNS TABLE ( id INT, name TEXT ) AS $$
  UPDATE parts SET topics_number=0, posts_number=0, last_message=NULL WHERE
  CASE COALESCE(ARRAY_LENGTH($1, 1), 0) WHEN 0 THEN TRUE ELSE id = ANY ( $1 ) END;
  UPDATE parts SET topics_number=t.topics_number, posts_number=t.posts_number,
  last_message=t.last_post FROM
  (SELECT part_id, COUNT(id) AS topics_number, SUM(posts_number) AS posts_number,
  MAX(last_post) AS last_post FROM topics
  GROUP BY part_id) AS t WHERE parts.id=t.part_id AND CASE COALESCE(ARRAY_LENGTH($1, 1), 0)
  WHEN 0 THEN TRUE ELSE parts.id = ANY ( $1 ) END RETURNING parts.id, parts.name;
  $$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION recalc_topics ( ids INT[] = ARRAY[]::INT[] )
  RETURNS TABLE ( id INT ) AS $$
  UPDATE topics SET posts_number=0, last_post=NULL WHERE
  CASE COALESCE(ARRAY_LENGTH($1, 1), 0) WHEN 0 THEN TRUE ELSE id = ANY ( $1 ) END;
  UPDATE topics SET posts_number=p.posts_number, last_post=p.last_post FROM
  (SELECT topic_id, COUNT(id) AS posts_number, MAX(id) AS last_post FROM posts
  GROUP BY topic_id) AS p WHERE topics.id=p.topic_id AND CASE COALESCE(ARRAY_LENGTH($1, 1), 0)
  WHEN 0 THEN TRUE ELSE topics.id = ANY ( $1 ) END RETURNING topics.id;
  $$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION recalc_users ( ids INT[] = ARRAY[]::INT[] )
  RETURNS TABLE ( id INT, name TEXT ) AS $$
  UPDATE users SET posts_number=0 WHERE
  CASE COALESCE(ARRAY_LENGTH($1, 1), 0) WHEN 0 THEN TRUE ELSE id = ANY ( $1 ) END;
  UPDATE users SET posts_number=p.posts_number FROM
  (SELECT author, COUNT(*) AS posts_number FROM posts JOIN topics ON posts.topic_id=topics.id
  WHERE topics.part_id > 0 GROUP BY author) AS p WHERE users.id=p.author
  AND CASE COALESCE(ARRAY_LENGTH($1, 1), 0) WHEN 0 THEN TRUE ELSE users.id = ANY ( $1 ) END;
  SELECT id, name FROM users WHERE id = ANY ( $1 );
  $$ LANGUAGE SQL;

SELECT recalc_topics( ARRAY[]::integer[] ); SELECT recalc_parts( ARRAY[]::integer[] );
CREATE TABLE posts_log (LIKE posts);
ALTER TABLE posts_log ADD villain INT NOT NULL;
ALTER TABLE posts_log ADD deleted TIMESTAMPTZ NOT NULL DEFAULT NOW())"
);
  posts.serialId = "topics_id_seq"; posts.serial = 1001;
  table(posts);

/***************
  notifications
****************/
  Table notifications("notifications", "notifications of users"); notifications
  +Column("user_id", Column::INTEGER | Column::NOTNULL)
  +Column("post_id", Column::INTEGER | Column::NOTNULL)
  +Column("what", Column::CHAR | Column::NOTNULL, 1);
  notifications.constraint(R"(CHECK (what IN ('A','P','Q')),
    PRIMARY KEY(user_id, post_id),
    FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY(post_id) REFERENCES posts(id) ON DELETE CASCADE)")
  .addenda("CREATE INDEX notifications_post_id ON notifications (post_id)")
  .addenda("-- Note: 'what' is: 'A' - answer, 'P' - private msg, 'Q' - quote");
  table(notifications);

/****************
  correspondence
*****************/
  Table correspondence("correspondence", "private correspondence of users"); correspondence
  +Column("to_topic", Column::INTEGER | Column::PRIMARY | Column::NOTNULL)
  +Column("from_topic", Column::INTEGER | Column::UNIQUE | Column::NOTNULL);
  correspondence.constraint("FOREIGN KEY(to_topic) REFERENCES topics(id) ON DELETE CASCADE")
  .constraint("FOREIGN KEY(from_topic) REFERENCES topics(id) ON DELETE CASCADE")
  .addenda(R"(
CREATE OR REPLACE FUNCTION corresponds ( topic_ids INT[] = ARRAY[]::INT[] )
  RETURNS TABLE ( topic_id INT ) AS $$
  WITH t1 AS (SELECT to_topic, from_topic FROM correspondence WHERE
  to_topic=ANY($1) OR from_topic=ANY($1)) SELECT to_topic AS topic
  FROM t1 UNION (SELECT from_topic AS topic FROM t1);
  $$ LANGUAGE SQL)"
);
  table(correspondence);

/*********
  options
**********/
  Table options("options", "aplication options"); options
  +Column("id", Column::VARCHAR | Column::PRIMARY | Column::NOTNULL, 64)
  +Column("value", Column::TEXT | Column::NOTNULL)
  +Column("modified", Column::TIMESTAMP | Column::NOTNULL);
  table(options);
}

// database structs

Str Column::str(DbType dbType) {
    std::ostringstream out;
    out << "    " << name << " ";
    switch(properties & typeMask) {
        case INTEGER: out << "INTEGER"; break;
        case TINYINT: if(dbType == POSTGRES) out << "SMALLINT";
                      else out << "TINYINT";
                      break;
        case SMALLINT: out << "SMALLINT"; break;
        case BIGINT: out << "BIGINT"; break;
        case SERIAL: if(dbType == SQLITE) out << "INTEGER";
                     else if(dbType == POSTGRES) out << "SERIAL";
                     else if(dbType == MYSQL) out << "INTEGER AUTO_INCREMENT";
                     else assert(0);
                     break;
        case BIGSERIAL: if(dbType == SQLITE) out << "BIGINT AUTOINCREMENT";
                     else if(dbType == POSTGRES) out << "BIGSERIAL";
                     else if(dbType == MYSQL) out << "BIGINT AUTO_INCREMENT";
                     else assert(0);
                     break;
        case FLOAT:  if(dbType == MYSQL) out << "FLOAT";
                     else out << "REAL";
                     break;
        case DOUBLE: if(dbType == SQLITE) out << "REAL";
                     else if(dbType == POSTGRES) out << "DOUBLE PRECISION";
                     else if(dbType == MYSQL) out << "DOUBLE";
                     else assert(0);
                     break;
        case DECIMAL:out << "DECIMAL(" << length << "," << decimals << ")"; break;
        case CHAR:   out << "CHAR(" << length << ")"; break;
        case VARCHAR:out << "VARCHAR(" << length << ")"; break;
        case TEXT:   out << "TEXT"; break;
        case DATETIME:if(dbType == POSTGRES) out << "TIMESTAMPTZ";
                     else out << "DATETIME";
                     break;
        case TIMESTAMP:if(dbType == SQLITE) out << "DATETIME DEFAULT (DATETIME())";
                     else if(dbType == POSTGRES) out << "TIMESTAMPTZ DEFAULT NOW()";
                     else if(dbType == MYSQL) out << "TIMESTAMP DEFAULT CURRENT_TIMESTAMP";
                     else assert(0);
                     break;
        case CUSTOM: out << type;
                     break;
        default:     assert(0);
    }
    if(properties & UNSIGNED && dbType == MYSQL) out << " UNSIGNED";
    if(properties & PRIMARY) out << " PRIMARY KEY";
    if((properties & typeMask) == SERIAL && dbType == SQLITE) out << " AUTOINCREMENT";
    if(properties & UNIQUE) out << " UNIQUE";
    if(properties & NOTNULL) out << " NOT NULL"; else out << " NULL";
    if(!defaul.empty())
        if(dbType == SQLITE) out << " DEFAULT(" << defaul << ")";
        else out << " DEFAULT " << defaul;
    if(properties & DEFAULT0)
        if(dbType == SQLITE) out << " DEFAULT(0)";
        else out << " DEFAULT 0";
    if(properties & DEFAULTESTR)
        if(dbType == SQLITE) out << " DEFAULT('')";
        else out << " DEFAULT ''";
    return out.str();
}

Str Table::str(DbType dbType, bool noData) {
    std::ostringstream out;
    for( size_t i = 0; i < thePres.size(); i ++ )
        out << thePres[i] << ";\n";
    out << "CREATE TABLE " <<  name << "( -- " << comment << "\n";
    for( size_t i = 0; i < columns.size(); i ++ ) {
        out << columns[i].str(dbType);
        if(i != columns.size() - 1 || !constraints.empty()) out << ",";
        out << "\n";
    }
    for( size_t i = 0; i < constraints.size(); i ++ ) {
        out << "    " << constraints[i]; if(i != constraints.size() - 1) out << ","; out << "\n";
    }
    out << ")";
    if(dbType == MYSQL) out << " Engine = InnoDB, Character Set utf8";
    out << ";\n\n";
    if( ! noData )
        for( size_t i = 0; i < theData.size(); i ++ )
            out << theData[i] << ";\n";
    for( size_t i = 0; i < theAddenda.size(); i ++ )
        out << theAddenda[i] << ";\n";
    if(dbType == POSTGRES && serial)
        out << "SELECT setval('" << serialId << "'," << serial << ");\n";
    out << "\n";
    return out.str();
}

Str Db::str(bool refer) {
    Str r;
    if(type !=  MYSQL) r += "BEGIN;\n\n";
    else r += "Set Character Set utf8;\n\n";
    for( int i = 0; i < preps.size(); i ++ )
        r += preps[i] + ";\n";
    assert(tables.size());
    for( int i = tables.size() - 1; i >= 0; i -- )
        r += "DROP TABLE IF EXISTS " + tables[i].name + " CASCADE;\n";
    r += "\n";
    for( size_t i = 0; i < tables.size(); i ++ ) {
        if(refer)
            r += "#### -- " + tables[i].comment + " (" + tables[i].name + ")\n<pre><code>\n";
        r += tables[i].str(type, noData);
        if(refer)
            r += "</code></pre>\n";
    }
    if(type !=  MYSQL) r += "COMMIT;\n";
    r += "\n";
    return r;
}


}} // dbDescriptor, x2

