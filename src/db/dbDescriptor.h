/*
    dbDescriptor.h
    database descriptor - class for various manipulations
    with database (creating, documenting, verifying and so on)

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#ifndef DBDESCRIPTOR_H_
#define DBDESCRIPTOR_H_

#include <string>
#include <vector>

typedef std::string Str;

namespace x2 {
namespace dbDescriptor {

enum DbType { SQLITE, MYSQL, POSTGRES };

struct Table;
struct Column {
    enum Type { INTEGER = 1, TINYINT = 2, SMALLINT = 3, BIGINT = 4, SERIAL = 5, BIGSERIAL = 6,
      FLOAT = 8, DOUBLE = 9, DECIMAL = 10,
      CHAR = 11, VARCHAR = 12, TEXT = 13, DATETIME = 15,  TIMESTAMP = 16, CUSTOM = 17 };
    static const int typeMask = 63;

    enum Property { UNSIGNED = 1 << 6, PRIMARY = 1 << 7, UNIQUE = 1 << 8,
      NOTNULL = 1 << 9, DEFAULT0 = 1 << 10, DEFAULTESTR = 1 << 12 };

    Column(Str name,  int props, short length = 0, short decimals = 0) : name(name),
      properties(props), length(length), decimals(decimals) { }
    Column(Str name,  Str type) : name(name), type(type),
      properties(CUSTOM|NOTNULL), length(0), decimals(0) { }
    Column &def( Str value ) { defaul = value; return *this; }
    Str str(DbType dbType);

    Str name, type;
    int properties;
    short length, decimals;
    Str defaul;
};

struct Db;
struct Table {

    Table(Str aName, Str aComment) : name(aName), comment(aComment), serial(0) {}
    Table &pres( Str aPres ) { thePres.push_back( aPres ); return *this; }
    void column(Column &aColumn) { columns.push_back(aColumn); }
    void add(Column aColumn) { column(aColumn); }
    Table &data( Str aData ) { theData.push_back( aData ); return *this; }
    Table &addenda( Str anAddenda ) { theAddenda.push_back( anAddenda ); return *this; }
    Table &constraint( Str aConstraint ) { constraints.push_back( aConstraint ); return *this; }
    Str str(DbType dbType, bool noData);
    Table &operator+(const Column &column) { add(column); return *this; }

    Str name, comment;
    std::vector<Column> columns;
    std::vector<Str> thePres, constraints, theAddenda, theData;
    Str serialId; int serial;
};

struct Db {
    DbType type;
    bool noData;

    Db(DbType type, bool noData) : type(type), noData(noData) { }
    Db &prep( Str prep ) { preps.push_back( prep ); return *this; }
    void table(Table &aTable) { tables.push_back(aTable); }
    void add(Table aTable) { table(aTable); }
    Str str(bool refer = false);

    std::vector<Str> preps;
    std::vector<Table> tables;
};

struct Database : Db {
    Database(DbType type, bool noData);
};

}} // dbDescriptor, x2

#endif // DBDESCRIPTOR_H_
