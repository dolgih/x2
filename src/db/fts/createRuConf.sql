-- Словари вы должны поместить в каталог $SHAREDIR/tsearch_data.
--   У меня в UBUNTU 14.04 расположение $SHAREDIR следующее: \usr\share\postgresql\9.3.
--   Обращаю ваше внимание, файлы словарей должны быть сохранены в кодировке UTF-8.

-- Создаем два словаря для русского и английского языка:
CREATE TEXT SEARCH DICTIONARY ispell_ru (
  template  =   ispell,
  dictfile  =   ru,
  afffile   =   ru,
  stopwords =   russian
);
CREATE TEXT SEARCH DICTIONARY ispell_en (
    template  = ispell,
    dictfile  = en,
    afffile   = en,
    stopwords = english
);
-- Мы создали словарь ispell_ru и словарь ispell_en. В каждом словаре мы указываем
-- следующие параметры:
--   template - используемый шаблон словаря;
--   dictfile - указывает на базовый файл конфигурации (расширение файла - .dict).
--     Полный путь к файлу будет таким $SHAREDIR/tsearch_data/ru.dict;
--   afffile - указывает на файл аффиксов (расширение файла - .affix), которые нужны
--     для присоединению к корню и образованию новых слов;
--   stopwords - указывает на файл стоп-слов (расширение файла - .stop). Содержит
--     слова которые будут исключены из поиска. Обычно в файл помещают часто употребляемые слова, которые есть в каждом из искомых текстов. Этот файл уже есть в каталоге "tsearch_data", вы может расширить его или заменить на свой.

-- После создания словарей, необходимо для них создать конфигурацию, которую мы сможем
--   использовать дальше для полнотекстового поиска:
CREATE TEXT SEARCH CONFIGURATION ru ( COPY = russian );
-- Настраиваем список словарей для конфигурации ru
ALTER TEXT SEARCH CONFIGURATION ru
    ALTER MAPPING
	FOR word, hword, hword_part
	WITH ispell_ru, russian_stem;

ALTER TEXT SEARCH CONFIGURATION ru
    ALTER MAPPING
        FOR asciiword, asciihword, hword_asciipart
        WITH ispell_en, english_stem;


-- Что получается... Мы создали два словаря: русский (ispell_ru) и английский
--   (ispell_en). Создали конфигурацию под именем ru и настроили ее для использования
--    наших словарей.

--ALTER TABLE posts ADD COLUMN fts tsvector;
--UPDATE posts SET fts =
--setweight( coalesce( to_tsvector('ru', title),''),'A') || ' ' ||
--setweight( coalesce( to_tsvector('ru', abstract),''),'B') || ' ' ||
--setweight( coalesce( to_tsvector('ru', content),''),'D');
--create index fts_index on posts using gin (fts);
