/**
    \file cachedSql.h
    cached postgresql database access

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

#define X2TimeT
#include "postgres.h"

namespace x2 {
namespace sql {

template<class T> class Iterator {
  T &result;
  int record;
public:
  Iterator( T &result, int record ) : result( result ), record( record ) { }
  bool operator == ( const Iterator &right ) const { return record == right.record; }
  bool operator != ( const Iterator &right ) { return ! operator == ( right ); }
  Iterator &operator ++ () { record ++; return *this; }
  T &operator*() { result.record( record ); return result; }
};


class PhSession;

class CachedResult: public Result, public Serializable {
public:
  CachedResult( PhSession &session );
  CachedResult( PhSession &session, Str key, Str query );
  CachedResult( PhSession &session, Str key, Str query, const Triggers &triggers );
  void validate() const;
  CachedResult &execQuery();
  CachedResult &exec( Str aCommand ) { Result::exec( query = aCommand ); return *this; }

  typedef sql::Iterator<CachedResult> Iterator;
  Iterator begin() { return Iterator( *this, 0 ); }
  Iterator end() { return Iterator( *this, recordCount()); }

  TimeT answerTime() const { return theAnswerTime; }

protected:
  PhSession &session;
  Str key, query;
  /** Время получения данных из БД, кэшируется. */
  TimeT theAnswerTime;
  Triggers triggers;
  Str resultStructureKey;

  /** \brief В этом классе не используется, будет переоределятся в потомках
    класса ComplexCachedResult.
    Должна вернуть false в случае, если ключ не изменен из-за того
    что нет, записей, в этом случае данные в кэшь записаны не будут. */
  virtual bool updateKey() { return true; }
  void storeInCache( Str key );
  void storeInCache( Str key, const Triggers &triggers );
  void save( Archive &a ) const override;
  bool load( Archive &a ) override;
  void saveOneRecord( Archive &a ) const;
  void loadOneRecord( Archive &a );

private:
  void doQuery();
friend class OneRecordOfCachedResult;
};

/** \brief Реализует сложное кэширование запроса, возвращающего множество записей.
  Практически весь смысл этого класса в том, чтобы кэшировать группу записей (например
  страницу пользователей, как в dta::ComplexUserResult) и одновременно иметь возможность
  получать из кэши одну конкретную запись из этой группы по идентификатору записи, а не
  по идентификатору группы (в нашем примере по идентификатору пользователя, а не по
  номеру страницы).
  \n\n кэширование разбивается на два типа объектов кэши: 1) в кэшь записывается map
  ComplexCachedResult::ids, содержащий номера записей по их идентификаторам, ключ для
  кэши - ComplexCachedResult::key 2) и в кэшь записываются все записи по одной, а ключи
  для них формируются из формата ComplexCachedResult::format и идентификатора записи,
  который берется из первого поля через Result::field(0) результата запроса базового
  класса Result. Эти вторые объекты в кэши не совместимы с объектами, которые кэширует
  CachedResult, поэтому CachedResult::resultStructureKey и CachedResult::key для одной
  и той же таблицы БД, но кэширумых разными классами должны быть разными.
  \n\n Особое значение имеет переменная ComplexCachedResult::soughtId: если она не
  нулевая, то считаем что ищем только одну запись с этим идентификатором, при получении
  записи из кэши считаем, что ключ ComplexCachedResult::key это ключ одной записи (а не
  ключ всей группы), и тип объета в кэши соответственно будет объектом одной записи. При
  этом если такой объект в кэши найден, то execQuery() вернет только одну эту запись, а
  если придется выполнять запрос к БД, то будут возвращены все полученные записи (в этом
  случае удобно использовать переопределенную функцию row(), или функцию find(), которые
  выставляют текущую запись на запись с идентификатором ComplexCachedResult::soughtId,
  если ее находят). А вот если запрос выполняется через execQueryAll(), то в любом
  случае будут искаться и возвращаться все записи множества.
  \n\n Есть требование к запросу ComplexCachedResult::query - первое поле должно быть
  полем идентификатора записи.
  \n\n Не забыть, что если ComplexCachedResult::soughtId не нулевая, то надо обязательно
  переопределять функцию updateKey(), которая создаст ключ для всей группы.
  \n\n И еще если ComplexCachedResult::soughtId не нулевая, а запрос идет через execQuery()
  (а не через execQueryAll()) и найдена только одна запись из кэша, то theAnswerTime,
  получаемый через answerTime(), заполняется текущим временем, а не временем получения
  запроса из бд.
  \n\n Ну и запрос ComplexCachedResult::query, он должен быть таким, чтобы БД в любом
  случае возвращала все множество, а не одну запись группы.
*/
class ComplexCachedResult : public CachedResult {
public:
  using CachedResult::CachedResult;
  ComplexCachedResult( PhSession &session, Str aResultStructureKey, Str key,
    Str format, Str query, const Triggers &triggers = Triggers());
  ComplexCachedResult( PhSession &session, Str aResultStructureKey, Id soughtId,
    Str format, Str query, const Triggers &triggers = Triggers());
  void validate() const;
  /** \brief Реализует сложный алгоритм получения множества записей.
    Например (как в dta::ComplexUserResult) надо поднять из кэша (или из БД - а мы не
    знаем) страницу записей, но номера страницы мы не знаем, а знам только идентификатор
    одной из записей страницы, а номер страницы может быть вычислен только в ходе
    выполнения запроса.
    \n\n В этом случае ключ CachedResult::key (в нем должен быть номер страницы) мы тоже
    не знаем, поэтому первоначальный ключ должен быть ключем одной записи с ее
    идентификтором (такой ключ создается из формата ComplexCachedResult::format, а
    индикатором такого вызова является ненулевой ComplexCachedResult::soughtId, в который
    надо заранее положить идентификатор этой одной записи), далее сразу после выполнения
    запроса к БД ключ должен быть изменен (в переопределенной виртуальной функции
    updateKey()) на ключ всего множества, эта функция будет вызвана сразу после запроса
    к БД, или если запроса к БД не было, то после того как эта одна запись была найдена в
    кэши.
    \n\n Желательно изменить и запрос ComplexCachedResult::query переопределением функции
    updateQuery() ради эффективности, потому что поиск страницы в БД по идентификатору
    принадлежащей странице одной записи, явно трудоемче запроса конкретной страницы
    (этот запрос будет выполнен, если в кэши найдена запись по идентификатору, а вся
    страница в кэши не найдена).
    \n\n Если по идентификатору найдена хоть одна запись (в кэши может быть найдена только
    одна запись), то ComplexCachedResult::soughtId обнуляется, иначе soughtId не трогается.
  */
  ComplexCachedResult &execQueryAll();

  bool find( Id id ) const;
  /** ищет запись и по soughtId, если тот не нулевой, иначе вызывает предков,
      генерирует исключение если не находит */
  const ComplexCachedResult &row() const;

  typedef sql::Iterator<ComplexCachedResult> Iterator;
  Iterator begin() { return Iterator( *this, 0 ); }
  Iterator end() { return Iterator( *this, recordCount()); }

protected:
  virtual void updateQuery() { }
  /** \brief Выполняется для каждой записи множества при записи в кэшь.
    Может быть переопределена для добавления дополнительных триггеров на уровне
    одной записи. По умолчанию добавляет ключ множества ComplexCachedResult::key. */
  virtual void addTriggersForOneRecord( Triggers &triggers ) const;

  void save( Archive &a ) const override;
  bool load( Archive &a ) override;

  Id soughtId = 0;
  Str format;
  mutable std::map<Id,int/*record number in Result*/> ids;
};

class PhSession : public Session {
public:
  using Session::exec;
  using Session::operator();
  PhSession &openIfNotOpened();
  PhSession &operator()() { return openIfNotOpened(); }
};

}} // sql, x2

