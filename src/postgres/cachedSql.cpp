/**
    \file cachedSql.cpp
    cached postgresql database access

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {
namespace sql {

// CachedResult

CachedResult::CachedResult( PhSession &session )
  : Result( session ), session( session ), theAnswerTime( 0 ) { }

CachedResult::CachedResult( PhSession &session, Str key, Str query )
: Result( session), session( session ), key( key ), query( query ), theAnswerTime( 0 )
  { }

CachedResult::CachedResult( PhSession &session, Str key, Str query,
    const Triggers &triggers )
: Result( session ), session( session ), key( key ), query( query ), theAnswerTime( 0 ),
  triggers( triggers ) { }

void CachedResult::validate() const {
  if( key.empty())
    throw std::runtime_error( "Empty key, invalid object of class " + utl::getClassName( *this ));
}

CachedResult &CachedResult::execQuery() {
  for( unsigned i = 0; i < cfg.cache.fill.tr.count; i ++ ) try {
    doQuery();
    return *this;
  } catch( sql::ErrorLockNotAvailable ) {
    usleep( cfg.cache.fill.tr.sleep );
    continue;
  }
  throw Error( "cfg.cache.fill.try.count exhausted, in execQuery; key='" + key + "'" );
}

void CachedResult::storeInCache( Str aKey ) {
  key = aKey;
  // store structure
  if( ! resultStructureKey.empty()) {
    Str structure; getAttributes( structure );
    cache.store( resultStructureKey, structure );
  }
  // store data
  Current saved = current; current = Current();
  cache.store( key, *this, triggers );
  current = saved;
}

void CachedResult::storeInCache( Str aKey, const Triggers &aTriggers ) {
  triggers = aTriggers;
  storeInCache( aKey, aTriggers );
}

void CachedResult::save( Archive &ar ) const {
  if( resultStructureKey.empty()) {
    Str attrs; getAttributes( attrs ); ar << attrs;
  }
  ar << theAnswerTime.t;
  ar << ( int ) recordCount();
  current = Current();
  while( next())
    saveOneRecord( ar );
}

bool CachedResult::load( Archive &ar ) {
  if( resultStructureKey.empty()) {
    Str attrs; ar >> attrs; makeEmpty(); setAttributes( attrs );
  }
  ar >> theAnswerTime.t;
  int recordCount; ar >> recordCount;
  for( current.record = 0; current.record < recordCount; current.record ++ )
    loadOneRecord( ar );
  return true;
}

void CachedResult::doQuery() {
  if( ! resultStructureKey.empty()) {
    Str structure;
    if( ! cache.fetch( resultStructureKey, structure )) goto notInCache;
    makeEmpty(); setAttributes( structure );
  }
  validate();
  if( ! cache.fetchWithVerifying( key, *this )) {
notInCache:
    session.openIfNotOpened();
    Transaction guard( session ); {
      Result::exec( query );
      std::time( &theAnswerTime.t );
      if( updateKey()) {
        // store structure
        if( ! resultStructureKey.empty()) {
          Str structure; getAttributes( structure );
          cache.store( resultStructureKey, structure );
        }
        // store data
        validate();
        cache.store( key, *this, triggers );
      }
    }; guard.commit();
  }
  current = Current();
}

void CachedResult::saveOneRecord( Archive &ar ) const {
  for( int j = 0, count = fieldCount(); j < count; j ++ ) {
    Str val; bool isNull = ! fetch( j, val ); ar << isNull;
    if( ! isNull ) ar << val;
  }
}

void CachedResult::loadOneRecord( Archive &ar ) {
  for( int j = 0, count = fieldCount(); j < count; j ++ ) {
    field( j );
    bool isNull; ar >> isNull;
    if( isNull ) setNull();
    else { Str val; ar >> val; setValue( val ); }
  }
}

// ComplexCachedResult

ComplexCachedResult::ComplexCachedResult( PhSession &session, Str aResultStructureKey,
  Str key, Str format, Str query, const Triggers &triggers )
: CachedResult( session, key, query, triggers ), format( format )
{ resultStructureKey = aResultStructureKey; }

ComplexCachedResult::ComplexCachedResult( PhSession &session, Str aResultStructureKey,
  Id soughtId, Str format, Str query, const Triggers &triggers )
: CachedResult( session, Str(), query, triggers ),
  soughtId( soughtId ), format( format )
{ resultStructureKey = aResultStructureKey; }

void ComplexCachedResult::validate() const {
  if( resultStructureKey.empty() || format.empty())
    throw std::runtime_error( "invalid object of class " + utl::getClassName( *this ));
}

ComplexCachedResult &ComplexCachedResult::execQueryAll() {
  if( soughtId ) {
    Str initialKey = key = utl::f( format, soughtId );
    execQuery();
    if( empty()) return *this;
    soughtId = 0;
    updateQuery();
    rewind();
    if( initialKey != key ) return *this /* Все записи найдены в БД */;
    // продолжение означает, что запись найдена в кэши, а это не все что нам надо,
    // это только одна запись, а нужны все
    updateKey();
  }
  execQuery();
  return *this;
}

bool ComplexCachedResult::find( Id id ) const {
  auto const found = ids.find( id );
  if( found == ids.end()) return false;
  record( found->second ); return true;
}

const ComplexCachedResult &ComplexCachedResult::row() const {
  if( ! soughtId ) CachedResult::row();
  else if( ! find( soughtId ))
    throw sql::Error(
      utl::f( "ComplexCachedResult::row() failed, soughtId={1}, key={2}, query={3}",
              soughtId, key, query ));
  return *this;
}

struct OneRecordOfCachedResult : Serializable {
  OneRecordOfCachedResult( CachedResult &cachedResult ) : cachedResult( cachedResult ) { }
  void save( Archive &ar ) const override { cachedResult.saveOneRecord( ar ); }
  bool load( Archive &ar ) override { cachedResult.loadOneRecord( ar ); return true; }
private:
  CachedResult &cachedResult;
};

void ComplexCachedResult::addTriggersForOneRecord( Triggers &triggers ) const {
  triggers.emplace_back( key );
}

void ComplexCachedResult::save( Archive &ar ) const {
  validate();
  ids.clear();
  current = Current();
  while( next()) {
    Id id; field( 0 ).fetch( id ); ids[ id ] = current.record;
    Triggers trs = triggers; addTriggersForOneRecord( trs );
    Str key = utl::f( format, id );
    OneRecordOfCachedResult one( const_cast<ComplexCachedResult&>( *this ));
    cache.store( key, one, trs );
  }
  ar << theAnswerTime.t;
  ar << ids;
}

bool ComplexCachedResult::load( Archive &ar ) {
  validate();
  ids.clear();
  current = Current();
  // our key is a key of one record
  if( soughtId ) {
    std::time( &theAnswerTime.t );
    ids[ soughtId ] = 0;
  } else { // many records
    ar >> theAnswerTime.t;
    ar >> ids;
  }
  std::map<int,Id> idsByRecord;
  for( const auto &id: ids ) idsByRecord[ id.second ] = id.first;
  for( const auto &id: idsByRecord ) {
    current.record ++;
    Str key = utl::f( format, id.second );
    OneRecordOfCachedResult one( *this );
    if( ! cache.fetch( key, one )) return false;
  }
  return true;
}

// PhSession

PhSession &PhSession::openIfNotOpened() {
  if( ! theConnection ) {
    open( cfg.postgres.database );
    Session::exec( "SET client_min_messages=WARNING;" );
    Session::exec( "SET SESSION TIME ZONE '"
      + utl::n2s( int( - timezone / 3600 )) + "';" );
    Session::exec( "SET default_text_search_config='"
      + cfg.postgres.defTSConfig + "';" );
  }
  return *this;
}

}} // sql, x2

