/**
    \file postgres.h
    c++ wrapper for the libpq. See also postgres.cpp

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#ifndef X2_POSTGRES_H_
#define X2_POSTGRES_H_

#ifndef PGconn
typedef struct pg_conn PGconn;
#endif
#ifndef PGresult
typedef struct pg_result PGresult;
#endif
#ifndef PQnoticeReceiver
typedef void ( *PQnoticeReceiver )( void *arg, const PGresult *res );
#endif

namespace x2 {
namespace sql {

// exceptions
struct Error : std::runtime_error {
  Error( const std::string &msg ) : std::runtime_error( msg ) { }
};

struct BadValueCastError : Error {
  BadValueCastError( const std::string msg ) : Error( msg + " bad_value_cast" ) { }
};

struct ConnectionError : Error {
  ConnectionError( const std::string msg ) : Error( msg ) { }
};

struct ErrorLockNotAvailable : Error {
  ErrorLockNotAvailable( const std::string msg ) : Error( msg ) { }
};

struct ErrorIntegrityConstraintViolation : Error {
  ErrorIntegrityConstraintViolation( const std::string msg ) : Error( msg ) { }
};

// end of exceptions

// utils
std::tm parseTime( char const *v );
std::string formatTime( std::tm const &v );
// end of utils

class noncopyable {
protected:
  noncopyable() { }
  ~noncopyable() { }
private:  // emphasize the following members are private
  noncopyable( const noncopyable& );
  const noncopyable& operator=( const noncopyable& );
};

class Result;
class Statement;
class Session : public noncopyable { public:
  Session() : theConnection( 0 ), pQnoticeReceiver( 0 ) { }
  Session( Session &&from );
  bool isOpened() const { return theConnection != 0; }
  void open( std::string aConnInfo );

  Result exec( std::string command );
  Result operator()( std::string command );
  /// выполнение sql-запроса, не возвращающего данные
  void command( std::string command );

  /// возвращает currval для поля типа serial
  long sequenceLast( std::string name );

  Statement prepare( std::string command );

  void close();
  ~Session() { close(); }

protected:

  static void noticeReceiver( void *arg, const PGresult *res );

friend class Result;
friend class Statement;
friend class Transaction;
  std::string connInfo;
  PGconn * theConnection;
  PQnoticeReceiver pQnoticeReceiver;
  bool activeSqlTransaction;
};


class Result { public:
  Result( const Result &from );
  Result( Result &&from );
  Result( Session &aSession ) : session( aSession ), theResult( 0 ) { }
  Result( Statement &aStatement );

  Result &operator=( const Result &from );
  Result &operator=( Statement &from );

  /// выполнение sql-запроса
  /** для случая isCommand запрос не должен возвращать данные */
  Result &exec( std::string command, bool isCommand = false );
  /// выполнение sql-запроса, не возвращающего данные
  Result &command( std::string command );
  /// выполнение sql-запроса, возвращающего данные
  Result &exec( Statement &stmt );

  /// возвращает количество столбцов в результате
  int fieldCount() const;

  /// возвращает название столбца результата с номером fieldNum
  std::string fieldName( int fieldNum ) const;

  /// возвращает номер столбца результата с названием fieldName
  int fieldNumber( std::string fieldName ) const;

  /// возвращает количество записей в результате
  int recordCount() const;

  /// возвращает true если записей нет или текущая запись дальше последней
  bool empty() const { int rc = recordCount(); return ! rc || current.record >= rc; }

  /// перемещается на следующую запись, возвращает false если конец
  bool next() const;

  /// перемещается на первую запись, генерирует исключение если записей не одна
  const Result &row() const;

  /// устанавливает начальные условия записи и столбца (на -1)
  const Result &rewind() const { current = Current(); return *this; }

  /// устанавливает текущее поле
  const Result &field( int fieldNumber ) const;
  const Result &operator[]( int fieldNumber ) const
  { return field( fieldNumber ); }
  const Result &field( std::string fieldName ) const
  { return field( fieldNumber( fieldName )); }
  const Result &operator[]( std::string fieldName ) const
  { return field( fieldName ); }

  /// устанавливает текущую запись
  const Result &record( int recordNumber ) const;

  /// возвращают значение поля
  bool fetch( std::string &val ) const;
  bool fetch( double &val ) const;
  bool fetch( int &val ) const;
  bool fetch( unsigned &val ) const;
  bool fetch( long &val ) const;
  bool fetch( unsigned long &val ) const;
  bool fetch( long long &val ) const;
  bool fetch( std::tm &val ) const;
  bool fetchTime( std::time_t &val ) const;
  bool fetchTime( int fieldNumber, std::time_t &val ) const {
    field( fieldNumber ); return fetchTime( val );
  }
  bool fetchTime( std::string fieldName, std::time_t &val ) const {
    field( fieldNumber( fieldName )); return fetchTime( val );
  }
#ifdef X2TimeT
  bool fetch( x2::utl::TimeT &val ) const { return fetchTime( val.t ); }
#endif
#ifdef WtDateTime
  bool fetch( Wt::WDateTime &val ) const {
    std::time_t t;
    if( ! fetchTime( t )) return false;
    val = Wt::WDateTime::fromTime_t( t );
    return true;
  }
#endif

  bool isNull() const;

  template <class T> bool fetch( int fieldNumber, T &val ) const {
    field( fieldNumber ); return fetch( val );
  }

  template <class T> bool fetch( std::string fieldName, T &val ) const {
    field( fieldNumber( fieldName )); return fetch( val );
  }

  template<class T = std::string> T get() const {
    T result;
    if( ! fetch( result ))
      throw Error( "sql get(" + fieldName( current.field )
      + ") failed, value is NULL." );
    return result;
  }

  template<class T = std::string> T get( T def ) const {
    T result; if( ! fetch( result )) return def;
    return result;
  }

  std::time_t getTime() const {
    std::time_t result;
    if( ! fetchTime( result ))
      throw Error( "sql get( std::time_t " + fieldName( current.field )
      + ") failed, value is NULL." );
    return result;
  }

  std::time_t getTime( std::time_t def ) const {
    std::time_t result;
    if( ! fetchTime( result )) return def;
    return result;
  }

  void print( int fd = 1, bool header = true, bool align = false, bool standard = false,
    bool html3 = false, bool expanded = false, bool pager = false,
    const char *fieldSep = " | ", const char *tableOpt = "", const char *caption = "",
    const char **fieldName =  { 0 } );

  void Throw( std::string cmd ) const;

  ~Result() { clear(); }
  void clear();

protected:
  Session &session;
  mutable PGresult * theResult;
  mutable struct Current {
    int record, field; Current() : record( -1 ), field( -1 ) { }
  } current;

  Result &makeEmpty();
  void setNull();
  void setValue( const std::string &value );
  void getAttributes( std::string &data ) const;
  void setAttributes( const std::string &data );
};


class Statement : public Result { public:
  Statement( Statement &&from ) : Result( from ) { params.swap( from.params ); }
  Statement(Session &aConnection) : Result(aConnection) {}

  /// подготовка запроса
  void prepare( std::string query );

  /// выполнение подготовленного запроса
  Result &exec() { return Result::exec( *this ); }

  /// выполняет запрос, перемещается на первую запись, генерирует исключение
  /// если записей не одна
  const Result &row() { return exec().row(); }

  /** параметры нумеруются с 1 */
  Statement &bind( std::string param, int number = -1 );
  Statement &bind( const char *param, int number = -1 );
  Statement &bindNull( int number = -1 );
  Statement &bind( std::tm param, int number = -1 );
  Statement &bind( std::time_t param, int number = -1 );

  template<class T> Statement &bind( T param, int number = -1 ) {
    std::ostringstream out; out << param;
    return bind( out.str().data(), number );
  }

private:
friend class Result;
  class Parameter { public:
    std::string value;
    bool isNull;
    Parameter() : isNull(true) { }
  };

  std::vector<Parameter> params;
};


class Transaction : public noncopyable { public:
  Transaction( Session &aSession );
  void commit();
  void rollback();
  ~Transaction();
protected:
  Session &session;
  bool done, disabled;
};


}} // sql, x2

#endif // X2_POSTGRES_H_
