/**
    \file postgres.cpp
    c++ wrapper for the libpq. See also postgres.h

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include <stdexcept>
#include <string>
#include <string.h>
#include <vector>
#include <sstream>
#include <ctime>
#include <libpq-fe.h>

#include "postgres.h"

#include <unistd.h>

namespace x2 {
namespace sql {

/* utils */

std::tm parseTime( char const *v ) {
  static const char errorMsg[] = "sql parse_time:";
  std::tm t = std::tm(); double sec = 0;
  int n = sscanf( v, "%d-%d-%d %d:%d:%lf", &t.tm_year, &t.tm_mon, &t.tm_mday,
    &t.tm_hour, &t.tm_min, &sec );
  if( n != 3 && n != 6)
    throw BadValueCastError( errorMsg );
  t.tm_year -= 1900;
  t.tm_mon -= 1;
  t.tm_isdst = -1;
  t.tm_sec = static_cast<int>( sec );
  if( mktime( &t ) == -1 )
    throw BadValueCastError( errorMsg );
  return t;
}

std::string formatTime( std::tm const &v ) {
  char buf[ 64 ] = {0}; strftime( buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", &v );
  return buf;
}

static std::string n2s( unsigned n ) {
  char str[32]; snprintf( str, sizeof str, "%u", n ); return str;
}

/* end of utils */

/* Session */

Session::Session( Session &&from ) {
  theConnection = from.theConnection; from.theConnection = 0;
  pQnoticeReceiver = from.pQnoticeReceiver;
}

void Session::open( std::string aConnInfo ) {
  if( theConnection )
    close();
  connInfo = aConnInfo;
  theConnection = PQconnectdb( connInfo.data());
  if( PQstatus( theConnection ) != CONNECTION_OK ) {
    std::string msg = std::string("Session to database failed: ")
      + PQerrorMessage( theConnection );
    close();
    throw Error( msg );
  }
  pQnoticeReceiver = PQsetNoticeReceiver( theConnection, noticeReceiver, this );
}

Result Session::exec( std::string command ) {
  Result result(*this); result.exec( command ); return result;
}

Result Session::operator()( std::string command ) {
  return exec( command );
}

void Session::command( std::string command ) {
  Result result(*this); result.command( command );
}

long Session::sequenceLast( std::string name ) {
  return exec("SELECT currval('" + name + "')").row()[ 0 ].get<long>();
}

Statement Session::prepare( std::string command ) {
  Statement statement( *this ); statement.prepare( command ); return statement;
}

void Session::close() {
  if( theConnection )
    PQfinish( theConnection );
  theConnection = 0;
}

void Session::noticeReceiver( void *arg, const PGresult *res ) {
  const char *code;
  if( PQresultStatus( res ) == PGRES_NONFATAL_ERROR &&
      ( code = PQresultErrorField( res, PG_DIAG_SQLSTATE )) &&
      ( ! strcmp( code, "25001" ))
  )
    ( ( Session * )arg )->activeSqlTransaction = true;
  else
    ( ( Session * )arg )->pQnoticeReceiver( 0, res );
}

/* Result */

Result::Result( const Result &from ) : session( from.session ),
  theResult( from.theResult ), current( from.current ) { from.theResult = 0; }

Result::Result( Result &&from ) : session( from.session ),
  theResult( from.theResult ), current( from.current ) { from.theResult = 0; }

Result::Result( Statement &aStatement ) : session( aStatement.session ),
  theResult( 0 )
{
  exec( aStatement );
}

Result &Result::operator=( const Result &from ) {
  clear();
  theResult = from.theResult; from.theResult = 0;
  current = from.current;
  return *this;
}

Result &Result::operator=( Statement &from ) {
  clear();
  exec( from );
  return *this;
}

Result &Result::exec( std::string command, bool isCommand ) {
  clear();
  theResult = PQexec( session.theConnection, command.data());
  int r = PQresultStatus( theResult );
  if( ( r == PGRES_TUPLES_OK && ! isCommand ) || r == PGRES_COMMAND_OK )
    return *this;
  Throw( command );
}

Result &Result::command( std::string theCommand ) {
  return exec( theCommand, true );
}

Result &Result::exec( Statement &stmt ) {
  clear();
  int size = stmt.params.size();
  const char **paramValues = new const char *[size];
  for(int i = 0; i < size; i ++)
    if(stmt.params[i].isNull)
      paramValues[i] = 0;
    else
      paramValues[i] = stmt.params[i].value.data();
  theResult = PQexecPrepared( session.theConnection,
      "",
      size,
      paramValues,
      0 /*  don't need param lengths since text */,
      0 /* default to all text params */,
      0 /* resultFormat */ );
  delete[] paramValues;
  ExecStatusType status = PQresultStatus( theResult );
  if( status != PGRES_COMMAND_OK && status != PGRES_TUPLES_OK )
    throw Error( std::string( "sql prepared exec failed: ")
    + PQerrorMessage( session.theConnection ));
  return *this;
}

int Result::fieldCount() const { return PQnfields( theResult ); }

int Result::recordCount() const { return PQntuples( theResult ); }

bool Result::next() const {
  return ( ++ current.record ) < recordCount();
}

const Result &Result::row() const {
  if( recordCount() > 1 )
    throw Error( "sql row, the result contains more then one records" );
  current.record = 0;
  return *this;
}

std::string Result::fieldName( int fieldNum ) const {
  if( char * result = PQfname( theResult, fieldNum ))
    return result;
  else {
    std::ostringstream out;
    out << "sql fieldName(" << fieldNum << ") failed.";
    throw Error(out.str());
  }
}

int Result::fieldNumber( std::string fieldName ) const {
  int result;
  if(-1 == ( result = PQfnumber( theResult, fieldName.data())))
    throw Error( "sql fieldNumber('" + fieldName + "') failed." );
  return result;
}

const Result &Result::field( int fieldNumber ) const {
  if( fieldNumber >= 0 && fieldNumber < fieldCount())
    current.field = fieldNumber;
  else
    throw Error( "sql field: invalid field number:" + n2s( fieldNumber ));
  return *this;
}

const Result &Result::record( int recordNumber ) const {
  if( recordNumber >= 0 && recordNumber < recordCount())
    current.record = recordNumber;
  else
    throw Error( "sql record: invalid record number:" + n2s( recordNumber ));
  return *this;
}

bool Result::fetch( std::string &val ) const {
  if( isNull()) return false;
  val = PQgetvalue( theResult, current.record, current.field );
  return true;
}

bool Result::fetch( double &val ) const {
  if( isNull()) return false;
  errno = 0;
  val = strtod( PQgetvalue( theResult, current.record, current.field ), 0 );
  if(errno == ERANGE)
    throw Error( "sql fetch( double " + fieldName( current.field )
    + " ) failed, Overflow or underflow occurred." );
  return true;
}

bool Result::fetch( int &val ) const {
  long l; if( ! fetch( l )) return false;
  val = l;
  if( val != l )
    throw Error( "sql fetch( int " + fieldName( current.field )
    + " ) failed, The resulting value was out of range." );
  return true;
}

bool Result::fetch( unsigned &val ) const {
  unsigned long l; if( ! fetch( l )) return false;
  val = l;
  if( val != l )
    throw Error( "sql fetch( unsigned " + fieldName( current.field )
    + " ) failed, The resulting value was out of range." );
  return true;
}

bool Result::fetch( long &val ) const {
  if( isNull()) return false;
  errno = 0;
  val = strtol( PQgetvalue( theResult, current.record, current.field ), 0, 0 );
  if(errno == ERANGE)
    throw Error( "sql fetch( long " + fieldName( current.field )
    + " ) failed, The resulting value was out of range." );
  return true;
}

bool Result::fetch( unsigned long &val ) const {
  if( isNull()) return false;
  errno = 0;
  val = strtoul( PQgetvalue( theResult, current.record, current.field ), 0, 0 );
  if(errno == ERANGE)
    throw Error( "sql fetch( unsigned long " + fieldName( current.field )
    + " ) failed, The resulting value was out of range." );
  return true;
}

bool Result::fetch( long long &val ) const {
  if( isNull()) return false;
  errno = 0;
  val = strtoll( PQgetvalue( theResult, current.record, current.field ), 0, 0 );
  if(errno == ERANGE)
    throw Error( "sql fetch( long long " + fieldName( current.field )
    + " ) failed, The resulting value was out of range." );
  return true;
}

bool Result::fetch( std::tm &val ) const {
  if( isNull()) return false;
  try{
    val = parseTime( PQgetvalue( theResult, current.record, current.field ));
  } catch( const Error &e ) {
    throw Error( "sql fetch( std::tm " + fieldName( current.field )
    + " ) failed, " + e.what());
  }
  return true;
}

bool Result::fetchTime( std::time_t &val ) const {
  std::tm t; if( ! fetch( t )) return false;
  val = std::mktime( &t );
  return true;
}

bool Result::isNull() const {
  return PQgetisnull( theResult, current.record, current.field ) != 0;
}

void Result::print( int fd, bool header, bool align, bool standard, bool html3,
  bool expanded, bool pager, const char *fieldSep, const char *tableOpt,
  const char *caption, const char **fieldName )
{
  PQprintOpt options = { header, align, standard, html3, expanded, pager,
    ( char * ) fieldSep, ( char * ) tableOpt, ( char * ) caption,
    ( char ** ) fieldName };
  if( FILE * out = fdopen( dup( fd ), "a" )) {
    PQprint( out, theResult, &options );
    fclose( out );
  } else {
      throw Error( "sql print: cannot open file descriptor:" + n2s( fd ));
  }
}

[[ noreturn ]] void Result::Throw( std::string cmd ) const {
  const char *code = PQresultErrorField( theResult, PG_DIAG_SQLSTATE ),
             *msg  = PQerrorMessage( session.theConnection );
  if( msg ) {
    if( code ) {
      if( ! strncmp( code, "08", 2 ))
        throw ConnectionError( msg );
      if( ! strncmp( code, "23", 2 ))
        throw ErrorIntegrityConstraintViolation( msg );
      if( ! strcmp( code, "55P03" ))
        throw ErrorLockNotAvailable( msg + std::string( "; sql=" ) + cmd );
    }
    throw Error( msg + std::string( "; sql=" ) + cmd );
  }
  throw Error( "sql: unknown code, no error message" );
}

void Result::clear() {
  if( theResult ) { PQclear( theResult ); theResult = 0; }
  current = Current();
}

Result &Result::makeEmpty() {
  clear();
  theResult = PQmakeEmptyPGresult( session.theConnection, PGRES_TUPLES_OK );
  if( ! theResult )
    throw Error( "sql memory alloc failed" );
  return *this;
}

void Result::setNull() {
  if( ! PQsetvalue( theResult, current.record, current.field, 0, -1 ))
    throw Error( std::string( "sql setNull failed: ")
    + PQerrorMessage( session.theConnection ));
}

void Result::setValue( const std::string &value ) {
  if( ! PQsetvalue( theResult, current.record, current.field,
    ( char * )value.data(), value.size()))
      throw Error( std::string( "sql setValue failed: ")
      + PQerrorMessage( session.theConnection ) + "; value=" + value );
}

void Result::getAttributes( std::string &data ) const {
  size_t length = sizeof( int ); int fldCnt = fieldCount();
  for( int i = 0; i < fldCnt; i ++ )
    length += sizeof( PGresAttDesc ) + strlen( PQfname( theResult, i )) + 1;
  data.resize( length );
  char * d = (char*)data.data();
  *(int*)d = fldCnt; d += sizeof( int );
  for( int i = 0; i < fldCnt; i ++ ) {
    PGresAttDesc *oneAttr = (PGresAttDesc *)d;
    oneAttr->name       = 0;
    oneAttr->tableid    = PQftable   ( theResult, i );
    oneAttr->columnid   = PQftablecol( theResult, i );
    oneAttr->format     = PQfformat  ( theResult, i );
    oneAttr->typid      = PQftype    ( theResult, i );
    oneAttr->typlen     = PQfsize    ( theResult, i );
    oneAttr->atttypmod  = PQfmod     ( theResult, i );
    d += sizeof( PGresAttDesc );
  }
  for( int i = 0; i < fldCnt; i ++ ) {
    strcpy( d, PQfname( theResult, i ));
    d += strlen( d ) + 1;
  }
}

void Result::setAttributes( const std::string &data ) {
  const char * d = data.data();
  int fieldCount =  *(int*)d; d += sizeof( int );
  PGresAttDesc * attrs = (PGresAttDesc *)d; d += sizeof( PGresAttDesc ) * fieldCount;
  for( int i = 0; i < fieldCount; i ++ ) {
    attrs[ i ].name = (char*)d;
    d += strlen( d ) + 1;
  }
  if( ! PQsetResultAttrs( theResult, fieldCount, attrs ))
    throw Error( std::string( "sql setAttributes failed: ")
    + PQerrorMessage( session.theConnection ));
}

/* Statement */

void Statement::prepare( std::string query ) {
  clear();
  params.clear();
  theResult = PQprepare( session.theConnection,  "", query.data(), 0, 0 );
  ExecStatusType status = PQresultStatus( theResult );
  if ( status != PGRES_COMMAND_OK )
    Throw( query );
}

Statement &Statement::bind( const char *param, int number ) {
  if( !number ) throw Error( "sql bind 'parameter number can't be 0'." );
  if( number == -1 ) number = params.size() + 1;
  if( number > static_cast<int>( params.size())) params.resize( number );
  params[ number - 1 ].value = param;
  params[ number - 1 ].isNull = false;
  return *this;
}

Statement &Statement::bind( std::string param, int number ) {
  return bind( param.data(), number );
}

Statement &Statement::bindNull( int number ) {
  if( !number ) throw Error( "sql bindNull 'parameter number can't be 0'." );
  if( number == -1 ) number = params.size() + 1;
  if( number > static_cast<int>( params.size())) params.resize( number );
  params[ number - 1 ].isNull = true;
  return *this;
}

Statement &Statement::bind( std::tm param, int number ) {
  return bind( formatTime( param ), number );
}

Statement &Statement::bind( std::time_t param, int number ) {
  std::tm t; localtime_r( &param, &t );
  return bind( formatTime( t ), number );
}

/* Transaction */

Transaction::Transaction( Session &aSession ) : session( aSession ),
  done( false )
{
  session.activeSqlTransaction = false;
  session( "BEGIN" );
  disabled = session.activeSqlTransaction;
}

void Transaction::commit() {
  if( done ) throw Error( "sql transaction already done" );
  if( disabled ) return;
  session( "END" );
  done = true;
}

void Transaction::rollback() {
  if( done ) throw Error( "sql transaction already done" );
  if( disabled ) return;
  session( "ROLLBACK" );
  done = true;
}

Transaction::~Transaction() {
  if( disabled ) return;
  try { if( ! done ) session( "ROLLBACK" ); }
  catch( ... ) { }
}


}}; // sql, x2

