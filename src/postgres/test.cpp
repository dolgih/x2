/**
    \file test.cpp
    test for x2 sql

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include <stdexcept>
#include <string>
#include <vector>
#include <sstream>
#include <ctime>
#include <iostream>
#include "postgres.h"

using namespace x2;

int main(void) {
  try {
    sql::Session sql;
    if( ! sql.isOpened())
      sql.open( "dbname=x2" );
    auto r = sql.prepare( "SELECT * FROM users WHERE id=$1" ).bind(102).row();
    while( r.next()) {
      for( int j = 0; j < r.fieldCount(); j ++ )
        std::cout << r[ j ].get() << "; ";
      std::cout << "\n";
    }
    sql::Transaction guard( sql );
    r = sql( "SELECT * FROM posts WHERE id=1 FOR UPDATE NOWAIT" );
    r.next();
    std::cout << "\r" << r[ 0 ].get();
    guard.commit();
  } catch ( const x2::sql::ErrorLockNotAvailable &e ) {
    std::cout << "Locked=== " << e.what();
  }
    catch ( const x2::sql::Error &e ) {
    std::cout << e.what();
  }

  return 0;
}
