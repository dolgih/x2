/**
    \file bbcode.cpp
    bbCode to html decoder
    implemented as described in http://www.bbcode.org/reference.php

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2017 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {
namespace bbcode {

// utilities

static inline Str lower( Str s ) {
  std::transform( s.begin(), s.end(), s.begin(), ::tolower );
  return s;
}

inline static size_t skipSpaces( Str src, size_t &from, size_t &len ) {
  while( len && src[from] && isspace( src[from] )) len --, from ++;
  if( ! src[from] && len ) throw std::runtime_error( "debug:skipSpaces" );
  return from;
}

inline static size_t accomplishWord( Str src, size_t &from, size_t &len ) {
  while( len && src[from] && isalpha( src[from] )) len --, from ++;
  if( ! src[from] && len ) throw std::runtime_error( "debug:accomplishWord" );
  return from;
}

inline static size_t accomplishString( Str src, char delim, size_t &from, size_t &len ) {
  while( len && src[from] && ! isspace( src[from] ) && src[from] != delim ) len --, from ++;
  if( ! src[from] && len ) throw std::runtime_error( "debug:accomplishString" );
  return from;
}

inline static size_t findToken( char c, Str src, size_t &from, size_t &len ) {
  while( len && src[from] && src[from] != c ) len --, from ++;
  if( ! src[from] && len ) throw std::runtime_error( "debug:findToken" );
  return from;
}

// end of  utilities

// FoundTag
class BbCode;
struct FoundTag {
  FoundTag( BbCode &bbCode ) : id( lastId ++ ), bbCode( bbCode ), descr( 0 ),
    state( tagNone ), b( 0 ), l( 0 ), takesData( tdNone ) { }
  BbCodable &bbcodable() const;
  const unsigned id;
  BbCode &bbCode;
  Descriptor *descr;
  enum State { tagNone, tagOpen, tagClose } state;
  size_t b /* begin */, l /* length */;
  Str value, data;
  std::map<Str, Str> params;
  enum TakesData { tdNone = 0,
    tdWithoutBbc /* for [url], [img], [youtube], those processing tag data */,
    tdIgnoreBbc /* for [nobbc], [html] */ } takesData;
private:
  static unsigned lastId;
};

unsigned FoundTag::lastId = 0;
// end of FoundTag

class BbCode {

public:
  static const unsigned char maxRecursionLevel = 20;

  BbCode( Str src, BbCodable &bbcodable, bool fullUrl, BbCodeInfo *bbCodeInfo,
          bool dryRun = false )
    : fullUrl( fullUrl ), dryRun( dryRun), bbCodeInfo( bbCodeInfo ),
      src( src ), bbcodable( bbcodable ) { }
  Str decode() {
    size_t begin = 0, length = src.length();
    Str decoded = decodeTags( begin, length );
    if( dryRun )
      return Str();
    if( begin != src.length() || length != 0 )
      throw std::runtime_error( "debug:decodeTags returned invalid result" );
    if( stack.size())
      throw std::runtime_error( "debug:decodeTags returned invalid \"stack.size()\"" );
    return "<div class=\"bbcode\" style=\"white-space: pre-wrap;\">" + decoded +
           "</div><!--end of bbcode-->";
  }

  bool fullUrl, dryRun;
  BbCodeInfo *bbCodeInfo;

private:

  Str src; // source
  BbCodable &bbcodable;
  std::list<FoundTag*> stack;

  Str decodeTags( size_t &b, size_t &l,
                  unsigned char level = 0 /* nested level of recursion*/,
                  FoundTag *preOpen = 0 /* in, opening tag, found previously in recursive call */,
                  const char **postClose = 0 /* out, closing tag, found inside recursive call */ )
  {
    if( level >= maxRecursionLevel ) return Str();

    Str decoded;
    for( ;; ) {
      FoundTag first( preOpen ? *preOpen : FoundTag( *this ));
      if( ! preOpen ) {
        findTag( first, b, l );

        if( first.state == FoundTag::tagNone ) {
          decoded += smilesFilter( src.substr( b, l )); /* all */
          shiftSegment( b, l, b + l );
          return decoded;
        }

        if( first.state == FoundTag::tagClose ) {
          decoded += smilesFilter( src.substr( b, first.b - b )); /* from begin to begin of tag */
          shiftSegment( b, l, first.b + first.l ); /* to end of tag */
          *postClose = first.descr->name;
          return decoded;
        }
      } else
        preOpen = 0;

      /* first.state == FoundTag::tagOpen */

      decoded += smilesFilter( src.substr( b, first.b - b )); /* from begin to begin of tag */
      shiftSegment( b, l, first.b + first.l ); /* to end of tag */

      if( first.takesData == FoundTag::tdNone )
        decoded += first.descr->renderOpen( first );

      if( first.descr->properties & Descriptor::single ) continue;

      stack.push_back( &first );

      {
        Str data; FoundTag second( *this ); const char *closed = 0;

        if( first.takesData == FoundTag::tdIgnoreBbc )
          findEndOfTag( first, second, b, l );
        else
          findTag( second, b, l );
        if( second.state == FoundTag::tagNone ) {
          /* the close tag not found, error, output all the source */
          second.descr = first.descr, second.state = FoundTag::tagClose,
          second.b = b + l, second.l = 0;
        }

        if( second.state == FoundTag::tagClose ) {
          closed = second.descr->name;
          data = src.substr( b, second.b - b ); /* from begin to begin of tag */
          shiftSegment( b, l, second.b + second.l ); /* to end of tag */

          if( first.takesData != FoundTag::tdNone ) {
            if( ! strcmp( first.descr->name, "html" )) {
              if( bbCodeInfo )
                bbCodeInfo->thereIsHtmlTag = true;
            } else
              data = utl::htEsc( data );
            first.descr->setData( first, data );
            decoded += first.descr->renderOpen( first );
          } else
            decoded += smilesFilter( data );
        }

        else { // second.state == tagOpen
          decoded += smilesFilter( src.substr( b, second.b - b )); /* from begin to begin of tag */
          shiftSegment( b, l, second.b ); /* to begin of tag */
          if( first.takesData == FoundTag::tdWithoutBbc ) { // error
            /* the tag to demonstrate error */
            decoded = decoded + "[" + first.descr->name + "] tag data error ";
          }
          decoded += decodeTags( b, l, level + 1, &second, &closed );
        }
        // end of tag
        decoded += first.descr->renderClose();
        stack.pop_back();
        if( closed && closed == first.descr->name ) {
          continue;
        } else {
          if( closed ) *postClose = closed;
          return decoded;
        }
      }
    }
  }

  void findTag( FoundTag &fnd, size_t b /* begin */, size_t l /* length */ )
  {
    if( ( l + b ) > src.length()) throw std::runtime_error( "debug:findTag badArgs" );

    for( ;; ) {
      FoundTag::State state = FoundTag::tagOpen;

      fnd.b = findToken( '[', b, l ); if( !l ) return;
      b ++; l --; /* found '[' */

      size_t spaces = l;

      fnd.l = skipSpaces( b, l ) - fnd.b;
      /* begin of tag name or '/' found */
      if( src[b] == '/' ) {
        state = FoundTag::tagClose;
        b ++; l --; fnd.l ++; /* found '/' */
      }

      fnd.l = skipSpaces( b, l ) - fnd.b;
      if( ! l ) return; /* begin of tag name found */

      spaces -= l;

      fnd.l = accomplishWord( b, l ) - fnd.b;
      if( ! fnd.l ) continue;
      if( ! l ) return; /* end of tag name found */
      /* tag name found */

      Str tagName = lower( Str( src, fnd.b + spaces + 1, fnd.l - spaces - 1 ));
      AllTags::const_iterator foundDescr = bbcodable.allTags.find( tagName );
      if( foundDescr == bbcodable.allTags.end()) continue;
      /* tag descriptor found */

      fnd.descr = foundDescr->second.get();
      if( state == FoundTag::tagOpen ) fnd.descr->init( fnd );
      fnd.l = skipSpaces( b, l ) - fnd.b;
      if( ! l ) return; /* ']' or value or params found */

      if( state == FoundTag::tagOpen ) {

        if( src[ b ] == '=' ) { // value
          b ++; l --; fnd.l ++; // skip '='
          if( fnd.descr->properties &
              ( Descriptor::mayHaveValue | Descriptor::mustHaveValue ))
          { // value
            size_t valueLen = fnd.descr->loadValue( fnd, src, b, l );
            if( valueLen == ( size_t ) -1 ) continue;
            if( valueLen > l ) throw std::runtime_error
              ( "debug:Descriptor successor::loadValue returned invalid result" );
            b += valueLen; l -= valueLen; fnd.l += valueLen;
            if( ! l ) return;
          } else continue;
        } else if( fnd.descr->properties & Descriptor::mustHaveValue )
          continue;
        // value found

        fnd.l = skipSpaces( b, l ) - fnd.b;
        if( ! l ) return; /* ']' or params found */

        if( fnd.descr->properties & Descriptor::mayHaveParameters ) {
          size_t paramLen = fnd.descr->loadParams( fnd, src, b, l );
          if( paramLen == ( size_t ) -1 ) continue;
          if( paramLen > l ) throw std::runtime_error
            ( "debug:Descriptor successor::loadParams returned invalid result" );
          b += paramLen; l -= paramLen; fnd.l += paramLen;
          if( ! l ) return;
        }
        // params found

        fnd.l = skipSpaces( b, l ) - fnd.b;
      } else { // state == FoundTag::tagClose
        for( std::list<FoundTag*>::const_reverse_iterator i = stack.rbegin();
             i != stack.rend(); i ++
           )
          if( ( **i ).descr->name == tagName ) goto found;
        continue;
found:;
      }

      if( src[ b ] != ']' ) continue;
      /* all found */

      b ++; l --; fnd.l ++; // skip ']'
      fnd.state = state;
      return;
    }
  }

  void findEndOfTag( const FoundTag &bgn, FoundTag &fnd, size_t b /* begin */, size_t l /* length */ )
  {
    if( ( l + b ) > src.length()) throw std::runtime_error( "debug:findEndOfTag badArgs" );

    for( ;; ) {
      fnd.b = findToken( '[', b, l ); if( !l ) return;
      b ++; l --; /* found '[' */

      size_t spaces = l;

      fnd.l = skipSpaces( b, l ) - fnd.b;
      /* begin of tag name or '/' found */
      if( src[b] == '/' ) {
        b ++; l --; fnd.l ++; /* found '/' */
      } else
        continue;

      fnd.l = skipSpaces( b, l ) - fnd.b;
      if( ! l ) return; /* begin of tag name found */

      spaces -= l;

      fnd.l = accomplishWord( b, l ) - fnd.b;
      if( ! fnd.l ) continue;
      if( ! l ) return; /* end of tag name found */
      /* tag name found */

      Str tagName = lower( Str( src, fnd.b + spaces + 1, fnd.l - spaces - 1 ));

      fnd.l = skipSpaces( b, l ) - fnd.b;
      if( ! l ) return; /* ']' found? */
      if( src[ b ] != ']' ) continue;
      b ++; l --; fnd.l ++; // skip ']'

      if( tagName == bgn.descr->name ) {
        fnd.descr = bgn.descr;
        fnd.state = FoundTag::tagClose;
        return;
      }
    }
  }

  std::string smilesFilter( std::string src ) {
    if( dryRun )
      return Str();
    src = utl::htEsc( src );
    const char eye[] = ";:", nose[] = "-=~><", mouth[] = ")(|][!}{";
    bool begin = true;
    for( Str::iterator i = src.begin(); i != src.end(); ) {
      short delSpace = *i == ' ' ? 1 : 0;
      if( begin )
        begin = false;
      else if( strncmp( ":)", &*i, 2 ) && ! isspace( *( i ++ )))
        continue;
      size_t len = src.end() - i, codeLen = 0;
      if( len >= 2 && strchr( eye, *i )) {
        if( strchr( mouth, i[1] ))
          codeLen = 2;
        else if( len >= 3 && strchr( nose, i[1] ) && strchr( mouth, i[2] ))
          codeLen = 3;
        else
          for( int j = 1; j < 16; j ++ ) {
            if( isupper( i[j] )) continue;
            else if( i[j] == ':' && j != 1 ) codeLen = j + 1;
            break;
          }
      }
      if( codeLen ) {
        Str smile = bbcodable.translateSmile( Str( i, i + codeLen ), fullUrl );
        if( smile.empty())
          i += codeLen;
        else {
          Str newSrc = Str( src.begin(), i - delSpace ) + smile + Str( i + codeLen, src.end());
          size_t len2 = src.end() - i - codeLen;
          src = newSrc;
          i = src.end() - len2;
        }
      }
    }
    return src;
  }

  void shiftSegment( size_t &begin, size_t &length, size_t newBegin ) {
    length -= ( newBegin - begin ); begin = newBegin;
  }
  size_t skipSpaces( size_t &from, size_t &len ) const {
    return bbcode::skipSpaces( src, from, len );
  }
  size_t accomplishWord( size_t &from, size_t &len ) const {
    return bbcode::accomplishWord( src, from, len );
  }
  size_t findToken( char c, size_t &from, size_t &len ) const {
    return bbcode::findToken( c, src, from, len );
  }

friend BbCodable &FoundTag::bbcodable() const;
};

// FoundTag
inline BbCodable &FoundTag::bbcodable() const { return bbCode.bbcodable; }
// end of FoundTag


// base classes for tag description

// for -1, we are Zorro
#pragma GCC diagnostic ignored "-Wsign-compare"

// Descriptor
Descriptor::Descriptor( const char *name, const char *htmlTag,
                        Properties properties, Category category, const char *title ) :
  name( name ), properties( properties ), category( category ), title( title ),
  htmlTag( htmlTag ) { }

Str Descriptor::id( FoundTag &f ) const {
  return "bbcodeTag" + utl::n2s( f.id ) + name;
}

Descriptor::~Descriptor() { }

void Descriptor::init( FoundTag & ) { }

size_t Descriptor::loadParams( FoundTag &f, Str src, size_t b, size_t l )
{ return /* length of the params taken out or -1 if error */ 0; }

size_t Descriptor::loadValue( FoundTag &f, Str src, size_t b, size_t l )
{ return /* length of the value taken out or -1 if error */ 0; }

Str Descriptor::renderOpen( FoundTag & ) const {
  return htmlTag ? Str( "<" ) + htmlTag + ">" : Str();
}

void Descriptor::setData( FoundTag &, Str ) { }

Str Descriptor::renderClose() const {
  return htmlTag ? Str( "</" ) + htmlTag + ">" : Str();
}

Str Descriptor::insertion() const { return Str(); }

const Descriptor::ValueVector &Descriptor::values() const
{ static ValueVector v; return v; }
// end of Descriptor

// Tags
class TagSingle : public Descriptor {
public:
  TagSingle( const char *name, const char *htmlTag, Category category,
             const char *title ) : Descriptor( name, htmlTag, single, category, title ) { }
};

class TagSimple : public Descriptor {
public:
  TagSimple( const char *name, const char *htmlTag, Category category,
             const char *title ) : Descriptor( name, htmlTag, simple, category, title ) { }
};

class TagWithValue : public Descriptor {
public:
  TagWithValue( const char *name, const char *htmlTag, Category category,
                const char *title ) : Descriptor( name, htmlTag, mustHaveValue, category,
                      title ) { }
  size_t loadValue( FoundTag &f, Str src, size_t b, size_t l ) {
    size_t oldB = b, oldL = l;
    findToken( ']', src, b, l );
    if( ! l ) return -1; // error: the end missed
    f.value = utl::rtrim( src.substr( oldB, b - oldB ));
    return /* length of the value taken out or -1 if error */ oldL - l;
  }
};

class TagWithParams : public TagWithValue {
public:
  TagWithParams( const char *name, const char *htmlTag, Category category,
                 const char *title ) : TagWithValue( name, htmlTag, category, title )
  { properties = mayHaveValue | mayHaveParameters; }
  void init( FoundTag &f ) { f.takesData = FoundTag::tdWithoutBbc; }
  size_t loadValue( FoundTag &f, Str src, size_t b, size_t l ) {
    // check format
    size_t valueLen = TagWithValue::loadValue( f, src, b, l );
    if( valueLen == -1 ) return -1;
    unsigned width, height; int n = -1;
    f.value = lower( f.value );
    if( sscanf( f.value.data(), "%ux%u%n", &width, &height, &n ) == 2 && n == f.value.length()) {
      f.params["width"] = utl::n2s( width ); f.params["height"] = utl::n2s( height );
      return valueLen;
    } else
      return -1;
  }
  size_t loadParams( FoundTag &f, Str src, size_t b, size_t l ) {
    for( size_t oldL = l, currB = b; ; currB = b ) {
      accomplishWord( src, b, l );
      Str var = lower( src.substr( currB, b - currB ));
      if( var.empty()) return oldL - l;
      if( ! l || ! ( var == "width" || var == "height" )) return -1;
      skipSpaces( src, b, l ); if( ! l ) return -1;
      if( src[ b ] != '=' ) return -1;
      b ++; l --; // skip '='
      skipSpaces( src, b, l ); if( ! l ) return -1;
      currB = b;
      Str val = src.substr( currB,  accomplishString( src, ']', b, l ) - currB );

      { // check format
        unsigned numValue; int n1 = -1, n2 = -1;
        if( sscanf( val.data(), "%u%n%*[%]%n", &numValue, &n1, &n2 ) != 1
            || ( n1 != val.length() && n2 != val.length())
        )
          return -1;
      } // end of check format

      f.params[var] = val;
      skipSpaces( src, b, l );
    }
  }
  void setData( FoundTag &f, Str aData ) { f.data = aData;}
};

// tags description

struct TagHr : TagSingle {
  TagHr() : TagSingle( "hr", "hr", categoryLine, "Horisontal line" ) { }
};

struct TagB : TagSimple {
  TagB() : TagSimple( "b", "strong", categoryFontStyle, "Bold" ) { }
};

struct TagI : TagSimple {
  TagI() : TagSimple( "i", "em", categoryFontStyle, "Italic" ) { }
};

struct TagU : TagSimple {
  TagU() : TagSimple( "u", "span", categoryFontStyle, "Underline" ) { }
  Str renderOpen( FoundTag & ) const {
    return Str( "<" ) + htmlTag +
           " class=\"bbcodenomargins\" style=\"text-decoration:underline;\">";
  }
};

struct TagS : TagSimple {
  TagS() : TagSimple( "s", "span", categoryFontStyle, "Strike through" ) { }
  Str renderOpen( FoundTag & ) const {
    return Str( "<" ) + htmlTag +
           " class=\"bbcodenomargins\" style=\"text-decoration:line-through;\">";
  }
};

struct TagTt : TagSimple {
  TagTt() : TagSimple( "tt", "span", categoryFontStyle, "Teletype" ) { }
  Str renderOpen( FoundTag & ) const {
    return Str( "<" ) + htmlTag +
           " class=\"bbcodenomargins\" style=\"font-family:monospace;\">";
  }
};

struct TagSup : TagSimple {
  TagSup() : TagSimple( "sup", "sup", categorySubSuperScript, "Superscript" ) { }
};

struct TagSub : TagSimple {
  TagSub() : TagSimple( "sub", "sub", categorySubSuperScript, "Subscript" ) { }
};

struct TagCenter : TagSimple {
  TagCenter() : TagSimple( "center", "div", categoryAlign, "Center text" ) { }
  Str renderOpen( FoundTag & ) const {
    return Str( "<" ) + htmlTag + " style=\"text-align:center;\">";
  }
};

struct TagLeft : TagSimple {
  TagLeft() : TagSimple( "left", "div", categoryAlign, "Left align text" ) { }
  Str renderOpen( FoundTag & ) const {
    return Str( "<" ) + htmlTag + " style=\"text-align:left;\">";
  }
};

struct TagRight : TagSimple {
  TagRight() : TagSimple( "right", "div", categoryAlign, "Right align text" ) { }
  Str renderOpen( FoundTag & ) const {
    return Str( "<" ) + htmlTag + " style=\"text-align:right;\">";
  }
};

struct TagUl : TagSimple {
  TagUl() : TagSimple( "ul", "ul", categoryList, "Unordered list" ) { }
  Str insertion() const { return "[ul]\n[li]...[/li]\n[li]...[/li]\n"; }
};

struct TagOl : TagSimple {
  TagOl() : TagSimple( "ol", "ol", categoryNone, "Ordered list" ) { }
};

struct TagList : TagSimple {
  TagList() : TagSimple( "list", "ol", categoryList, "Ordered list" ) { }
  Str insertion() const { return "[list]\n[li]...[/li]\n[li]...[/li]\n"; }
};

struct TagLi : TagSimple {
  TagLi() : TagSimple( "li", "li", categoryNone, "List item" ) { }
};

struct TagCode : TagSimple {
  TagCode() : TagSimple( "code", "code", categoryInsertion, "Formatted code" ) { }
};

struct TagTable : TagSimple {
  TagTable() : TagSimple( "table", "table", categoryNone, "Table" ) { }
};

struct TagTr : TagSimple {
  TagTr() : TagSimple( "tr", "tr", categoryNone, "Table row" ) { }
};

struct TagTh : TagSimple {
  TagTh() : TagSimple( "th", "th", categoryNone, "Table header cell" ) { }
};

struct TagTd : TagSimple {
  TagTd() : TagSimple( "td", "td", categoryNone, "Table cell" ) { }
};

struct TagNobbc : TagSimple {
  TagNobbc() : TagSimple( "nobbc", 0, categoryNone, "Nobbc text" ) { }
  void init( FoundTag &f ) { f.takesData = FoundTag::tdIgnoreBbc; }
  Str renderOpen( FoundTag &f ) const { return f.data; }
  void setData( FoundTag &f, Str aData ) { f.data = aData; }
};

struct TagHtml : TagSimple {
  TagHtml() : TagSimple( "html", "div", categoryNone, "Html text" ) { }
  void init( FoundTag &f ) { f.takesData = FoundTag::tdIgnoreBbc; }
  Str renderOpen( FoundTag &f ) const {
    return Str( "<" ) + htmlTag +
      " class=\"bbcodenomargins\" style=\"display: inline; white-space: normal;\">"
      + f.data;
  }
  void setData( FoundTag &f, Str aData ) { f.data = aData; }
};

struct TagSize : TagWithValue {
  TagSize() : TagWithValue( "size", "span", categoryFontColorSize, "Font size" ) { }
  size_t loadValue( FoundTag &f, Str src, size_t b, size_t l ) {
    size_t valueLen = TagWithValue::loadValue( f, src, b, l );
    if( valueLen == -1 ) return -1;
    int n = -1; unsigned size; char unit[16] = "";
    if( sscanf( f.value.data(), "%u%n%2[%ptxem]%n", &size, &n, unit, &n ) >= 1 && n == f.value.length()) {
      Str u = unit;
      if( u.empty())
        f.value += "px";
      if( u == "" || u == "%" || u == "em" || u == "ex" || u == "pt" || u == "px" )
        return valueLen;
    } else {
      n = -1; char word[32] = "";
      if( sscanf( f.value.data(), "%31c%n", word, &n ) == 1 && n == f.value.length())
        for( ValueVector::const_iterator i = values().begin(); i != values().end(); i ++ )
          if( ! strcasecmp( *i, word ))
            return valueLen;
    }
    return -1;
  }
  Str renderOpen( FoundTag &f ) const {
    return Str( "<" ) + htmlTag +
           " class=\"bbcodenomargins\" style=\"font-size: " + f.value + ";\">";
  }
  const ValueVector &values() const {
    static ValueVector v = {
      "smaller",
      "xx-small",
      "x-small",
      "small",
      "medium",
      "large",
      "x-large",
      "xx-large",
      "larger"
    };
    return v;
  }
};

// TagColour
struct AllColors {
  struct Comp {
    bool operator() ( const char *left, const char *right ) const
    { return strcasecmp( left, right ) < 0; }
  };
  static std::set<const char*, Comp> theColors;
};
std::set<const char*, AllColors::Comp> AllColors::theColors =
{ "IndianRed", "LightCoral", "Salmon", "DarkSalmon", "LightSalmon", "Crimson", "Red", "FireBrick",
  "DarkRed", "Pink", "LightPink", "HotPink", "DeepPink", "MediumVioletRed", "PaleVioletRed", "LightSalmon", "Coral", "Tomato",
  "OrangeRed", "DarkOrange", "Orange", "Gold", "Yellow", "LightYellow", "LemonChiffon", "LightGoldenrodYellow", "PapayaWhip",
  "Moccasin", "PeachPuff", "PaleGoldenrod", "Khaki", "DarkKhaki", "Lavender", "Thistle", "Plum", "Violet", "Orchid", "Fuchsia",
  "Magenta", "MediumOrchid", "MediumPurple", "BlueViolet", "DarkViolet", "DarkOrchid", "DarkMagenta", "Purple", "Indigo",
  "SlateBlue", "DarkSlateBlue", "Cornsilk", "BlanchedAlmond", "Bisque", "NavajoWhite", "Wheat", "BurlyWood", "Tan",
  "RosyBrown", "SandyBrown", "Goldenrod", "DarkGoldenRod", "Peru", "Chocolate", "SaddleBrown", "Sienna", "Brown", "Maroon",
  "Black", "Gray", "Silver", "White", "Fuchsia", "Purple", "Red", "Maroon", "Yellow", "Olive", "Lime", "Green", "Aqua", "Teal",
  "Blue", "Navy", "GreenYellow", "Chartreuse", "LawnGreen", "Lime", "LimeGreen", "PaleGreen", "LightGreen",
  "MediumSpringGreen", "SpringGreen", "MediumSeaGreen", "SeaGreen", "ForestGreen", "Green", "DarkGreen", "YellowGreen",
  "OliveDrab", "Olive", "DarkOliveGreen", "MediumAquamarine", "DarkSeaGreen", "LightSeaGreen", "DarkCyan", "Teal", "Aqua",
  "Cyan", "LightCyan", "PaleTurquoise", "Aquamarine", "Turquoise", "MediumTurquoise", "DarkTurquoise", "CadetBlue",
  "SteelBlue", "LightSteelBlue", "PowderBlue", "LightBlue", "SkyBlue", "LightSkyBlue", "DeepSkyBlue", "DodgerBlue",
  "CornflowerBlue", "MediumSlateBlue", "RoyalBlue", "Blue", "MediumBlue", "DarkBlue", "Navy", "MidnightBlue", "White", "Snow",
  "Honeydew", "MintCream", "Azure", "AliceBlue", "GhostWhite", "WhiteSmoke", "Seashell", "Beige", "OldLace", "FloralWhite",
  "Ivory", "AntiqueWhite", "Linen", "LavenderBlush", "MistyRose", "Gainsboro", "LightGrey", "LightGray", "Silver", "DarkGray",
  "DarkGrey", "Gray", "Grey", "DimGray", "DimGrey", "LightSlateGray", "LightSlateGrey", "SlateGray", "SlateGrey",
  "DarkSlateGray", "DarkSlateGrey", "Black" };
struct TagColour : TagWithValue {
  TagColour() : TagWithValue( "color", "span", categoryFontColorSize, "Font color" ) { }
  size_t loadValue( FoundTag &f, Str src, size_t b, size_t l ) {
    size_t valueLen = TagWithValue::loadValue( f, src, b, l );
    if( valueLen == -1 ) return -1;
    int n = -1; sscanf( f.value.data(), "#%*x%n", &n );
    if( n == f.value.length())
      return valueLen;
    n = -1; char name[32];
    if( sscanf( f.value.data(), "%31[A-Za-z]%n", name, &n ) == 1 && n == f.value.length())
      return AllColors::theColors.find( name ) == AllColors::theColors.end() ? -1 : valueLen;
    return -1;
  }
  Str renderOpen( FoundTag &f ) const {
    return Str( "<" ) + htmlTag +
           " class=\"bbcodenomargins\" style=\"color: " + f.value + ";\">";
  }
  const ValueVector &values() const {
    static ValueVector v = {
      "Silver",
      "Gray",
      "Red",
      "OrangeRed",
      "Maroon",
      "Yellow",
      "Olive",
      "Lime",
      "Green",
      "Aqua",
      "Teal",
      "Blue",
      "Indigo",
      "Navy",
      "Fuchsia",
      "Purple",
    };
    return v;
  }
};

struct TagUrl : TagWithValue {
  TagUrl() : TagWithValue( "url", "a", categoryUrl, "URL" )
  { properties = mayHaveValue; }
  TagUrl( const char *name, const char *title ) :
    TagWithValue( name, "a", categoryUrl, title )
  { properties = mayHaveValue; }
protected:
  void init( FoundTag &f ) { f.takesData = FoundTag::tdWithoutBbc; }
  size_t loadValue( FoundTag &f, Str src, size_t b, size_t l ) {
    size_t valueLen = TagWithValue::loadValue( f, src, b, l );
    f.takesData = f.value.empty() ? FoundTag::tdWithoutBbc : FoundTag::tdNone;
    return valueLen;
  }
  Str renderOpen( FoundTag &f ) const {
    Str url = f.value.empty() ? f.data : utl::htEsc2( f.value );
    return "<a href=\"" + url + "\" title=\"" +
           url + "\" target=\"_blank\">" + f.data;
  }
  void setData( FoundTag &f, Str aData ) { f.data = aData; }
};

struct TagMail : TagUrl {
  TagMail() : TagUrl( "email", "Email address" ) { }
protected:
  Str renderOpen( FoundTag &f ) const {
    Str url = f.value.empty() ? f.data : f.value;
    return "<a href=\"mailto:" + url + "\" title=\"" + url + "\">" + f.data;
  }
};

struct TagQuote : TagWithValue {
  TagQuote() : TagWithValue( "quote", "div", categoryInsertion, "Quoted text" )
  { properties = mayHaveValue; }
  Str renderOpen( FoundTag &f ) const {
    Str title, author; long long postId;
    if( ! f.value.empty()) {
      if( ( postId = atoll( f.value.data()))) {
        if( f.bbCode.bbCodeInfo )
          f.bbCode.bbCodeInfo->quotedPosts.insert( postId );
        f.bbcodable().quoteData( postId, title, author, f.bbCode.fullUrl );
        title = "in \"<b>" + title + "</b>\" &nbsp;";
        author = "<i>" + author + " </i>wrote:<br>";
      } else
        author = "<i>" + f.value + " </i>wrote:<br>";
    }
    return Str( "<" ) + htmlTag + " class=\"x1bbcodequote\">"
           "<span style=\"color:red;\"><b>&#8220;&emsp;</b></span>" + title + author;
  }
};

struct TagSpoiler : TagWithValue {
  TagSpoiler() : TagWithValue( "spoiler", "details", categoryInsertion, "Collapsed text" )
  { properties = mayHaveValue; }
  Str renderOpen( FoundTag &f ) const {
    Str pId = id( f ) + "P";
    Str summary = "<summary class=\"x1bbcodespoilersummary\""
                  " onclick=\"var p=document.getElementById('" + pId + "');p.hidden=!p.hidden;\">";
    if( f.value.empty()) summary += f.bbcodable().gettextBb( "Details" );
    else summary += f.value;
    summary += "</summary>";
    return "<details class=\"x1bbcodespoilerdetails\">" + summary +
           "<p id=\"" + pId + "\" class=\"x1bbcodespoilerp\">" +
           "<script>document.getElementById('" + pId + "').hidden=true;</script>";
  }
  Str renderClose() const { return "</p></details>"; }
};

struct TagImg : TagWithParams {
  TagImg() : TagWithParams( "img", "img", categoryUrl, "Image URL" ) { }
  Str renderOpen( FoundTag &f ) const {
    Str out;
    for( std::map<Str, Str>::const_iterator i = f.params.begin();
         i != f.params.end(); i ++
    )
      out = out + " " + i->first + "=\"" + i->second + "\"";
    out += " src=\"" + f.data + "\" alt=\"Image\" style=\"max-width:100%\"";
    return Str( "<" ) + htmlTag + out + ">";
  }
  Str renderClose() const { return Str(); }
};

struct TagYoutube : TagWithParams {
  TagYoutube() : TagWithParams( "youtube", "iframe", categoryUrl, "Youtube" ) { }
  TagYoutube( const char *name, const char *htmlTag, Category category,
              const char *title ) : TagWithParams( name, htmlTag, category, title ) { }
  Str findId( Str src, Str pattern, Str delims ) const {
    if( const char *found = strcasestr( src.data(), pattern.data())) {
      found = found + pattern.length();
      if( const char *found2 = strstr( found, "&quot;" ))
        return Str( found, found2 - found );
      else if( const char *found2 = strpbrk( found, delims.data()))
        return Str( found, found2 - found );
      else
        return found;
    }
    return Str();
  }
  Str renderOpen( FoundTag &f ) const {
    Str id = f.data;
    static const Str patterns[] =
      { "youtu.be/", "youtube.com/watch?v=", "youtube.com/embed/" };
    for( size_t i = 0; i < ( sizeof patterns / sizeof patterns[0] ); i ++ ) {
      Str found = findId( f.data, patterns[i], "&\"" );
      if( !found.empty()) {
        id = found; break;
      }
    }
    Str out;
    for( std::map<Str, Str>::const_iterator i = f.params.begin();
         i != f.params.end(); i ++
    )
      out = out + " " + i->first + "=\"" + i->second + "\"";
    out += " class=\"youtube-player\""
           " src=\"http" + ( f.bbcodable().httpsBb() ? "s" : Str()) +
           "://www.youtube.com/embed/" + id +
           "?feature=player_embedded\" allowfullscreen frameborder=\"0\"";
    return Str( "<" ) + htmlTag + out + ">";
  }
};

struct TagMedia : TagYoutube {
  TagMedia() : TagYoutube( "media", "iframe", categoryUrl, "Media" ) { }
  Str fillFrame( Str addr, Str cssClass, Str id ) const {
    return " class=\"" + cssClass + "\" src=\"" + addr + id +
           "\" webkitAllowFullScreen mozallowfullscreen allowfullscreen frameborder=\"0\"";
  }
  Str renderOpen( FoundTag &f ) const {
    Str id;
    Str out;
    if( ! ( id = findId( f.data, "rutube.ru/play/embed/", "?&\"" )).empty())
      out += fillFrame( "https://rutube.ru/video/embed/", "rutube-player", id );
    else if ( ! ( id = findId( f.data, "player.vimeo.com/video/", "&\"" )).empty()
              || ! ( id = findId( f.data, "vimeo.com/", "\"" )).empty())
    { out += fillFrame( "https://player.vimeo.com/video/", "vimeo-player", id ); }
    else if ( ! ( id = findId( f.data, "vk.com/video_ext.php?oid=", "\"" )).empty())
      out += fillFrame( "https://vk.com/video_ext.php?oid=", "vk-player", id );
    else if ( ! ( id = findId( f.data, "facebook.com/plugins/video.php?href=", "&\"" )).empty())
      out += fillFrame( "https://www.facebook.com/plugins/video.php?href=", "fb-player", id );
    else
      return TagYoutube::renderOpen( f );
    for( std::map<Str, Str>::const_iterator i = f.params.begin();
         i != f.params.end(); i ++
    )
      out = out + " " + i->first + "=\"" + i->second + "\"";
    return Str( "<" ) + htmlTag + out + ">";
  }
};

// end of Tags

Smiles  BbCodable::smiles = {
  { ":)",   Smile( "Smile", "data:image/gif;base64,R0lGODlhDwAPAJEBA\r\n"
    "AAAAL+/v///AAAAACH5BAEAAAEALAAAAAAPAA8AAAIujA2Zx5EC4WIgWnnqvQBJLTyhE4khaG5\r\n"
    "Wqn4tp4ErFnMY+Sll9naUfGpkFL5DAQA7" ) },
  { ":(",   Smile( "Sad", "data:image/gif;base64,R0lGODlhDwAPAJEAAAA\r\n"
    "AAL29vYyt/729vSH5BAEAAAMALAAAAAAPAA8AAAIvnA2Zx5MC4WJAxirAwfhKlEVipXWPZKaXK\r\n"
    "rarC5+oMjrW+Nnwte1ZI0M1NgoNowAAOw==" ) },
  { ":TU:", Smile( "Thumb Up", "data:image/gif;base64,R0lGODlhFAAQAK\r\n"
    "EAAAAAAPXesx6Q/7+/vyH5BAEAAAMALAAAAAAUABAAAAI3nI8AyG0QwnIUSlrjxMnuqijRhVnj\r\n"
    "qJTnGn5PuEqC4A5mDMz1zepNi/MxeKfWL6apdVohjrNRAAA7" ) },
  { ":TD:", Smile( "Thumb Down", "data:image/gif;base64,R0lGODlhFAAQ\r\n"
    "AKEAAAAAAPXesx6Q/7+/vyH5BAEAAAMALAAAAAAUABAAAAI1nI+pB+2+WJh0tjhA3U+9HQCCAH\r\n"
    "ggJZLmGY5l8nHuenYwe2GPZmEe//IhgMJbqDjsIRnBRAEAOw==" ) },
};

AllTags BbCodable::allTags = {
  { "hr",      ( DescriptorPtr ) new TagHr },
  { "b",       ( DescriptorPtr ) new TagB },
  { "i",       ( DescriptorPtr ) new TagI },
  { "u",       ( DescriptorPtr ) new TagU },
  { "s",       ( DescriptorPtr ) new TagS },
  { "tt",      ( DescriptorPtr ) new TagTt },
  { "sup",     ( DescriptorPtr ) new TagSup },
  { "sub",     ( DescriptorPtr ) new TagSub },
  { "size",    ( DescriptorPtr ) new TagSize },
  { "color",   ( DescriptorPtr ) new TagColour },
  { "center",  ( DescriptorPtr ) new TagCenter },
  { "left",    ( DescriptorPtr ) new TagLeft },
  { "right",   ( DescriptorPtr ) new TagRight },
  { "quote",   ( DescriptorPtr ) new TagQuote },
  { "spoiler", ( DescriptorPtr ) new TagSpoiler },
  { "url",     ( DescriptorPtr ) new TagUrl },
  { "email",   ( DescriptorPtr ) new TagMail },
  { "img",     ( DescriptorPtr ) new TagImg },
  { "ul",      ( DescriptorPtr ) new TagUl },
  { "ol",      ( DescriptorPtr ) new TagOl },
  { "list",    ( DescriptorPtr ) new TagList },
  { "li",      ( DescriptorPtr ) new TagLi },
  { "code",    ( DescriptorPtr ) new TagCode },
  { "table",   ( DescriptorPtr ) new TagTable },
  { "tr",      ( DescriptorPtr ) new TagTr },
  { "th",      ( DescriptorPtr ) new TagTh },
  { "td",      ( DescriptorPtr ) new TagTd },
  { "nobbc",   ( DescriptorPtr ) new TagNobbc },
  { "html",    ( DescriptorPtr ) new TagHtml },
  { "youtube", ( DescriptorPtr ) new TagYoutube },
  { "media",   ( DescriptorPtr ) new TagMedia }
};
// data for gettext
#define gettext( msgid )
gettext( "Horisontal line" );
gettext( "Bold" );
gettext( "Italic" );
gettext( "Underline" );
gettext( "Strike through" );
gettext( "Teletype" );
gettext( "Superscript" );
gettext( "Subscript" );
gettext( "Center text" );
gettext( "Left align text" );
gettext( "Right align text" );
gettext( "Unordered list" );
gettext( "Ordered list" );
gettext( "List item" );
gettext( "Formatted code" );
gettext( "Table" );
gettext( "Table row" );
gettext( "Table header cell" );
gettext( "Table cell" );
gettext( "Nobbc text" );
gettext( "Html text" );
gettext( "Font size" );
gettext( "Font color" );
gettext( "URL" );
gettext( "Email address" );
gettext( "Quoted text" );
gettext( "Collapsed text" ); gettext( "Details" );
gettext( "Image URL" );
gettext( "Media" );
#undef gettext

void BbCodable::quoteData( long long postId, Str &postTitle, Str &authorName, bool ) {
  postTitle = "post#" + utl::n2s( postId );
  authorName = "theAuthorOfThePost" + utl::n2s( postId );
}

Str BbCodable::translateSmile( Str code, bool fullUrl ) {
  Smiles::const_iterator s = smiles.find( code );
  if( s == smiles.end())
    return Str();
  else {
    Str translatedTitle = gettextBb( s->second.title.data());
    return "<img class=\"smile\" alt=\"" + translatedTitle + "\" title=\"" +
           translatedTitle + "\" style=\"width:" + s->second.width + ";height:" +
           s->second.height + ";\" src=\"" +
           ( fullUrl && s->second.path.substr( 0, 5 ) != "data:" ? hostBb() : Str()) +
           s->second.path + "\"\r\n>";
  }
}

Str BbCodable::bbDecode( Str src, bool fullUrl, BbCodeInfo *bbCodeInfo, bool dryRun ) {
  try {
    x2::bbcode::BbCode bbcode( src, *this, fullUrl, bbCodeInfo, dryRun );
    return bbcode.decode();
  } catch ( const std::exception &e ) {
    Str err = "bbcode.cpp error:"; err += e.what();
    throw std::runtime_error( err );
  }
}

}} // namespace bbcode, x2

