/**
    \file test.cpp
    bbCode test
    bbCode realization as in http://www.bbcode.org/reference.php

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2017 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

int main( int argc, char **argv ) {

  if( argc < 2 ) {
    std::cerr << "Usage: test <inputFileName>\n";
    return 1;
  }

  std::ifstream input ( argv[ 1 ] );
  if( ! input.good()) {
    std::cerr << "can not open file\n";
    return 1;
  }

  std::string source;
  std::getline( input, source, '\0' );
  x2::bbcode::BbCodable bbCode;
  std::cout << bbCode.bbDecode( source, false ) << std::endl;

  return 0;
}

