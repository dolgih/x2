/**
    \file bbcode.h
    bbCode to html decoder

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2017 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#ifndef X2_BBCODE_H_
#define X2_BBCODE_H_


namespace x2 {
namespace bbcode {

struct Smile {
  Str title, path, width, height;
  Smile() { }
  Smile( Str title, Str path, Str width = "1em", Str height = "1em" )
    : title( title ), path( path ), width( width ), height( height ) { }
};
typedef std::map<Str, Smile> Smiles;

struct FoundTag;
struct Descriptor {
  enum Properties { propertiesNone = 1,
                    single = 2 /* for <hr> and so */,
                    simple = 4 /* for [b], [i] and so, without parameters or value */,
                    mustHaveValue = 8 /* for [size=...], [color=...] and so, with value */,
                    mayHaveValue = 16 /* for [quote...], [spoiler=...] */,
                    mayHaveParameters = 32 /* for [img], [youtube] */, };

  enum Category { categoryNone = 0, categoryFontStyle, categorySubSuperScript,
                  categoryFontColorSize, categoryUrl, categoryLine, categoryList,
                  categoryInsertion, categoryAlign, categoryAnother, categoryBound };

  Descriptor( const char *name, const char *htmlTag, Properties properties,
              Category category, const char *title );
  Str id( FoundTag &f ) const;
  virtual ~Descriptor();
  virtual void init( FoundTag & );
  virtual size_t loadParams( FoundTag &f, Str src, size_t b, size_t l );
  virtual size_t loadValue( FoundTag &f, Str src, size_t b, size_t l );
  virtual Str renderOpen( FoundTag & ) const;
  virtual void setData( FoundTag &, Str );
  virtual Str renderClose() const;

  const char *name; int properties;

  // The members for editors tool bar
  Category category;
  const char *title;
  virtual Str insertion() const;
  typedef std::vector<const char *> ValueVector;
  virtual const ValueVector &values() const;

protected:
  const char *htmlTag;
};
typedef std::shared_ptr<Descriptor> DescriptorPtr;
typedef std::map<Str, DescriptorPtr> AllTags;

struct BbCodeInfo {
  typedef std::set<long long> QuotedPosts;
  BbCodeInfo() : thereIsHtmlTag( false ) { }
  bool thereIsHtmlTag;
  QuotedPosts quotedPosts;
};

class BbCodable {
public:
  virtual Str gettextBb( const char *msgid ) { return msgid; }
  virtual bool httpsBb() { return true; }
  virtual Str hostBb() { return Str(); }
  virtual void quoteData( long long postId, Str &postTitle, Str &authorName, bool fullUrl );
  virtual Str translateSmile( Str code, bool fullUrl );
  /** BBCode (Bulletin Board Code) parser. */
  Str bbDecode( Str src, bool fullUrl, BbCodeInfo *BbCodeInfo = 0, bool dryRun = false );

  static Smiles smiles;

protected:
friend class BbCode;
  static AllTags allTags;
};

}} // bbcode, x2

#endif // X2_BBCODE_H_
