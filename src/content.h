/**
    \file content.h
    \brief Various content classes

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 {

/** Url */

struct Url {
  Str parent, key, tail;
  Url() { }
  Url( const Str &key ) : key( makeKey( key )) { }
  Url( const Str &parent, const Str &key ) : parent( parent ), key( makeKey( key )) { }
  Url( const Str &parent, const Str &key, const Str &tail );
  Str operator()() const { return withSlash( parent ) + withSlash( key ); }
  static Str makeKey( const Str &part ) {
    return part[ 0 ] == '/' ? part.substr( 1 ) : part;
  }
  static Str withSlash( const Str &part ) {
    return part.empty() ? Str() : ( part[ 0 ] == '/' ? part : '/' + part );
  }
  // make with parameters
  static Str mkParam() { return Str(); }
  template<class ...Ts, class T> static Str mkParam( const T &param, const Ts &...params ) {
    return '/' + utl::str( param ) + mkParam( params... );
  }
  template<class ...Ts> Str operator()( const Ts &...params ) const {
    return operator()() + mkParam( params... );
  }
  // arguments
  static bool args( const Str & ) { return true; }
  template<class ...Ts, class T>
  static bool args( const Str &path, T &value, Ts &...values ) {
    Str tail;
    if( ! nextPart( path, tail, value )) return false;
    return args( tail, values... );
  }
  static bool nextPart( Str path, Str &tail, Str &val );
  static bool nextPart( const Str &path, Str &tail, long &val );
  static bool nextPart( const Str &path, Str &tail, int &val );
};

/** Current user */

struct User {
  User();
  void init( Id id, sql::PhSession &sql );
  Id id;
  Str name, theme /* preffered theme */;
  bool hasMessages, hasAnswers;
  bool isAdmin, isAnonymous, isModerator;
  std::set<Id> groupsOfUser, notifications;

  bool hasAnonymousId() const { return id == predefined::user::anonymous || id < 0; }
  Id privatePart() const { return -id; }
  bool isMemberOfGroup( Id gId ) const { return groupsOfUser.find( gId ) != groupsOfUser.end(); }
  bool hasNotification( Id notifId ) const { return notifications.find( notifId ) != notifications.end(); }

  bool canCorrespondence( Id userId ) const {
    return userId > predefined::user::anonymous
      && id > predefined::user::anonymous
      && userId != id
      && userId != predefined::user::system;
  }

  void createAnonymous( sql::PhSession &sql );

  const std::locale &locale() const { return *theLocale; }
  void locale( const std::locale &locale ) { theLocale = &locale; }

private:
  /** Функция определяет, состоит ли пользователь членом группы из
      диапазона от -1 до -100 */
  bool bannedEverywhere() const {
    std::set<Id>::const_iterator i = groupsOfUser.lower_bound( 0 );
    if( i == groupsOfUser.begin())
      return false;
    return *(--i) >= predefined::group::minBannedEverywhere;
  }

  const std::locale *theLocale;
};

/** Menu */

struct MenuItem {
    enum Type { Simple = 1, SubMenu = 2, Separator = 3 };
    static const int typeMask = 3;

    enum Property { Hidden = 1 << 5, Disabled = 1 << 6 };

    bool isSimple()    const { return ( properties & typeMask ) == Simple; }
    bool isSubMenu()   const { return ( properties & typeMask ) == SubMenu; }
    bool isSeparator() const { return ( properties & typeMask ) == Separator; }
    bool isHidden()    const { return properties & Hidden; }
    bool isDisabled()  const { return properties & Disabled; }

    MenuItem( int id, int properties = Separator ) : id( id ), properties( properties ) { }

    MenuItem( int id, Str name, Str link, Str title,
      int properties = 0 ) : id( id ), name( name ), link( link ), title( title ),
      properties( ( properties & ~typeMask ) | Simple ) { }

    struct IdComp {
      bool operator() ( const MenuItem &left, const MenuItem &right ) const
      { return left.id < right.id; }
    };
    typedef std::set<MenuItem,MenuItem::IdComp> Menu;
    MenuItem( int id, Str name, Menu &subMenu, Str title = Str(),
      int properties = 0 ) : id( id ), name( name ), title( title ),
      properties( (properties & ~typeMask) | SubMenu ), subMenu( subMenu ) { }

    int id;
    Str name, link, title;
    int properties;
    Menu subMenu;
};

/** Timer */

struct Timer : timeval {
  Timer() { gettimeofday( this, 0 ); }
  unsigned diff() { // returns milliseconds
    Timer t1;
    return ( t1.tv_sec - tv_sec ) * 1000 + ( t1.tv_usec - tv_usec ) / 1000;
  }
  Str sDiff() { return n2s( diff()); }
};

class Request; class Response;

/** Cookie */

class Cookie {
public:
  Cookie( const Request &request, Response &response );
  void clear();
  void set( Id anId, const Str &aLocale );
  Id getId() const { return id; }
  void setCsrf();
  unsigned short getCsrfUS() const { return csrf; }
  Str getCsrf() const { return n2s( csrf ); }
  const Str &getLocale() const { return locale; }

  void set();

protected:
  const Request &request; Response &response;
  Id id; unsigned short csrf; Str locale;
  bool changed;
};

enum MsgBoxOptions { msgBoxYes = 1, msgBoxNo = 1 << 1, msgBoxOK = 1 << 2,
  msgBoxCancel = 1 << 3,   msgBoxIDoNotKnow = 1 << 4, msgBoxIDoNotKnowAsTo = 1 << 5,
  msgBoxQuestion = 1 << 6, msgBoxWarning = 1 << 7,    msgBoxInfo = 1 << 8,
  msgBoxError = 1 << 9,    msgBoxLinkForTo = 1 << 10,
  msgBoxLinkForFrom = 1 << 11 };

/** Base Page for all pages */

class Thread;
namespace dta { class Users2rise; }

enum NotificationWhat { notificationAnswer = 'A', notificationPrivate = 'P',
                        notificationQuote = 'Q' };

/** \brief Базовый класс для всех web-страниц.
  Любая страница (в наследованном классе) должна определить статический метод
  вида static const char *key(), возвращающий ключ, который является частью
  URL.
  \n Кроме того, должны быть переопределены виртуальные методы renderMain() или
  даже более общий метод render(), выводящие данные для создания страницы.
  \n Подготовка данных может выполняться в переопределенном методе load() и/или
  конструкторе.
  \n Для страниц, обрабатывающих запрос POST, с полученными данными в форматах
  "application/x-www-form-urlencoded" или "multipart/form-data" должны быть
  переопределены методы validate() и save(), а RPC-запрос в формате
  "application/json" обрабатывается в виртуальном методе jsonRpc().
  \n Алгоритм вызова этих методов задан в невиртуальных методах doGet() и
  doPost().
*/
class Page : public ParentOf<Form> {
public:
  /** \brief Создает страницу, возвращает указатель на созданный объект.
    Вполне может вернуть 0, сгенерировать исключение. */
  typedef Page *( *Demiurge )( const Thread & );

  const Request &request; Response &response;
  std::ostream &out;
  User user;
  Cookie cookie;
  mutable sql::PhSession sql;

  /** \brief Главный класс, связывающий web-страницу с ее URL.
    Web-адреса и соответствующие им создающие web-страницы функции вида
    Page::Demiurge складываются в #map.
    Диспетчеры организованы деревом, ключи дочерних и соответствующие им
    указатели складываются в #children.
  */
  class Dispatcher {
  public:
    /** \details \param key определяется частью URL, по которому диспетчер будет
      найден, для корневого диспетчера задается нулем.
      \param parent задается родительским (в дереве) диспетчером, для корневого
      задается самим собою (*this).
    */
    Dispatcher( const char *key, const Dispatcher &parent );
    /** \details Должен быть выполнен для корневого диспетчера перед началом
      работы, но когда сконструированы все дочерние диспетчеры. */
    void init();
    virtual ~Dispatcher();
    /** \details #theUrl создается из ключа #key и url() родительского диспетчера
      #parent. */
    Str url() const { return theUrl(); }
    Demiurge findDemiurge( const Thread &thread ) const;
    /** Вспомогательная функция.
      Для удобства добавления элементов в #map. */
    template<class T> static std::pair<Str,Demiurge> dispatch()
    { return std::pair<Str,Demiurge>( T::key(), create<T> ); }
  protected:
    /** Массив создающих web-страницы функций по ключам. */
    std::map<Str,Demiurge> map;
  private:
    Url theUrl;
    const char *const key;
    const Dispatcher &parent;
    /** Массив дочерних диспетчеров по ключам. */
    mutable std::map<Str,Dispatcher*> children;
  };

  Page( const Thread &thread );
  virtual Str title() const { return theTitle; }
  virtual Str description() const { return theDescription; }

  virtual void load() { /* load data from DB or ... */ }
  void doGet() /* load post data into forms and render  */;
  void doPost() /* load post data into forms, validate etc ...  */;
  virtual void createMenu();
  virtual bool validate();
  virtual bool save() { return false;
    /* save any data (from the posts) to DB or ... and redirect,
       if returns false, then will be called render() */ }

  virtual int jsonRpc( utl::Json query, utl::Json &answer ) { return -1; }

  static Str tr( const char *text, const std::locale &locale );
  Str tr( const char *text ) const { return tr( text, user.locale()); };
  Str t2s( const std::tm &tm, const char *format = "%c" ) const;
  Str t2s( const TimeT &t, const char *format = "%c" ) const;

  static Str xss( const Str &html );
  static Str markdown( const Str &html );
  Str bbcode( const Str &bbc, bool fullUrl = false ) const;

  /** creates identifier of the DOM element on the page */
  unsigned generateId() { return ++ lastId; }
  /** Создает нумерованного безымянного пользователя.
      Вносит его в базу данных и делает его текущим в сессии */
  void createAnonymous();
  /** Письмо посылает */
  void mail( Str to, Str subj, Str msg, Str html = Str());
  void sendSysMsgAboutNewUser( Id to, Id newUser );
  /** Создает личное сообщение */
  Id generateMessage( Id toId, Id topicId, Str title, Str abstract,
                      Str content, Str format, Id fromId );
  /** Создает извещения */
  void generateNotification( Id toId, Id messageId, bool doNotMail,
    dta::Users2rise &users2rise, NotificationWhat what, Str bbDecoded = Str());

  template<class T, class ...Ts> static Str urlOf( const Ts &...params )
  { return Url( T::dispatcher.url(), T::key())( params... ); }
  template<class T> static Page *create
  ( const Thread &thread ) { return new T( thread ); }
  static struct PageDispatcher : Dispatcher { PageDispatcher(); } dispatcher;

  void renderMsgBox( int options, Str msg, Str to, Str from = Str()) const;

  virtual ~Page() { }

protected:
  Page( const Page &page, const Url &url ); // "rewrite" constructor

  bool msgBox           () const;
  bool yesPressed       () const;
  bool noPressed        () const;
  bool oKPressed        () const;
  bool cancelPressed    () const;
  bool iDoNotKnowPressed() const;
  bool approvedPressed  () const;

  virtual void render();
  virtual void renderBody();
  virtual void renderMain();
  void renderSunflower() const;
  virtual void renderStyle() const;
  virtual void renderHeader() const;
  virtual void renderMenu() const;
  void renderLinkButton ( const Str &href, const Str &value, const Str &title ) const;
  void renderAvatar( Str url, const Str &name ) const;
  void renderCorrespondence( Id userId, short imgSize ) const;
  void renderRss( Id userId ) const;
  void renderOnlineIndicator( Id id ) const;
  void caught( const std::exception& e, const char * module, const char * file, int line ) const;
#define CAUGHT_EXCEPTION( e ) caught( e , __PRETTY_FUNCTION__,  __FILE__, __LINE__ )

  mutable Str error;
  enum MenuType { MenuNone, MenuBar, MenuSideBar, MenuPicture } menuType = MenuBar;
  Str theTitle, theDescription; const Url &url;
  bool changed /* user has changed the content */;
  MenuItem::Menu theMenu;
  Timer timer;

private:
  /** last identifier of the DOM element on the page */
  unsigned lastId = 0;
};

/**
  Class for HTTP status 304 "Not Modified ".
*/
class LastModified {
protected:
  std::time_t t;
public:
  LastModified() { std::time( &t ); }
  LastModified( std::time_t t0 ) { setTime( t0 ); }
  void setTime( std::time_t t0 ) { t = t0; }
  bool notModified( const Request &request, Response &response );
  bool notModified( Page& page );
};

} // x2

