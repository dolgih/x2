/**
    \file cache.h
    cache in memory classes

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

#ifndef NDEBUG
  #define IFDEBUG( expr ) expr
  #define IFDEBUGC( expr ) IFDEBUG( expr ),
  #define IFDEBUGS( expr ) IFDEBUG( expr );
#else
  #define IFDEBUG( expr )
  #define IFDEBUGC( expr )
  #define IFDEBUGS( expr )

#endif

#include "memSerial.h"

namespace x2 {

typedef list<Str> Triggers;

class CacheImpl;

extern class Cache {
public:
  Cache();
  void init();

  template<class T> void store( Str key, const T &serializable,
    const Triggers &triggers = Triggers())
  {
    Archive oa; oa << serializable; insert( IFDEBUGC( typeid( serializable )) key, oa.str(), triggers );
  }

  template<class T> bool fetch( Str key, T &serializable ) {
    Str data;
    if( ! get( key, data )) return false;
    Archive ia( data ); ia >> serializable;
    return true;
  }

  bool fetchWithVerifying( Str key, Serializable &serializable ) {
    Str data;
    if( ! get( key, data )) return false;
    Archive ia( data );
    return serializable.load( ia );
  }

  void rise( Str key );

  unsigned getMaxNumber () const;
  unsigned getFreeNumber() const;
  unsigned getMaxSum    () const;
  size_t   getFreeSum   () const;
  void getPicture( std::ostream &out ) const;

  void clear();

  ~Cache();

private:
  CacheImpl * impl;

  void insert( IFDEBUGC( const std::type_info &ti ) const Str &key, const Str &data, const Triggers &triggers );
  bool get( const Str &key, Str &data  );
} cache;

} // x2

