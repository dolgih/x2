/**
    \file memSerial.h
    serialization in mermory class

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 {

class Archive;
class Serializable {
protected:
  virtual void save( Archive &a ) const;
  virtual bool load( Archive &a );
  virtual void serialize( Archive &a ) { }
friend class Archive;
friend class Cache;
};

class Archive {
public:
  Archive(); /// output archive
  Archive( const Str &data ); /// input archive

  const Str &str() const { return data; }

  Archive &operator << ( const Serializable &serial ) { serial.save( *this ); return *this; }
  Archive &operator >> ( Serializable &serial ) { serial.load( *this ); return *this; }

  Archive &operator << ( const Str &val );

  template<class T> Archive &operator << ( const list<T> &val ) {
    out( val.size()); for( const T &v: val ) out( v ); return *this;
  }

  template<class K, class V> Archive &operator << ( const std::map<K,V> &val ) {
    out( val.size());
    for( const auto &v: val ) { out( v.first ); out( v.second ); }
    return *this;
  }

#define operatorOut( type ) Archive &operator << ( type val ) { return out( val ); }
  operatorOut( char )      operatorOut( unsigned char )
  operatorOut( short )     operatorOut( unsigned short )
  operatorOut( int )       operatorOut( unsigned int )
  operatorOut( long )      operatorOut( unsigned long )
  operatorOut( long long ) operatorOut( unsigned long long )
  operatorOut( float )
  operatorOut( double )
  operatorOut( bool )
#undef operatorOut

  Archive &operator >> ( Str &val );

  template<class T> Archive &operator >> ( list<T> &val ) {
    typename list<T>::size_type s, i; typename list<T>::value_type v;
    val.clear();
    in( s ); for( i = 0; i < s; i ++ ) { in( v ); val.push_back( v ); }
    return *this;
  }

  template<class K, class V> Archive &operator >> ( std::map<K,V> &val ) {
    typename std::map<K,V>::size_type s, i; K k; V v;
    val.clear();
    in( s ); for( i = 0; i < s; i ++ ) { in( k ); in( v ); val[ k ] = v; }
    return *this;
  }

#define operatorIn( type ) Archive &operator >> ( type &val ) { return in( val ); }
  operatorIn( char )       operatorIn( unsigned char )
  operatorIn( short )      operatorIn( unsigned short )
  operatorIn( int )        operatorIn( unsigned int )
  operatorIn( long )       operatorIn( unsigned long )
  operatorIn( long long )  operatorIn( unsigned long long )
  operatorIn( float )
  operatorIn( double )
  operatorIn( bool )
#undef operatorIn

  template <class T>
  Archive &operator % ( T &val )
  { if( mode == Output ) return operator << ( val ); else return operator >> ( val ); }

private:
  Str data;
  size_t cursor;
  enum { Input, Output } mode;

  template <class T> Archive &out( T val )
  { data += Str( reinterpret_cast<const char *>( &val ), sizeof val ); return *this; }
  template <class T> Archive &in( T &val ) {
    val = * reinterpret_cast<const T *>( data.data() + cursor );
    cursor += sizeof val;
    IFDEBUG( if( cursor > data.size()) throw std::runtime_error( inError ));
    return *this;
  }
  static const char inError[];
};

} // x2

