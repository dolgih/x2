/**
    \file cache.cpp
    cache in memory classes

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

class CacheObj;
typedef std::set<CacheObj> CacheSet;
typedef list<CacheSet::iterator> CacheList;

class TriggerObj;
typedef std::set<TriggerObj> TriggerSet;

class CacheObj {
public:
  CacheObj( IFDEBUGC( const std::type_info &ti ) const Str &key, const Str &data )
  : IFDEBUGC( ti( ti )) key( key ), data( data ) { }
  CacheObj( const Str &key ) : IFDEBUGC( ti( typeid( 0 ))) key( key ) { }
  bool operator < ( const CacheObj &right ) const { return key < right.key; }
  IFDEBUGS( const std::type_info &ti )
  const Str key;
private:
friend class CacheImpl;
  mutable Str data;
  struct TriggersCompare { bool operator()
    ( const TriggerSet::iterator &left, const TriggerSet::iterator &right ) const; };
  mutable std::set<TriggerSet::iterator, TriggersCompare> triggers;
  mutable CacheList::iterator listIt;
};

class TriggerObj {
public:
  TriggerObj( const Str &key ) : key( key ) { }
  bool operator < ( const TriggerObj &right ) const { return key < right.key; }
  const Str key;
private:
friend class CacheImpl;
  struct DepsCompare { bool operator()( const CacheSet::iterator &left,
    const CacheSet::iterator &right ) const { return left->key < right->key; } };
  mutable std::set<CacheSet::iterator, DepsCompare> dependents;
};

bool CacheObj::TriggersCompare::operator()
  ( const TriggerSet::iterator &left, const TriggerSet::iterator &right ) const
{ return left->key < right->key; }

// end of common declarations

// begin of CacheImpl

class CacheImpl {
public:
  CacheImpl() : maxSum( size_t( cfg.cache.maxSum ) << 20 /* bytes from megabytes */ ),
                sum( 0 )
  { }

#ifndef NDEBUG
  struct Stat { /* Сбор статистики для целей отладки и наблюдения */
    unsigned
             i  /* Количество вызовов insert */,
             in /* Количество созданных в insert */,
             it /* Количество вставленных триггеров */,
             g  /* Количество запросов get */,
             gf /* Количество элементов, найденных в кэши */,
             r  /* Количество вызовов rise */,
             rk /* Количество удаленных в rise по ключю */,
             rt /* Количество удаленных в rise по триггерам */;
    Stat() : i( 0 ), in( 0 ), it( 0 ), g( 0 ), gf( 0 ), r( 0 ), rk( 0 ), rt( 0 ) { }
    Str get( size_t cnt, size_t tCnt, size_t sum ) const {
      Str out; out.resize( 1024 );
      snprintf( const_cast<char *>( out.data()), out.size(), "Stat i=%u, in=%u,"
        " it=%u, g=%u, gf=%u, r=%u, rk=%u, rt=%u, cnt=%u, tCnt=%u, maxCnt=%u,"
        " sum=%lu, maxSum=%uMB",
        i, in, it, g, gf, r, rk, rt, ( unsigned ) cnt, ( unsigned ) tCnt,
        cfg.cache.maxNumber, ( unsigned long ) sum, cfg.cache.maxSum );
      return out.data();
    }
  } stat;
#endif
  void insert( IFDEBUGC( const std::type_info &ti ) const Str &key, const Str &data, const Triggers &triggers ) {
    std::lock_guard<std::mutex> guard( mutex );
    size_t size = data.size();
    if( size > maxSum ) {
      LOG_ERROR( "data size too big" );
      return;
    }
#ifndef NDEBUG
    stat.i ++;
#endif
    if( cacheList.size() >= cfg.cache.maxNumber )
      freeLast();
    sum += size;
    while( sum > maxSum )
      freeLast();
    auto insertedResult = cacheSet.emplace( IFDEBUGC( ti ) key, data );
    auto inserted = insertedResult.first;
    if( insertedResult.second )  { // new element created
#ifndef NDEBUG
      stat.in ++;
#endif
      cacheList.emplace_front( inserted );
      inserted->listIt = cacheList.begin();
    } else {
      sum -= inserted->data.size();
      inserted->data = data;
      inserted->triggers.clear();
    }
    auto &triggersInObj = inserted->triggers;
    for( const Str &triggerKey: triggers ) {
      if( triggerKey == key ) break;
#ifndef NDEBUG
      stat.it ++;
#endif
      auto insertedTrigger = triggerSet.emplace( triggerKey ).first;
      insertedTrigger->dependents.emplace( inserted );
      triggersInObj.emplace( insertedTrigger );
    }
  }

  bool get( const Str &key, Str &data ) {
    std::lock_guard<std::mutex> guard( mutex );
#ifndef NDEBUG
      stat.g ++;
#endif
    CacheSet::iterator found = cacheSet.find( key );
    if( found == cacheSet.end()) return false;
#ifndef NDEBUG
      stat.gf ++;
#endif
    data = found->data;
    cacheList.splice( cacheList.begin(), cacheList, found->listIt );
    return true;
  }

  void rise( const Str &key ) {
    std::lock_guard<std::mutex> guard( mutex );
#ifndef NDEBUG
      stat.r ++;
#endif
    { // object
      auto found = cacheSet.find( key );
      if( found != cacheSet.end()) {
#ifndef NDEBUG
        stat.rk ++;
#endif
        eraseObj( found );
      }
    }
    { // trigger
      auto found = triggerSet.find( key );
      if( found == triggerSet.end()) return;
      const auto dependents = found->dependents;
      for( CacheSet::iterator o: dependents ) {
#ifndef NDEBUG
        stat.rt ++;
#endif
        eraseObj( o );
      }
    }
  }

  unsigned getMaxNumber () const { return cfg.cache.maxNumber; }
  unsigned getFreeNumber() const { return getMaxNumber() - cacheList.size(); }
  unsigned getMaxSum    () const { return cfg.cache.maxSum; }
  size_t   getFreeSum   () const { return maxSum - sum; }
  void getPicture( std::ostream &out ) {
    out << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
           "<cache>\n";
#ifndef NDEBUG
    out << "<debug>\n";
    out << stat.get( cacheSet.size(), triggerSet.size(), sum ) << "\n</debug>\n";
#endif
    std::lock_guard<std::mutex> guard( mutex );
    out  << "<Objects>\n";
    for( const auto &i: cacheList ) {
      out << "<object key=\"" << utl::htEsc( i->key ) << "\" size=\""
          << i->data.size() << "\">\n"
    IFDEBUG( "  <class>" << utl::htEsc( utl::getClassName( i->ti )) << "</class>\n" )
             "  <triggers>\n";
      for( const auto &j: i->triggers )
        out << "    <trigger>" << j->key << "</trigger>\n";
      out << "  </triggers>\n";
      out << "</object>\n";
    }
    out << "</Objects>\n"
           "<Triggers>\n";
    for( const auto &i: triggerSet ) {
      out << "<trigger key=\"" << utl::htEsc( i.key ) << "\">\n"
             "  <dependents>\n";
      for( const auto &j: i.dependents )
        out << "    <dependent>" << j->key << "</dependent>\n";
      out << "  </dependents>\n"
             "</trigger>\n";
    }
    out << "</Triggers>\n"
           "</cache>\n";
}

  void clear() {
    std::lock_guard<std::mutex> guard( mutex );
    cacheSet.clear();
    cacheList.clear();
    triggerSet.clear();
    sum = 0;
  }

  ~CacheImpl() {
#ifndef NDEBUG
    if( ! cfg.logging.cacheCont.empty()) {
      std::ofstream out( cfg.logging.cacheCont );
      out << stat.get( cacheSet.size(), triggerSet.size(), sum ) << "\nObjects:";
      for( const auto &i: cacheList ) {
        out << "\nobject=" << i->key << "; class='" << utl::getClassName( i->ti ) << "'; triggers:";
        for( const auto &j: i->triggers ) {
          out << " '" << j->key << "'";
        }
      }
      out << "\nTriggers:";
      for( const auto &i: triggerSet ) {
        out << "\ntrigger='" << i.key << "'; dependents:";
        for( const auto &j: i.dependents ) {
          out << " '" << j->key << "'";
        }
      }
    }
#endif
    // additional control
    if( cacheList.size() != cacheSet.size())
      throw std::runtime_error( "Cache: cacheList.size() != cacheMap.size()" );
    size_t controlSum = 0; for( const auto &i: cacheSet ) controlSum += i.data.size();
    if( controlSum != sum )
      throw std::runtime_error( "Cache: sum != controlSum" );
  }

private:
  CacheSet     cacheSet;
  CacheList    cacheList;
  TriggerSet   triggerSet;
  std::mutex   mutex;
  const size_t maxSum;
  size_t       sum;

  void eraseObj( CacheSet::iterator it ) {
    sum -= it->data.size();
    for( const auto trigger: it->triggers ) {
      trigger->dependents.erase( it );
      if( trigger->dependents.empty())
        triggerSet.erase( trigger );
    }
    cacheList.erase( it->listIt );
    cacheSet.erase( it );
  }

  void freeLast( ) {
    for( short i = 0; i < 5; i ++ ) {
      auto last = cacheList.end();
      if( cacheList.begin() == last ) return;
      eraseObj( * ( -- last ));
    }
  }
};

// end of CacheImpl

Cache::Cache() : impl( 0 ) { }

void Cache::init() { impl = new CacheImpl(); }

void Cache::insert( IFDEBUGC( const std::type_info &ti ) const Str &key, const Str &data, const Triggers &triggers ) {
  try {
    if( key.size() > cfg.cache.maxKeySize )
      throw( std::runtime_error( utl::f( "cache: key size of '{1}' exceeds"
        " maxKeySize={2}", key, cfg.cache.maxKeySize )));
    impl->insert( IFDEBUGC( ti ) key, data, triggers );
  } catch ( std::exception &e ) {
    LOG_ERROR( e.what());
  }
}

bool Cache::get( const Str &key, Str &data  ) {
  try {
    return impl->get( key.data(), data );
  } catch ( std::exception &e ) {
    LOG_ERROR( e.what());
  }
  return false;
}

void Cache::rise( Str key ) {
  try {
    impl->rise( key.data());
  } catch ( std::exception &e ) {
    LOG_ERROR( e.what());
  }
}

unsigned Cache::getMaxNumber () const { return impl->getMaxNumber(); }
unsigned Cache::getFreeNumber() const { return impl->getFreeNumber(); }
unsigned Cache::getMaxSum    () const { return impl->getMaxSum(); }
size_t   Cache::getFreeSum   () const { return impl->getFreeSum(); }
void Cache::getPicture( std::ostream &out ) const { impl->getPicture( out ); }

void Cache::clear() { impl->clear(); }

Cache::~Cache() {
  delete impl;
}

Cache cache;

} // x2

