/**
    \file memSerial.cpp
    serialization in mermory class

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Serializable

void Serializable::save( Archive &a ) const
{ ( ( Serializable * ) this )->serialize( a ); }

bool Serializable::load( Archive &a )
{ serialize( a ); return true; }

// Archive

Archive::Archive() : cursor( 0 ), mode( Output ) { }

Archive::Archive( const Str &data ) : data( data ), cursor( 0 ), mode( Input ) { }

Archive &Archive::operator << ( const Str &val ) {
  operator << ( val.size());
  data += val;
  return *this;
}

Archive &Archive::operator >> ( Str &val ) {
  size_t sz; operator >> ( sz );
  val.assign( data.data() + cursor, sz );
  cursor += sz;
  return *this;
}

IFDEBUG( const char Archive::inError[] = "Archive::in: extra reading" );

} // x2

