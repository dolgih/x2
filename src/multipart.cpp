/**
    \file multipart.cpp
    \brief multipart/form-data parsing

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.

    Large part of this file is taken from "CppCMS -- C++ Web Development Framework"
    of Artyom Beilis (Tonkikh) <artyomtnk@yahoo.com>.
*/

#include "x2.h"

namespace x2 {

namespace multipart {

/**
static std::set<char> separators = {
  '(', ')', '<', '>', '@', ',', ';', ':', '\\', '"',
  '/', '[', ']', '?', '=', '{', '}', ' ', '\t'
};

inline bool separator( char c ) { return separators.find( c ) != separators.end(); }
*/

struct ContentType {
  Str type, subtype, media_type;
  std::map<Str,Str> parameters;
};

struct Field { Str name, filename, contentType; };

} // multipart

namespace Beilis {

inline bool separator(char c) {
    switch(c) {
    case '(':
    case ')':
    case '<':
    case '>':
    case '@':
    case ',':
    case ';':
    case ':':
    case '\\':
    case '"':
    case '/':
    case '[':
    case ']':
    case '?':
    case '=':
    case '{':
    case '}':
    case ' ':
    case '\t':
        return true;
    default:
        return false;
    }
}

template<typename It> It skipWs(It p,It end) {
    while(p<end) {
        switch(*p) {
        case '\r': // Check for LWS (CRLF 1*( SP | HT))
            if(p+2 < end && *(p+1)=='\n' && (*(p+2)==' ' || *(p+2)=='\t')) {
                p+=2;
                break;
            }
            return p;
        case ' ':
        case '\t':
            break;
        default:
            return p;
        }
        p++;
    }
    return p;
}

template<typename It> It tocken(It begin,It end) {
    char c;
    while(begin < end && 0x20<=(c=*begin) && c<=0x7E && !separator(c))
        begin++;
    return begin;
}

template<typename It> Str unquote(It &begin,It end) {
    It p=begin;
    Str result;
    if(p>=end || *p!='\"')
        return result;
    result.reserve(end-p);
    p++;
    while(p < end) {
        char c=*p++;
        if(c=='\"') {
            begin=p;
            return result;
        }
        else if(c=='\\' && p<end)
            result+= *p++;
        else
            result+=c;
    }
    result.clear();
    return result;
}

void parseContentType(char const *begin,char const *end,multipart::ContentType &contentType) {
    begin = skipWs(begin,end);
    if(begin == end)
        return;
    char const *type_end = tocken(begin,end);
    if(type_end == begin)
        return;
    Str type(begin,type_end);
    begin = type_end;
    if(begin == end || *begin++ != '/')
        return;
    type_end = tocken(begin,end);
    if(type_end == begin)
        return;
    Str subtype(begin,type_end);
    begin = type_end;
    std::transform(type.begin(),type.end(),type.begin(),tolower);
    std::transform(subtype.begin(),subtype.end(),subtype.begin(),tolower);
    contentType.type = type;
    contentType.subtype = subtype;
    contentType.media_type = type + '/' + subtype;
    while(begin!=end) {
        begin = skipWs(begin,end);
        if(begin == end || *begin++ != ';' || (begin=skipWs(begin,end))==end)
            return;
        char const *param_end = tocken(begin,end);
        if(param_end == begin)
            return;
        Str param(begin,param_end);
        std::transform(param.begin(),param.end(),param.begin(),tolower);
        begin = skipWs(param_end,end);
        if(begin==end || *begin++!='=')
            return;
        begin = skipWs(begin,end);
        if(begin==end)
            return;
        Str value;
        if(*begin == '"') {
            char const *tmp = begin;
            value = unquote(tmp,end);
            if(tmp == begin)
                return;
            begin = tmp;
        }
        else {
            char const *tmp = tocken(begin,end);
            if(tmp == begin)
                return;
            value.assign(begin,tmp);
            begin = tmp;
        }
        contentType.parameters.insert(std::make_pair(param,value));
    }
}

bool parse_pair( Str::const_iterator &p, Str::const_iterator e, Str &name, Str &value ) {
    if(p==e)
        return false;
    if(*p!=';')
        return false;
    p=skipWs(p+1,e);
    if(p==e)
        return false;
    Str::const_iterator start = p;
    Str::const_iterator end = tocken(p,e);
    if(start==end)
        return false;
    if(end==e)
        return false;
    if(*end!='=')
        return false;
    name.assign(start,end);
    p=skipWs(end+1,e);
    if(p==e)
        return false;
    if(*p=='"') {
        Str::const_iterator tmp=p;
        value=unquote(tmp,e);
        if(p==tmp)
            return false;
        p=tmp;
        return true;
    }
    else {
        Str::const_iterator tmp=tocken(p,e);
        if(tmp==p)
            return false;
        value.assign(p,tmp);
        tmp=p;
        return true;
    }
};

bool parseHeader( Str const &hdr, multipart::Field &field ) {

  auto parse_content_disposition = [ &field ] (Str::const_iterator p,Str::const_iterator e) -> bool {

    p=skipWs(p,e);
    while(p!=e) {
        Str name,value;
        if(!parse_pair(p,e,name,value))
            return false;
        for(unsigned i=0;i<name.size();i++)
            name[i]=tolower(name[i]);
        if(name=="filename")
            field.filename = value;
        else if(name=="name")
            field.name = value;
        p=skipWs(p,e);
    }
    return true;
  };

  size_t pos = 0;
  while(pos < hdr.size()) {
      size_t next = hdr.find("\r\n",pos);
      if(next==Str::npos) {
          return false;
      }
      if(next == pos) {
          return true;
      }
      Str one_header=hdr.substr(pos,next-pos);
      pos=next+2;
      Str::iterator p=one_header.begin(),e=one_header.end();
      p=skipWs(p,e);
      Str::iterator start=p;
      Str::iterator end=tocken(p,e);
      Str header_name(start,end);
      p=skipWs(end,e);
      if(p==e || *p!=':')
          return false;
      p=skipWs(p+1,e);

      if(strcasecmp(header_name.data(),"Content-Disposition")==0) {
          start = p;
          end=tocken(p,e);
          if(strcasecmp(Str(start,end).data(),"form-data")!=0)
              return false;
          p=end;
          if(!parse_content_disposition(p,e))
              return false;
      }
      else if(strcasecmp(header_name.data(),"Content-Type")==0) {
          multipart::ContentType contentType;
          parseContentType(&*p,&*e,contentType);
          field.contentType = contentType.media_type;
      }
  }
  return false;

};

} // Beilis

bool Request::parseFormMultipart( Str boundary ) {

  struct ParseFormMultipartError : ServerError {
    ParseFormMultipartError( const Str &msg )
      : ServerError( 400, "parseFormMultipart: " + msg ) { }
  };

  { // get the boundary value
    Str::const_iterator b = boundary.cbegin(), e = boundary.cend();
    for( Str name, value; ; ) {
      b = Beilis::skipWs( b, e );
      if( ! Beilis::parse_pair( b, e, name, value ))
        throw ParseFormMultipartError( "cannot parse_pair()" );
      if( strcasecmp( name.data(), "boundary" ))
        continue;
      if( ( boundary = value ).empty())
        throw ParseFormMultipartError( "empty boundary" );
      break;
    }
  }

  Str input; get( input );
  if( input.empty())
    throw ParseFormMultipartError( "empty input" );

  size_t curr = 0;
  size_t endPos;
  if( ( endPos = input.find( "--" + boundary, curr )) == Str::npos )
    throw ParseFormMultipartError( "boundary not found" );
  curr = endPos + 2 + boundary.size();
  for(;;) {
    Str suff = input.substr( curr, 2 );
    if( suff == "--" )
      return true;
    if( suff != "\r\n" )
      throw ParseFormMultipartError( "bad boundary" );
    curr += 2;

    size_t eohPos = input.find( Str( "\r\n\r\n" ), curr );
    Str hdrs;
    if( eohPos == Str::npos )
      throw ParseFormMultipartError( "bad headers" );
    hdrs = input.substr( curr, eohPos + 4 - curr );
    curr += hdrs.size();
    multipart::Field field;
    if( ! Beilis::parseHeader( hdrs, field ))
      throw ParseFormMultipartError( "can not parseHeader()" );

    if( ( endPos = input.find( "\r\n--" + boundary, curr )) == Str::npos
      || field.name.empty()
    )
      throw ParseFormMultipartError( "name not found or bad boundary" );
    posts.insert( std::pair<Str,Str>( field.name, input.substr( curr, endPos - curr )));
    if( ! field.filename.empty() || ! field.contentType.empty())
      files[ field.name ] = { field.filename, field.contentType };
    curr = endPos + 4 + boundary.size();
  }
}

} // x2

