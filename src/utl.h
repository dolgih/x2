/**
    \file utl.h
    Various utilities. See also utl.C

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

using boost::container::list;

typedef std::string Str;
typedef long Id; const Id Id_MIN = LONG_MIN;
typedef std::vector<bool> Bools;

namespace x2 {
namespace utl {

/** Converts value to Str. */
template<class T, int maxSize = 32> inline Str n2s( T val, const char *format ) {
  char out[ maxSize ]; snprintf( out, sizeof out, format, val ); return out;
}

/** Converts unsigned to Str. */
inline Str n2s( unsigned val ) { return n2s( val, "%u" ); }
/** Converts signed to Str. */
inline Str n2s( int val ) { return n2s( val, "%i" ); }
/** Converts unsigned long to Str. */
inline Str n2s( unsigned long val ) { return n2s( val, "%lu" ); }
/** Converts long to Str. */
inline Str n2s( long val ) { return n2s( val, "%li" ); }
/** Converts unsigned long long to Str. */
inline Str n2s( unsigned long long val ) { return n2s( val, "%llu" ); }
/** Converts long long to Str. */
inline Str n2s( long long val ) { return n2s( val, "%lli" ); }

/** Converts Str to int. */
inline int s2i( const Str &s ) { return atoi( s.data()); }
/** Converts Str to long. */
inline long s2l( const Str &s ) { return atol( s.data()); }
/** Converts Str to unsigned long. */
inline unsigned long s2ul( const Str &s ) { return strtoul( s.data(), 0, 10 ); }

/** std::time_t improved  */
struct TimeT {
  std::time_t t;
  TimeT() : t( std::time_t()) { }
  TimeT( std::time_t t ) : t( t ) { }
  TimeT( std::tm tm );
  operator std::time_t() const { return t; }
  operator std::tm() const;
  std::tm gmt() const;
};

/** Converts std::tm to Str or stream using strftime format. */
struct T2s {
  T2s( const std::tm &time, const Str &format = "%c" )
    : time( time ), format( format ) { }
  std::ostream &out( std::ostream &stream ) const;
  Str str( const std::locale &locale = std::locale());

  const std::tm time;
  const Str format;
};

inline std::ostream &operator << ( std::ostream &stream, const T2s &t2s ) {
  return t2s.out( stream );
}

/** Converts std::tm to Str using std::locale. */
Str t2s( const std::tm &tm, const char *format = "%F %T", const std::locale &loc = std::locale());
Str t2s( const TimeT &t, const char *format = "%F %T", const std::locale &loc = std::locale());

/** Converts Str to Id. */ inline Id s2id( Str s ) { return utl::s2l( s ); }
/** Converts Id to Str. */ inline Str id2s( Id id ) { return utl::n2s( id ); }

/* Converts all to Str. */
inline Str str( const std::tm &tm, const char *format = "%F %T" )
{ return t2s( tm, format ); }
inline Str str( const TimeT &timeT, const char *format = "%F %T" )
{ return t2s( timeT, format ); }
inline const Str &str( const Str &val ) { return val; }
inline Str str( const char *val ) { return val; }
template<class T> inline Str str( T val ) { return n2s( val ); }

/** Format string */
class F { public:
  F( const Str &fmt ) : theFormat( fmt ) { }
  F &operator()( const Str &v )          { store[ store.size() ] = v; return *this; }
  F &operator()( const char *v )         { store[ store.size() ] = v; return *this; }
  template<class T> F &operator()( T v ) { store[ store.size() ] = n2s( v ); return *this; }
  template<class T> F &operator%( const T &v ) { return operator()( v ); }
  operator Str() const;
  Str str() const { return operator Str(); }
protected:
  Str theFormat;
  std::map<unsigned char,Str> store;
};
template<typename ...Ts> Str f( const char *fmt, const Ts &...args ) {
  F o( fmt );
  int dummy[ sizeof...(Ts) + 1 ] = { ( o( args ), 0 )... }; dummy[ 0 ] ++;
  return o;
};
template<typename ...Ts> Str f( Str fmt, const Ts &...args ) {
  return f( fmt.data(), args... );
};

inline std::ostream &operator << ( std::ostream &stream, const F &f ) {
  return stream << (Str)f;
}

/** Format string for postgresql query */
class Pf : public F { public:
  using F::F;
  Pf &operator()( const Str &v );
  Pf &operator()( const char *v ) { return operator()( Str( v )); }
  Pf &operator()( char c ) { return operator()( Str( 1, c )); }
  Pf &operator()( const std::tm &v );
  Pf &operator()( const TimeT &t ) { return operator()( (std::tm)t ); }
  template<class T> Pf &operator()( T v ) { store[ store.size() ] = n2s( v ); return *this; }
};
template<typename ...Ts> Str pf( const char *fmt, const Ts &...args ) {
  Pf o( fmt );
  int dummy[ sizeof...(Ts) + 1 ] = { ( o( args ), 0 )... }; dummy[ 0 ] ++;
  return o;
};
template<typename ...Ts> Str pf( Str fmt, const Ts &...args ) {
  return pf( fmt.data(), args... );
};

/**
  Returns a new string in which all occurrences of a specified string
  in the current instance are replaced with another specified string.
*/
Str replace( Str s, Str pattern, Str repl );

/** Removes spaces from start. */
inline Str ltrim( Str s ) {
  s.erase( s.begin(), std::find_if( s.begin(), s.end(),
      std::not1( std::ptr_fun<int, int>( std::isspace ))));
  return s;
}

/** Removes spaces from end. */
inline Str rtrim( Str s ) {
  s.erase( std::find_if( s.rbegin(), s.rend(),
      std::not1( std::ptr_fun<int, int>( std::isspace ))).base(), s.end());
  return s;
}

/** Removes spaces from both ends. */
inline Str trim( Str s ) { return ltrim( rtrim( s )); }

/** html encoding */
Str htEsc( Str data );
/** То же что и htEsc(Str), но не обрабатывающая данные, которые
    уже обработаны ранее функцией htEsc(Str) или htEsc2(Str). */
Str htEsc2( Str data );


/* Logging */
enum LogLevel { LogInvalid, LogDebug, LogInfo, LogWarn, LogError, LogFatal };
void log( LogLevel level, Str msg, const char *func, const char *file, int line );
#define LOG_DEBUG( m )  log( x2::utl::LogDebug, m, __PRETTY_FUNCTION__,  __FILE__, __LINE__ )
#define LOG_INFO( m )   log( x2::utl::LogInfo, m, __PRETTY_FUNCTION__, __FILE__, __LINE__ )
#define LOG_WARN( m )   log( x2::utl::LogWarn, m, __PRETTY_FUNCTION__, __FILE__, __LINE__ )
#define LOG_ERROR( m )  log( x2::utl::LogError, m, __PRETTY_FUNCTION__, __FILE__, __LINE__ )
#define LOG_FATAL( m )  log( x2::utl::LogFatal, m, __PRETTY_FUNCTION__, __FILE__, __LINE__ )

/** Gets sha1 hash. */
Str sha1( Str s );

/** Gets executable name. */
extern class SelfName { public:
  SelfName();
  operator const Str& () const { return theName; }
private:
  Str theName;
} selfName;

/** Gets class name. */
Str getClassName( const std::type_info &ti );
template<class T> Str getClassName( const T &var ) { return getClassName( typeid( var )); }

// date & time

/** Returns a date formatted as HTTP-date, datetime in RFC2616 format */
Str htdate( std::tm *tm );
/** Returns a date formatted as HTTP-date, datetime in RFC2616 format */
Str htdate( std::time_t t );

/** date regex. */
const char *dateRegex();
const char *dateFormat();

// Json

class Json;
class JsonIt : json_object_iterator {
public:
  JsonIt( const json_object_iterator &from );
  bool operator == ( const JsonIt &right );
  bool operator != ( const JsonIt &right ) { return ! operator == ( right ); }
  JsonIt &operator ++ ();
  struct Pair { const char *name() { return n; }; const char *n; Json &val(); json_object *o; };
  Pair operator*();
};

class Json {
public:
  Json() : o( 0 ) { }
  Json( const Json &other );
  const Json &operator = ( const Json &other );
  Json &parse( const Str &str );
  Json( bool );
  Json( int );
  Json( long );
  Json( double );
  Json( const char * );
  Json( const Str & );
  static Json createObj();
  static Json createArray();
  Json( const JsonIt::Pair &pair );
  bool empty() const;
  operator bool() const;
  operator double() const;
  operator int() const { return operator long(); }
  operator long() const;
  operator Str() const;
  Json operator[] ( const char *key ) const;
  Json operator[] ( const Str &key ) const { return operator[]( key.data()); }
  Str str( bool pretty = false ) const;
  Json &add( const Str &key, const Json &el );
  Json &del( const Str &key );
  Json &add( const Json &el );
  Json operator[] ( int idx ) const;
  int arraySize() const;

  JsonIt begin();
  JsonIt end();

  ~Json();
private:
  json_object *o;
};

} // utl

using utl::n2s;
using utl::TimeT;

} // x2

