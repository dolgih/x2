/**
    \file msgBox.cpp
    interface page for "Yes", "No", "OK" and so on questions.

    This file is a part of x1 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/


#include "x2.h"

namespace x2 {

struct MsgBox : Page {
  Url url; int options; Str msg, to, from;
  MsgBox( const Page &page,  int options, Str msg, Str to, Str from  )
    : Page( page, url ), options( options ), msg( msg ), to( to ), from( from )
  { menuType = MenuSideBar; }
  void renderMain() override;
};

static const char  msgBoxId[] = "msgBox",
                  yesButton[] = "YesButton",
                   noButton[] = "NoButton",
                   oKButton[] = "OKButton",
               cancelButton[] = "CancelButton",
           iDoNotKnowButton[] = "IDoNotKnowButton";

struct msgBoxForm : Form {
  struct Subm : Button {
    Subm( Form &form, const Str &value, const char *name, Str color = "Blue" )
      : Button( form, value, name, name )
    {
      cssClass( "msgBoxButton" ); style( "color:" + color + ';' ); title( value );
    }
  } yes, no, OK, cancel, IDoNotKnow;
  msgBoxForm( const Page &page ) : Form( page ),
    yes       ( *this, tr("Yes"),           yesButton ),
    no        ( *this, tr("No"),            noButton,         "Red" ),
    OK        ( *this, tr("OK"),            oKButton ),
    cancel    ( *this, tr("Cancel"),        cancelButton,     "Red" ),
    IDoNotKnow( *this, tr("I Do Not Know"), iDoNotKnowButton, "Fuchsia" )
  { }
};

void MsgBox::renderMain() {

  msgBoxForm form( *this );

  out << R"(<article>
<style scoped>
article {
  width: 87%;
  font-size: 150%;
  text-align:center;
  border: 3px solid DeepPink;
  padding: 1%;
}
.msgBoxButton {
    color: Blue;
    font-size: 150%;
}
</style>
<h2>)";
  if( options & msgBoxQuestion )
    out << Img( cfg.media + "/question.png", "question" ) << tr("Question") << '.';
  if( options & msgBoxWarning )
    out << Img( cfg.media + "/warning.png", "warning" ) << tr("Warning") << '!';
  if( options & msgBoxInfo )
    out << Img( cfg.media + "/info.png", "information" ) << tr("Information") << '!';
  if( options & msgBoxError )
    out << Img( cfg.media + "/error.png", "error" ) << tr("Error") << '!';
  out << "</h2>\n"
  << msg << "<br><br><p>" << tr("Push the Button!") << "</p>\n";

  bool yesBefore  = options & msgBoxYes,        yesAt = yesBefore,
       oKBefore   = options & msgBoxOK,         oKAt  = oKBefore,
       noAfter    = options & msgBoxNo,         noAt  = noAfter,
       cncAfter   = options & msgBoxCancel,     cncAt = cncAfter,
       iDNBefore  = options & msgBoxIDoNotKnow, iDNAt = iDNBefore, iDNAfter = iDNBefore;

  yesBefore = yesBefore && ( options & msgBoxLinkForTo );
  oKBefore  = oKBefore  && ( options & msgBoxLinkForTo );
  iDNBefore = iDNBefore && ( options & msgBoxLinkForTo ) && ( options & msgBoxIDoNotKnowAsTo );

  noAfter  = noAfter  && ( options & msgBoxLinkForFrom );
  cncAfter = cncAfter && ( options & msgBoxLinkForFrom );
  iDNAfter = iDNAfter && ( options & msgBoxLinkForFrom ) && ! ( options & msgBoxIDoNotKnowAsTo );

  yesAt = yesAt && ! yesBefore;
  oKAt  = oKAt  && ! oKBefore;
  noAt  = noAt  && ! noAfter;
  cncAt = cncAt && ! cncAfter;
  iDNAt = iDNAt && ! iDNBefore && ! iDNAfter;

  if( yesBefore ) out << Link( to, form.yes.html(),        Str());
  if( oKBefore  ) out << Link( to, form.OK.html(),         Str());
  if( iDNBefore ) out << Link( to, form.IDoNotKnow.html(), Str());

  out << R"(<form method="post" style="display:inline;">)" << form.csrf <<
  "<input type=\"hidden\" name=\"" << msgBoxId << "\" value=\"" << msgBoxId << "\">\n";
  if( yesAt ) out << form.yes       .formaction( to );
  if( oKAt )  out << form.OK        .formaction( to );
  if( noAt )  out << form.no        .formaction( from );
  if( cncAt ) out << form.cancel    .formaction( from );
  if( iDNAt ) out << form.IDoNotKnow.formaction(
    ( options & msgBoxIDoNotKnowAsTo ) ? to : from );
  out << "</form>\n";

  if( noAfter )  out << Link( from, form.no.html(),         Str());
  if( cncAfter ) out << Link( from, form.cancel.html(),     Str());
  if( iDNAfter ) out << Link( from, form.IDoNotKnow.html(), Str());

  out << "</article>\n";
  renderSunflower();
  out << "<br>GCC )" __VERSION__ "<br>buid time: " __DATE__ " " __TIME__ "<br>\n";
}

void Page::renderMsgBox( int options, Str msg, Str to, Str from ) const {
  MsgBox( *this, options, msg, to, from ).render();
}

bool Page::msgBox    () const { return request.post( msgBoxId ) == msgBoxId; }
bool Page::yesPressed() const { return request.post( yesButton ) == yesButton; }
bool Page::noPressed () const { return request.post( noButton ) == noButton; }
bool Page::oKPressed () const { return request.post( oKButton ) == oKButton; }
bool Page::cancelPressed    () const { return request.post( cancelButton ) == cancelButton; }
bool Page::iDoNotKnowPressed() const { return request.post( iDoNotKnowButton ) == iDoNotKnowButton; }
bool Page::approvedPressed  () const { return yesPressed() || oKPressed(); }

} // x2

