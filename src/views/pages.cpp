/**
    \file pages.cpp
    \brief all pages

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Home

Home::Home( const Thread &thread ) : Page( thread ) {
  theTitle += " :: Home Page";
  theDescription += " :: Home Page";
}

void Home::renderMain() {
  Str home;
  if( ! cache.fetch( key(), home )) {
    sql::Result r( sql.openIfNotOpened());
    r.exec( "SELECT value FROM options WHERE id='site.home'" ).row();
    if( r.empty() || utl::trim( home = r[ 0 ].get()).empty())
      home = n2s( predefined::page::main );
    cache.store( key(), home );
  }
  Id pageId = utl::s2id( home );
  if( pageId ) ThePage( *this, pageId ).renderMain();
  else response.redirect( home );
}

// ThePage

ThePage::ThePage( const Thread &thread ) : Page( thread ) {
  if( ! url.args( url.tail, id )) throw AddressError();
}

ThePage::ThePage( const Page &page, Id id ) : Page( page, theUrl ), id( id ) { }

void ThePage::renderMain() {
  ph::Topic topic( *this, id ); topic.load(); topic.renderMain();
}

// About

About::About( const Thread &thread ) : Page( thread ) {
  theTitle += " :: " + tr("About");
  theDescription += " :: " + tr("The info about our company");
}

void About::renderMain() { ThePage( *this, predefined::page::about ).renderMain(); }

// Contacts

Contacts::Contacts( const Thread &thread ) : Page( thread ) {
  theTitle += " :: " + tr("Contacts");
  theDescription += " :: " + tr("How To Contact Us");
}

void Contacts::renderMain() { ThePage( *this, predefined::page::contacts ).renderMain(); }

// RpcJs

/*
  Структура предназначена для считывания в формат std::tm времени компиляции
  файла-источника,
  переменная должна определяться с параметром "__DATE__ __TIME__".
*/
/*struct SrcTimeStamp : std::tm {
  SrcTimeStamp( const char *time ) throw( std::exception ) {
    bzero( this, sizeof *this );
    if( strptime( time, "%b %d %Y%T", this )[ 0 ] != 0 )
      throw std::runtime_error( "SrcTimeStamp: parse error" );
  }
};
#define SRCTIMESTAMP SrcTimeStamp(__DATE__ __TIME__)*/

void RpcJs::render() {
  response.setContentType( "text/javascript; charset=UTF-8" );
  static LastModified lastModified( 333222333 );
  if( lastModified.notModified( request, response ))
    return;

  out << R"(/*
    js/rpc.js
    RPC

    This file is a part of x1 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

function JsonRpc( url ) { // http://xmlhttprequest.ru/
  this.run = function( method, rpcParams, localParams ) {
    var xhr = new XMLHttpRequest();
    xhr.open( "post", url );
    // Required by JSON-RPC over HTTP
    xhr.setRequestHeader( "Content-Type", "application/json" );
    var request = JSON.stringify( { "method": method, "params": rpcParams, "id":1 } );
    xhr.onreadystatechange = function() {
      if ( xhr.readyState === 4 ) {
        var res;
        if( xhr.status === 200 ) {
          var result = JSON.parse( xhr.responseText );
          if( result.error == null ) {
            res = result.result;
          }
          else {
            console.log( "JsonRpc:", "result.error", result.error );
            res = undefined;
          }
        }
        else {
          console.log( "JsonRpc:", "Invalid Status ", xhr.status );
          res = undefined;
        }
        self [method] ( res, localParams );
      }
    }
    xhr.send( request );
  }
})";
}

Str RpcJs::include( const Str &url ) {
  return
R"(<script type="text/javascript" src=")" + urlOf<RpcJs>() + R"("></script>
<script type="text/javascript">
  var jsonRpc = new JsonRpc( ")" + url + R"(" );
</script>)";
}

// RpcServer

int RpcServer::jsonRpc( utl::Json query, utl::Json &answer ) {
  Str method = query[ "method" ]; utl::Json params = query[ "params" ];
  if( method == "getVisits" ) getVisits( params, answer );
  else if ( method == "getActivities" ) getActivities( params, answer );
  else return -1; // error
  return 0;
}

void RpcServer::renderMain() {
  out << "<h1>This is x2 RPC server</h1><p>url=" << url() << "</p><p>tail=" << url.tail << "</p>\n";
}

} // x2

