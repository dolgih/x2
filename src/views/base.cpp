/**
    \file views/base.cpp
    \brief base page for all pages

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Page

void Page::createMenu() {
  theMenu.insert( MenuItem( 1, tr("Home"), urlOf<Home>(), tr("The X2 home page!")));
  theMenu.insert( MenuItem( 2, tr("Phorum"), urlOf<ph::Phorum>(), tr("X2 Phorum")));
  theMenu.insert( MenuItem( 3, tr("Search"), urlOf<Search>(), tr("Search on the site")));
  theMenu.insert( MenuItem( 4, tr( "Profile" ),
    urlOf<UserProfile>( user.id ), tr("Manage your account")));
  if( user.id > predefined::user::anonymous )
    theMenu.insert( MenuItem( 5, tr("Private"), urlOf<ph::PrivatePart>(),
      tr("Read your private messages")));
  theMenu.insert( MenuItem( 6, tr("Source"),
    urlOf<ThePage>( predefined::page::source ), tr("X2 Source Files")));
  if( user.isAdmin )
    theMenu.insert( MenuItem( 7, tr("Admin"), urlOf<adm::Admin>(), tr("X2 Admin Portal")));
  theMenu.insert( MenuItem( 90000 )); // separator
  theMenu.insert( MenuItem( 90001, tr("RSS"), urlOf<Rss>(), tr("RSS feed")));
  theMenu.insert( MenuItem( 90002, tr("About"), urlOf<About>(),
     tr("The info about our company")));
  theMenu.insert( MenuItem( 90003, tr("Contacts"), urlOf<Contacts>(),
    tr("How To Contact Us")));
}

void Page::render() {
  out << R"(<!DOCTYPE html>
<html>
<head>
<title>)" << utl::htEsc2( title()) << R"(</title>
<meta name="description" content=")" << utl::htEsc2( description()) << R"(">
<meta name="keywords" content=")" << cfg.keywords << R"(">
<meta charset="utf-8">
)"; renderStyle(); out <<
R"(<link rel="icon" type="image/x-icon" href="/favicon.ico">
<link rel="shortcut icon" href="/favicon.ico">
</head>
<body>
)";
  renderBody();
  out << R"(</body>
</html>
)";
}

void Page::renderBody() {
  auto renderError = [ this ] {
    if( ! error.empty()) {
      out << "<p class=\"error\">" << error << "<p>\n";
      error = Str();
    }
  };

// logo
  Str logo = cfg.themes.get( user.theme ).logoFile;
  if( ! logo.empty())
    out << Link( urlOf<Home>(),
      Img( cfg.media + '/' + logo, "'Jump to the home' picture" ).id("logo").html(),
      tr("Home"));
// login area
  if( typeid( *this ) != typeid( Login )) {
    out << "<span id=\"login\">\n"
    "<a href=\"" << urlOf<Login>() << "\" style=\"color:ForestGreen;\">";
    if( user.id )
      out << "<span style=\"font-size:70%\">" << tr("logout") << " </span>{" << user.name
      << '}';
    else
      out << tr("Enter");
    out << "</a>\n"
    "</span>\n";
  }
// end of login area
// header
  out << "<header>"; renderHeader(); out << "</header>\n";
// end of header
// notification area
  if( user.hasMessages || user.hasAnswers ) {
    out << "<h4 id=\"notifications\">\n";
    if( user.hasMessages )
      out << Link( urlOf<Notifications>( Notifications::isPrivateId ),
                   tr("Messages for You"),
                   tr("You have message(s)"));
    if( user.hasAnswers )
      out << "&emsp;" << Link( urlOf<Notifications>(), tr("Replies to your posts"),
             tr("You have answers to your post(s)"));
    out << "</h4>\n";
  }
// end of notification area
// wrapper
  out << "<div id=\"wrapper\">\n"
  "<aside id=\"sidebar\"";
  if( menuType == MenuSideBar ) out << " class=\"menuSideBar\"";
  out << '>';
  if( menuType != MenuNone ) { createMenu(); renderMenu(); }; out << "</aside>\n";
  // main
  out << "<main id=\"main\"";
  if( menuType == MenuSideBar ) out << " class=\"menuSideBar\"";
  out << '>';
  renderError(); if( ! response.isErrorStatus()) renderMain(); renderError();
  out << "</main>\n"
  // end of main
  "</div><!-- wrapper -->\n";
// end of wrapper
// footer
  out << R"(<footer>
Copyright (c) anatoliDolguikh, 2015;&nbsp; Powered by
<a href="https://bitbucket.org/dolgih/x2/src/" target="_blank" style="color:Red;font-size:130%">
  <b>tolata</b></a>;&nbsp;
<a href="https://jigsaw.w3.org/css-validator/check/referer" target="_blank">
  <img style="border:0;width:88px;height:31px"
  src="https://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!"></a>&nbsp;
<a href="https://validator.w3.org/check?uri=referer" target="_blank"
  style="font-size:120%">Valid XHTML 1.0!</a>
<span style="font-size:80%">PageGenTime=)" << timer.diff() << R"(ms</span>
</footer>
)";
// enf of footer
}

void Page::renderStyle() const {
  out << R"(<link rel="stylesheet" href=")" << cfg.css <<
R"(/style.css" type="text/css">
)";
  Str cssFile = cfg.themes.get( user.theme ).cssFile;
  if( ! cssFile.empty())
    out << R"(<link rel="stylesheet" href=")" << cfg.css << '/' << cssFile <<
R"(" type="text/css">
)";
}

void Page::renderHeader() const
{ out << Link( urlOf<Home>(), title() , title()); }

void Page::renderMenu() const {
  for( const MenuItem &i: theMenu  ) {
    if( ! i.isHidden()) {
      if( i.isDisabled()) out << i.name;
      else if ( i.isSeparator()) out << "•" /*"&#8226;"*/;
      else {
        out << "<a";
        if( menuType == MenuSideBar ) out << " class=\"menuSideBar\"";
        out << " href=\"" << i.link << "\" title=\"" << i.title << "\">"
          << i.name << "</a>";
      }
      out << '\n';
    }
  }
}

void Page::renderMain() { out << "Empty main"; }

void Page::renderSunflower() const {
  if( ! error.empty()) return;
  Str eom = cfg.themes.get( user.theme ).endOfMain;
  if( ! eom.empty())
    out << Link( "#login",
      Img( cfg.media + '/' + eom, "'Jump to the top' picture" ).html(),
      tr("Jump to the top"));
}

void Page::renderLinkButton( const Str &href, const Str &value, const Str &title ) const {
  Form form( *this ); Button button( form, value, "submit", "submit" );
  button.cssClass( "link-button inbrackets" ).title( title );
  out << "<form method=\"post\" action=\"" << href << "\" style=\"display:inline;\">\n"
      << form.csrf << button
      << "</form><!--End of linkButton-->\n";
}

/** \brief Рендеринг аватара, с предварительной денормализацией.
  url начинающийся с "/media" считается нормализованным, и заменяется на
  значение x2::Cfg::media из конфига.
*/
void Page::renderAvatar( Str url, const Str &name ) const {
  if( ! url.empty()) {
    if( ! strncmp( url.data(), "/media/", 7 ))
      url = cfg.media + url.substr( 6 ); // denormalize the avatar path
    out << Img( url, name ).style( "width:6em;" );
  }
}

void Page::renderCorrespondence( Id userId, short imgSize ) const {
  if( user.canCorrespondence( userId ))
  out << Link( urlOf<ph::Post>( userId, 0, user.privatePart()),
               Img( cfg.media + "/email.png", tr("correspondence"))
                  .style( "width:" + n2s( imgSize ) + "em;height:" + n2s( imgSize ) +"em;" )
                  .html(),
               tr("Create new private correspondence with the user"));
}

void Page::renderRss( Id userId ) const {
  if( userId != predefined::user::system && userId )
  out << Link( urlOf<Rss>( "user", userId ),
               Img( cfg.media + "/RSS.png", "RSS" ).style( "width:2em;height:2em;" )
                  .html(),
               tr("Get RSS feed of the user"));
}

void Page::renderOnlineIndicator( Id id ) const {
  out << "<span id=\"onlineIndicator" << n2s( id )
      << "\" style=\"display:none;color:green;background:white;\">"
      << tr("(online)") << "</span>\n";
}

} // x2

