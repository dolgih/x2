/**
    \file views/rss.cpp
    \brief controller for rss feed

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

Rss::Rss( const Thread &thread ) : Page( thread ) { }

void Rss::render() {
  response.setXmlContentType();
  Str what; Id id;
  if( ! url.args( url.tail, what, id )) throw AddressError();
  if( what == "part" ) renderRssOfPart( id );
  else if( what == "user" ) renderRssOfUser( id );
  else if( what == Str()) renderRss();
  else throw AddressError();
}

void Rss::renderRssOfPart( Id id ) {
  dta::ph::PartsResult part( sql );
  part.init( id, user );
  renderRss( " AND parts.id=" + n2s( part.id()),
       " :: " + part.name(),
       " :: " + part.description(),
       urlOf<ph::Part>( part.id()) ,
       url( "part", part.id()));
}

void Rss::renderRssOfUser( Id id ) {
  dta::UserResult user( sql, id );
  renderRss( " AND posts.author=" + n2s( user.id()),
       " (of user " + user.name() + ')',
       " (of user " + user.name() + ')',
       urlOf<UserProfile>( user.id()),
       url( "user", user.id()));
}

void Rss::renderRss() {
  renderRss( Str(), Str(), Str(), urlOf<Home>(), url());
}

void Rss::renderRss( Str where, Str titleAddendum, Str descrAddendum,
  Str link, Str atomLink )
{
  out << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
    "<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">\n"
    "<channel>\n"
    "<title>" + cfg.title + titleAddendum + "</title>\n"
    "<link>" + request.hostAsPrefix() + link + "</link>\n"
    "<description>" + cfg.description + descrAddendum + "</description>\n"
    "<atom:link\nhref=\"" + request.hostAsPrefix() + atomLink + "\"\n"
    "rel=\"self\" type=\"application/rss+xml\" />\n";
  dta::ph::PartsResult parts( sql ); parts.init();
  sql::Result r = sql()( "SELECT posts.* FROM posts JOIN topics"
    " ON posts.topic_id=topics.id JOIN parts ON topics.part_id=parts.id"
    " WHERE parts.id>0 AND parts.id NOT IN (" + parts.unseen( user ) +
    " ) " + where + "ORDER BY posts.id DESC LIMIT 20" );
  while( r.next()) {
    Str link = request.hostAsPrefix() + urlOf<ph::Topic>( r[ "id" ].get<Id>()),
        title = utl::htEsc( r[ "title" ].get()),
        abstract = utl::htEsc( r[ "abstract" ].get());
    TimeT updT = r[ "modified" ].get<TimeT>(),
          crT  = r[ "created" ].get<TimeT>();
    Str updated = t2s( updT, "%Y-%m-%dT%T%z" );
    Str published = t2s( crT, "%Y-%m-%dT%T%z" );
    Str pubDate = utl::t2s( crT, "%a, %d %b %Y %T %z", std::locale( "en_US.UTF-8" ));
    out << "<item>\n" << "<title>" << title << "</title>\n"
    << "<link>" << link << "#post" << r[ "id" ].get<Str>() << "</link>\n"
    << "<guid>" << link << "</guid>\n"
    << "<description>" << ( abstract.empty() ? title : abstract ) << "</description>\n"
    << "<updated>" << updated << "</updated>\n"
    << "<published>" << published << "</published>\n"
    << "<pubDate>" << pubDate << "</pubDate>\n"
    << "</item>\n";
  }
  out << "</channel>\n</rss>\n";
}

} // x2

