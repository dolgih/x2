/**
    \file views/adm/part.cpp
    \brief View of the part editor

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Part::EditForm

Part::EditForm::EditForm( Page &page )
  : Form( page ),
  author  ( *this, "author" ),      lAuthor     ( author, tr("Author Id")),
  name    ( *this, "name", dta::ph::PartsResult::nameRegex()),
                                    lName       ( name, tr("Part Title")),
  description( *this, "description", dta::ph::PartsResult::descriptionRegex()),
                                    lDescription( description, tr("Description")),
  weight  ( *this, "weight" ),      lWeight     ( weight, tr("Weight")),
  banGroup( *this, "banGroup", Str( "(" ) + dta::adm::GroupsResult::nameRegex() + ")|^$" ),
                                    lBanGroup   ( banGroup, tr("Ban Group")),
  submit  ( *this, tr("Submit"), "submit" ),
  remove  ( *this, tr("Remove"), "remove" ),
  clear   ( *this, tr("Clear"), "clear" )
{
  author.message =   tr("valid user id"); author.size( "7" );
  name.message =     tr("up to 96 symbols, minimum 4"); name.size( "48" ).autofocus( true );
  description.message = tr("up to 128 symbols, minimum 6"); description.size( "48" );
  weight.message =   tr("number from 1 to 999"); weight.min( "1" ).max( "999" ).size( "4" );
    weight.readonly( ! parent.user.isAdmin );
    weight.set( 800 /* default blogger weight */ );
  banGroup.message = tr("up to 32 symbols, without spaces, minimum 3");
    banGroup.size( "32" );
  submit.title( tr("send data to the server"));
    submit.style( "font-size:150%;" );
  remove.title( tr("cancel editing and remove the group"));
  clear. title( tr("clear the Part"));
};

// Part

void Part::renderMain() {
  out << "<article>\n"
  "<form method=\"post\">" << form.csrf << "<table><caption><h4>";
  if( id ) out << tr("Part (id=") << n2s( id ) << ')';
  else out << tr("New part");
  out << "</h4></caption>\n";
  form.render( out, form.author, form.banGroup );
  out << "<tr><th>" << tr("modified") << "</th><td>"
      << ( id ? t2s( part.modified()) : Str()) << "</td></tr>\n";
  if( form.accessList.empty()) throw std::runtime_error( "Part::renderMain: accessList.empty" );
  form.render( out, *form.accessList.front(), *form.accessList.back());
  form.render( out, form.submit, form.clear );
  out << R"(</table></form>
<h5>)" << tr("Legend") << R"(</h5>
<ul>
  <li>Rd: )" << tr("the read access in part") << R"(</li>
  <li>Wr: )" << tr("the write acces in part, includes the read access") << R"(</li>
  <li>Cr: )" << tr("the create topic access in part, includes the read and write accesses") << R"(</li>
  <li>Ow: )" << tr("the moderate access in own topic, includes the Rd, Wr and Cr accesses") << R"(</li>
  <li>Md: )" << tr("the moderate access in part, includes the Rd, Wr, Cr and Ow accesses") << R"(</li>
  <li>Ht: )" << tr("the write plain html access in part, includes the read and write accesses") << R"(</li>
</ul>
</article>
)";
}

}} // adm, x2

