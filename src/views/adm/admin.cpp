/**
    \file views/adm/admin.cpp
    \brief Base view for all admin pages, and main admin page simultaneously

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Base

void Base::renderStyle() const {
  Page::renderStyle();
  out << R"(<style>
#logo {
  height: 64px;
}
</style>
)";
}

}} // adm, x2

