/**
    \file views/adm/groups.cpp
    \brief View of the groups

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Groups::EditForm

Groups::EditForm::EditForm( Page &page )
  : Form( page ),
  name   ( *this, "name", dta::adm::GroupsResult::nameRegex()),
    lName( name, tr("Group Name")),
  submit ( *this, tr("Submit"), "submit" ),
  remove ( *this, tr("Remove"), "remove" )
{
  name.message = tr("up to 32 symbols, without spaces, minimum 3");
  name.size( "32" ).autofocus( true );
  submit.title( tr("send data to the server"));
  remove.title( tr("cancel editing and remove the group"));
};

// Groups

void Groups::renderMain() {

  out << "<article>\n";

  if( groups.empty())
    out << "<h4>" << tr("No groups (There are probably errors).") << "</h4>\n";
  else

  out << "<table><caption><h2>" << tr("Groups") << "</h2></caption><thead><tr>\n"
         "<th>" << tr("Id") << "</th>\n"
         "<th>" << tr("Name") << "</th>\n"
         "</tr></thead>\n"
         "<tbody>\n";

  for( auto &g: groups ) {
    out << "<tr>\n"
           "<td>" << n2s( g.id()) << "</td>\n"
           "<td>" << Link( urlOf<adm::Groups>( g.id()) + "#edit",
                           g.name(),
                           tr("Edit"))
           << "</td></tr>\n";
  }

  out << "</tbody></table>\n"
  "<br><hr><h4 id=\"edit\">" << tr("Edit") << ": ";
  if( id ) out << tr("group (Id=") << id << ')';
  else out << tr("new group");
  out << "</h4>\n"
  << "<form method=\"post\">" << form.csrf << "<table class=\"withoutBorder\">";
  form.renderFromBeginToEnd( out );
  out << "</table></form>\n"
  "<p>" << tr("modified:") << ' ' << form.modified << "</p>\n"
  "</article>\n";
}

}} // adm, x2

