/**
    \file views/adm/admin.h
    \brief Base view for all admin pages, and main admin page simultaneously

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 { namespace adm {

/** Base Page for all ph pages */
class Base : public Page {
public:
  Base( const Thread &thread ) : Page( thread ) { }
  void createMenu() override;
  void renderStyle() const override;

  static const struct Dispatcher : Page::Dispatcher {
    Dispatcher( const Page::Dispatcher &parent );
  } dispatcher;
};

/** Main admin page */
class Admin : public Base {
public:
  Admin( const Thread &thread );
  static const char *key() { return ""; }

protected:
  void load() override;
  void renderMain() override;

  Navigation nav;
  Id id;
  dta::ComplexUserResult users;
};

class Groups : public Base {
public:
  Groups( const Thread &thread );
  static const char *key() { return "groups"; }

protected:
  void load() override;
  void renderMain() override;
  bool save() override;

  Id id;
  struct EditForm : Form {
    Regex name; Label lName; Str modified;
    Submit submit, remove;
    EditForm( Page &page );
  } form;
  dta::adm::GroupsResult groups;
};

class Part : public Base {
public:
  Part( const Thread &thread );
  static const char *key() { return "part"; }

protected:
  void load() override;
  void renderMain() override;
  bool save() override;

  Id id;
  struct EditForm : Form {
    Number author;                   Label lAuthor;
    Regex  name;                     Label lName;
    Regex  description;              Label lDescription;
    Number weight;                   Label lWeight;
    Regex  banGroup;                 Label lBanGroup;
    list<MultiCheckBox*> accessList; list<Label*> lAccessList;
    Submit submit;
    Submit remove;
    Submit clear;

    EditForm( Page &page );
  } form;
  dta::adm::PartsResult part; std::map<Str,dta::adm::PartAccess> accesses;

private:
  static Str sAGId( int id ) { // string identifier of access group to the part
    return utl::f( "accessGroup{1}", id );
  }
};

class ClearPart : public Base {
public:
  ClearPart( const Thread &thread );
  static const char *key() { return "clearPart"; }

protected:
  void load() override;
  bool save() override;
  void render() override;

  Id id /* part to clear */;
  dta::ph::PartsResult part;
};

class Sys : public Base {
public:
  Sys( const Thread &thread );
  static const char *key() { return "sys"; }

protected:
  void load() override;
  static const char cachePictureId[];
  void render() override;
  void renderMain() override;
  bool save() override;
  void recalcStatistics();

  struct TheForm : Form {
    Submit clearCache;
    Submit recalcStatistics;
    Submit cachePicture;
    TheForm( Page &page );
  } form;
};

class Xss: public Base {
public:
  Xss( const Thread &thread );
  static const char *key() { return "xss"; }

protected:
  void renderMain() override;
  bool save() override;

  struct TheForm : Form {
    TextArea inHtml, outHtml;
    Submit submit;
    TheForm( Page &page );
  } form;
  unsigned inSize, outSize;
};

class Sql : public Base {
protected:
  struct QueryForm : Form {
    TextArea query;
    Submit exec;
    Hidden history;
    QueryForm( const Page &page ) : Form( page ),
      query  ( *this, "query" ),
      exec   ( *this, "exec", "exec" ),
      history( *this, "history" )
      { query.cols( "75" ).rows( "8" ).required( true ).autofocus( true ); }
  } form;
  Str answer;

public:
  Sql( const Thread &thread ) : Base( thread ), form( *this )
  { theTitle = "Execute Sql Query"; }
  void load() override;
  bool save() override;
  void renderMain() override;
  static const char *key() { return "sql"; }
};

}} // adm, x2

