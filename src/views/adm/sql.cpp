/**
    \file sql.cpp
    \brief simple page to execute the sql queries (tested on postgresql only!)

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/


#include "x2.h"

namespace x2 { namespace adm {


void Sql::load() {
  menuType = MenuNone;

  if( ! form.query.get().empty())
    form.history.set( form.query.get() + "<br>______<br>" + form.history.get());
  form.query.set( utl::trim( form.query ));
}

bool Sql::save() {
  if( user.id != predefined::user::admin && ! ( cfg.admin.policy.godMode && user.isAdmin )) {
    error = "Get out!";
    response.setStatus( 403 /* forbidden */ );
    return false;
  }
  if( ! form.query.get().empty()) try
  {
    LOG_WARN( "user '" + user.name + "' executed query(" + form.query.get() + ") from ip="
      + request.remoteAddr());
    sql::Result r( sql()); r.exec( form.query );
    if( ! r.next())
        answer += "<h4>No data returned from the Sql server.</h4>\n";
    else {
        answer += "<table border=\"1\"><caption>";
        answer += utl::htEsc( form.query );
        answer += "</caption>\n <thead><tr>";
        for( int j = 0; j < r.fieldCount(); j ++ ) {
            answer += "<th>";
            answer += utl::htEsc( r.fieldName( j ));
            answer += "</th>";
        }
        answer += "</tr></thead>\n<tbody>\n";
        do {
            answer += " <tr>";
            for( int j = 0; j < r.fieldCount(); j ++ ) {
                answer += "<th>";
                answer += utl::htEsc( r[ j ].isNull() ? "NULL" : r.get<Str>());
                answer += "</th>";
            }
            answer += "</tr>\n";
        } while ( r.next());
        answer += "</tbody></table>\n";
    }
  } catch (const std::exception& e) {
    answer += "<p class=\"error\">";
    answer += utl::htEsc( Str(" Error:") + e.what());
    answer += "</p>\n";
  }
  return false;
}

void Sql::renderMain() {
  out << R"(<article>
<h1>SQL query to execute</h1>
<form method="post">)" << form.csrf << R"(
<p>Enter the Query?</p>
)" << form.query << "<br>" << form.exec << form.history << R"(
</form>
)"
  << answer << R"(<br><h3>History</h3>
)" << form.history.get() << R"(
</article>
)";
  renderSunflower();
}

}} // adm, x2

