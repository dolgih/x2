/**
    \file views/adm/sys.cpp
    \brief View of the sys page

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Sys::TheForm

Sys::TheForm::TheForm( Page &page )
  : Form( page ),
  clearCache       ( *this, tr("Clear Cache"), "clearCache" ),
  recalcStatistics ( *this, tr("Recalc Statistics"), "recalcStatistics" ),
  cachePicture     ( *this, tr("Get Cache Picture"), "cachePicture" )
{
  clearCache.title( clearCache.initialValue );
  recalcStatistics.title( recalcStatistics.initialValue );
  cachePicture.title( tr("Show details about the cache"));
};

// Sys

void Sys::render() {
  if( url.tail == cachePictureId ) {
    response.setXmlContentType();
    cache.getPicture( out );
  } else Base::render();
}

void Sys::renderMain() {

  out << "<article>\n"
  "<h2>" << tr("Cache manager") << "</h2>\n"
  "<h4>Config Data</h4>\n"
  "maxNumber: "  << cfg.cache.maxNumber <<  "<br>\n"
  "maxSum: "     << cfg.cache.maxSum <<   "MB<br>\n"
  "maxKeySize: " << cfg.cache.maxKeySize << "<br>\n"
  "fill try count: " << cfg.cache.fill.tr.count << " sleep: " <<
                        cfg.cache.fill.tr.sleep << "<br>\n"
  "<h4>Free Number: " << x2::cache.getFreeNumber() << " (cached "
    << x2::cache.getMaxNumber() - x2::cache.getFreeNumber() << ")</h4>\n"
  "<h4>Free Sum: " << x2::cache.getFreeSum() << " (cached "
    << ( size_t( x2::cache.getMaxSum()) << 20 ) - x2::cache.getFreeSum() << ")</h4>\n"
  "<form method=\"post\">" << form.csrf <<
  "<p>" << form.clearCache << " &nbsp; " << form.cachePicture << "</p>"
  "<br><h2>" << tr("Statistics manager") << "</h2>\n"
  "<p>" << form.recalcStatistics << "</p>\n"
  "</form>\n"
  "<br><h2>" << tr("Another system data") << "</h2>\n"
  "<p>Request accepted from host <i>" << request.serverName() << "</i></p>"
  "<p>Thread Id=" << pthread_self() << "</p><h5>Environment</h5>\n";
  request.printEnvironment();
  out << "<br><p>" "GCC " __VERSION__ " build time: " __DATE__ " " __TIME__ "</p>\n"
  "</article>\n";
}

}} // adm, x2

