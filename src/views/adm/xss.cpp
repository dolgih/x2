/**
    \file views/adm/xss.cpp
    \brief view of the Xss filter page

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Xss::TheForm

Xss::TheForm::TheForm( Page &page )
  : Form( page ),
  inHtml ( *this, "inHtml" ),
  outHtml( *this, "outHtml" ),
  submit ( *this, tr("Submit"), "submit" )
{
  inHtml.rows( "12" ).cols( "80" ).autofocus( true );
  outHtml.rows( "2" ).cols( "80" ).readonly( true );
};

// Xss

void Xss::renderMain() {
  out << "<article>\n"
  "<h2>" << tr("Xss Filter") << "</h2>\n"
  "<form method=\"post\">" << form.csrf
  << "<h4>" << tr("Input HTML") << ", " << tr("size") << '=' << inSize << "</h4>\n"
  << form.inHtml
  << "<h4>" << tr("Output HTML") << ", " << tr("size") << '=' << outSize<< "</h4>\n"
  << form.outHtml
  << "<br>" << form.submit << "</form>\n"
  << "</article>\n";
}

}} // adm, x2

