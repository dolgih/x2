/**
    \file views/adm/users.cpp
    \brief View of the users table

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Admin

void Admin::renderMain() {

  out << R"(
<style scoped>
  td, th { white-space: nowrap; overflow: hidden; max-width: 20em; }
</style>
<article style="overflow:auto; padding: 0 0.5em 0.6em;">
)";
  nav.render();

  if( users.empty())
    out << "<h4>" << tr("No users (There are probably errors).") << "</h4>\n";
  else {

  out << "<table><caption><h2>" << tr("Users") << "</h2></caption><thead><tr>\n"
         "<th>" << tr("Id") << "</th>\n"
         "<th>" << tr("Name") << "</th>\n"
         "<th>" << tr("Human Name") << "</th>\n"
         "<th>" << tr("Web Site") << "</th>\n";

  dta::ph::PartsResult parts( sql ); parts.init();
  std::set<Id> partsOfUser; parts.getPartsOfAuthor( user.id, partsOfUser );
  bool isBlogger = ! partsOfUser.empty() /* a blogger */;
  if( user.isAdmin || isBlogger )
    out << "<th>" << tr("E-Mail") << "</th>\n"
           "<th>" << tr("Phone") << "</th>\n"
           "<th>" << tr("Phone2") << "</th>\n";
  out << "</tr></thead>\n"
      << "<tbody>\n";

  for( auto &u: users ) {
    u.getProperties();
    out << "<tr id=\"user" << u.id() << "\">\n"
      "<td>"; renderCorrespondence( u.id(), 1 ); out << u.id() << "</td>\n"
      "<td>" << Link( urlOf<UserProfile>( u.id()),
                      u.name(),
                      tr("full view | edit")) << "</td>\n"
      "<td>" << utl::htEsc( u.humanName()) << "</td>\n"
      "<td>" << u.webSite() << "</td>\n";
      if( user.isAdmin || isBlogger )
        out << "<td>" << u.email() << "</td>\n"
               "<td>" << u.phone() << "</td>\n"
               "<td>" << u.phone2() << "</td>\n";
      out << "</tr>\n";
  }}; // for users, not empty

  out << "</tbody></table>\n";

  if( user.isAdmin )
    out << "<br><h4>" << Link( urlOf<UserProfile>( 0 ),
                               tr("Create"),
                               tr("Create new user")).cssClass( "inbrackets" ) << "</h4>\n";
  nav.render();
  out << "</article>\n";
}

}} // adm, x2

