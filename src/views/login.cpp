/**
    \file views/login.cpp
    \brief login rendering

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Login::TheForm

Login::TheForm::TheForm( Page &parent )
  : Form( parent ),
    name(     *this, "name",
      '(' + Str( dta::UserResult::nameRegex())
        + ")|(" + dta::UserResult::emailRegex() + ")" ), nameLabel( name, tr("User Name")),
    password( *this, "password" ), passwordLabel( password, tr("Password")),
    login(    *this, "login", "login" ),
    // "forgot" data
    email(    *this, "email" ), emailLabel( email, "E-mail" ),
    forgot(   *this, tr("Send"), "forgot" ),
    referer(  *this, "referer", parent.request.referer())
{
  name.size( "32" ); name.autofocus( true );
  name.message = tr("up to 32 symbols, without spaces, minimum 3");

  password.size( "32" );

  login.title( tr("Login with these name and password"));

  email.size( "32" );
  email.message = tr("up to 128 symbols, minimum 5");

  forgot.title( tr("Send a new password by email"));
}

// Login

void Login::renderMain() {
  out << "<article style=\"padding: 0 0.5em;\"><h2 style=\"padding: 0.5em 0;\">"
    + tr("Enter the data to log in") + R"(</h2>
<form method="post" action=")" + url() + "\">" << form.csrf << R"(<table class="withoutBorder">
)";
  form.render( out, form.name, form.login );
  out << R"(</table><br><details>
<summary><h2 style="padding: 0.5em 0;">)" + tr("Did you forget your password?") + R"(</h2></summary>
<table class="withoutBorder">
)";
  form.render( out, form.email, form.referer );
  out << R"(</table>
</details>
</form>
)" << Link( urlOf<UserProfile>( 0 ),
        "<h2 style=\"padding: 0.5em 0;\">" + tr("Not Registered? Click here to register now.") + "</h2>",
        tr("Open the page for creating a new profile"))
  << "</article>\n";
  renderSunflower();
}

} // x2

