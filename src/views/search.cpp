/**
    \file views/search.cpp
    \brief view of the search

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Search::TheForm

Search::TheForm::TheForm( Page &parent )
  : Form( parent ),
    query( *this, "query" ),
    date1( *this, "date1", utl::dateRegex()), date2( *this, "date2", utl::dateRegex()),
    exec ( *this, tr("Submit"), "submit" ),
    prev ( *this, "< " + tr("prev page"), "prev" ),
    next ( *this, tr("next page") + " >", "next" ),
    page ( *this, "page" )
{
  query.type( "search" ).autofocus( true ).required( true ).size( "40" ).maxlength( "100" );
  date1.type( "date" ).required( true ).size( "10" ).maxlength( "10" );
    date1.attrs[ "min" ] = "2016-01-01"; date1.attrs[ "max" ] = "2099-01-01";
    date1.message = tr("'date to' error");
  date2.type( "date" ).required( true ).size( "10" ).maxlength( "10" );
    date2.attrs[ "min" ] = "2016-01-01"; date2.attrs[ "max" ] = "2099-01-01";
    date2.message = tr("'date from' error");
  exec.title( tr("Perform the query"));
  prev.title( tr("Show previous page"));
  next.title( tr("Show next page"));
}

// Search

void Search::renderMain() {
  out << R"(
<article style="padding: 0 0.5em;">
)";
  out << "<h1>" << tr("Query to search") << "</h1>\n"
      "<form method=\"post\">" << form.csrf
      << "<p>" << tr("Enter the Query?") << "</p>\n"
      << "<p style=\"margin: 4px 0\">" << form.query << "</p>\n"
      "<p class=\"error\">" << error << "</p>\n"
      << tr("search from") << ' ' << form.date1
      << ' ' << tr("up to") << ' ' << form.date2
      << "<br><br>\n"
      << form.exec
      << "<br><br>\n"
      << ( page ? form.prev.html() : Str())
        << " (" << tr("page") << ' ' << page + 1 << ") "
        << ( cfg.pageSize * 2 == result.recordCount() ? form.next.html() : Str())
      << form.page
      << "</form>\n";

  try {
    if( ! result.next())
      out << tr("<h4>No data returned from the Sql server.</h4>\n");
    else {
      do {
        Id id = result[ "id" ].get<Id>();
        Str title    = utl::htEsc( result[ "title" ].get<Str>()),
          abstract = utl::htEsc( result[ "abstract" ].get<Str>()),
          headline = result[ "headline" ].get<Str>();
        out << "<hr>"
            << Link( urlOf<ph::Topic>( id ) + utl::f( "/#post{1}", id ),
                     "<span style=\"font-size:150%;\">" + title + " </span>" + abstract,
                     tr("Go to the post"))
            << "<p> ... " << headline << " ... </p><br>\n";
      } while ( result.next());
    }
  } catch( const std::exception& e ) { CAUGHT_EXCEPTION( e ); }

  out << "</article>\n";
  renderSunflower();
}

} // x2

