/**
    \file views/user.h
    \brief view of the user profile

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 {

class UserProfile : public Page {
public:
  UserProfile( const Thread &thread );
  static const char *key() { return "user"; }

  static void cleanMembership( sql::PhSession &sql, Id userId, Id groupId );

protected:
  Id id = 0; // user id
  /** access level of the current user */
  enum AccessLevel { NoAccessLevel, AnonymousLevel, UserLevel, ViewLevel,
    BloggerLevel, OwnerLevel, AdminLevel } accessLevel = AnonymousLevel;
  bool isNew = false;

  struct BaseForm : Form {
    BaseForm( const Page &page ) : Form( page ) { }
    void checkAuthorization( Input &widget, AccessLevel canView, AccessLevel canEdit = NoAccessLevel );
  };

  struct Main : BaseForm {
    struct Banned : GroupedInputs {
      Id groupId; // group id
      struct Values { bool checked; Str before, description; } values;
      CheckBox  switcher;
      Regex     before;
      Text      description;
      Label     label, lSwitcher, lBefore, lDescription;
      Banned( Form &form, const Str &label );
      void init( const dta::GroupsOfUserResult &groupOfUser );
      void setOrCmp();
      bool get();
    };
    typedef list<Banned> BannedInGroups;

    struct WebSiteField : Regex {
      WebSiteField( Form &form );
      void render( std::ostream &out ) const override;
    };

    Regex          name;
    Password       password, password2;
    Regex          humanName;
    Regex          email;
    WebSiteField   webSite;
    Regex          phone;
    Regex          phone2;
    Regex          avatar;
    File           uploadedAvatar;
    Select         groupsOfUser;
    // restrictions
    MultiCheckBox  restrictions;
    Submit         validateEmail;
    BannedInGroups bannedInGroups;
    // referer to return on the calling page
    Hidden         referer;

    Label lName, lPassword, lPassword2, lHumanName, lEmail, lWebSite, lPhone,
          lPhone2, lAvatar, lUploadedAvatar, lGroupsOfUser, lRestrictions;
    // end of restrictions
    Main( const Page &page );
    void checkAuthorizationAll();
  } main;

  struct Preferences : BaseForm {
    Select locale, theme;
    MultiCheckBox answers, quotes;
    Label lLocale, lTheme, lAnswers, lQuotes;
    Preferences( const Page &page );
    void checkAuthorizationAll();
  } preferences;

  struct Actions : BaseForm {
    Submit submit;
    Submit remove;
    Actions( const Page &page );
    void checkAuthorizationAll();
  } actions;

  void load() override;
  void loadAvatar();
  void checkForAUserIsAdmin();
  bool currUsrIsOwnerOfGroup( Id group ) const {
    return partsOfCurrentUser.find( - group ) != partsOfCurrentUser.end();
  }
  bool validate() override;
  bool checkExistenceNameAndEmail();
  void updateGroupsOfUserAndRestrictions();
  void cleanMembership( Id groupId ) { cleanMembership( sql, id, groupId ); }
  void setMembership( Id groupId );
  bool save() override;
  void saveAvatar();
  void renderMain() override;

  // remove
  Str removeCommand;
  bool belongs2group = false;
  static const char removeCommandId[];
  static const char removeWithPostsCommandId[];
  void removeUser();

  dta::UserResult user;
  dta::GroupsOfUserResult groupsOfUser;
  std::set<Id> partsOfCurrentUser; // the user owns part(s) (is blogger)
  // various values
  Bools groupsOfUserValues, restrictionsValues;
  std::set<Str> groupsOfUserDiff, restrictionsDiff;
  // end of various values

  // activate
  void mail4activate( Str to, Id userId );

  static Str sGoUId( Id id ) {
    if( id >= 0) // string identifier of group of user
      return utl::f( "groupOfUser{1}", id );
    else  // string identifier of restriction of user
      return utl::f( "restrictionOfUser{1}", -id );
  }

};

/** Активация пользователя */
class Activate : public Page {
public:
  Activate( const Thread &thread );
  static const char *key() { return "activate"; }
  static Str createActivationKey( Str mail, Id userId );

protected:
  void load() override;
  void render() override { }

  Id id; Str activationKey;
};

} // x2

