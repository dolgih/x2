/**
    \file sitemap.cpp
    \brief sitemap

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Sitemap

Sitemap::Sitemap( const Thread &thread ) : Page( thread ) { }

void Sitemap::render() {
  LOG_INFO( "sitemap got addr: " + request.remoteAddr() + " user agent: "
    + request.userAgent());

  response.setXmlContentType();

  out << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
    "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";

  TimeT t = time( 0 );
  renderUrlTag( urlOf<Home>(), t );
  renderUrlTag( urlOf<About>(), t );
  renderUrlTag( urlOf<Contacts>(), t );
  renderUrlTag( urlOf<ph::Phorum>(), t );

  dta::ph::PartsResult parts( sql ); parts.init();
  for( auto &p: parts ) {
    dta::PartAccess access( p.id()); access.getData( sql );
    User anonymous /* anonymous */;
    p.getRights( anonymous, access );
    if( ! p.rights.R() || p.id() == predefined::part::bin )
      continue;

    dta::ph::TopicsResult topics( sql ); topics.init( p.id(), 0 /* page */ );
    bool notEmptyTopics = topics.next();
    for( int pg = 0; notEmptyTopics && pg <= p.maxTopicsPage(); pg ++ ) {
      renderUrlTag( urlOf<ph::Part>( p.id(), pg ), p.modified());

      topics.init( p.id(), pg );
      for( auto &t: topics ) {
        dta::ph::PostsResult posts( sql ); posts.init( 0, t.id(), 0 /* page */ );
        bool notEmptyPosts = posts.next();
        for( int pg = 0; notEmptyPosts && pg <= t.maxPostsPage(); pg ++ )
          renderUrlTag( urlOf<ph::Topic>( 0, t.id(), pg ), t.modified());
      }
    }
  }

  out << "</urlset>\n";
}

void Sitemap::renderUrlTag( const Str &path, TimeT date ) {
  out << "<url>\n"
  "<loc>" << request.hostAsPrefix() << path << "</loc>\n"
  "<lastmod>" << utl::T2s( date, "%Y-%m-%d" ) << "</lastmod>\n"
  "<changefreq>daily</changefreq>\n"
  "<priority>0.5</priority>\n"
  "</url>\n";
}

} // x2

