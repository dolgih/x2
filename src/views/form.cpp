/**
    \file views/form.cpp
    \brief Form & widgets

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Dom

void Dom::render( std::ostream &out ) const {
  renderOpen( out ); out << '\n';
}

void Dom::renderOpen( std::ostream &out ) const {
  out << '<' << tag << ' '; renderAttrs( out ); out << '>';
}

void Dom::renderClose( std::ostream &out ) const { out << "</" << tag << ">\n"; }

void Dom::renderAttrs( std::ostream &out ) const {
  for( const auto &attr: attrs ) {
    out << attr.first << "=\"" << utl::htEsc2( attr.second ) << '"';
    if(  &*( -- attrs.end()) != &attr ) out << ' ';
  }
}

Str Dom::html() const { std::ostringstream out; render( out ); return out.str(); }

// Img

Img::Img( const Str &src, const Str &alt ) : Dom( "img" ) {
  attrs[ "src" ] = src; attrs[ "alt" ] = alt;
}

// Link

Link::Link( const Str &href, const Str &value, const Str &aTitle )
  : Dom( "a" ), value( value )
{ attrs[ "href" ] = href; title( aTitle ); }

void Link::render( std::ostream &out ) const {
  Dom::renderOpen( out ); out  << value; Dom::renderClose( out );
}

// Label

Label::Label( Input &input, const Str &value )
  : Dom( "label" ), value( value ), input( input )
{
  if( input.label ) throw std::runtime_error( "Label: input already has label" );
  input.label = this;
  if( input.id().empty()) input.generateId();
}

void Label::render( std::ostream &out ) const {
  out << "<label ";
  renderAttrs( out );
  out << " for=\"" << input.id() << "\">"
      << utl::htEsc( value ) << "</label>";
}

Label::~Label() { input.label = 0; }

// Input

Input::Input( Form &parent, const Str &aName, const char *aType, const Str &value )
  : Dom( "input" ), ChildOf( parent ), value( value )
{
  name( aName.empty() ? generateId() : aName );
  type( aType );
}

Input::Input( Form &parent, const Str &aName, const Str &value )
  : Dom( "input" ), ChildOf( parent ), value( value )
{ name( aName.empty() ? generateId() : aName ); }

void Input::takePost() {
  if( ! readonly()) {
    value = parent.page().request.post( name());
    taken = true;
  }
}

void Input::render( std::ostream &out ) const {
  out << "<input ";
  renderAttrs( out );
  out << " value=\"" << utl::htEsc( value ) << "\">";
}

void Input::renderLabel( std::ostream &out ) const { if( label ) label->render( out ); }

static const char roAttr[] = "readonly";
bool Input::readonly() const { return getAttr( roAttr ) == roAttr; }
Input &Input::readonly( bool v ) {
  if( v ) attrs[ roAttr ] = roAttr; else attrs.erase( roAttr ); return *this;
}

const Str &Input::generateId()
{ Str &id = attrs["id"]; id = parent.generateId(); return id; }

Input::~Input() { delete label; }

// Submit

Submit::Submit( Form &parent, const Str &initialValue, const Str &name )
  : Input( parent, name, "submit", Str()),
    initialValue( initialValue.empty() ? parent.generateId() : initialValue )
{ }

// Button

Button::Button( Form &parent, const Str &value, const Str &name, const Str &initialValue )
  : Input( parent, name, value ),
    initialValue( initialValue.empty() ? parent.generateId() : initialValue )
{ }

void Button::render( std::ostream &out ) const {
  out << "<button ";
  renderAttrs( out );
  out << " value=\"" << initialValue << "\">" << utl::htEsc( value ) << "</button>";
}

// Regex

bool Regex::validate() { return boost::regex_match( value, boost::regex( regex )); }

// TextArea

void TextArea::render( std::ostream &out ) const {
  out << "<textarea ";
  renderAttrs( out );
  out << '>' << utl::htEsc( value ) << "</textarea>";
}

// CheckBox

CheckBox::CheckBox( Form &parent, const Str &initialValue, const Str &name )
  : Submit( parent, initialValue, name )
{
  type( "checkbox" );
}


// Select

void Select::getDiff( const Bools &vals, std::set<Str> &diff ) {
  if( vals.size() != options.size()) throw std::runtime_error( "Select::getDiff: different sizes" );
  std::set<Str> sels; get( sels ); // отмеченные пользователем
  auto v = vals.begin();
  diff.clear();
  for( auto &option: options )
    if( *( v ++ ) != ( option.selected = sels.find( option.id ) != sels.end()))
      diff.insert( option.id );
  changed = ! diff.empty();
}

void Select::set( const Bools &vals ) {
  if( vals.size() != options.size()) throw std::runtime_error( "Select::set: different sizes" );
  auto option = options.begin();
  for( bool selected: vals ) ( option ++ )->selected = selected;
}

void Select::setOrCmp( const Bools &vals, std::set<Str> &diff ) {
  if( taken ) getDiff( vals, diff );
  else set( vals );
}

void Select::render( std::ostream &out ) const {
  out << "<select ";
  renderAttrs( out );
  out << ">\n";
  bool ro = readonly();
  for( const auto &option: options ) {
    out << "<option value=\"" << option.id << '"';
    if( option.id == value || option.selected )
      out << " selected=\"selected\"";
    if( ro )
      out << " disabled=\"disabled\"";
    out << '>'
        << utl::htEsc( option.value ) << "</option>\n";
  }
  out << "</select>\n";
}

void Select::get( std::set<Str> &v ) const {
  v.clear();
  Str val = value;
  while( ! val.empty()) {
    size_t sz = strlen( val.data());
    v.insert( Str( val.data(), sz ));
    if( sz < val.size())
      val = val.substr( sz + 1 );
    else break;
  }
}

// GroupedInputs

GroupedInputs &GroupedInputs::add( Input &i ) {
  i.grouped = true; i.readonly( readonly()); members.emplace_back( &i );
  return *this;
}

bool GroupedInputs::validate() {
  if( ! Input::validate()) return false;
  for( auto *input: members )
    if( ! input->valid || ! ( input->valid = input->validate())) valid = false;
  return valid;
}

void GroupedInputs::render( std::ostream &out ) const {
  out << "<span ";
  renderAttrs( out );
  out << ">\n";
  for( auto *input: members ) {
    input->renderLabel( out ); out << "&nbsp;"; input->render( out ); out << '\n';
  }
  out << "</span>\n";
}

Input &GroupedInputs::readonly( bool v ) {
  Input::readonly( v );
  for( auto *input: members ) input->readonly( v );
  return *this;
}

// MultiCheckBox

MultiCheckBox &MultiCheckBox::add( const Str &id, const Str &label ) {
  CheckBox *c = new CheckBox( parent, id, id );
  c->name( idPrefix() + c->name());
  GroupedInputs::add( *c );
  new Label( *c, label );
  return *this;
}

CheckBox &MultiCheckBox::operator[]( unsigned char i ) const {
  return dynamic_cast<CheckBox&>( *members[ i ] );
}

void MultiCheckBox::get( Bools &vals ) const {
  vals.resize( members.size());
  auto v = vals.begin();
  for( Input *input: members ) {
    CheckBox &cb = dynamic_cast<CheckBox&>( *input );
    cb.checked( *( v ++ ) = cb );
  }
}

void MultiCheckBox::set( const Bools &vals ) {
  if( vals.size() != members.size()) throw std::runtime_error( "MultiCheckBox::set: different sizes" );
  auto input = members.begin();
  for( bool checked: vals ) dynamic_cast<CheckBox&>( **( input ++ )).set( checked );
}

void MultiCheckBox::setOrCmp( const Bools &vals, std::set<Str> &diff ) {
  if( vals.size() != members.size()) throw std::runtime_error( "MultiCheckBox::setOrCmp: different sizes" );
  auto v = vals.begin();
  diff.clear();
  for( auto *input: members ) {
    CheckBox &cb = dynamic_cast<CheckBox&>( *input );
    cb.setOrCmp( *( v ++ )  );
// поздновато, да и не надо совсем if( cb.taken ) taken = true;
    if( cb.changed )
      diff.insert( cb.name().substr( idPrefix().size()));
  }
  changed = ! diff.empty();
}

// File

File::File( Form &parent, const Str &name ) : Input( parent, name, "file", Str()) {
  attrs[ "onchange" ] = R"(
  for( var i in this.files )
    if( this.files[ i ].size > )" + n2s( cfg.server.maxFile ) + R"( ) {
      alert( ')"
        + utl::f( parent.tr("The file is too large.\\nPlease select another file"
                  " with the size not more then {1} byte(s)."), cfg.server.maxFile )
        + R"(' );
      break;
  })";
}

Str File::filename() const {
  auto found = parent.page().request.files.find( name());
  if( found == parent.page().request.files.end()) return Str();
  return found->second.filename;
}

Str File::contentType() const {
  auto found = parent.page().request.files.find( name());
  if( found == parent.page().request.files.end()) return Str();
  return found->second.contentType;
}

// Form

Form::Form( const Page &parent )
  : ChildAndParentOf( parent ), csrf( *this, "_csrf", parent.cookie.getCsrf())
{ }

/** \brief Возвращает true если в форме нет пригодных для рендеринга виджетов.
  Специальное поле Form::csrf не считается, не считаются также виджеты, у
  которых Input::grouped равно true, за их рендеринг отвечает GroupedInputs.
*/
bool Form::empty() const {
  for( auto i = ++ /* skip csrf */ children.begin(); i != children.end(); i ++ )
    if( ! ( *i )->grouped ) return false;
  return true;
}

Str Form::tr( const char *text ) const { return parent.tr( text ); }

Str Form::generateId() const { return "id" + n2s( parent.generateId()); }

void Form::takePosts() { for( Input *input: children ) input->takePost(); }

bool Form::changed() const {
  for( const Input *input: children )
    if( input->changed ) return true;
  return false;
}

bool Form::validate() {
  valid = true;
  for( Input *input: children ) {
    if( input->grouped ) continue;
    if( ! input->valid || ! ( input->valid = input->validate())) valid = false;
  }
  return valid;
}

void Form::render( std::ostream &out ) const {
  out << "<form method=\"post\">" << csrf << "<table>\n";
  renderFromBeginToEnd( out );
  out << "</table></form>\n";
}

void Form::renderFromBeginToEnd( std::ostream &out ) const {
  auto begin = ++ children.begin(); // skip csrf
  if( begin == children.end()) return;
  render( out, **begin, *children.back());
}

void Form::render(  std::ostream &out, const Input &first, const Input &last  ) const {
  bool inRange = false;
  for( const Input *input: children ) {
    if( ! inRange ) {
      if( input == &first ) inRange = true;
      else continue;
    }
    if( inRange ) {
      render( out, *input );
      if( input == &last ) break;
    }
  }
}

void Form::render(  std::ostream &out, const Input &input  ) const {
  if( input.grouped ) return;
  out << "<tr><th>"; input.renderLabel( out ); out << "</th>"
    "<td>" << input << "</td>";
  if( input.valid ) out << '\n';
  else
    out << "<td class=\"error\">" << input.message << "</td></tr>\n";
}

void Form::renderCsrf( std::ostream &out ) const { out << csrf << '\n'; }

// end of Form

} // x2

