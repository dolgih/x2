/**
    \file pages.h
    \brief all pages

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 {

class Login : public Page {
public:
  Login( const Thread &thread );
  static const char *key() { return "login"; }

protected:
  struct TheForm : Form {
    TheForm( Page &parent );
    bool validate() override;
    // login data
    Regex name; Label nameLabel;
    Password password; Label passwordLabel;
    Submit login;
    // "forgot" data
    Email email; Label emailLabel;
    Submit forgot;

    Hidden referer;
  } form;
  dta::UserResult user;

  void load() override;
  bool login();
  void login( Str email, Str expTime, Str key );
  void renderMain() override;
  bool save() override { return login(); }

private:
  Str emailParam, expTimeParam, keyParam;

  void findUserByNameOrEmail( Str nameOrEmail );
  void mail4login( Str to );
  Str  loginKey( Str email, TimeT expTime );
};

class Notifications : public Page {
  struct TheForm : Form { TheForm( Page &parent ); Submit clear; } form;
public:
  static const char isPrivateId[];
  Notifications( const Thread &thread );
  static const char *key() { return "notifications"; }

protected:
  void load() override;
  void renderMain() override;
  bool save() override;

  bool isPrivateMessages;
  dta::NotificationsResult notifications;
};

/** Главная страница.
    Ее источник или в кэши с ключем x2::Home::key() (см. \ref home_key_subsec)
    или в таблице БД "options" в записи с идентификатором "site.home".
    \n В качестве источника может быть задано число, и тогда это идентификатор
    поста, если же строка не может быть истолкована как число, то выполняется
    x2::Response::redirect() на эту строку.
    \n Если ни в БД, ни в кэши источник не указан, будет выведена страница
    x1::predefined::page::main.
*/
class Home : public Page {
public:
  Home( const Thread &thread );
  void renderMain() override;
  /** Кроме ключа страницы, это еще и ключ кэши для
      источника этой страницы (см. \ref home_key_subsec). */
  static const char *key() { return "home"; }
};

class Search : public Page {
public:
  Search( const Thread &thread );
  static const char *key() { return "search"; }

protected:
  void load() override;
  bool validate() override;
  bool save() override;
  void renderMain() override;

  struct TheForm : Form {
    Text   query;
    Regex  date1, date2;
    Submit exec;
    Submit prev;
    Submit next;
    Hidden page;
    TheForm( Page &parent );
  } form;
  short page = 0;
  sql::Result result;
};

class Rss : public Page {
public:
  Rss( const Thread &thread );
  static const char *key() { return "rss"; }

protected:
  void render() override;
  void renderRssOfPart( Id id );
  void renderRssOfUser( Id id );
  void renderRss();
  void renderRss( Str where, Str titleAddendum, Str descrAddendum, Str link, Str atomLink );
};

class Sitemap : public Page {
public:
  Sitemap( const Thread &thread );
  static const char *key() { return "sitemap.xml"; }

protected:
  void render() override;
  void renderUrlTag( const Str &path, TimeT date );
};

class ThePage : public Page {
public:
  ThePage( const Thread &thread );
  ThePage( const Page &page, Id id );
  void renderMain() override;
  static const char *key() { return "page"; }

protected:
  Id id;
  Url theUrl;
};

class About : public Page {
public:
  About( const Thread &thread );
  void renderMain() override;
  static const char *key() { return "about"; }
};

class Contacts : public Page {
public:
  Contacts( const Thread &thread );
  void renderMain() override;
  static const char *key() { return "contacts"; }
};

class RpcJs : public Page {
public:
  RpcJs( const Thread &thread ) : Page( thread ) { }
  void render() override;
  static const char *key() { return "rpc.js"; }
  static Str include( const Str &url );
};

/**
  RPC-сервер, главный и единственный, обслуживающий любые запросы.
*/
class RpcServer : public Page {
public:
  RpcServer( const Thread &thread ) : Page( thread ) { theTitle = "RpcServer Page"; }
  static const char *key() { return "rpcServer"; }

protected:
  int jsonRpc( utl::Json query, utl::Json &answer ) override;
  void renderMain() override;

private:
  void getVisits( utl::Json params, utl::Json &answer );
  void getActivities( utl::Json params, utl::Json &answer );
};

} // x2

