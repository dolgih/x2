/**
    \file views/notifications.cpp
    \brief view of the notifications

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Notifications::TheForm

Notifications::TheForm::TheForm( Page &parent )
  : Form( parent ),
    clear( *this, tr("Remove all notifications"), "removeNotifications" )
{
  clear.title( tr("Remove all notifications"));
}

// Notifications

void Notifications::renderMain() {
  out << R"(
<style scoped>
  td, th { overflow: hidden; max-width: 20em; }
</style>
<article style="overflow:auto; padding: 0 0.5em 0.6em;">
)";
  if( notifications.empty())
    out << "<h4>" << tr("There is no notifications.") << "</h4>\n";
  else {
    out << "<table><caption><h3>";
    if( isPrivateMessages ) out << tr("Messages for");
    else out << tr("Answers&amp;Quotes to posts of");
    out << ' ' << user.name << "</h3></caption>\n"
    "<tr>";
    if( ! isPrivateMessages )
      out << "<th title=\"" << tr("What kind of notification") << "\">W</th>\n";
    out << "<th>" << tr("MsgId") << "</th><th>" << tr("Time") << "</th><th>"
        << tr("Title") << "</th><th>" << tr("Abstract") << "</th><th>"
        << tr("Author") << "</th>\n"
    "</tr>\n";

  for( const auto &n: notifications ) {
    out << "<tr class=\"notification\">\n";
    if( ! isPrivateMessages )
      out << "<td title=\"" << tr("A - answer, Q - quote") << "\">"
          << n.what() << "</td>\n";
    Str sPostId = n2s( n.postId());
    out << "<td>" << sPostId << "</td>\n"
           "<td>" << Link( urlOf<ph::Topic>( sPostId ) + "#post" + sPostId,
                           t2s( n.created()),
                           tr("View the message")) << "</td>\n"
           "<td>" << utl::htEsc( n.title()) << "</td>\n"
           "<td>" << utl::htEsc( n.abstract()) << "</td>\n"
           "<td>" << n.authorName() << "</td>\n"
    "</tr>\n";
  } // for
  out << "</table>\n"
  "<br><form method=\"post\">" << form.csrf  << form.clear << "</form>\n";
  if( notifications.recordCount() == notifications.limit())
    out << "<h3>" << tr("Remove these notifications to see more, please.") << "</h3>\n";

  } // not empty
  out << "</article>\n";
}

} // x2

