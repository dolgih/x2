/**
    \file views/ph/phorum.h
    \brief Base view for all ph pages, and main ph page simultaneously

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 { namespace ph {

/** Base Page for all ph pages */
class Base : public Page {
public:
  Base( const Thread &thread ) : Page( thread ) { }
  Base( const Page &page, const Url &url ) : Page( page, url ) { }

  static const struct Dispatcher : Page::Dispatcher {
    Dispatcher( const Page::Dispatcher &parent );
  } dispatcher;
};

/** Main ph page */
class Phorum : public Base {
public:
  Phorum( const Thread &thread );
  static const char *key() { return ""; }

protected:
  void load() override;
  void renderMain() override;
  void renderLeftPart() const;
  bool prepareItem();

  dta::ph::PartsResult parts;

  // statistics
  Id lastMessage = 0;//, lastMessageAuthor = predefined::user::anonymous;
  Str lastMessageTitle, lastMessageAuthorName;
  TimeT lastMessageTime = 0;
  int topicsNumber = 0, postsNumber = 0;

};

/**
  Page title and description.
*/
struct Headers {
  struct T { Str pr /** part */, tp /** topic */, ps /** post */; } t /** title */;
  struct D { Str pr /** part */, tp /** topic */, ps /** post */; } d /** description (summary) */;
  bool isPages;
  void init( Str prT /** part name */, Str prD /** part description */,
             Str tpT /** topic title */, Str tpD /** topic abstract */,
             Str psT /** post title */, Str psD /** post abstract */,
             bool itIsPages )
  {
    isPages = itIsPages;
    t.pr = utl::htEsc( prT ); t.tp = utl::htEsc( tpT.empty() ? "{}" : tpT );
                              t.ps = utl::htEsc( psT.empty() ? "{}" : psT );
    d.pr = utl::htEsc( prD ); d.tp = utl::htEsc( tpD.empty() ? ( tpT.empty() ? "{}" : tpT ) : tpD );
                              d.ps = utl::htEsc( psD.empty() ? ( psT.empty() ? "{}" : psT ) : psD );
  }
  Str partTitle() const { return phPr() + t.pr; }
  Str partDescr() const { return phPr() + d.pr; }
  Str topicTitle() const { return  ( isPages ? phPr() : ( partTitle() + " :: " )) + t.tp; }
  Str topicDescr() const { return partDescr() + " :: " + d.tp; }
  Str postTitle() const { return topicTitle() + ( t.ps.empty() ? "" : ( " :: " + t.ps )); }
  Str postDescr() const { return topicDescr() + ( d.ps.empty() ? "" : ( " :: " + d.ps )); }

private:
  static Str phPr() { return cfg.title + " :: "; }
};

/** Раздел форума */
class Part : public Base {
public:
  Part( const Thread &thread );
  Part( const Page &p, Id id, int pageNumber = 0 );
  static const char *key() { return "part"; }

protected:
friend class PrivatePart;
  void load() override;
  void renderMain() override;
  void renderHeader() const override;

  Id id;
  dta::ph::PartsResult part; dta::ph::TopicsResult topics;
  Headers headers;

private:
  Navigation nav;
  Url theUrl;
};

/** Раздел личной переписки */
class PrivatePart : public Base {
public:
  PrivatePart( const Thread &thread );
  void render() override;
  static const char *key() { return "privatePart"; }
};


// bbcode

struct BbCodable : bbcode::BbCodable {
  BbCodable( const Page &page ) : page( page ) { }
  utl::Json data();
protected:
  Str gettextBb( const char * msgid ) override { return page.tr( msgid ); }
  bool httpsBb() override { return page.request.proto() == "https"; }
  Str hostBb() override { return page.request.hostAsPrefix(); }
  void quoteData( long long postId, Str &postTitle, Str &authorName, bool fullUrl ) override;
  const Page &page;
};

/** Данные для рисования bbcode и эмоций */
class BbcodeData : public Base {
public:
  BbcodeData( const Thread &thread ) : Base( thread ) { }
  void render() override;
  static const char *key() { return "bbcodeData.js"; }
};

/** \brief Обработка шаблонов.

    Шаблон должен обрабатывать только заглавные статьи раздела
    x1::predefined::part::pages, когда format == dta::ph::fTemplate.
    Для тестирования этих условий предназначен метод fit().
*/
struct Template {
  Template( const Page &page ) : page( page ) { }
  static bool fit( Id postId, Id topicId, Id partId, const Str &format );
  Str toHtml( const Str &content );

  static Str tr( const char *text );

protected:
  // handlers
  Str templateHandlerPost( const std::map<Str, Str> &params );
  Str templateHandlerTopic( const std::map<Str, Str> &params );
  Str templateHandlerLastComments( const std::map<Str, Str> &params );
  typedef Str ( Template::*TmplHandler )( const std::map<Str, Str> &params );
  static std::map<Str, TmplHandler> tmplHandlers;
  // end of handlers

  const Page &page;
};

/** Базовая структура для страниц веток и страниц  редактирования постов */
class BasePost : public Base {
protected:
  BasePost( const Thread &thread, bool newPost );
  BasePost( const Page &page, const Url &url );

  void renderPost() const;
  void renderEditPost() const;
  void renderLeftPart() const;
  void renderRightPart() const;
  void renderHeader() const override;
  virtual void renderSubHeader() const;

  struct EditForm : Form {
    Text     title;
    TextArea abstract;
    TextArea content;
    Select   format;
    Regex    pinned;
    Hidden   author2;
    Submit   submit;
    Submit   preview;
    CheckBox setAsHome;
    /*File     attachment;*/
    EditForm( Page& page );
    bool validate() override;
    void setAutofocus( Input &input ) { input.autofocus( true ); }
    void setTitle( Str aTitle );
  } form;

  bool canClose() const       { return topic.canClose( part, user ); }
  bool canAnswer() const      { return topic.canAnswer( part ); }
  bool canMoveToTrash() const { return topic.canMoveToTrash( part, user ); }

  bool isStartTopic() const   { return postId == topicId; }

  Id postId = 0, topicId = 0, partId = 0;

  dta::ph::PartsResult part; dta::ph::TopicsResult topic; dta::ph::PostsResult post;

  bool newPrivateMessage = false, preview = false;
  Id author = 0, author2 = 0;
  Str authorName, authorName2;

  Headers headers;
};

/** Тема (ветка) форума */
class Topic : public BasePost {
public:
  Topic( const Thread &thread );
  Topic( const Page &p, Id postId, Id topicId = 0, int pageNumber = 0 );
  static const char *key() { return "topic"; }

protected:
friend class x2::ThePage;
  void load() override;
  void renderMain() override;

  dta::ph::PostsResult &posts;

private:
  Navigation nav;
  Url theUrl;
};

class Post : public BasePost {
public:
  Post( const Thread &thread );
  static const char *key() { return "post"; }

protected:
  void load() override;
  void renderMain() override;
  void renderSubHeader() const override;

  bool validate() override;
  bool save() override;

private:
  void generateNotification( Id messageId, bool doNotMail, dta::Users2rise &users2rise );

  Id correspTopic = 0; // corresponding topic of the private message
};

/** The helper class for deletetng posts of a part or of an author or one post(topic) */
struct Removing {
  Removing( sql::PhSession &sql, Id user ) : sql( sql ), user( user ) { }
  void of( int part, int author, int post );
  void rise();

private:
  sql::Session &sql;
  Id user;
  void crTbl( Str tblName, Str id, Str from );
  std::set<Id> updatedAuthors, updatedTopics, updatedParts;
};

/** Удаление поста или ветки */
class Remove : public Base {
public:
  Remove( const Thread &thread );
  static const char *key() { return "remove"; }

protected:
  void load() override;
  bool save() override;
  void render() override;

  Removing rm;
  Id id = 0 /* post or topic to remove */;
  dta::ph::PartsResult part; dta::ph::TopicsResult topic; dta::ph::PostsResult post;
};

/** Удаление заметки */
class Delivered : public Base {
public:
  Delivered( const Thread &thread );
  static const char *key() { return "delivered"; }

protected:
  bool save() override;

  Id id = 0 /* post or topic to remove notification */;
};

/** Закрыти или повторное окрытие ветки */
class Close : public Base {
public:
  Close( const Thread &thread );
  static const char *key() { return "close"; }

protected:
  bool save() override;

  Id id = 0 /* topic to close */;
};

/** Перемещение ветки в корзину */
class Trash : public Base {
public:
  Trash( const Thread &thread );
  static const char *key() { return "trash"; }

protected:
  bool save() override;
  void render() override;

  Id id = 0 /* topic to trash */;
};

}} // ph, x2

