/**
    \file views/ph/phorum.cpp
    \brief Base view for all ph pages, and main ph page simultaneously

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace ph {

// Phorum

void Phorum::renderMain() {
  try {
    out << "<article>\n";

    if( parts.empty())
      out << "<h4>" << tr("Empty Phorum (There are probably errors).") << "</h4>\n";
    else

    while( parts.next()) if( prepareItem()) {
      out << "<div class=\"partdiv\" ";
      Str lM = n2s( parts.lastMessage()), lMT = t2s( parts.lastMessageTime());
      if( parts.id() == predefined::part::announcements ) {
        out << "style=\"display:block;\">\n";
        renderLeftPart();
        out << Link( urlOf<Topic>( lM ),
          "<h3>" + parts.lastMessageTitle() + "</h3><h5>" + tr("at") + ' ' + lMT
          + ' ' + tr("by") + ' ' + parts.lastMessageAuthorName() + "</h5>\n",
          tr("Go to last announcement"));
      } else {
        out << ">\n"; // end of partdiv open tag
        renderLeftPart();
        out << "<div class=\"partright\">\n";
        if( parts.lastMessage()) {
          out << Link( urlOf<Topic>( lM ) + "/#post" + lM,
            "<h5>" + parts.lastMessageTitle() + "<br>" + tr("at") + ' ' + lMT + "</h5>\n",
            tr("Go to last message"))
          << Link( urlOf<UserProfile>( parts.lastMessageAuthor()),
            tr("by") + ' ' + parts.lastMessageAuthorName(),
            tr("Go to user profile"));
        }
        out << "</div><!--partright-->\n";
      }
      out << "</div> <!--partdiv-->\n";
    }
    if( user.isAdmin ) {
      out << "<h4>" << Link( urlOf<adm::Part>( 0 ), tr("Create new part"),
        tr("Create new part")).cssClass( "inbrackets" )
      << "</h4>\n";
    }
    out << "<div><h3>" << tr("Common X2 Phorum data") << "</h3>\n"
    << utl::F( tr("Topics: {1}; posts: {2};" )) % topicsNumber % postsNumber
    << "<br>\n"
    << Link( urlOf<Topic>( lastMessage ),
             "<h5>" + tr("Last Message") + ' ' + t2s( lastMessageTime )
             + ": " + lastMessageTitle + "</h5>\n",
             tr("Go to last message"))
    << "</div>\n"
    << "</article>\n";
    renderSunflower();
  } catch (const std::exception& e) { CAUGHT_EXCEPTION( e ); }
}

void Phorum::renderLeftPart() const {
  const dta::ph::PartsResult &r = parts;

  Str id = n2s( r.id());
  out << "<div class=\"partleft\">\n"
    << Link( urlOf<Part>( id ),
       "<h2>" + r.name() + "</h2>", r.description())
    << "<p>" << r.description() << "</p>\n"
    << Link( urlOf<Rss>( "part", id ), "RSS", tr("Get Rss feed"))
    << "&nbsp;";
    if ( r.canEdit( user )) {
      out << Link( urlOf<adm::Part>( id ),
        tr("Edit this part"),
        tr("Edit this part")).cssClass( "inbrackets" );
      if( r.id() == predefined::part::bin )
        out << "&nbsp;" << Link( urlOf<adm::ClearPart>( id ),
          tr("Clear Bin"), tr("Clear Bin")).cssClass( "inbrackets" );
    }
    out << "&nbsp;<span title=\"" << tr("Topics/Posts in the part") << "\">"
    << r.topicsNumber() << '/' << r.postsNumber() << "</span>\n" <<
  "</div><!--partleft-->\n";
}

}} // ph, x2

