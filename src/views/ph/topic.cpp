/**
    \file views/ph/topic.cpp
    \brief View of the topic of phorum

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace ph {

// bbcodeData

void BbcodeData::render() {
  response.setContentType( "text/javascript; charset=UTF-8" );
  static LastModified lastModified;
  if( lastModified.notModified( *this ))
    return;
  out << "var bbcodeData=" <<  BbCodable( *this ).data().str( true ) << ';';
}

// BasePost::EditForm

BasePost::EditForm::EditForm( Page &page )
  : Form( page ),
  title      ( *this, "title" ),
  abstract   ( *this, "abstract" ),
  content    ( *this, "content" ),
  format     ( *this, "format", dta::ph::fBbcode ),
  pinned     ( *this, "pinned", dta::ph::TopicsResult::pinnedRegex()),
  author2    ( *this, "author2" ),
  submit     ( *this, tr("Submit"), "submit" ),
  preview    ( *this, tr("Preview"), "preview" ),
  setAsHome  ( *this, "setAsHome", "setAsHome" )/*,
  attachment ( *this, "attachment" )*/
{
  title.message = tr("The title cannot be empty");
  abstract.message = utl::f( tr("The abstract is too big, max size = {1}"),
    cfg.message.maxAbstract );
  abstract.id( "abstract" ); abstract.rows( "2" ).cols( "55" );
  content.message = tr("The content cannot be empty");
  content.id ( "content" ); content.rows( "12" ).cols( "75" );
    content.required( true );
  format.id  ( "format" );
    format.add( dta::ph::fBbcode ).add( dta::ph::fMarkdown ).add( dta::ph::fText );
  pinned.message = tr("The pinned must be from 'A' to 'Z' or empty");
    pinned.size( "1" );
};

bool BasePost::EditForm::validate() {
  if( utl::trim( title ).empty())
    title.valid = false;
  if( utl::trim( content ).empty())
    content.valid = false;

  // size limits
  if( abstract.get().size() > cfg.message.maxAbstract )
    abstract.valid = false;
  // content size limit
  size_t messageLimit = cfg.message.maxSize.user;
  if( parent.user.isAnonymous )  messageLimit = cfg.message.maxSize.anonymous;
  else if( parent.user.isAdmin ) messageLimit = cfg.message.maxSize.admin;
  if( content.get().size() > messageLimit ) {
    content.valid = false;
    content.message = utl::f( tr("The content is too big, max size = {1}"), messageLimit );
  }

  return Form::validate();
}

void BasePost::EditForm::setTitle( Str aTitle ) {
  if( aTitle.empty())
    return;
  if(  utl::trim( title ).empty()) {
    title.set( "Re: " + aTitle );
    title.valid = true;
  }
}

// BasePost

void BasePost::renderPost() const {
  out << "<!--Post Rendering-->\n";
  if( headers.isPages && isStartTopic())
    renderRightPart();
  else {
    out << "<div class=\"postdiv\" id=\"post" + n2s( postId ) + "\">";
    renderLeftPart();
    renderRightPart();
    out << "</div><!--postdiv-->\n";
  }
  out << "<!--End of Post Rendering-->\n";
}

void BasePost::renderEditPost() const {
  out << "<!--Edit Post-->\n"
         "<form method=\"post\" enctype=\"multipart/form-data\" action=\""
  << urlOf<ph::Post>( postId, topicId, partId ) << "\">" << form.csrf
  << "<p><strong>" << tr("Title") << "</strong></p>\n"
  "<p>" << form.title << "</p>\n"
  "<details ";
  if( ! form.abstract.get().empty()) out << "open";
  out << ">\n"
  "<summary title=\"" << tr("Open(close) summary")
  << "\" onclick=\"editor.abstract.toggle();\">"
  "<strong>" << tr("Summary") << "</strong></summary><br>\n"
  << form.abstract
  << "</details>\n"
  << "<br><strong>" << tr("Detail") << "</strong><br>\n"
  << form.content << "<br><br>\n"
  << form.submit
  << form.preview
  << form.format;
  if( part.rights.M() && ! part.isPrivate())
    out << tr("pinned:") << ' ' << form.pinned;
  if( partId == predefined::part::pages && isStartTopic() && postId )
    out << " <label>" << tr("Set as Home Page:") << ' ' << form.setAsHome << "</label>\n";
  out /*<< "<br>" << tr("Attachment") << ' ' << form.attachment << '\n'*/ << form.author2
  << "</form>\n";
  out << R"(<script type="text/javascript">
  var media=")" << cfg.media << R"(";
</script>
<script type="text/javascript" src=")" << urlOf<ph::BbcodeData>() << R"("></script>
<script type="text/javascript" src=")" << cfg.js + "/editor.js" << R"("></script>
<!--End of Edit Post-->
)";
}

void BasePost::renderLeftPart() const {
  int authorPostsNumber = 0;
  utl::Json authorProperties;
  if( postId ) {
    authorPostsNumber = post.authorPostsNumber();
    authorProperties = post.authorProperties();
  }

  out << "<!--Left Rendering-->\n"
  "<div class=\"postleft\">\n"
  << Link( urlOf<UserProfile>( author ),
           "<span style=\"font-size:50%\">" + tr("by")
           + " </span><div style=\"word-break:break-all;display:inline-block\">"
           + authorName + "</div>",
           tr("The author of the message"));
  if( author == predefined::user::admin )
    out << Img( cfg.media + "/badge.png", tr("Admin badge"))
              .title( tr("The user is the admin")).style( "width:1em;height:1em;" );
  out << "<span style=\"font-size:50%\">(" << authorPostsNumber << "&nbsp;"
      << tr("post(s)") << ")</span>\n";
  renderAvatar( authorProperties[ "avatar" ], authorName );
  Str webSite = authorProperties[ "web_site" ];
  if( ! webSite.empty())
    out << Link( webSite,
                 Img( cfg.media + "/internet.png", tr("Web site")  )
                    .style( "width:2em;height:2em;" ).html(),
                 tr("Open the authors web site")).target( "_blank" );
  renderCorrespondence( author, 2 );
  renderRss( author );
  renderOnlineIndicator( postId );
  out << "</div><!--postleft-->\n<!--End of Left Rendering-->\n";
}

void BasePost::renderRightPart() const {
  Str title, abstract, content, format, created, modified;
  if( preview ) {
    form.title.get( title ); form.abstract.get( abstract ); form.content.get( content );
    form.format.get( format );
  } else {
    title = post.title(); abstract = post.abstract(); content = post.content();
    format = post.format();
    created = t2s( post.created()); modified = t2s( post.modified());
  }

  out << "<!--Right Rendering-->\n"
  "<div class=\"";
  if( headers.isPages && isStartTopic())
    out << "page";
  else
    out << "postright";
  out << "\">\n";
  if( part.isPrivate() && user.id == author )
    out << "<h3>(" << tr("Message to") << ' ' << authorName2 << ")</h3>\n";
  if( ! isStartTopic() || headers.isPages ) {
    if( postId )
      out << Link( urlOf<ph::Topic>( postId ) + utl::f( "/#post{1}", postId ),
                   "<h3 class=\"postTitle\">" + title + "</h3>",
                   tr("Link to") + " &quot;" +  title + "&quot;" );
    else
      out << "<h3 class=\"postTitle\">" << utl::htEsc( title ) << "</h3>\n";
  }
  out << "<h4 class=\"postAbstract\">" << utl::htEsc( abstract )
      << "</h4><div class=\"postContent\">\n";
  if( format == dta::ph::fText ) out << "<pre>" << utl::htEsc( content ) << "</pre>\n";
  else if( format == dta::ph::fMarkdown ) out << xss( markdown( content )) << '\n';
  else if( format == dta::ph::fBbcode ) out << bbcode( content ) << '\n';
  else if( format == dta::ph::fHtml ) out << content << '\n';
  else if( Template::fit( postId, topicId, partId, format )) out
    << Template( *this ).toHtml( content ) << '\n';
  else out << "format error";
  out << "</div><!-- end of postContent -->\n<h5 class=\"postTime\">\n"
    "<span style=\"font-size:70%\">" << tr("at") << " </span>\n"
    "<span title=\"" << tr("The time of the message") << "\">"
      << created << "</span>\n";
    if( postId && canAnswer())
      out << Link( urlOf<ph::Post>( post.quoteId( postId ), topicId, partId ),
                   tr("Quote"),
                   tr("Quote") + " &quot;" + title + "&quot;"
             ).cssClass( "inbrackets" )
              .onclick( "editor.quote.click(event,this," + n2s( postId ) + ')' );
    if( post.canEdit( part, topic, user ))
      out << Link( urlOf<ph::Post>( postId ),
                   tr("Edit"),
                   tr("Edit") + " &quot;" + title + "&quot;"
             ).cssClass( "inbrackets" );
    if( post.canRemove( part, topic, user ))
      out << Link( urlOf<ph::Remove>( postId ),
                   tr("Remove"),
                   tr("Remove") + " &quot;" + title + "&quot;"
             ).cssClass( "inbrackets" );
    if( user.hasNotification( postId ))
      renderLinkButton( urlOf<ph::Delivered>( postId ) ,
                        part.isPrivate() ? tr("Delivered") : tr("has been read"),
                        tr("Delete notification of this message"));
  out << "</h5><!-- end of postTime -->\n";
  if( post.isModified( partId ))
    out << "<p>\n"
    << tr("Last edit at") << " <span title=\""
    << tr("The time of the last message modification") << "\">"
    << modified << "</span>\n"
    << tr("by") << ' ' << authorName2
    << "</p>\n";
  out << "</div><!--postright-->\n<!--End of Right Rendering-->\n";
}

void BasePost::renderHeader() const {
  out << "<!--header()-->\n"
  << Link( urlOf<Home>(), cfg.title , cfg.title ) << " :: ";
  if( headers.isPages ) out << headers.t.tp; else renderSubHeader();
  out << "<!--end of header()-->\n";
}

void BasePost::renderSubHeader() const {
  /* the subHeader for the topic, the subHeader for the post will be overriden */
  if( ! headers.isPages ) {
    out << "<!--subHeader()-->\n" << Link( urlOf<ph::Part>( partId ),
                 headers.t.pr, headers.d.pr );
    if( ! headers.t.tp.empty())
      out << " :: " << Link( urlOf<ph::Topic>( 0, topicId ),
                 headers.t.tp, headers.d.tp );
    out << "<!--end of subHeader()-->\n";
  }
}

// Topic

void Topic::renderMain() {

  out << "<article>\n";
  nav.render();
  out << "<h1>"; renderSubHeader(); out << "</h1>\n";

  if( posts.empty())
    out << "<h4>" << tr("Empty Topic.") << "</h4>\n";
  else

  while( posts.next()) {
    postId  = posts.id();
    author  = posts.author();  authorName  = posts.authorName();
    author2 = posts.author2(); authorName2 = posts.authorName2();
    renderPost();
  }

  out << "<h5>\n";
  if( canClose())
    renderLinkButton( urlOf<ph::Close>( topic.id()),
                 topic.closed() ? tr("Reopen Topic") : tr("Close Topic"),
                 Str());
  if( canMoveToTrash())
    out << Link( urlOf<ph::Trash>( topic.id()),
                 tr("Move to Trash"),
                 Str()).cssClass( "inbrackets" );
  out << "</h5>\n";
  if( canAnswer()) {
    out << "<h5>" << Link( urlOf<ph::Post>( 0, topic.id()),
                 tr("Create Answer"),
                 tr("Create Answer")).cssClass( "inbrackets" )
    << "</h5>\n"
    "<h2>" << tr("Create Answer") << "</h2>\n";
    postId = 0;
    renderEditPost();
  }
  nav.render();
  out << "</article>\n";
  renderSunflower();
  out << RpcJs::include( urlOf<RpcServer>())
  << R"(<script type="text/javascript">
  function getActivities( r, a ) {
    var authors = {)";

  Str authors, postAuthors;
  for( posts.rewind(); posts.next(); ) {
    if( ! authors.empty()) { authors += ','; postAuthors += ','; }
    authors += n2s( posts.author());
    postAuthors += n2s( posts.id()) + ':' + n2s( posts.author());
  }

  out << postAuthors << R"(};
    for( var postId in authors ) {
      var span = document.getElementById( "onlineIndicator" + postId );
      if( span )
        span.style.display =  r[ authors[ postId ] ][ "isActive" ] ? "" : "none";
    }
  }
  function updateActivities(evt) {
    jsonRpc.run( "getActivities", [[)" << authors << R"(]] );
  }
  window.setInterval( updateActivities, 300000 );
  updateActivities();
</script>
)";
}

}} // ph, x2

