/**
    \file views/ph/part.cpp
    \brief View of the part of phorum

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace ph {

// Part

void Part::renderMain() {

  out << "<article>\n";
  nav.render();
  out << "<h2>" << part.name() << "</h2>\n"
  "<p>" << part.description() << "</p><br>\n";
  if( topics.empty())
    out << "<h4>" << tr("Empty Part.") << "</h4>\n";
  else

  while( topics.next()) {
    Str topicId = n2s( topics.id());
    out << "<div class=\"topicdiv\">\n"
    "<div class=\"topicleft\">\n"
    << Link( urlOf<ph::Topic>( 0, topicId ),
             "<h3>" + topics.title() + "</h3>\n",
             tr("Go to the topic"))
    << "<p>" << topics.abstract() << "</p>\n"
    << Link( urlOf<UserProfile>() + '/' + n2s( topics.author()),
             tr("by") + ' ' + topics.authorName() + ';',
             tr("The author of this topic"))
    << "&nbsp;<span title=\"" + tr("Visits/Posts in the topic")
      + "\"><span id=\"topicVisits" + topicId + "\"></span>/"
      + n2s( topics.postsNumber()) + ";</span>\n";
    if( ! topics.pinned().empty())
      out << "&nbsp;<span title=\"" << tr("This topic is fixed") << "\">"
      << tr("(Pinned)") << ";</span>\n";
    if( topics.closed())
      out << "&nbsp;<span title=\"" << tr("This topic is closed") << "\">"
      << tr("(Closed)") << "</span>\n";
    out << "</div><!--topicleft-->\n"
    << "<div class=\"topicright\">\n"
    << Link( urlOf<ph::Topic>( topics.lastPost()) + utl::f( "/#post{1}", topics.lastPost()),
             "<h5>" + topics.lastPostTitle() + "<br>\n"
             + tr("at") + ' ' + t2s( topics.lastPostTime()) + "</h5>\n",
             tr("Go to last message"))
    << Link( urlOf<UserProfile>( topics.lastPostAuthor()),
             tr("by") + ' ' + topics.lastPostAuthorName(),
             tr("Go to user profile"))
    << "</div><!--topicright-->\n"
    "</div><!--topicdiv-->\n";
  }
  if ( part.rights.C()) {
    out << "<h5>" << Link( part.isPrivate() ? urlOf<adm::Admin>() :
                             urlOf<ph::Post>( 0, 0, part.id()),
                           tr("Create Topic"),
                           tr("Create Topic")).cssClass( "inbrackets" )
    << "</h5>\n";
  }
  nav.render();
  out << "</article>\n";
  renderSunflower();
  out << RpcJs::include( urlOf<RpcServer>())
  << R"(
<script type="text/javascript">
  function getVisits( r, a ) {
    for (var topicId in r)
      document.getElementById( "topicVisits" + topicId ).textContent = r[ topicId ];
  }
  function updateVisits(evt) {
    jsonRpc.run( "getVisits", [[)";
  for( int c = topics.recordCount(), i = 0; i < c; i ++ ) {
    topics.record( i );
    out << topics.id();
    if( i + 1 != c ) out << ',';
  }
  out << R"(]] );
  }
  window.setInterval( updateVisits, 300000 );
  updateVisits();
</script>
)";
}

void Part::renderHeader() const
{
  out << Link( urlOf<Home>(), cfg.title , cfg.title ) << " :: " << headers.t.pr;
}

}} // ph, x2

