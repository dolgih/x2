/**
    \file views/ph/post.cpp
    \brief View of the post of phorum

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace ph {

// Post

void Post::renderMain() {

  out << "<article>\n"
      << "<h3>"; renderSubHeader(); out << "</h3>\n";
  if( form.preview ) {
     out << "<h1>Post preview</h1>\n";
    renderPost();
  }
  out << "<h1>\n";
    if( postId ) out << tr("Post to edit"); else out << tr("Compose a post");
    if( partId < 0 ) out << ' ' << tr("for User") << ' ' << authorName2;
  out << "</h1>\n";
  if( ! form.pinned.valid )
    out << "<p class=\"error\">" << form.pinned.message   << "</p>\n";
  if( ! form.title.valid )
    out << "<p class=\"error\">" << form.title.message    << "</p>\n";
  if( ! form.abstract.valid )
    out << "<p class=\"error\">" << form.abstract.message << "</p>\n";
  if( ! form.content.valid )
    out << "<p class=\"error\">" << form.content.message  << "</p>\n";
  renderEditPost();
  out << "</article>\n";
  renderSunflower();
}

void Post::renderSubHeader() const {
  out << "<!--post subHeader()-->\n"
      << Link( urlOf<ph::Part>( partId ), headers.t.pr, headers.d.pr );
  if( ! headers.t.tp.empty())
    out << " :: " << Link( urlOf<ph::Topic>( 0, topicId ), headers.t.tp, headers.d.tp );
  if( ! headers.t.ps.empty())
    out << " :: <span title=\"" << headers.d.ps << "\">"
        << headers.t.ps << "</span>\n";
  out << "<!--end of post subHeader()-->\n";
}

}} // ph, x2

