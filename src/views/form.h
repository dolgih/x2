/**
    \file views/form.h
    \brief Form & widgets

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 {

/** my Tree */

template <class Parent, class Self> struct ChildOf {
  ChildOf( const Parent &aParent ) : parent( (Parent&)aParent ) { parent.add( (Self*)this ); }
  virtual ~ChildOf() { parent.del( (Self*)this ); }
protected:
  Parent &parent;
};

template <class Child> struct ParentOf {
  virtual ~ParentOf() { while( ! children.empty()) delete children.front(); }
  void add( Child *child ) { children.emplace_back( child ); }
  void del( Child *child ) { children.remove( child ); }
protected:
  list<Child*> children;
};

template <class P, class C, class Self>
struct ChildAndParentOf : ParentOf<C>, ChildOf<P,Self> {
  ChildAndParentOf( const P &parent ) : ChildOf<P,Self>( parent ) { }
};

/** DOM (HTML) Element */

struct Dom {
  Dom( const char *tag ) : tag( tag ) { }
  virtual void render( std::ostream &out ) const;
  void renderOpen( std::ostream &out ) const;
  void renderClose( std::ostream &out ) const;
  void renderAttrs( std::ostream &out ) const;
  Str html() const;

#define DEFINEATTR0( n, t ) \
  const Str &n() const { return getAttr(t); } \
  auto n( const Str &v ) -> decltype( *this ) & { attrs[t] = v; return *this; }
#define DEFINEATTR( n ) DEFINEATTR0( n, #n )
#define DEFINEBOOLATTR( n ) \
  bool n() const { return ! getAttr( #n ).empty(); } \
  auto n( bool v ) -> decltype( *this ) & { if( v ) attrs[ #n ] = #n; else attrs.erase( #n ); return *this; }

  DEFINEATTR(id)
  DEFINEATTR(title)
  DEFINEATTR0(cssClass,"class")
  DEFINEATTR(style)
  DEFINEATTR(onclick)

  const Str &getAttr( const Str &n ) const {
    auto f = attrs.find(n);
    static const Str empty;
    if( f == attrs.end()) return empty; else return f->second;
  }

  virtual ~Dom() { }

  const char *const tag;
  std::map<Str,Str> attrs;
};

inline std::ostream &operator << ( std::ostream &out, const Dom &dom ) {
  dom.render( out ); return out;
}

/** Img */

struct Img : Dom {
  Img( const Str &src, const Str &alt );
};

/** Link */

struct Link : Dom {
  Link( const Str &href, const Str &value, const Str &title );
  void render( std::ostream &out ) const override;
  DEFINEATTR(target)
  Str value;
};

/** Label */

class Input;

struct Label : Dom {
  Label( Input &input, const Str &value );
  void render( std::ostream &out ) const override;

  DEFINEATTR0(fr,"for")

  ~Label() override;

  Str value;

protected:
  Input &input;
};

/** Base Input */

class Form;

struct Input : Dom, ChildOf<Form,Input> {
  Input( Form &parent, const Str &name, const char *type, const Str &value );
  void takePost();
  virtual bool validate() { return true; }
  void render( std::ostream &out ) const override;
  void renderLabel( std::ostream &out ) const;

  bool readonly() const;
  virtual Input &readonly( bool v );

  DEFINEATTR(type)
  DEFINEATTR(name)
  DEFINEATTR(size)
  DEFINEATTR(maxlength)
  DEFINEATTR(placeholder)
  DEFINEBOOLATTR(autofocus)
  DEFINEBOOLATTR(required)

  void get( int &v )           const { v = utl::s2i( value ); }
  void get( long &v )          const { v = utl::s2l( value ); }
  void get( unsigned long &v ) const { v = utl::s2ul( value ); }
  void get( Str &v )           const { v = value; }
  const Str &get()             const { return value; }
  template <class T> T get()   const { T v; get( v ); return v; }
  operator const Str&()        const { return value; }

  int           set( int v )           { value = n2s( v ); return v; }
  long          set( long v )          { value = n2s( v ); return v; }
  unsigned long set( unsigned long v ) { value = n2s( v ); return v; }
  const Str    &set( const Str &v )    { value = v; return v; }

  template<class T> void setOrCmp( T v ) {
    if( taken ) changed = v != get<T>();
    else set( v );
  }

  const Str &generateId();
  ~Input() override;

  Str message;
  bool valid   = true,
       taken   = false /* значение получено из post */,
       changed = false,
       grouped = false /* не рендерить, поле является составляющим группы */;

protected:
  Str value;

  Input( Form &parent, const Str &name, const Str &value );

friend class Label;
  Label *label = 0;
};

/** Submit */
struct Submit: Input {
  Submit( Form &parent, const Str &initialValue, const Str &name = Str());
  void render( std::ostream &out ) const override {
    const_cast<Str&>( value ) = initialValue; /* Zorro */
    Input::render( out );
  }

  DEFINEATTR(formaction)

  void get( bool &v ) const { v = get(); }
  bool get() const { return value == initialValue; }
  operator bool() const { return get(); }

  const Str initialValue;
};

/** Button */
struct Button: Input {
  Button( Form &parent, const Str &value, const Str &name = Str(),
    const Str &initialValue = Str());
  void render( std::ostream &out ) const override;

  DEFINEATTR(formaction)

  void get( bool &v ) const { v = get(); }
  bool get() const { return value == initialValue; }
  operator bool() const { return get(); }

  const Str initialValue;
};

/** Text */
struct Text: Input {
  Text( Form &parent, const Str &name, const Str &value = Str()) :
    Input( parent, name, "text", value ) { }
};

/** Regex */
struct Regex: Text {
  Regex( Form &parent, const Str &name, const Str &regex, const Str &value = Str()) :
    Text( parent, name, value ), regex( regex ) { }
  bool validate() override;
protected:
  Str regex;
};

/** TextArea */
struct TextArea: Input {
  TextArea( Form &parent, const Str &name, const Str &value = Str()) :
    Input( parent, name, value ) { }
  void render( std::ostream &out ) const override;
  DEFINEATTR(cols)
  DEFINEATTR(rows)
};

/** Password */
struct Password: Input {
  Password( Form &parent, const Str &name, const Str &value = Str()) :
    Input( parent, name, "password", value ) { }
};

/** Hidden */
struct Hidden: Input {
  Hidden( Form &parent, const Str &name, const Str &value = Str()) :
    Input( parent, name, "hidden", value ) { }
};

/** Email */
struct Email: Input {
  Email( Form &parent, const Str &name, const Str &value = Str()) :
    Input( parent, name, "email", value ) { }
};

/** UrlField */
struct UrlField: Input {
  UrlField( Form &parent, const Str &name, const Str &value = Str()) :
    Input( parent, name, "url", value ) { }
};

/** Number */
struct Number: Input {
  Number( Form &parent, const Str &name, const Str &value = Str()) :
    Input( parent, name, "number", value ) { }
  DEFINEATTR(min)
  DEFINEATTR(max)
  DEFINEATTR(step)
};

/** CheckBox */
struct CheckBox: Submit {
  CheckBox( Form &parent, const Str &initialValue = Str(), const Str &name = Str());
  DEFINEBOOLATTR(checked)

  bool set( bool v ) { checked( v ); return v; }
  void setOrCmp( bool v ) {
  /** Как и в MultiCheckBox и в Select вызывает set для подготовки
      повторного рендеринга. */
    if( taken ) changed = v != set( get());
    else set( v );
  }
};

/** Select */
struct Select: Input {
  using Input::get; using Input::setOrCmp;
  Select( Form &parent, const Str &name, const Str &value = Str())
    : Input( parent, name, value ) { }
  DEFINEATTR(size)
  DEFINEBOOLATTR(multiple)
  Select &add( const Str &v ) { return add( v, v ); }
  Select &add( const Str &id, const Str &v ) { options.emplace_back( Item( id, v )); return *this; }
  Select &add( Id id, const Str &v ) { return add( n2s( id ), v ); }
  /** Возвращает список выбранных идентификаторов, так как их возвращает браузер */
  void get( std::set<Str> &v ) const;
  /** В diff возвращает идентификаторы измененных пользователем опций, в vals получает
      первоначальные значения (отмеченные опции) для сравнения,
      заодно значения из браузера подготавливаются для повторного
      рендеринга. */
  void getDiff( const Bools &vals, std::set<Str> &diff );
  /** Устанавливает выбранные (переданные в vals). */
  void set( const Bools &vals );
  /** Устанавливает выбранные или сравнивает с теми, что вернул браузер (в случае
      обработки поста), в diff возвращает идентификаторы измененных пользователем
      опций, вызывает getDiff или set для выполнения. */
  void setOrCmp( const Bools &vals, std::set<Str> &diff );

  void render( std::ostream &out ) const override;

  struct Item {
    Str id, value; bool selected = false;
    Item( const Str &id, const Str &value )
      : id( id ), value( value ) { }
  };
  list<Item> options;
};

/*
type="date"
type="month"
type="week"
type="time"
type="datetime"
type="datetime-local"
*/

/** GroupedInputs */
struct GroupedInputs: Input {
  GroupedInputs( Form &parent, const Str &name ) : Input( parent, name, Str()) { }
  GroupedInputs &add( Input &i );
  bool validate() override;
  void render( std::ostream &out ) const override;

  using Input::readonly;
  Input &readonly( bool v ) override;

protected:
  std::vector<Input*> members;
};

/** MultiCheckBox */

struct MultiCheckBox: GroupedInputs {
  MultiCheckBox( Form &parent, const Str &name ) : GroupedInputs( parent, name ) { }

  MultiCheckBox &add( const Str &label ) { return add( Str(), label ); }
  MultiCheckBox &add( const Str &id, const Str &label );
  MultiCheckBox &add( Id id, const Str &label ) { return add( n2s( id ), label ); }
  CheckBox &operator[]( unsigned char i /* чтобы ограничить маленьким числом */ ) const;
  size_t size() const { return members.size(); }
  bool empty() const { return members.empty(); }
  /** Возвращает вектор значений всех checkbox'ов, заодно значения из браузера
      подготавливаются для повторного рендеринга. */
  void get( Bools &vals ) const;
  /** Устанавливает выбранные (переданные в vals). */
  void set( const Bools &vals );
  /** Устанавливает выбранные или сравнивает с теми, что вернул браузер (в случае
      обработки поста), в diff возвращает идентификаторы измененных пользователем
      опций, вызывает getDiff или set для выполнения. */
  void setOrCmp( const Bools &vals, std::set<Str> &diff );

protected:
  Str idPrefix() const { return id() + "CheckBox"; }
};

/** File */
struct File: Input {
  File( Form &parent, const Str &name );
  DEFINEATTR(accept)
  Str filename() const;
  Str contentType() const;
};

#undef DEFINEATTR
#undef DEFINEATTR0
#undef DEFINEBOOLATTR

/** Base Form */

class Page;

struct Form : ChildAndParentOf<Page,Input,Form> {
  Form( const Page &parent );
  bool empty() const;
  const Page &page() const { return parent; }
  Str tr( const char *text ) const;
  Str generateId() const;
  void takePosts();
  bool changed() const;
  virtual bool validate();

  virtual void render( std::ostream &out ) const;
  void renderFromBeginToEnd( std::ostream &out ) const;
  void render( std::ostream &out, const Input &first, const Input &last ) const;
  void render( std::ostream &out, const Input &input ) const;
  void renderCsrf( std::ostream &out ) const;

  virtual ~Form() { }

  bool valid = true;

  Hidden csrf;
};

inline std::ostream &operator << ( std::ostream &out, const Form &form ) {
  form.render( out ); return out;
}

} // x2

