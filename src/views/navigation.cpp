/**
    \file views/navigation.cpp
    \brief Navigation line

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

Str Navigation::urlForPage( int page ) const { return utl::f( urlForPageFormat , page ); }

void Navigation::render() const {
  out << "<!--Page Navigtion-->\n";
  if( canLeftOrRight()) {
    out << "<h5>\n";
    if( canLeft())
      out << Link( urlForPage( 0 ) , "[&lt;&lt;]", tr("Go to first page"))
          << Link( urlForPage( leftPage()), "[&lt;]", tr("Go to previous page"));
    out << tr("Page") << ' ' << n2s( currPage + 1 ) << ' '
        << tr("of") << ' ' << n2s( maxPage + 1 ) << ' ';
    if ( canRight())
      out << Link( urlForPage( rightPage()), "[&gt;]", tr("Go to next page"))
          << Link( urlForPage( maxPage ), "[&gt;&gt;]", tr("Go to last page"));
    out << "</h5>\n";
  }
}

} // x2

