/**
    \file views/navigation.h
    \brief Navigation line

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 {

struct Navigation {
  int currPage = 0, maxPage = 0;
  Str urlForPageFormat;
  Navigation( const Page &p ) : out( p.out ), p( p ) { }
  bool canLeft() const { return currPage > 0; }
  bool canRight() const { return currPage < maxPage; }
  bool canLeftOrRight() const { return canLeft() || canRight(); }
  int leftPage() const { return currPage - 1; }
  int rightPage() const { return currPage + 1; }
  Str urlForPage( int page ) const;
  void render() const;

protected:
  Str tr( const char *text ) const { return p.tr( text ); }
  std::ostream &out;
private:
  const Page &p;
};

} // x2

