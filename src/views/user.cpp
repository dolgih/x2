/**
    \file views/user.cpp
    \brief view of the user profile

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// UserProfile

void UserProfile::renderMain() {
  out << R"(<style scoped>
table.lightGray, table.lightGray td, table.lightGray th { border-width: 0;
  border-bottom: 1px solid Silver; }
</style>
<article>
<form method="post" enctype="multipart/form-data">)" << main.csrf << R"(
<table class="lightGray"><caption><h4>)";
  renderAvatar( main.avatar, main.name );
  if( id ) {
    renderCorrespondence( id, 2 );
    renderRss( id );
    renderOnlineIndicator( id );
    out << tr("User (id=") << id << "); <span style=\"font-size:70%\">"
        << user.postsNumber() << "&nbsp;" << tr("post(s)") << "</span>\n";
  } else
    out << tr("New user") << '\n';
  out << "</h4></caption>\n";
  main.renderFromBeginToEnd( out );
  if( id ) {
    out << "<tr><th>" << tr("created") << "</th><td>" << utl::T2s( user.created()) << "</td></tr>\n"
    "<tr><th>" << tr("modified") << "</th><td>" << utl::T2s( user.modified()) << "</td></tr>\n"
    "<tr><th>" << tr("last activity") << "</th><td id=\"lastActivity\"></td></tr>\n";
  }
  if( ! preferences.empty()) {
    out << "<tr><td></td><td><h4><br>" << tr("Preferences") << "</h4></td></tr>\n";
    preferences.renderFromBeginToEnd( out );
  }
  if( ! actions.empty()) {
    out << "<tr><td></td><td><h4><br>" << tr("Actions") << "</h4></td></tr>\n";
    actions.renderFromBeginToEnd( out );
  }
  out << R"(</table>
</form>
)";
  if( id )
    out << RpcJs::include( urlOf<RpcServer>()) << R"(
<script type="text/javascript">
  function getActivities( r, a ) {
    var userId = )" << id << R"(;
    document.getElementById( "onlineIndicator" + userId ).style.display =
      r[ userId ][ "isActive" ] ? "" : "none";
    document.getElementById( "lastActivity" ).textContent = r[ userId ][ "lastActivity" ];
  }
  function updateActivities(evt) {
    jsonRpc.run( "getActivities", [[ )" << id << R"( ]] );
  }
  window.setInterval( updateActivities, 300000 );
  updateActivities();
</script>
<script type="text/javascript"> // clear the password
  var oldOnload = document.body.onload;
  document.body.onload = function() {
    if( oldOnload ) oldOnload();
    document.getElementById( "password" ).value = "";
  }
</script>)";
  out << "</article>\n";
}

// UserProfile::BaseForm

void UserProfile::BaseForm::checkAuthorization( Input &widget, AccessLevel canView,
  AccessLevel canEdit )
{
  UserProfile &page = dynamic_cast<UserProfile&>( parent );
  if( page.accessLevel < canView )
    del( &widget );
  if( page.accessLevel <  ( canEdit == NoAccessLevel ? canView : canEdit ))
    widget.readonly( true );
}

// UserProfile::Main::Banned

UserProfile::Main::Banned::Banned( Form &form, const Str &label )
  : GroupedInputs( form, form.generateId()),
  groupId( 0 ),
  switcher( form, name(), name() + "switcher" ),
  before( form, name() + "before", "^2[01]\\d{2}-\\d{2}-\\d{2}( \\d{2}:\\d{2}:\\d{2})?$" ),
  description( form, name() + "description" ),
  label( *this, label ),
  lSwitcher( switcher, form.tr("Ban")), lBefore( before, form.tr("until")),
  lDescription( description, form.tr("because_of"))
{
  GroupedInputs::add( switcher ).add( before ).add( description );
  before.message = form.tr("banned until, datetime in format \"YYYY-MM-DD HH:MM:SS\" or date in format \"YYYY-MM-DD\"");
  before.size( "20" );
  description.message = form.tr("up to 128 symbols");
  description.size( "20" ).maxlength( "128" );
  message = before.message;
}

void UserProfile::Main::Banned::init( const dta::GroupsOfUserResult &groupOfUser ) {
  name( sGoUId( groupId = groupOfUser.id()));
  values.checked = groupOfUser.belongs();
  values.before = sql::formatTime( groupOfUser.before());
  values.description = groupOfUser.description();
}

void UserProfile::Main::Banned::setOrCmp() {
  switcher.setOrCmp( values.checked );
  before.setOrCmp( values.before );
  description.setOrCmp( values.description );
  changed = switcher.changed || before.changed || description.changed;
}

bool UserProfile::Main::Banned::get() {
  // returns if changed description or before
  bool changed = values.before != before.get() || values.description != description.get();
  values.before = before;
  values.checked = switcher;
  values.description = description;
  return changed;
}

// UserProfile::Main::WebSiteField

UserProfile::Main::WebSiteField::WebSiteField( Form &form )
  : Regex( form, "webSite", dta::UserResult::webSiteRegex()) { }

void UserProfile::Main::WebSiteField::render( std::ostream &out ) const {
  auto found = attrs.find( "readonly" );
  if( found == attrs.end() || found->second.empty())
    Regex::render( out );
  else
    out << Link( value, value, value ).target( "_blank" ).id( id());
}

// UserProfile::Main

UserProfile::Main::Main( const Page &page ) : BaseForm( page ),
  name          ( *this, "name",      dta::UserResult::nameRegex()),
  password      ( *this, "password" ),
  password2     ( *this, "password2" ),
  humanName     ( *this, "humanName", dta::UserResult::humanNameRegex()),
  email         ( *this, "email",     dta::UserResult::emailRegex()),
  webSite       ( *this ),
  phone         ( *this, "phone",     dta::UserResult::phoneRegex()),
  phone2        ( *this, "phone2",    dta::UserResult::phoneRegex()),
  avatar        ( *this, "avatar",    dta::UserResult::avatarRegex()),
  uploadedAvatar( *this, "uploadedAvatar" ),
  groupsOfUser  ( *this, "groupsOfUser" ),
  restrictions  ( *this, "restrictions" ),
  validateEmail ( *this, tr("Validate Email"), "validateEmail" ),
  referer       ( *this, "referer" ),
  lName     ( name,      tr("User Name") + '*' ),
  lPassword ( password,  tr("Password") + '*' ),
  lPassword2( password2, tr("Once Again") + '*' ),
  lHumanName( humanName, tr("Human Name") + '*' ),
  lEmail    ( email,     tr("E-Mail") + '*' ), lWebSite  ( webSite,   tr("Web Site")),
  lPhone    ( phone,     tr("Phone")),    lPhone2   ( phone2,    tr("Second Phone")),
  lAvatar   ( avatar,    tr("Avatar")),
  lUploadedAvatar( uploadedAvatar, tr("upload the avatar")),
  lGroupsOfUser  ( groupsOfUser,   tr("Groups of users")),
  lRestrictions  ( restrictions,   tr("Restricted"))
{
  name.message = tr("up to 32 symbols, without spaces, minimum 3");
    name.size( "32" ).autofocus( true );
  password.message = tr("up to 32 symbols, minimum 6; or empty");
    password.size( "32" ).id( "password" );
  password2.message = tr("once again the password");
    password2.size( "32" );
  humanName.message = tr("up to 128 symbols, minimum 3");
    humanName.size( "32" );
  email.message = tr("up to 128 symbols, minimum 5");
    email.type( "email" ).size( "32" ).maxlength( "128" );
  webSite.message = tr("up to 128 symbols, without spaces, minimum 5; or empty");
    webSite.type( "url" ).size( "32" );
  phone.message = tr("up to 16 symbols, without spaces, minimum 5; or empty");
    phone.size( "16" );
  phone2.message = tr("up to 16 symbols, without spaces, minimum 5; or empty");
    phone2.size( "16" );
  avatar.message = webSite.message;
    avatar.size( "32" );
  uploadedAvatar.message = utl::f( tr("The avatar file is too large, max size = {1}"),
    cfg.avatar.maxSize );
  uploadedAvatar.accept( "image/*" );
  groupsOfUser.multiple( true );
}

void UserProfile::Main::checkAuthorizationAll() {
  checkAuthorization( name,           AnonymousLevel, OwnerLevel );
  checkAuthorization( password,       OwnerLevel ); checkAuthorization( password2, OwnerLevel );
  checkAuthorization( humanName,      UserLevel,      OwnerLevel );
  checkAuthorization( email,          ViewLevel,      OwnerLevel );
  checkAuthorization( webSite,        AnonymousLevel, OwnerLevel );
  checkAuthorization( phone,          ViewLevel,      OwnerLevel );
  checkAuthorization( phone2,         ViewLevel,      OwnerLevel );
  checkAuthorization( avatar,         ViewLevel,      OwnerLevel );
  checkAuthorization( uploadedAvatar, OwnerLevel,     OwnerLevel );
  checkAuthorization( groupsOfUser,   UserLevel,      AdminLevel );
  checkAuthorization( restrictions,   UserLevel,      AdminLevel );
  checkAuthorization( validateEmail,  OwnerLevel );
}

// UserProfile::Preferences

UserProfile::Preferences::Preferences( const Page &page ) : BaseForm( page ),
  locale  ( *this, "locale" ),
  theme   ( *this, "theme" ),
  answers ( *this, "answers" ),
  quotes  ( *this, "quotes" ),
  lLocale ( locale,  tr("Locale")),
  lTheme  ( theme,   tr("Theme")),
  lAnswers( answers, tr("Answers")),
  lQuotes ( quotes,  tr("Quotes"))
{
  answers.add( tr("Don't keep track of"));
  answers.add( "E-Mail" );
  quotes.add( tr("Don't keep track of"));
  quotes.add( "E-Mail" );
}

void UserProfile::Preferences::checkAuthorizationAll() {
  checkAuthorization( locale,  OwnerLevel );
  checkAuthorization( theme,   OwnerLevel );
  checkAuthorization( answers, OwnerLevel );
  checkAuthorization( quotes,  OwnerLevel );
}

// UserProfile::Actions

UserProfile::Actions::Actions( const Page &page )
  : BaseForm( page ),
  submit( *this, tr("Submit"), "submit" ),
  remove( *this, tr("Remove"), "remove" )
{ }

void UserProfile::Actions::checkAuthorizationAll() {
  checkAuthorization( submit, BloggerLevel );
  checkAuthorization( remove, OwnerLevel );
}

} // x2

