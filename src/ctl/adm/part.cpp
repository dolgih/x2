/**
    \file ctl/adm/part.cpp
    \brief controller for the admin part editor

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Part

Part::Part( const Thread &thread )
  : Base( thread ), form( *this ), part( sql )
{
  if( ! url.args( url.tail, id )) throw AddressError();

  theTitle += " :: " + tr("Part");
  theDescription += " :: " + tr("Part of users");

  menuType = MenuSideBar;

  form.clear.formaction( urlOf<adm::ClearPart>( id ));
  try {
    // access
    sql::CachedResult r( sql, dta::adm::PartAccess::cacheKey( id ),
                         dta::adm::PartAccess::select( id ), { part.cacheKey( id ) } );
    r.execQuery();
    while( r.next()) {
      dta::adm::PartAccess group;
      group.fetch( r );
      accesses[ sAGId( group.id ) ] = group;
      form.accessList.push_back( new MultiCheckBox( form, sAGId( group.id )));
      auto &mcb = *form.accessList.back();
      form.lAccessList.push_back( new Label( mcb, group.name ));
      mcb.add( "Rd" );
      mcb.add( "Wr" );
      mcb.add( "Cr" );
      mcb.add( "Ow" );
      mcb.add( "Md" );
      mcb.add( "Ht" );
    }
  } catch( const std::exception & ) { id = 0; part.clear(); accesses.clear(); }
  // end of access

}

void Part::load() {
  try {
    if( id && ! part.init( id )) {
      response.setStatus( 404 );
      error = utl::f( "Part Id={1} not found", id );
      return;
    }

    if( ! part.canEdit( user )) { response.redirect( urlOf<Home>()); return; }

    if( id ) {
      form.author.setOrCmp     ( part.author());
      form.name.setOrCmp       ( part.name());
      form.description.setOrCmp( part.description());
      form.weight.setOrCmp     ( part.weight());
      { // banGroup
        sql::CachedResult r( sql, part.banGroupNameCacheKey(),
                             part.selectBanGroup( id ), { part.cacheKey() } );
        r.execQuery().row();
        form.banGroup.setOrCmp( r.empty() ? Str() : r[ 0 ].get());
      } // end of banGroup
    } else
      form.author.setOrCmp( user.id );

    // accessList
    for( auto *a: form.accessList ) {
      dta::Rights r = accesses[ a->name() ];
      Bools bools = { r.r, r.w, r.c, r.o, r.m, r.h };
      std::set<Str> dummy; a->setOrCmp( bools, dummy  );
    }
    // end of accessList

  } catch( const std::exception &e ) {
    CAUGHT_EXCEPTION( e ); theTitle = "Error";
  }
}

bool Part::save() {
  try {
    if( form.remove ) {
      if( id && ! part.isPrivate()) { // is not a private part, can remove
        sql::Transaction guard( sql());
        part.removeBanGroup( sql, id );
        part.remove( sql, id );
        guard.commit();
      }
    } else {
      sql::Transaction guard( sql());

      if( id ) {
        if( changed )
          part.update( sql, id, form.author.get<Id>(), form.name,
                       form.description, form.weight.get<int>());
        if( form.banGroup.changed ) {
          if( form.banGroup.get().empty())
            part.removeBanGroup( sql, id );
          else
            part.insertBanGroup( sql, id, form.banGroup );
        }
      } else {
        id = part.insert( sql, form.author.get<Id>(), form.name,
                          form.description, form.weight.get<int>());
        if( ! form.banGroup.get().empty())
          part.insertBanGroup( sql, id, form.banGroup );
      }

      // accessList
      for( auto *a: form.accessList ) {
        if( a->changed ) {
          Bools newAccess; a->get( newAccess );
          auto &group = accesses[ a->name() ];
          group.setAccess( newAccess );
          const char *remove = "DELETE FROM access_list WHERE part_id={1} AND group_id={2}",
            *insert = "INSERT INTO access_list (part_id, group_id,r,w,c,o,m,h) VALUES({1},{2},{3},{4},{5},{6},{7},{8})";
          sql( utl::pf( remove, id, group.id ));
          if( group.hasRights())
            sql( utl::pf( insert, id, group.id,
              group.r ? 'r' : '-', group.w ? 'w' : '-',
              group.c ? 'c' : '-', group.o ? 'o' : '-',
              group.m ? 'm' : '-', group.h ? 'h' : '-' ));
        }
      }

      guard.commit();
    }
  } catch( const std::exception &e ) {
    CAUGHT_EXCEPTION( e ); theTitle = "Error"; return false;
  }

  cache.rise( part.cacheKey( id ));
  cache.rise( part.allPartsCacheKey());
  response.redirect( urlOf<ph::Phorum>());
  return true;
}

}} // adm, x2

