/**
    \file ctl/adm/groups.cpp
    \brief controller for the admin groups page

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Groups

Groups::Groups( const Thread &thread )
  : Base( thread ), form( *this ), groups( sql )
{
  if( ! url.args( url.tail, id )) throw AddressError();

  theTitle += " :: " + tr("Groups");
  theDescription += " :: " + tr("Groups of users");

  menuType = MenuSideBar;
}

void Groups::load() {
  if( ! user.isAdmin ) { response.redirect( urlOf<Home>()); return; }

  try {
    groups.init();

    if( id )
      for( auto &g: groups )
        if( g.id() == id ) {
          form.name.setOrCmp( g.name());
          form.modified = t2s( g.modified());
          break;
        }

  } catch( const std::exception &e ) {
    CAUGHT_EXCEPTION( e ); theTitle = "Error";
  }
}

bool Groups::save() {
  try {
    if( form.remove )
      groups.remove( sql(), id );
    else {
      if( id ) { if( changed ) groups.update( sql(), id, form.name ); }
      else groups.insert( sql(), form.name );
    }
    cache.rise( groups.allGroupsCacheKey());
  } catch( const std::exception &e ) {
    CAUGHT_EXCEPTION( e ); theTitle = "Error"; return false;
  }
  response.redirect( url( 0 ));
  return true;
}

}} // adm, x2

