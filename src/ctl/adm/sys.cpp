/**
    \file ctl/adm/sys.cpp
    \brief controller for the sys page

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Sys

const char Sys::cachePictureId[] = "cachePicture";

Sys::Sys( const Thread &thread ) : Base( thread ), form( *this ) {
  theTitle += " :: " + tr("System Page");
  theDescription += " :: " + tr("System Page");

  menuType = MenuSideBar;
  form.cachePicture.formaction( url( cachePictureId ));
}

void Sys::load() {
  if( ! user.isAdmin ) { response.redirect( urlOf<Home>()); return; }
}

bool Sys::save() {
  if( form.clearCache )
    cache.clear();
  else if( form.recalcStatistics )
    try {
      recalcStatistics();
      cache.clear();
    } catch( const std::exception& e ) {
      CAUGHT_EXCEPTION( e ); theTitle = "Error";
    }
  return false;
}

void Sys::recalcStatistics() {
  sql::Transaction tr( sql());
  sql( "SELECT COUNT(*) FROM recalc_topics()" );
  sql( "SELECT COUNT(*) FROM recalc_parts()" );
  sql( "SELECT COUNT(*) FROM recalc_users()" );
  tr.commit();
}

}} // adm, x2

