/**
    \file ctl/adm/admin.cpp
    \brief controller for admin pages

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Dispatcher

Base::Dispatcher::Dispatcher( const Page::Dispatcher &parent )
  : Page::Dispatcher( "admin", parent )
{
  map = { dispatch<Admin>(),      dispatch<Sys>(),        dispatch<Sql>(),
          dispatch<Part>(),       dispatch<ClearPart>(),  dispatch<Groups>(),
          dispatch<Xss>() };
}

// Base

void Base::createMenu() {
  Page::createMenu();
  theMenu.insert( MenuItem( 501, "Sql", urlOf<Sql>(), "X1 God Mode" ));
  theMenu.insert( MenuItem( 502, tr("Sys"), urlOf<Sys>(), tr("System functions")));
  theMenu.insert( MenuItem( 503, tr("Groups"), urlOf<Groups>() + "#edit",
    tr("Edit groups of users")));
}

}} // adm, x2

