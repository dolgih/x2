/**
    \file ctl/adm/clearPart.cpp
    \brief controller for the admin part editor

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Part

ClearPart::ClearPart( const Thread &thread )
  : Base( thread ), part( sql )
{
  if( ! url.args( url.tail, id )) throw AddressError();

  theTitle += " :: " + tr("Clear the Part"); // will be shown in case of error
}

void ClearPart::load() {
  try {
    if( ! part.init( id )) {
      response.setStatus( 404 );
      error = utl::f( "Part Id={1} not found", id );
      return;
    }

    if( ! part.canEdit( user )) {
      response.setStatus( 403 );
      error = "Unable to clear the Part";
      return;
    }
  } catch( const std::exception &e ) { CAUGHT_EXCEPTION( e ); }
}

bool ClearPart::save() {
  if( ! approvedPressed()) return false;
  try {
    sql::Transaction guard( sql());
      ph::Removing rm( sql, user.id ); rm.of( id, 0, 0 );
    guard.commit();
    rm.rise();
    response.redirect( urlOf<ph::Phorum>());
  } catch( const std::exception& e ) { CAUGHT_EXCEPTION( e ); return false; }
  return true;
}

void ClearPart::render() {
  if( msgBox() || response.isErrorStatus()) Base::render();
  else {
    renderMsgBox( msgBoxWarning | msgBoxYes | msgBoxNo | msgBoxIDoNotKnow
                | msgBoxLinkForFrom,
      utl::f( tr("The part \"{1}\" will be cleared. All its posts and"
      " topics will be deleted. This operation can not be undone. Are you"
      " sure?"), part.name()),
      urlOf<ClearPart>( id ),
      urlOf<ph::Phorum>());
  }
}

}} // adm, x2

