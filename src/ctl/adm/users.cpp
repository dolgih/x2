/**
    \file ctl/adm/users.cpp
    \brief controller for the ph part page

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Admin

Admin::Admin( const Thread &thread )
  : Base( thread ), nav( *this ), users( sql )
{
  if( ! url.args( url.tail, id, nav.currPage )) throw AddressError();

  theTitle += " :: Admin Page";
}

void Admin::load() {
  if( user.hasAnonymousId()) { response.redirect( urlOf<ph::Phorum>()); return; }
  try {
    if( ! users.init( id, nav.currPage )) {
      response.setStatus( 404 );
      error = utl::f( "User Id={1} not found", id );
      return;
    }
    if( users.next()) {
      nav.urlForPageFormat = url( 0, "{1}" );
      nav.currPage = users.page();
      nav.maxPage = users.maxPage();
    }
    users.rewind();
  } catch( const std::exception &e ) {
    CAUGHT_EXCEPTION( e ); theTitle = "Error";
  }
}

}} // adm, x2

