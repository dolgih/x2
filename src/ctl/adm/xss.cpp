/**
    \file ctl/adm/xss.cpp
    \brief controller for the Xss filter page

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace adm {

// Xss

Xss::Xss( const Thread &thread )
  : Base( thread ), form( *this ), inSize( 0 ), outSize( 0 )
{
  theTitle += " :: " + tr("Xss Filter");
  theDescription += " :: " + tr("Xss Filter Service");
}

bool Xss::save() {
  TextArea &in = form.inHtml, &out = form.outHtml;
  in.rows( "2" ).readonly( true ).autofocus( false );
  out.rows( "12" ).autofocus( true );
  inSize = in.get().size();
  if( ! inSize ) {
    response.redirect( urlOf<Home>());
    return true;
  }
  out.set( xss::filter( in.get(), false ));
  outSize = out.get().size();
  in.set( Str());
  return false;
}

}} // adm, x2

