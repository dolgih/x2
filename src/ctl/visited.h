/**
    \file ctl/visited.h
    all classes for tracking of topic visits or another user activities.

    This file is a part of x1 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 {

/**
  Главный класс для отслеживания посещений тем или любой активности пользователей.
*/
extern class Visited {
public:

  /** Описывает активность пользователей (время последнего запроса на сайт). */
  typedef std::map<Id, TimeT> Activities;

  /** Описывает посещаемость тем (ид темы, количество). */
  typedef std::map<Id, unsigned> Visits;

  /** Записывает время активности пользователя (или посещение темы). */
  void put( Id user, unsigned short csrf, TimeT time, Id topic = 0 );

  /// Возвращает активность пользователей.
  /** Перед вызовом activities должна быть заполнена идентификаторами пользователей
      для которых будет получена активность. */
  void get( sql::PhSession &sql, Activities &activities ) { userCache.get( sql, activities ); }
  /// Возвращает посещаемость тем.
  /** Перед вызовом visits должен быть заполнен идентификаторами тем
      для которых будет получена посещаемость. */
  void get( sql::PhSession &sql, Visits &visits ) { topicCache.get( sql, visits ); }

  /// Вызывается во время простоя процесса.
  /** Если время хранения данных в памяти без записи в БД превысило
      maxTimeInCache,
      т.е. ( modifiedCount && ( lastWriteTime + maxTimeInCache ) < now )
      или количество незаписанных пользователей ( modifiedCount ) в списке
      превысило minUsers или количество незаписанных тем ( modifiedCount )
      в списке превысило minTopics, то все незаписанные данные записываются
      в базу данных. */
  void idle();
  void clear() /** Записывает несохраненные данные в базу данных. */;

private:

  /// Кэшь
  /** Организована по принципу LRU (англ. Least Recently Used)
      - вытесняется элемент, неиспользованный дольше всех. */
  template <class T> struct Cache {
    typedef std::list< T > List;
    typedef std::map< Id, typename List::iterator > Index;
    List list; Index index;
    std::recursive_mutex guard;
    TimeT lastWriteTime;
    unsigned    modifiedCount;

    struct Stat { /* Сбор статистики для целей отладки и наблюдения */
      unsigned g  /* Количество запросов get */,
               q  /* количество запрашиваемых элементов */,
               f  /* Количество элементов, найденных в кэши */,
               i  /* Количество вызовов insert */,
               d  /* Количество удаленных */,
               fi /* Количество отказов вставки */,
               ri /* Количество действительно вставленных */,
               w  /* Количество вызовов write */,
               wq /* Количество элементов для записи */;
      Stat() : g( 0 ), q( 0 ), f( 0 ), i( 0 ), d( 0 ), fi( 0 ), ri( 0 ),
        w( 0 ), wq( 0 ) { }
      Str get() {
        return utl::F( "Stat g={1}, q={2}, f={3}, i={4}, d={5}, fi={6}, ri={7}, w={8}, wq={9}" )
          % g % q % f % i % d % fi % ri % w % wq;
      }
    } stat;

    Cache() : lastWriteTime( std::time( 0 )), modifiedCount( 0 ) { }

    /// Вставка в кэшь одного элемента.
    /** Если элемент существует, то он заменяется на новый, если новый
        больше старого;
        Удаляем элемент, если список переполнен */
    void insert( const T &newItem );

    /// Получение данных из кэша
    /** Out заполняется имеющимися в кэши значениями, остальные получаем
        из БД, заполняя при этом кэшь вызовом insert. */
    template<class Out> void get( sql::PhSession &sql, Out &out );

    /// Записывает несохраненные данные в БД
    void write( sql::PhSession &sql );

    /// Сдвигает элемент списка в начало списка
    void splice( const typename List::iterator &it );

    /// Запись в БД при простое
    void idle( sql::PhSession &sql, TimeT now );
  };

  /// Состояние элемента кэши
  /** clearState - запись соответствует записи в БД,
      dirtyState - запись должна будет записана в БД. */
  enum CacheState { clearState = 0, dirtyState };

  /// Базовый элемент кэши
  struct CacheElement {
    CacheElement const &operator = ( CacheElement const & ); // noncopyable
    CacheState state;
    Id id;
    CacheElement( Id id ) : state( clearState ), id( id ) { }
  };

  /// Элемент кэши темы
  struct Topic : CacheElement {
    static unsigned minElements() { return cfg.activity.minTopics; }
    static unsigned maxElements() { return cfg.activity.maxTopics; }
    bool isValid; // означает валидность visitsNumber
    unsigned visitsNumber;
    unsigned add;
    Cache<Topic>::Index::iterator index;
    Topic( Id id ) : CacheElement( id ), isValid( false ), visitsNumber( 0 ),
      add( 1 ) { state = dirtyState; }
    Topic( sql::Result &r ) : CacheElement( r[ 0 ].get<Id>()), isValid( true ),
      visitsNumber( r[ 1 ].get<unsigned>()), add ( 0 ) { }
    void update( const Topic &right );
    bool get( unsigned &to ) const;
    static Str select( Str need );
    void get( Str &to );
    static Str update( Str values );
  };

  /// Кэшь темы
  Cache<Topic> topicCache;

  /// Элемент кэши пользователя
  struct User : CacheElement {
    static unsigned minElements() { return cfg.activity.minUsers; }
    static unsigned maxElements() { return cfg.activity.maxUsers; }
    TimeT lastActivity, lastTopicTime;
    Id lastTopic;
    Cache<User>::Index::iterator index;
    User( Id id, TimeT lastActivity, Id lastTopic ) : CacheElement( id ),
      lastActivity( lastActivity ), lastTopicTime( lastActivity ),
      lastTopic( lastTopic ) { state = dirtyState; }
    User( sql::Result &r ) : CacheElement( r[ 0 ].get<Id>()),
      lastActivity( r[ 1 ].getTime()), lastTopicTime( 0 ), lastTopic( 0 ) { }
    void update( const User &right );
    bool get( TimeT &to ) const;
    static Str select( Str need );
    void get( Str &to ) const;
    static Str update( Str values );
  };

  /// Кэшь пользователя
  Cache<User> userCache;
} visited;

} // x2

