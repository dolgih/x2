/**
    \file ctl/rmUser.cpp
    \brief remove user profile

    This file is a part of x1 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// UserProfile

const char UserProfile::removeCommandId         [] = "remove";
const char UserProfile::removeWithPostsCommandId[] = "removeWithPosts";

void UserProfile::removeUser() {

  auto mkUrl = [ this ] ( const Str &rmCmd ) -> Str {
    return urlOf<UserProfile>( id ) + ( rmCmd.empty() ? Str() : '/' + rmCmd );
  };

  auto removePrivatePart = [ this ] ( ph::Removing &corr ) {
    Id privatePart = dta::ph::PartsResult::getPrivateOf( id );
    corr.of( privatePart, 0, 0 );
    sql( utl::pf( "DELETE FROM parts WHERE id={1}", privatePart ));
  };

  try {
    if( accessLevel < OwnerLevel ) { response.setStatus( 403 ); return; }

    std::set<Id> partsOfAuthor;
    dta::ph::PartsResult( sql ).getPartsOfAuthor( id, partsOfAuthor );
    if( ! partsOfAuthor.empty()) {
      renderMsgBox( msgBoxError | msgBoxOK | msgBoxLinkForTo,
        utl::f( tr("The user \"{1}\" can not"
        " be deleted because it owns one or more parts of phorum."),
        user.name()), mkUrl( Str()), Str());
      return;
    }

    if( belongs2group && accessLevel != OwnerLevel ) {
      renderMsgBox( msgBoxError | msgBoxOK | msgBoxLinkForTo,
        utl::f( tr("The user \"{1}\" can not"
        " be deleted because it belongs to one or more groups of users."),
        user.name()), mkUrl( Str()), Str());
      return;
    }

    if( predefined::user::isPredefined( id )) {
      response.redirect( urlOf<Home>());
      return;
    }

    if( accessLevel < AdminLevel ) removeCommand = removeCommandId;

    if( isNew && removeCommand == removeCommandId ) {
      /* Здесь удаляется анонимус (без личной переписки - у него ее нет),
         а его посты сохраняются */
      if( ! yesPressed()) {
        renderMsgBox( msgBoxQuestion | msgBoxYes | msgBoxNo | msgBoxIDoNotKnow
                    | msgBoxLinkForFrom,
          utl::f( tr("The user \"{1}\" will be deleted. All user's posts and"
          " <b>topics</b> will be moved to the special user \"Guest\". This"
          " operation can not be undone. Are you sure?"), user.name()),
          mkUrl( removeCommandId ),
          mkUrl( Str()));
        return;
      } else {
        sql::Transaction guard( sql());
          cleanMembership( 0 );
          sql( utl::pf( "UPDATE posts SET author={1} WHERE author={2}",
            predefined::user::guest, id ));
          sql( utl::pf( "UPDATE posts SET author2={1} WHERE author2={2}",
            predefined::user::guest, id ));
          user.remove( sql, id );
        guard.commit();
        cache.rise( user.cacheKey( id ));
        if( id == Page::user.id )
          cookie.clear();
      }
    } else if( removeCommand.empty()) {
      renderMsgBox( msgBoxQuestion | msgBoxYes | msgBoxNo | msgBoxIDoNotKnow
        | msgBoxIDoNotKnowAsTo, utl::f( tr("The user \"{1}\" and its private"
        " correspondence will be deleted. Do you want to leave the user's posts"
        " and <b>topics</b>?"), user.name()),
        mkUrl( removeCommandId ),
        mkUrl( removeWithPostsCommandId ));
      return;
    } else if( removeCommand == removeCommandId ) {
      if( ! yesPressed()) {
        renderMsgBox( msgBoxQuestion | msgBoxYes | msgBoxNo | msgBoxIDoNotKnow
                    | msgBoxLinkForFrom,
          utl::f( tr("The user \"{1}\" and its private correspondence will be"
          " deleted. Are you sure?"), user.name()),
          mkUrl( removeCommandId ),
          mkUrl( Str()));
        return;
      } else {
        sql::Transaction guard( sql());
          cleanMembership( 0 );
          ph::Removing corr( sql, Page::user.id ); removePrivatePart( corr );
          User newUser; newUser.createAnonymous( sql );
          sql( utl::Pf( "UPDATE users SET id={1},name={2},password={3},email={4}"
                            ",phone={5},properties={6} WHERE id={7}" )
                   ( newUser.id )( newUser.name )( newUser.name )
                   ( newUser.name + "@nomail.nodomen" )( Str() /* empty phone */ )
                   ( "{\"avatar\":\"/media/stdAvatars/gone.jpg\"}" )
                   ( id ));
        guard.commit();
        corr.rise();
        cache.rise( user.cacheKey( id ));
        if( id == Page::user.id )
          cookie.set( newUser.id, cookie.getLocale());
      }
    } else if( removeCommand == removeWithPostsCommandId ) {
      if( ! approvedPressed()) {
        renderMsgBox( msgBoxWarning | msgBoxYes | msgBoxNo | msgBoxIDoNotKnow
                    | msgBoxLinkForFrom,
          utl::f( tr("The user \"{1}\" and all its posts, <b>topics</b> and"
          " private correspondents will be deleted. This operation can not be"
          " undone. Are you sure?"), user.name()),
          mkUrl( removeWithPostsCommandId ),
          mkUrl( Str()));
        return;
      } else {
        sql::Transaction guard( sql());
          cleanMembership( 0 );
          ph::Removing rm( sql, Page::user.id ); rm.of( 0, id, 0 );
          ph::Removing corr( sql, Page::user.id ); removePrivatePart( corr );
          user.remove( sql, id );
        guard.commit();
        rm.rise();
        corr.rise();
        cache.rise( user.cacheKey( id ));
        if( id == Page::user.id )
          cookie.clear();
      }

    } else { /* bad command */ response.setStatus( 403 ); return; }

    cache.rise( dta::ComplexUserResult::usersAllPagesCacheKey());

  } catch( const std::exception& e ) { CAUGHT_EXCEPTION( e ); return; }

  response.redirect( urlOf<adm::Admin>());
}

} // x2

