/**
    \file ctl/visited.cpp
    all classes for tracking of topic visits or another user activities.

    This file is a part of x1 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/


#include "x2.h"

namespace x2 {

Visited visited;

// begin of RpcServer

void RpcServer::getVisits( utl::Json params, utl::Json &answer ) {
  utl::Json topics = params[ 0 ];
  Visited::Visits visits;
  for( int i = 0; i < topics.arraySize(); i ++ )
    visits[ topics[ i ] ] = 0;
  visited.get( sql, visits );
  answer = utl::Json::createObj();
  for( const auto &i: visits )
    answer.add( n2s( i.first ), (long)i.second );
}

void RpcServer::getActivities( utl::Json params, utl::Json &answer ) {
  utl::Json users = params[ 0 ];
  Visited::Activities activities;
  for( int i = 0; i < users.arraySize(); i ++ )
    activities[ users[ i ] ] = 0;
  visited.get( sql, activities );
  answer = utl::Json::createObj();
  for( const auto &i: activities ) {
    utl::Json o = utl::Json::createObj();
    o.add( "isActive", ( i.second + cfg.activity.breakTime ) >= std::time( 0 ));
    o.add( "lastActivity", t2s( i.second ));
    /* t2s instead of ( format( "{1,datetime,local}" ) % i.second ).str( user.locale()) */
    answer.add( n2s( i.first ), o );
  }
}

// end of RpcServer

// begin of Visited

// begin of Cache

// begin of Topic

void Visited::Topic::update( const Topic &right ) {
  if( right.isValid ) {
    if( ! isValid )
      { isValid = true; visitsNumber = add + right.visitsNumber; add = 0;
        state = right.state; }
    else if( right.visitsNumber > ( visitsNumber + add ))
      { visitsNumber = right.visitsNumber; add = 0; state = right.state; }
  } else {
    add += right.add;
    state = right.state;
  }
}

bool Visited::Topic::get( unsigned &to ) const {
  if( isValid ) to = visitsNumber + add;
  return isValid;
}

Str Visited::Topic::select( Str need ) {
  return "SELECT id, visits_number FROM topics WHERE id IN (" + need + ')';
}

void Visited::Topic::get( Str &to ) {
  to = ( isValid ? "true," : "false," ) +
    n2s( visitsNumber ) + ',' + n2s( add );
  if( isValid ) visitsNumber += add;
  add = 0;
}

Str Visited::Topic::update( Str values ) {
  return "UPDATE topics AS u SET visits_number="
  " CASE WHEN new.is_valid THEN new.visits_number ELSE u.visits_number END"
  " + new.add FROM (VALUES " + values + ") AS new (id,is_valid,visits_number,add)"
  " WHERE u.id=new.id AND (NOT new.is_valid OR u.visits_number<"
  "(new.visits_number+new.add))";
}

// end of Topic

// begin of User

// Pseudo Identifiers

/** Для анонимных пользователей с идентификатором x1::predefined::user::anonymous
    временно, без записи в базу данных, создаются и используются отрицательные
    идентификаторы вида (::Id_MIN + (unsigned short)), т.е. самые большие по
    модулю отрицательные идентификаторы. См. Псевдоидентификаторы
    \ref pseudo_identifiers_subsubsec */
inline static Id pseudoIdFrom( unsigned short csrf ) { return Id_MIN + csrf; }
inline static bool isPseudo( Id id ) { return ( id >> 16 ) == ( Id_MIN >> 16 ); }

// end of Pseudo Identifiers

void Visited::User::update( const User &right ) {
  if( right.lastActivity > lastActivity )
    { lastActivity = right.lastActivity; state = right.state; }
  if( right.lastTopic && right.lastTopicTime >= lastTopicTime )
    { lastTopic = right.lastTopic, lastTopicTime = right.lastTopicTime;
      state = right.state; }
}

bool Visited::User::get( TimeT &to ) const
{ to = lastActivity; return true; }

Str Visited::User::select( Str need ) {
  return "SELECT id, last_activity FROM users WHERE id IN (" + need + ')';
}

void Visited::User::get( Str &to ) const {
  if( isPseudo( id )) to = Str(); // псевдоидентификаторы не пишем в БД
  else
    to = "timestamptz '" + sql::formatTime( lastActivity ) + "'";
}

Str Visited::User::update( Str values ) {
  return "UPDATE users AS u SET last_activity=new.last_activity FROM"
    "(VALUES " + values + ") AS new (id,last_activity) WHERE u.id=new.id"
    " AND u.last_activity<new.last_activity AND u.last_activity>'1985-01-01'";
}

// end of User

template<class T> void Visited::Cache<T>::insert( const T &newItem ) {
  stat.i ++; // статистика
  std::unique_lock<std::recursive_mutex> g( guard );
  auto found = index.find( newItem.id );
  if( found != index.end()) { // элемент существует
    CacheState oldState = found->second->state;
    found->second->update( newItem );
    if( found->second->state != oldState ) {
      if( oldState == clearState ) modifiedCount ++;
      else modifiedCount --;
    }
    return;
  }
  if( list.size() >= T::maxElements()) {
    for( auto i = list.begin(); i != list.end(); i ++ )
      if( i->state == clearState ) {
        index.erase( i->index ); list.erase( i );
        stat.d ++; // статистика
        goto erased;
      }
    stat.fi ++; // статистика
    return; // ничего удалить не удалось, места для вставки нет
  erased:;
  }
  stat.ri ++; // статистика
  list.emplace_back( newItem );
  auto &inserted = list.back();
  inserted.index = index.insert( std::pair<Id, typename
    Cache<T>::List::iterator>( inserted.id, -- list.end())).first;
  if( inserted.state == dirtyState )
    modifiedCount ++;
}

template<class T> template<class Out> void Visited::Cache<T>::get( sql::PhSession &sql, Out &out ) {
  stat.g ++;               // статистика
  stat.q += out.size(); // статистика

  Str need; unsigned needCount = 0; // идентификаторы элементов, которых нет в кэши
  {
    std::unique_lock<std::recursive_mutex> g( guard );
    for( auto i = out.begin(); i != out.end(); i ++ ) {
      auto found = index.find( i->first );
      if( found == index.end() || ! found->second->get( i->second )) {
      // элемента нет в кэши или не получено
        i->second = 0;
        need = ( need.empty() ? Str() : ( need + ',' )) + n2s( i->first );
        needCount ++;
      } else { // элемент получен из кэши
        stat.f ++; // статистика
        splice( found->second );
      }
    }
  } // lock guard
  if( need.empty()) return;
  sql::Result r( sql.openIfNotOpened()); r.exec( T::select( need ));
  {
    std::unique_lock<std::recursive_mutex> g( guard );
    if( needCount > ( T::maxElements() - modifiedCount ))
      write( sql );
    while( r.next()) {
      T newItem( r ); insert( newItem ); newItem.get( out[ newItem.id ] );
    }
  } // lock guard
}

template<class T> void Visited::Cache<T>::write( sql::PhSession &sql ) {
  stat.w ++; // статистика
  std::unique_lock<std::recursive_mutex> g( guard );
  if( ! modifiedCount ) return;
  Str values;
  for( auto i = list.begin(); i != list.end(); i ++ ) {
    if( i->state == dirtyState ) {
      i->state = clearState;
      Str value; i->get( value ); if( value.empty()) continue;
      stat.wq ++; // статистика
      values = ( values.empty() ? Str() : ( values + ',' )) +
        '(' + n2s( i->id ) + ',' + value + ')';
    }
  }
  if( ! values.empty() /* бессмысленные запросы к БД не делаем */ )
    sql( T::update( values ));
  modifiedCount = 0; lastWriteTime = std::time( 0 );
}

template<class T> void Visited::Cache<T>::splice( const typename List::iterator &it ) {
  list.splice( list.end(), list, it );
}

template<class T> void Visited::Cache<T>::idle( sql::PhSession &sql,
  TimeT now )
{
  if( ( modifiedCount && ( lastWriteTime + cfg.activity.maxTimeInCache ) < now )
    || modifiedCount > T::minElements())
  {
    write( sql.openIfNotOpened());
    // debug
    if( list.size() != index.size())
      throw std::runtime_error( "Visited::Cache fatal error" );
    LOG_DEBUG( utl::getClassName( typeid( Cache<T> )) + " size=" + n2s( list.size())
      + ' ' + stat.get());
  }
}

// end of Cache

void Visited::put( Id user, unsigned short csrf, TimeT now, Id topic ) {
  /** О функции pseudoIdFrom См. \ref pseudo_identifiers_subsubsec */
  user = user == predefined::user::anonymous ? pseudoIdFrom( csrf ) : user;
  TimeT lastTopicTime = 0; Id lastTopic = 0;
  if( user != predefined::user::anonymous ) {
    auto foundUser = userCache.index.find( user );
    if( foundUser != userCache.index.end()) {
      lastTopicTime = foundUser->second->lastTopicTime;
      lastTopic     = foundUser->second->lastTopic;
    }
    userCache.insert( User( user, now, topic ));
  }
  if( topic && ( topic != lastTopic || ( lastTopicTime + cfg.activity.breakTime ) < now ))
    topicCache.insert( Topic( topic ));
}

void Visited::idle() {
  sql::PhSession sql;
  TimeT now = time( 0 );
  userCache.idle( sql, now );
  topicCache.idle( sql, now );
}

void Visited::clear() {
  sql::PhSession sql; sql.openIfNotOpened();
  topicCache.write( sql );
  userCache.write( sql );
  LOG_DEBUG( "Cleared" );
}

// end of Visited

} // x2

