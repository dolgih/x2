/**
    \file ctl/login.cpp
    \brief controller for x2 login page

    This file is a part of x1 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

bool Login::TheForm::validate() {
  Str n = utl::trim( name ), e = utl::trim( email );
  if( forgot || ( n.empty() && ! e.empty()))
    return email.validate();
  else
    return name.validate() && password.validate();
}

Login::Login( const Thread &thread ) : Page( thread ), form( *this ), user( sql ) {
  url.args( url.tail, emailParam, expTimeParam, keyParam );
  theTitle = "Login Page";
}

/** Simple login. */
bool Login::login() {
  try {
    if( form.login ) { /* login */
      try {
        findUserByNameOrEmail( form.name  );
        if( user.password() == utl::sha1( form.password.get() + user.name())) {
          cookie.set( user.id(), user.locale());
          response.redirect( form.referer );
          return true;
        }
      } catch( const dta::UserResult::ErrorNotFound & ) { }
        catch( const std::exception & ) { throw; }
      error = tr( "User name and(or) password do not match" );
      return false;
    } /* end of login */ else { /* forgot */
      mail4login( form.email );
      renderMsgBox( msgBoxOK, tr("You will receive an email shortly with"
        " link for log in. Thank you."), urlOf<Home>());
      return true;
    } /* end of forgot */
  } catch( const std::exception& e ) { CAUGHT_EXCEPTION( e ); }
  return false;
}

/** Login via E-Mail.
 \param email base64 encoded e-mail address,
 \param expTime base64 encoded time of expiration,
 \param key the key */
void Login::login( Str email, Str expTime, Str key ) {
  email   = base64::decode( email );
  expTime = base64::decode( expTime );
  std::tm expTm = sql::parseTime( expTime.data());
  if( ! key.empty() && key == loginKey( email, expTm )) {
    cookie.set( user.id(), user.locale());
    response.redirect( urlOf<UserProfile>( user.id()));
  } else
    response.setStatus( 403 );
}

void Login::load() {
  if( ! emailParam.empty() && ! expTimeParam.empty() && !keyParam.empty()) {
    login( emailParam, expTimeParam, keyParam );
    return;
  }
  if( ! Page::user.hasAnonymousId()) {
    cookie.clear();
    Str redirectTo = request.referer();
    if( redirectTo.empty()) redirectTo = urlOf<UserProfile>( Page::user.id );
    response.redirect( redirectTo );
    return;
  }
  if( ! request.isPost()  ) {
    // referer must be from x2 site only
    Str referer = url.tail;
    if( referer.empty()) {
      referer = request.referer();
      if( strcasestr( referer.data(), request.host().data()))
        form.referer.set( referer );
      else
        form.referer.set( urlOf<Home>());
    } else
      form.referer.set( referer );
  }
  menuType = MenuSideBar;
}

void Login::findUserByNameOrEmail( Str nameOrEmail ) {
  user.findBy( "nameOrEmail", nameOrEmail,
    utl::pf( "SELECT " + user.columnList() +
             "FROM users WHERE (name={1} OR email={1})"
             " FOR UPDATE OF users NOWAIT",
             nameOrEmail ));
}

void Login::mail4login( Str to ) {
  TimeT expTime = std::time( 0 ) + 3600; // to live one hour only
  Str key = loginKey( to, expTime );
  if( ! key.empty()) {
    Str sExpTime = sql::formatTime( (std::tm) expTime );
    mail( to, "Change your password, please",
    "To enter in your profile, please click on the URL below.\r\n"
    "\r\n" + request.hostAsPrefix()
    + urlOf<Login>( base64::encode( to, false, false ),
                    base64::encode( sExpTime, false, false ), key ) +
    "\r\n"
    "\r\n"
    "This link will expire at (universal time) " + utl::t2s( expTime.gmt()));
  }
}

Str Login::loginKey( Str email, TimeT expTime ) {
  try {
    if( std::time( 0 ) > expTime )
      error = "loginKey(): expired, email:" + email;
    else {
      findUserByNameOrEmail(  email );
      return utl::sha1( email + user.password() + n2s( expTime ) + request.host());
    }
  } catch( const dta::UserResult::ErrorNotFound & ) {
    error = "loginKey(): Forbidden";
  } catch( const std::exception& e ) { CAUGHT_EXCEPTION( e ); }
  return Str();
}

} // x2

