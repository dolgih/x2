/**
    \file ctl/search.cpp
    \brief controller for the search page

    This file is a part of x1 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Search

Search::Search( const Thread &thread )
  : Page( thread ), form( *this ), result( sql )
{
  menuType = MenuSideBar;

  theTitle += " :: " + tr("Search Page");
}

void Search::load() {
  TimeT t = std::time( 0 );
  form.date1.setOrCmp( t2s( t - 2592000 /* a month before */, utl::dateFormat()));
  form.date2.setOrCmp( t2s( t, utl::dateFormat()));
  form.query.setOrCmp( utl::trim( form.query.get()));
}

bool Search::validate() {
  bool validated = Page::validate();
  if( ! form.date1.valid ) error = form.date1.message;
  if( ! form.date2.valid ) {
    if( ! error.empty()) error +=  "; ";
    error += form.date2.message;
  }
  return validated;
}

bool Search::save() {
  page = form.page.get<int>();
  if( form.prev ) page --;
  else if( form.next ) page ++;

  if( ! form.query.get().empty()) try
  {
    LOG_INFO( "user '" + user.name + "' executed query(" + form.query.get()
      + ") from ip=" + request.remoteAddr());

    Str privCond; // private part condition
    if( user.id > predefined::user::anonymous )
      privCond = " OR topics.part_id=" + n2s( - user.id );
    sql();
    result.exec( utl::Pf(

    R"(SELECT posts.id AS id, title, abstract,
ts_headline(coalesce(title,'')||' '||coalesce(abstract,'')||' '
||coalesce(content,''),plainto_tsquery({1}),
'StartSel="<b style=""color:red;"">",StopSel=</b>,MaxWords=16')
AS headline FROM posts JOIN topics ON posts.topic_id=topics.id WHERE
setweight(to_tsvector(coalesce(title,'')),'A')
||setweight(to_tsvector(coalesce(abstract,'')),'C')
||setweight(to_tsvector(coalesce(content,'')),'D')
@@plainto_tsquery({1})
AND posts.modified>={2} AND date(posts.modified)-1<={3}
AND (topics.part_id>0)" + privCond + ") ORDER BY posts.id DESC LIMIT {4} OFFSET {5};")

    ( form.query.get())( form.date1.get())( form.date2.get())
    ( cfg.pageSize * 2)( cfg.pageSize * 2 * page ));

    form.page.set( page );

  } catch( const std::exception &e ) { CAUGHT_EXCEPTION( e ); }

  return false;
}

} // x2

