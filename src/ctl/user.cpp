/**
    \file ctl/user.cpp
    \brief controller for x2 user profile

    This file is a part of x1 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// UserProfile

UserProfile::UserProfile( const Thread &thread ) : Page( thread ),
  main( *this ), preferences( *this ), actions( *this ),
  user( sql ), groupsOfUser( sql )
{
  menuType = MenuSideBar;

  if( ! url.args( url.tail, id, removeCommand )) throw AddressError();

  theTitle += tr(" :: User profile");
  theDescription = theTitle;

  if( ! id && Page::user.isAnonymous ) // anonymous
    id = Page::user.id;

  if( ! Page::user.isAnonymous )
    accessLevel = Page::user.isAdmin ? AdminLevel : UserLevel;
  if( accessLevel < AdminLevel && ( Page::user.id == id || ! id ))
    accessLevel = OwnerLevel;

  isNew = ! id || ( ! predefined::user::isPredefined( id )
                  && ( id < 0 && accessLevel >= OwnerLevel ));

  try {
    // Blogger
    dta::ph::PartsResult( sql ).getPartsOfAuthor( Page::user.id, partsOfCurrentUser );
    if( ! partsOfCurrentUser.empty())
      accessLevel = std::max( BloggerLevel, accessLevel );

    user.findById( id );
    groupsOfUser.init( id );

    checkForAUserIsAdmin();

    bool insertValidateEmail = false;
    for( const auto &g: groupsOfUser ) {
      if( g.id() >= 0 || g.belongs() || accessLevel == AdminLevel
        || currUsrIsOwnerOfGroup( g.id()))
      {
        if( ( ( Page::user.isAdmin && Page::user.id == id ) // admin in his own profile
              || id == predefined::user::admin
              || ! Page::user.isAdmin // or user
              || accessLevel == ViewLevel ) // or an admin looks at admin
            && ! g.belongs() && ! currUsrIsOwnerOfGroup( g.id())
        )
          continue;
        if( g.id() < 0 ) { // restrictions
          if( g.id() == predefined::group::notActivated && g.belongs()) {
            main.restrictions.add( sGoUId( predefined::group::notActivated ), tr("notActivated"));
            restrictionsValues.push_back( true );
              if( ! isNew )
                insertValidateEmail = true;
          }
          if( g.id() == predefined::group::notApproved && g.belongs()) {
            main.restrictions.add( sGoUId( predefined::group::notApproved ), tr("notApproved"));
            restrictionsValues.push_back( true );
          }
          bool bannedInPartGroup = predefined::group::bannedInPart( g.id());
          if( g.id() == predefined::group::banned || bannedInPartGroup )
          { /* banned or banned in part of the blogger */
            /* a blogger */
            bool blogger = currUsrIsOwnerOfGroup( g.id());
            /* a blogger in his own profile */
            bool bloggerInProfile = blogger && id == Page::user.id;
            /* an admin does not need blogger's rights */
            bool adminInBloggerBans = bannedInPartGroup && accessLevel == AdminLevel;
            if( ! bloggerInProfile && ! adminInBloggerBans ) {
              main.bannedInGroups.emplace_back( main, g.id() == predefined::group::banned ? tr("Everywhere Banned") : g.name());
              Main::Banned &b = main.bannedInGroups.back();
              main.checkAuthorization( b, UserLevel, blogger ? BloggerLevel : AdminLevel );
              b.init( g );
            }
          }
          // end of restrictions
        } else { // positive group ids
          main.groupsOfUser.add( sGoUId( g.id()), g.name() +
            ( g.modified() ? t2s( g.modified(), " | %c" ) : Str()));
          bool value = ( /* default group "users" for new accounts */
            isNew && g.id() == predefined::group::users ) || g.belongs();
          groupsOfUserValues.push_back( value );
        }
      }
      if( g.id() > 0 && g.belongs())
        belongs2group = true;
    }
    main.groupsOfUser.size( n2s( std::max( size_t( 1 ),
                     std::min( size_t( 12 ), groupsOfUserValues.size()))));

    if( main.restrictions.empty()) main.del( &main.restrictions );
    if( ! insertValidateEmail )     main.del( &main.validateEmail );

    if( request.isGet()) main.referer.set( request.referer());

    // locale
    preferences.locale.add( Str(), "?" );
    for( const auto &locale : cfg.localization.locales )
      preferences.locale.add( locale.first );
    // end of locale

    // theme
    preferences.theme.add( Str(), "?" );
    for( const auto &theme : cfg.themes.allThemes )
      preferences.theme.add( theme.first );
    // end of theme

    main.       checkAuthorizationAll();
    preferences.checkAuthorizationAll();
    actions.    checkAuthorizationAll();

  } catch( const std::exception &e ) {
    CAUGHT_EXCEPTION( e ); id = 0; user.clear(); groupsOfUser.clear();
  }
}

void UserProfile::load() {
  try {

    if( actions.remove || ! removeCommand.empty()) return;

    // DB 2 Form
    main.groupsOfUser.setOrCmp( groupsOfUserValues, groupsOfUserDiff );

    main.name     .setOrCmp( id ? user.name     () : Str());
    main.password .setOrCmp( Str());
    main.password2.setOrCmp( Str());
    main.humanName.setOrCmp( id ? user.humanName() : Str());
    main.email    .setOrCmp( id ? user.email    () : Str());
    main.webSite  .setOrCmp( id ? user.webSite  () : Str());
    main.phone    .setOrCmp( id ? user.phone    () : Str());
    main.phone2   .setOrCmp( id ? user.phone2   () : Str());
    main.avatar   .setOrCmp( id ? user.avatar   () : Str());

    main.restrictions.setOrCmp( restrictionsValues, restrictionsDiff );
    for( auto &banned: main.bannedInGroups ) banned.setOrCmp();

    preferences.locale.setOrCmp( id ? user.locale() : Str());
    preferences.theme .setOrCmp( id ? user.theme()  : Str());
    std::set<Str> dummy;
    preferences.answers.setOrCmp( { user.doNotTrackAnswers(), user.mailOnAnswer() }, dummy );
    preferences.quotes. setOrCmp( { user.doNotTrackQuotes(), user.mailOnQuote() }, dummy );

    loadAvatar();
  } catch( const std::exception &e ) { CAUGHT_EXCEPTION( e ); }
}

/** \brief Запись загруженного файла аватара во временный каталог.
  Пока не пройдена валидация форм и данные не отправляются в БД, загруженный
  пользователем файл аватара записывается во временный каталог tmp, который в
  каталоге (x2::Request::docRoot() + x2::Cfg::media + /avatars).
*/
void UserProfile::loadAvatar() {
  auto Throw = [] ( const Str &msg ) {
    throw std::runtime_error( "UserProfile::loadAvatar(): " + msg );
  };
  auto mkDir = [ Throw ] ( const Str &dir ) {
    if( mkdir( dir.data(), 0777 ) == -1 && errno != EEXIST )
      Throw( utl::f( "mkdir({1}) failed", dir ));
  };
  File &ava = main.uploadedAvatar;
  if( ava.get().empty()) return;
  if( ava.get().size() > cfg.avatar.maxSize )
    ava.valid = false;
  else
    if( strncmp( ava.contentType().data(), "image/", 6 )) {
      ava.valid = false;
      ava.message = tr("bad file type for the avatar, image required");
    } else {
      Str docRoot = request.docRoot();
      Str fName( docRoot + cfg.media + "/avatars" ); mkDir( fName );
      fName += "/tmp"; mkDir( fName );
      Str extension = ava.filename().substr( ava.filename().rfind( '.' ));
      if( extension.empty()) Throw( utl::f( "empty extension of {1}", ava.filename()));
      // generating of temporary name
      fName += '/' + t2s( time( 0 ), "%F_%H_%M_%S" )
            + n2s( pthread_self() % 100000, "_%5.5i" ) + extension;
      std::ofstream out( fName ); out << ava.get(); if( ! out.good())
        Throw( utl::f( "std::ofstream({1}) failed", fName ));
      main.avatar.set( fName.data() + docRoot.size() /* minus docRoot */ );
      main.avatar.changed = true;
    }
  ava.set( Str());
}

void UserProfile::checkForAUserIsAdmin() {
/** An admin, exept for predefined::user::admin, cannot edit another admin
    and any predefined user */
  for( const auto &g: groupsOfUser )
    if( ( ( g.id() == predefined::group::admin && g.belongs())
          || ( predefined::user::isPredefined( id ) && id ))
        && Page::user.isAdmin && Page::user.id != predefined::user::admin
        && id != Page::user.id )
    {
      accessLevel = ViewLevel;
      return;
    }
}

bool UserProfile::validate() {
  if( actions.remove || ! removeCommand.empty()) return true;

  if( ! Page::validate()) return false;

  if( ! changed ) return true;

  if( ! main.password.readonly()) {
    boost::regex regex( dta::UserResult::passwordRegex());
    Str pass = main.password;
    if( ! boost::regex_match( pass, regex )) {
      main.password.valid = false;
      return false;
    } else if( main.password.get() != main.password2.get()) {
      main.password.valid = false; main.password2.valid = false;
      main.password.message = tr("The passwords do not match");
      main.password2.message = main.password.message;
      return false;
    } else if( id && user.name() != main.name.get() && pass.empty()) {
      main.password.valid = false; main.password2.valid = false;
      main.password.message = tr("The passwords must be entered, because the name is changed");
      main.password2.message = main.password.message;
      return false;
    }
  }

  return checkExistenceNameAndEmail();
}

bool UserProfile::checkExistenceNameAndEmail() {
  sql::Result r( sql());
  r.exec( utl::pf( "WITH n AS (SELECT COUNT(*) AS c FROM users"
    " WHERE name={1}), e AS (SELECT COUNT(*) AS c FROM users WHERE"
    " email={2}) SELECT n.c AS n, e.c AS e FROM n,e",
    main.name.get(), main.email.get())).row();
  int n = r[ "n" ].get<int>(), e = r[ "e" ].get<int>();
  if( n  && main.name.changed ) {
    main.name.valid = false;
    main.name.message = "The name you entered is already registered by another user.";
  }
  if( e && main.email.changed ) {
    main.email.valid = false;
    main.email.message = "The email you entered already exists in database.";
  }
  return ( ! main.name.changed || ! n ) && ( ! main.email.changed || ! e );
}

void UserProfile::updateGroupsOfUserAndRestrictions() {

  auto updateUsers2groups = [ this ]
  ( Id gId, bool belongs, const std::tm &before = std::tm(),
    const Str &description = Str(), bool change = false )
  {
    if( ! change ) {
      if( ! belongs && gId < 0 )
        sql( utl::pf( "DELETE FROM restricted WHERE user_id={1} AND group_id={2}",
          id, gId ));
      sql( utl::pf( ! belongs ?
        "DELETE FROM users2groups WHERE user_id={1} AND group_id={2}" :
        "INSERT INTO users2groups (user_id, group_id) VALUES({1},{2})",
          id, gId ));
      if( belongs && gId < 0 )
        sql( utl::pf( "INSERT INTO restricted (user_id, group_id, before, description)"
          " VALUES({1},{2},{3},{4})", id, gId, before, description ));
    } else
      sql( utl::pf( "UPDATE restricted SET before={1}, description={2}, modified=NOW()"
        " WHERE user_id={3} AND group_id={4}", before, description, id, gId ));
  };

  if( ! id ) return;

  if( isNew && ! Page::user.isAdmin )
    /* for new accounts default group "users" */
    updateUsers2groups( predefined::group::users, true );
  if( accessLevel < AdminLevel && partsOfCurrentUser.empty())
    return;
  for( auto &g: groupsOfUser )
    if( g.id() >= 0 ) { // groups
      if( groupsOfUserDiff.find( sGoUId( g.id())) != groupsOfUserDiff.end())
        updateUsers2groups( g.id(), ! g.belongs());
    } else { // restrictions
      if( ( g.id() == predefined::group::notActivated || g.id() == predefined::group::notApproved )
         && restrictionsDiff.find( sGoUId( g.id())) != restrictionsDiff.end()
      )
        { updateUsers2groups( g.id(), ! g.belongs()); continue; }
      // banned
      for( auto &b: main.bannedInGroups ) if( g.id() == b.groupId ) {
        if( g.belongs() != b.switcher.get())
          updateUsers2groups( b.groupId, b.switcher.get(),
            sql::parseTime( b.before.get().data()), b.description );
        if( b.get() && b.values.checked )
          updateUsers2groups( b.groupId, true,
            sql::parseTime( b.values.before.data()), b.values.description, true );
      }
    } // end of restrictions
}

/** Если groupId нулевое, то удаляются все ограничения и группы */
void UserProfile::cleanMembership( sql::PhSession &sql, Id userId, Id groupId ) {
  Str whereGroup = groupId ? ( " AND group_id=" + n2s( groupId )) : Str();
  sql( utl::Pf( "DELETE FROM restricted WHERE user_id={1}" + whereGroup )( userId ));
  sql( utl::Pf( "DELETE FROM users2groups WHERE user_id={1}" + whereGroup )( userId ));
}

void UserProfile::setMembership( Id groupId ) {
  sql( utl::pf( "INSERT INTO users2groups (user_id, group_id) VALUES({1},{2})",
    id, groupId ));
  sql( utl::pf( "INSERT INTO restricted (user_id, group_id, before)"
    " VALUES({1},{2},'2100-01-01')", id, groupId ));
}

bool UserProfile::save() {

  auto redirect = [ this ] {
    response.redirect(
      Page::user.id == id /* current user is the owner of the profile */
        ? main.referer
        : ( urlOf<adm::Admin>( id ) + utl::f( "/0#user{1}", id )));
  };

  auto password = [ this ] {
    return main.password.get().empty() ? Str()
      : utl::sha1( main.password.get() + main.name.get());
  };

  auto validateEmail = [ this, redirect ] {
    sql.openIfNotOpened();
    cleanMembership( predefined::group::notActivated );
    setMembership( predefined::group::notActivated );
    mail4activate( main.email, id );
    renderMsgBox( msgBoxInfo |  msgBoxOK, tr("<h3>Mail sent</h3>"
      "<p>You will receive an email shortly with link for activating your"
      " account.</p>"), urlOf<ph::Phorum>());
  };

  try {
    // remove
    if( actions.remove || ! removeCommand.empty()) { removeUser(); return true; }
    // validateEmail
    if( main.validateEmail && ! changed ) { validateEmail(); return true; }
    // not changed
    if( ! changed ) { redirect(); return true; }

    sql::Transaction guard( sql );
    dta::Users2rise users2rise;
    if( main.humanName.changed ) user.humanName( main.humanName );
    if( main.webSite.changed ) user.webSite( main.webSite );
    if( main.phone2.changed ) user.phone2( main.phone2 );
    if( main.avatar.changed ) user.avatar( main.avatar );
    if( preferences.locale.changed ) user.locale( preferences.locale );
    if( preferences.theme.changed ) user.theme( preferences.theme );
    Bools answers; preferences.answers.get( answers );
    if( preferences.answers[ 0 ].changed ) user.doNotTrackAnswers( answers[ 0 ] );
    if( preferences.answers[ 1 ].changed ) user.mailOnAnswer( answers[ 1 ] );
    Bools quotes; preferences.quotes.get( quotes );
    if( preferences.quotes[ 0 ].changed ) user.doNotTrackQuotes( quotes[ 0 ] );
    if( preferences.quotes[ 1 ].changed ) user.mailOnQuote( quotes[ 1 ] );
    if( id ) {
      if( changed ) {
        if( isNew && id != predefined::user::anonymous )
          users2rise.insert( id );
        id = user.update( sql(), id, main.name, main.email, main.phone,
                                 password(), isNew );
      }
    } else
      id = user.insert( sql(), main.name, main.email, main.phone,
                               request.clientIp(), password());
    updateGroupsOfUserAndRestrictions();
    if ( Page::user.isAnonymous /* created himself */ || id == Page::user.id /* changed himself */ )
      cookie.set( id, preferences.locale );

    users2rise.insert( id );
    if( isNew ) {
      if( cfg.admin.policy.newUser.requiresApproval )
        setMembership( predefined::group::notApproved );
      for( const Str admName: cfg.admin.policy.newUser.notify ) {
        sql::Result r( sql );
        r.exec( utl::pf( "SELECT id FROM users WHERE name={1}", admName )).row();
        if( r.empty())
          throw std::runtime_error( "error in config: admin not found '" + admName + "'" );
        else {
          int user2notify = r[ 0 ].get<Id>();
          sendSysMsgAboutNewUser( user2notify, id );
          users2rise.insert( user2notify );
        }
      }
    }

    if( ( main.email.changed || main.validateEmail )
      && cfg.admin.policy.validateEmail
    )
      validateEmail();
    else
      redirect();

    saveAvatar();

    guard.commit();

    users2rise.rise();
    cache.rise( dta::ComplexUserResult::usersAllPagesCacheKey());

  } catch( const std::exception &e ) { CAUGHT_EXCEPTION( e ); return false; }

  return true;
}

/** \brief Окончательная запись загруженного файла аватара и нормализация.
  Из временного каталога (x2::Request::docRoot() + x2::Cfg::media +
  /avatars/tmp) аватар перемещается в каталог (x2::Request::docRoot()
  + x2::Cfg::media + /avatars).
  Название файла складывается из идентификатора пользователя и расширения
  загруженного файла. Это если аватар загружен пользователем с компьютера.
  \n\n Потом уже для всех, а не только загруженных с компьютера, выполняется
  нормализация. Для аватаров, url которых начинаются с x2::Cfg::media, это
  начало заменяется на "/media".
*/
void UserProfile::saveAvatar() {
  // temporary avatar to permanent
  Str tmpPrefix( cfg.media + "/avatars/tmp/" ),
      tmpName( main.avatar ),
      permanentName;
  if( ! strncmp( tmpName.data(), tmpPrefix.data(), tmpPrefix.size())) {
    permanentName = cfg.media + "/avatars/" + n2s( id )
                    + /* extension */ tmpName.substr( tmpName.rfind( '.' ));
    Str docRoot = request.docRoot();
    if( rename( ( docRoot + tmpName ).data(),
                ( docRoot + permanentName ).data()) == -1 )
      throw std::runtime_error( utl::f( "UserProfile::saveAvatar(): rename({1},{2}) failed",
                                        tmpName, permanentName ));
  } else
    permanentName = main.avatar;
  // normalize the avatar path
  if( ! strncmp( permanentName.data(), ( cfg.media + '/' ).data(), cfg.media.size() + 1 ))
    permanentName = "/media" + permanentName.substr( cfg.media.size());
  // save the new name
  if( tmpName != permanentName ) {
    user.avatar( permanentName );
    sql.exec( utl::pf( "UPDATE users SET properties={1}, modified=NOW() WHERE id={2}",
                       user.allProperties.str(), id ));
  }
}

void UserProfile::mail4activate( Str to, Id userId ) {
  mail( to, "Please verify your account",
  "To validate your X1 Phorum account, please click on the URL below.\r\n"
  "\r\n" + request.hostAsPrefix()
  + urlOf<Activate>( userId, Activate::createActivationKey( to, userId )) +
  "\r\n"
  "\r\n"
  "Once verified, you can login to X1 Phorum at\r\n" +
  request.hostAsPrefix() + urlOf<Login>());
}

// Activate

Activate::Activate( const Thread &thread ) : Page( thread ) {
  if( ! url.args( url.tail, id, activationKey )) throw AddressError();
}

Str Activate::createActivationKey( Str mail, Id userId ) {
  return utl::sha1( cfg.crypt + n2s( userId ) + mail );
}

void Activate::load() {
  try {
    dta::UserResult user( sql(), id );
    if( activationKey == createActivationKey( user.email(), id )) {
      UserProfile::cleanMembership( sql, id, predefined::group::notActivated );
      cache.rise( dta::UserResult::cacheKey( id ));
      renderMsgBox( msgBoxOK | msgBoxInfo, tr("E-Mail address verified. Thank you."),
        urlOf<ph::Phorum>());
    } else
      throw dta::UserResult::ErrorNotFound( Str());
  } catch( const dta::UserResult::ErrorNotFound &e ) {
    renderMsgBox( msgBoxOK | msgBoxError, "Activate::load(): " + tr("Forbidden"),
      urlOf<ph::Phorum>());
    response.setStatus( 403 );
  } catch( const std::exception &e ) {
    renderMsgBox( msgBoxOK | msgBoxError, e.what(), urlOf<Home>());
    CAUGHT_EXCEPTION( e );
  }
}

} // x2

