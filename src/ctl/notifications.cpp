/**
    \file ctl/notifications.cpp
    \brief controller for notifications

    This file is a part of x1 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Notifications

const char Notifications::isPrivateId[] = "private";

Notifications::Notifications( const Thread &thread )
  : Page( thread ), form( *this ), isPrivateMessages( url.tail == isPrivateId ),
    notifications( sql(), isPrivateMessages )
{
  menuType = MenuSideBar;

  if( isPrivateMessages ) theTitle += " :: " + tr("Messages for");
  else theTitle += " :: " + tr("Answers&amp;Quotes to posts of");
  theTitle += ' ' + user.name;
}

void Notifications::load() {
  if( user.hasAnonymousId()) { response.redirect( urlOf<Home>()); return; }
  try {
    notifications.init( user.id );
  } catch( const std::exception &e ) { CAUGHT_EXCEPTION( e ); }
}

bool Notifications::save() {
  if( msgBox()) {
    if( approvedPressed()) {
      try {
        notifications.removeNotifications( sql, isPrivateMessages, user.id );
        cache.rise( dta::UserResult::cacheKey( user.id ));
      } catch (const std::exception& e) {
        CAUGHT_EXCEPTION( e ); return false;
      }
    }
    response.redirect( urlOf<Home>());
  } else {
    Str formAction = url() + ( isPrivateMessages ? '/' + Str( isPrivateId ) : Str());
    renderMsgBox( msgBoxWarning | msgBoxYes | msgBoxNo | msgBoxIDoNotKnow,
        tr("All your notifications will be deleted."
           " This operation can not be undone. Are you sure?"),
        formAction, formAction );
  }

  return true;
}

namespace ph {

// Delivered

Delivered::Delivered( const Thread &thread ) : Base( thread ) {
  if( ! Url::args( url.tail, id )) throw AddressError();

  theTitle += " :: " + tr("Delivered"); // will be shown in case of error
}

bool Delivered::save() {
/** Сообщение прочитано, удалить извещение из списка новых. */
  if( user.id != predefined::user::anonymous ) {
    try {
      dta::NotificationsResult::remove( sql(), user.id, id );
      cache.rise( dta::UserResult::cacheKey( user.id ));
    } catch( const std::exception& e ) { CAUGHT_EXCEPTION( e ); return false; }
    response.redirect( request.referer() + "#post" + n2s( id ));
  }
  return true;
}

} // ph

// Page

/**
  \brief Sends notification to user "toId".

  В случае личного сообщения, уведомление генерируется безусловно.
  Для случаев ответов или цитирования уведомление генерируется или
  нет в зависимости от предпочтений пользователя.
  Личное сообщение посылается пользователю "toId" от пользователя
  "msg.author()", в "msg" в этом случае само сообщение. Для случаев ответа или
  цитирования уведомление создается пользователю "toId" о сообщении "msg"
  пользователя "msg.author()".
  \n "bbDecoded" заполнено, если "what" == notificationQuote или
  "what" == notificationAnswer.
*/
void Page::generateNotification( Id toId, Id messageId, bool doNotMail,
  dta::Users2rise &users2rise, NotificationWhat what, Str bbDecoded )
{
  dta::ph::PostsResult msg( sql );
  try {
    msg.init( messageId );
  } catch ( std::exception &e ) {
    throw std::runtime_error( "can not generate notification for messageId="
      + n2s( messageId ) + "; " + e.what());
  }

  if( toId == msg.author() || users2rise.find( toId ) != users2rise.end())
    return;

  dta::UserResult to( sql ), from( sql );
  try {
    to.findById( toId ); from.findById( msg.author());
  } catch ( std::exception &e ) {
    throw std::runtime_error( "can not generate notification for user id="
      + n2s( toId ) + " or addressee id=" + n2s( msg.author()) + "; " + e.what());
  }

  bool isPrivate = what == notificationPrivate;
  if( isPrivate || ( what == notificationAnswer && ! to.doNotTrackAnswers())
                || ( what == notificationQuote && ! to.doNotTrackQuotes())
  ) {
    dta::NotificationsResult::insert( sql, to.id(), msg.id(), what );
    users2rise.insert( to.id());
  }
  if( doNotMail || ( what == notificationAnswer && ! to.mailOnAnswer())
                || ( what == notificationQuote && ! to.mailOnQuote())
                || toId <= predefined::user::anonymous
  )
    return;

  Str text, html,
      title = utl::htEsc( msg.title()),
      abstract = utl::htEsc( msg.abstract());
  if( msg.format() == dta::ph::fText )
    text = msg.content();
  else if( msg.format() == dta::ph::fHtml )
    html = msg.content();
  else if( msg.format() == dta::ph::fBbcode ) {
    text = msg.content();
    if( ! isPrivate )
      html = bbDecoded;
    else
      html = bbcode( msg.content(), true );
  } else if( msg.format() == dta::ph::fMarkdown ) {
    text = msg.content();
    html = xss( markdown( msg.content()));
  }
  Str msgUrl = utl::F( "{1}{2}/{3}#post{3}" )
      % request.hostAsPrefix() % urlOf<ph::Topic>() % messageId;
  Str fmtText;
  if( ! text.empty()) {
    if( ! abstract.empty())
      text = abstract + "\r\n" + text;
    if( isPrivate )
      fmtText = tr(
"Hello {1}!\n"
"You've got a private message from {2}\n--BEGIN OF PRIVATE MESSAGE--\n"
"{3}\n\n"
"{4}\n--END OF PRIVATE MESSAGE--\n"
"URL of this message see below\n<{5}>\n" );
    else if( what == notificationQuote )
      fmtText = tr(
"Hello {1}!\n"
"{2} qouted you in this message\n--BEGIN OF MESSAGE--\n"
"{3}\n\n"
"{4}\n--END OF MESSAGE--\n"
"URL of this message see below\n<{5}>\n" );
    else fmtText = tr(
"Hello {1}!\n"
"{2} replied to your post in this message\n--BEGIN OF MESSAGE--\n"
"{3}\n\n"
"{4}\n--END OF MESSAGE--\n"
"URL of this message see below\n<{5}>\n" );
    fmtText = utl::replace( fmtText, "\n", "\r\n" );
    text = utl::F( fmtText )
      % ( to.humanName().empty() ? to.name() : to.humanName())
      % ( from.humanName().empty() ? from.name() : from.humanName())
      % title % text % msgUrl;
  }
  if( ! html.empty()) {
    if( isPrivate )
      fmtText = tr(
"<h2>Hello {1}!</h2>"
"<p>You've got a private message from <b><i>{2}</i></b><br>--BEGIN OF PRIVATE MESSAGE--</p>"
"<h1>{3}</h1>"
"<h3>{4}</h3>{5}<p>--END OF PRIVATE MESSAGE--<br>"
"URL of this message see below<br><a href=\"{6}\">{6}</a></p>" );
    else if( what == notificationQuote )
      fmtText = tr(
"<h2>Hello {1}!</h2>"
"<p><b><i>{2}</i></b> quoted you in this message<br>--BEGIN OF MESSAGE--</p>"
"<h1>{3}</h1>"
"<h3>{4}</h3>{5}<p>--END OF MESSAGE--<br>"
"URL of this message see below<br><a href=\"{6}\">{6}</a></p>" );
    else fmtText = tr(
"<h2>Hello {1}!</h2>"
"<p><b><i>{2}</i></b> replied to your post in this message<br>--BEGIN OF MESSAGE--</p>"
"<h1>{3}</h1>"
"<h3>{4}</h3>{5}<p>--END OF MESSAGE--<br>"
"URL of this message see below<br><a href=\"{6}\">{6}</a></p>" );
    html = utl::F( fmtText )
      % ( to.humanName().empty() ? to.name() : to.humanName())
      % ( from.humanName().empty() ? from.name() : from.humanName())
      % title % abstract % html % msgUrl;
  }
  Str subj;
  if( isPrivate )
    subj = tr( "You've got a new private message" );
  else if( what == notificationQuote )
    subj = tr( "People quoted you" );
  else
    subj = tr( "You have new answers" );
  try {
    mail( to.email(), subj, text, html );
  } catch ( const std::exception& e ) {
    Str error = "SMTP, mail(" + to.email() + "); " + e.what();
    LOG_ERROR( error );
    if( toId != predefined::user::admin )
      generateMessage( predefined::user::admin, 0, "SMTP, mail failed", Str(),
        "Hello Admin!\r\n------------\r\n"
        "There is critical server error here:\r\n" + error + "\r\n",
        dta::ph::fMarkdown,
        predefined::user::system
      );
  }
}

} // x2

