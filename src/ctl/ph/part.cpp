/**
    \file ctl/ph/part.cpp
    \brief controller for the ph part page

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace ph {

// Part

Part::Part( const Thread &thread )
  : Base( thread ), part( sql ), topics( sql ), nav( *this )
{
  if( url.args( url.tail, id, nav.currPage )) {
    if( ! id ) id = predefined::part::news;
  } else
    throw AddressError();
}

Part::Part( const Page &p, Id id, int pageNumber )
  : Base( p, theUrl ), id( id ), part( sql ), topics( sql ),
  nav( *this ), theUrl( dispatcher.url(), key())
{
  nav.currPage = pageNumber;
}

void Part::load() {
  try {
    if( ! part.init( id, user )) {
      response.setStatus( 404 );
      error = utl::f( "Part Id={1} not found", id );
      return;
    }

    topics.init( id, nav.currPage );

    if( ! part.rights.R()) {
      response.redirect( urlOf<Home>());
      return;
    }

    nav.urlForPageFormat = url( id, "{1}" );
    nav.maxPage = part.maxTopicsPage();

    headers.init( part.name(), part.description(), Str(), Str(), Str(), Str(),
      part.id() == predefined::part::pages );
    theTitle =       headers.partTitle();
    theDescription = headers.partDescr();

    // LastModified
    LastModified lm( topics.answerTime());
    if( lm.notModified( *this ))
      return;

  } catch( const std::exception &e ) {
    CAUGHT_EXCEPTION( e ); theTitle = "Error";
  }
}

// PrivatePart

PrivatePart::PrivatePart( const Thread &thread )
  : Base( thread )
{ theTitle += " :: PrivatePart Page"; }

void PrivatePart::render() {
  if( user.hasAnonymousId())
    response.redirect( urlOf<ph::Phorum>());
  else {
    Part part( *this, - user.id ); part.doGet();
  }
}

}} // ph, x2

