/**
    \file ctl/ph/template.cpp
    template parser for x2 application

    This file is a part of x1 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"


namespace x2 {
namespace ph {

std::map<Str, Template::TmplHandler> Template::tmplHandlers = {
  { "post",         &Template::templateHandlerPost },
  { "topic",        &Template::templateHandlerTopic },
  { "lastComments", &Template::templateHandlerLastComments }
};

// utils

inline const int skipSpaces( const char &c ) {
  const char *i = &c; while( isspace( *i )) i ++;
  return i - &c;
}


class Variables { public:
  Variables( const std::map<Str, Str> &params ) : params( params ) { }
  Str get( Str name ) {
    auto found = params.find( name );
    if( found == params.end()) return Str();
    return found->second;
  }
  bool getBool( Str name ) {
    Str str = get( name );
    return ! strcasecmp( str.data(), "true" ) || ! strcasecmp( str.data(), "on" )
      || ! strcasecmp( str.data(), "t" ) || str == "1";
  }
  int getInt( Str name ) { return utl::s2i( get( name )); }
  Id getId( Str name ) { return utl::s2id( get( name )); }
private:
  const std::map<Str, Str> &params;
};

// end of utils

Str Template::templateHandlerPost( const std::map<Str, Str> &params ) {
  Variables vars( params );
  Str r;
  dta::ph::PostsResult post( page.sql ); dta::ph::TopicsResult topic( page.sql );
  dta::ph::PartsResult part( page.sql );
  try {
    Id id;
    if( ! ( id = vars.getId( "id" )))
      throw std::runtime_error( "id must be set" );
    if( ! post.init( id ))
      throw std::runtime_error( utl::f( "can not open post id={1}", id ));
    if( ! topic.init( post.topicId()))
      throw std::runtime_error( utl::f( "can not open topic id={1}", post.topicId()));
    if( ! part.init( topic.partId(), page.user ))
      throw std::runtime_error( utl::f( "can not open part id={1}", topic.partId()));
    if( ! part.rights.R())
      throw std::runtime_error( utl::f( "invisible (for the userId={1}) part id={2}",
        page.user.id, topic.partId()));
    bool hasTitle    = ! vars.getBool( "noTitle" ),
         hasAbstract = ! vars.getBool( "noAbstract" ),
         hasContent  = ! vars.getBool( "noContent" ),
         hasBody     = hasTitle || hasAbstract || hasContent,
         hasTime     = ! vars.getBool( "noTime" );
    if( hasBody ) {
      r += "<div class=\"postBody\">\n";
      if( vars.getBool( "author" )) {
        r += "<div class=\"postAuthor\">\n"
          + Link( Page::urlOf<UserProfile>( post.author()),
          "<span style=\"font-size:50%\">" + tr("by") + " </span>" + post.authorName(),
          tr("The author of the message")).html()
          + "</div><!-- end of postAuthor -->\n";
      }
      if( hasTitle ) {
        r += Link( Page::urlOf<ph::Topic>( id ) + utl::f( "/#post{1}", id ),
             "<h3 class=\"postTitle\">" + utl::htEsc( post.title()) + "</h3>",
             tr("Link to") + " &quot;" + post.title() + "&quot;" ).html();
      }
      if( hasAbstract )
        r += "<h4 class=\"postAbstract\">" + utl::htEsc( post.abstract()) + "</h4>\n";
      if( hasContent ) {
        r += "<div class=\"postContent\">\n";
        if( post.format() == dta::ph::fText )
          r += "<pre>" + utl::htEsc( post.content()) + "</pre>\n";
        else if( post.format() == dta::ph::fMarkdown )
          r += page.xss( page.markdown( post.content())) + '\n';
        else if( post.format() == dta::ph::fBbcode )
          r += page.bbcode( post.content()) + '\n';
        else if( post.format() == dta::ph::fHtml || post.format() == dta::ph::fTemplate )
          r += post.content() + '\n';
        else r += "format error";
        r += "</div><!-- end of postContent -->\n";
      }
      if( hasTime )
        r += "\n<h5 class=\"postTime\"><span style=\"font-size:70%\">" + tr("at")
          + " </span><span title=\"" + tr("The time of the message")
          + "\">" + page.t2s( post.created()) + "</span></h5>\n";
      r += "</div><!-- end of postBody -->\n";
    }
  } catch( std::exception &e ) {
    return Str ( "<p class=\"error\">x2:post error: " ) + e.what() + "</p>";
  }
  return r;
}

Str Template::templateHandlerTopic( const std::map<Str, Str> &params ) {
  return "<p>topic</p>";
}

Str Template::templateHandlerLastComments( const std::map<Str, Str> &params ) {
  Variables vars( params );
  short number = vars.getInt( "number" );
  std::vector<Id> lastPosts;
  try {
    dta::ph::LastPostsResult r( page.sql.openIfNotOpened(), page.user ); r.init();
    lastPosts = r.get( number );
  } catch( std::exception &e ) {
    LOG_ERROR( e.what());
    return Str ( "<p class=\"error\">x2:lastComments error: " ) + e.what() + "</p>";
  }
  Str templateHandlerLastComments;

  std::map<Str, Str> newParams = params;
  if( params.find( "abstract" ) == params.end())
    newParams[ "noAbstract" ] = "1";
  if( params.find( "content" ) == params.end())
    newParams[ "noContent" ] = "1";

  Str style = vars.get( "style" );
  if( ! style.empty()) style = " style=\"" + style + "\"";
  templateHandlerLastComments += "<div class=\"lastPosts\"" + style + ">\n";

  for( auto id: lastPosts ) {
    newParams[ "id" ] = n2s( id );
    templateHandlerLastComments += templateHandlerPost( newParams );
  }
  return templateHandlerLastComments + "</div><!-- end of lastPosts -->\n";
}

bool Template::fit( Id postId, Id topicId, Id partId, const Str &format ) {
  return partId == predefined::part::pages && postId == topicId && format == dta::ph::fTemplate;
}

Str Template::toHtml( const Str &content )
{
  Str r /* result */; const Str &i = content;
  int b = 0 /* begin */, c = 0 /* current position */;
loop:
  while( const char *lt = strchr( &i[ c ], '<' )) {
    c += lt - &i[ c ] + 1; // '<' found
    c += skipSpaces( i[ c ] ); // spaces after '<'
    char x2[] = "x1"; if( strncmp( &i[ c ], x2, sizeof x2 - 1 )) goto loop; // "< x2" found
    c += sizeof x2 - 1; c += skipSpaces( i[ c ] ); // spaces after "< x2"
    if( i[ c ] != ':' ) goto loop;  // "< x2 :" found
    c ++; c += skipSpaces( i[ c ] ); // spaces after "< x2 :"
    if( ! isalpha( i[ c ] )) goto loop; //  "< x2 : " and first letter of the method found
    char methodLength = 1; // length of method name
    while( isalnum( i[ c + methodLength ] )) methodLength ++;
    Str method( &i[ c ], methodLength ); c += methodLength;
    auto foundMethod = tmplHandlers.find( method );
    if( foundMethod == tmplHandlers.end()) goto loop; // the method found
    std::map<Str, Str> attributes;
    for(;;) { // attributes
      c += skipSpaces( i[ c ] );
      if( i[ c ] == '>' ) break; // end of template method
      if( ! isalpha( i[ c ] )) goto loop; // first letter of the attribute found
      char attrLength = 1; // length of attribute name
      while( isalnum( i[ c + attrLength ] )) attrLength ++;
      Str attribut( &i[ c ], attrLength ); c += attrLength; // the attribute name found
      c += skipSpaces( i[ c ] ); // spaces after the attribut name
      if( i[ c ] != '=' ) goto loop;  // "attr =" found
      c ++; c += skipSpaces( i[ c ] ); // spaces after "attr ="
      if( i[ c ] != '"' ) goto loop;  // "attr = \"" found
      c ++; char attrValLength = 0; // length of attribute value
      while( i[ c + attrValLength ] != '"' ) attrValLength ++;
      attributes[ attribut ] = Str( &i[ c ], attrValLength );
      c += attrValLength;
      if( i[ c ] != '"' ) goto loop;  // "attr = \"value\" found
      c ++;
    }
    c ++; // skip '>'
    r += i.substr(  b, lt - &i[ b ] );
    b = c;
    r += ( this->*( foundMethod->second ))( attributes );
  }
  r += Str( &i[ b ] );
  return r;
}

Str Template::tr( const char *text ) {
  return Page::tr( text, cfg.localization.defaultLocale.second );
}

}} // ph, x2

