/**
    \file ctl/ph/phorum.cpp
    \brief controller for ph pages

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace ph {

// Dispatcher

Phorum::Dispatcher::Dispatcher( const Page::Dispatcher &parent )
  : Page::Dispatcher( "ph", parent )
{
  map = { dispatch<Phorum>(),     dispatch<Part>(),       dispatch<PrivatePart>(),
          dispatch<BbcodeData>(), dispatch<Topic>(),      dispatch<Post>(),
          dispatch<Remove>(),     dispatch<Delivered>(),  dispatch<Close>(),
          dispatch<Trash>() };
}

// Phorum

Phorum::Phorum( const Thread &thread ) : Base( thread ), parts( sql ) {
  theTitle += " :: Main Phorum Page";
  theDescription += " :: Main Phorum Page";
}

void Phorum::load() {
  try {
    parts.init();

    // LastModified
    LastModified lm( parts.answerTime());
    if( lm.notModified( *this ))
      return;

  } catch ( const std::exception &e ) { CAUGHT_EXCEPTION( e ); }
}

bool Phorum::prepareItem() {
  dta::PartAccess access( parts.id());
  access.getData( sql );
  parts.getRights( user, access );
  if( ( parts.id() == predefined::part::pages && ! parts.rights.W())
    || ! parts.rights.R()  ) return false;// not visible
  // Statistics
  topicsNumber += parts.topicsNumber();
  postsNumber += parts.postsNumber();
  if( parts.lastMessage() > lastMessage ) {
    lastMessage           = parts.lastMessage();
    //lastMessageAuthor     = parts.lastMessageAuthor;
    lastMessageTitle      = parts.lastMessageTitle();
    lastMessageAuthorName = parts.lastMessageAuthorName();
    lastMessageTime       = parts.lastMessageTime();
  }
  // end of Statistics
  return true;
}

}} // ph, x2

