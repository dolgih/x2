/**
    \file ctl/ph/actions.cpp
    \brief controller for the close or trash a topic or delivere a post

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace ph {

// Close

Close::Close( const Thread &thread ) : Base( thread ) {
  if( ! Url::args( url.tail, id )) throw AddressError();

  theTitle += " :: " + tr("Close Topic"); // will be shown in case of error
}

bool Close::save() {
/** The method closes an opened topic or openes a closed topic. */
  dta::ph::PartsResult part( sql );
  dta::ph::TopicsResult topic( sql );

  try {
    if( ! topic.init( id )) {
      response.setStatus( 404 );
      error = utl::f( "Topic Id={1} not found", id );
      return false;
    }
    part.init( topic.partId(), user );
    if( topic.canClose( part, user )) {
      sql::Transaction guard( sql());
        sql::Result r( sql );
        r.exec( utl::pf( "UPDATE topics SET closed={1} WHERE id={2} OR"
          " id IN (SELECT topic_id FROM corresponds(ARRAY[CAST({2} AS INT)]))"
          " RETURNING id", topic.closed() ? Str() : "C", id ));
        sql( utl::pf( "DELETE FROM correspondence WHERE to_topic={1} OR from_topic={1}",
          id ));
      guard.commit();
      while( r.next())
        cache.rise( dta::ph::TopicsResult::cacheKey( r[ 0 ].get<Id>()));
    }
  } catch( const std::exception& e ) {
    CAUGHT_EXCEPTION( e ); return false;
  }
  response.redirect( request.referer());
  return true;
}

// Trash

Trash::Trash( const Thread &thread ) : Base( thread ) {
  if( ! Url::args( url.tail, id )) throw AddressError();

  theTitle += " :: " + tr("Move Topic to Trash"); // will be shown in case of error
}

bool Trash::save() {
/** The method moves to trash a topic. */
  if( ! approvedPressed()) return false;

  if( predefined::page::isPredefined( id )) {
    response.redirect( urlOf<Home>());
    return true;
  }

  dta::ph::PartsResult part( sql );
  dta::ph::TopicsResult topic( sql );

  try {
    if( ! topic.init( id )) {
      response.setStatus( 404 );
      error = utl::f( "Topic Id={1} not found", id );
      return false;
    }
    part.init( topic.partId(), user );
    if( topic.canMoveToTrash( part, user )) {
      sql()
      ( utl::pf( "WITH u AS (UPDATE topics SET part_id={1} WHERE id={2} RETURNING part_id)"
        "SELECT COUNT(*) FROM recalc_parts( ARRAY(SELECT part_id FROM topics WHERE id={2})"
        " || ARRAY(SELECT part_id FROM u GROUP BY part_id))",
        predefined::part::bin, id )).row();
      cache.rise( dta::ph::TopicsResult::topicsOfPartAllPagesCacheKey( part.id()));
      cache.rise( dta::ph::TopicsResult::topicsOfPartAllPagesCacheKey( predefined::part::bin ));
      cache.rise( dta::ph::PartsResult::allPartsCacheKey());
      response.redirect( urlOf<ph::Part>( part.id()));
    }
  } catch( const std::exception& e ) { CAUGHT_EXCEPTION( e ); return false; }
  return true;
}

void Trash::render() {
  if( msgBox()) Base::render();
  else {
    renderMsgBox( msgBoxYes | msgBoxNo | msgBoxCancel | msgBoxIDoNotKnow | msgBoxWarning
                | msgBoxLinkForFrom,
      utl::f( tr("The topic ({1}) will be moved to trash. This operation cannot"
      " be reverted. Are you ready?"), id ),
      urlOf<Trash>( id ),
      request.referer());
  }
}

}} // ph, x2

