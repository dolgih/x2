/**
    \file ctl/ph/topic.cpp
    \brief controller for the ph topic page

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace ph {

// BbCodable

utl::Json BbCodable::data() {
  utl::Json bbcode = utl::Json::createObj(), tags = utl::Json::createObj(), smiles = utl::Json::createObj();
  for( const auto &i: bbcode::BbCodable::allTags ) {
    if( i.second->category == bbcode::Descriptor::categoryNone )
      continue;
    utl::Json tag = utl::Json::createObj();
    tag.add( "name", i.second->name );
    tag.add( "category", int( i.second->category ));
    tag.add( "title", page.tr( i.second->title ));
    Str insertion = i.second->insertion(); if( ! insertion.empty())
      tag.add( "insertion", insertion );

    utl::Json values = utl::Json::createArray();
    for( const auto &v: i.second->values()) values.add( v );
    if( values.arraySize()) tag.add( "values", values );

    if( i.second->properties & bbcode::Descriptor::single )
      tag.add( "single", true );
    tags.add( i.first, tag );
  }
  bbcode.add( "categoryBound", int( bbcode::Descriptor::categoryBound ));
  bbcode.add( "tags", tags );
  for( const auto &i: BbCodable::smiles ) {
    utl::Json smile = utl::Json::createObj();
    smile.add( "code" , i.first );
    smile.add( "title" , page.tr( i.second.title.data()));
    smile.add( "path" , i.second.path );
    smiles.add( i.first , smile );
  }
  bbcode.add( "smiles" , smiles );
  return bbcode;
}

/** Находит данные для цитаты. */
void BbCodable::quoteData( long long postId, Str &postTitle, Str &authorName, bool fullUrl )
{
  dta::ph::PostsResult post( page.sql );
  try {
    post.init( postId );
    Str hst = fullUrl ? page.request.hostAsPrefix() : Str();
    postTitle = utl::f( "<a href=\"{1}{2}/{3}#post{3}\">{4}</a>", hst,
      Page::urlOf<ph::Topic>(), postId, post.title());
    authorName = utl::f( "<a href=\"{1}{2}\">{3}</a>", hst,
      Page::urlOf<UserProfile>( post.author()), post.authorName());
  } catch( sql::Error &e ) {
    postTitle = "error";
    authorName = "?";
  }
}

// BasePost

BasePost::BasePost( const Thread &thread, bool newPost )
  : Base( thread ), form( *this ), part( sql ), topic( sql ), post( sql )
{ }

BasePost::BasePost( const Page &page, const Url &url )
  : Base( page, url ), form( *this ), part( sql ), topic( sql ), post( sql )
{ }

// Topic

Topic::Topic( const Thread &thread )
  : BasePost( thread, false ), posts( BasePost::post ), nav( *this )
{
  // или postId/topicId/page, и либо postId либо topicId
  // должен быть ненулевым

  if( ! url.args( url.tail, postId, topicId, nav.currPage )) throw AddressError();
}

Topic::Topic( const Page &p, Id aPostId, Id aTopicId, int aPageNumber )
  : BasePost( p, theUrl ), posts( BasePost::post ),
  nav( *this ), theUrl( dispatcher.url(), key())
{
  postId = aPostId; topicId = aTopicId; nav.currPage = aPageNumber;
}

void Topic::load() {
  try {
    if( posts.init( postId, topicId, nav.currPage ) && posts.next()) {
      topicId = posts.topicId();
      posts.rewind();
    } else {
      response.setStatus( 404 );
      error = utl::f( "Topic of (post={1} topic={2} page={3}) not found",
        postId, topicId, nav.currPage );
      return;
    }

    topic.init( topicId ); partId = topic.partId();
    part.init( topic.partId(), user );

    if( ! part.rights.R()) {
      response.redirect( urlOf<Home>());
      return;
    }

    nav.urlForPageFormat = url( 0, topicId, "{1}" );
    nav.maxPage = topic.maxPostsPage();

    headers.init( part.name(), part.description(), topic.title(), topic.abstract(),
      Str(), Str(), part.id() == predefined::part::pages );
    theTitle       = headers.topicTitle();
    theDescription = headers.topicDescr();

    // activity
    if( ! part.isPrivate()) visited.put( user.id, cookie.getCsrfUS(), timer.tv_sec, topicId );

    if( posts.next()) {
      // LastModified depends on templated posts
      if( ! Template::fit( posts.id(), topicId, partId, posts.format())) {
        LastModified lm( posts.answerTime());
        if( lm.notModified( *this ))
          return;
      }
      nav.currPage = posts.page();
      posts.rewind();
    }

  } catch( const std::exception &e ) {
    CAUGHT_EXCEPTION( e ); theTitle = "Error";
  }
}

}} // ph, x2

