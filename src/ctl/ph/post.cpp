/**
    \file ctl/ph/post.cpp
    \brief controller for the ph edit post page

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace ph {

// Post

Post::Post( const Thread &thread )
  : BasePost( thread, true )
{
  if( ! url.args( url.tail, postId, topicId, partId )) throw AddressError();
/**
  Parameters post, topic, part:\n
  ! post && ! topic && part       : create new topic in the part;\n
  ! post && topic                 : create new post in the topic;\n
  post                            : edit the post;\n
  Special cases:\n
  post < 0 && topic               : create new post in the topic; quote post*(-1)\n
  post && ! topic && private part : create new private message to user in "post" field\n
*/

  /* for preview of a new post */
  author = user.id; authorName = user.name;

  if( partId < 0 && postId && ! topicId ) {
    /* special case: negative partId with null topic and positive postId
       means a new private message to user id = post id.
       It is wrong, but we are Zorro. */
    author2 = postId;
    postId = 0;
    newPrivateMessage = true;
  }
}

void Post::load() {
  try {

    // quote in new post
    Str citation;
    if( postId < 0 && topicId ) {
      if( ! request.isPost()) {
        dta::ph::PostsResult quote( sql );
        if( quote.init( post.quoteId( postId )))
          citation = "[quote=" + n2s( quote.id()) + ']' + quote.content() + "[/quote]";
      }
      postId = 0;
    }
    // end of quote

    // loading
    {                   bool found = true;
      if( postId ) {
        found = found && post.init( postId );
        if( found ) topicId = post.topicId();
      }
      if( topicId ) {
        found = found && topic.init( topicId );
        if( found ) partId = topic.partId();
      }
      if( partId )
        found = found && part.init( partId, user );
      if( ! part.rights.R()) {
        response.redirect( urlOf<Home>());
        return;
      }
      if( ! found ) {
        response.setStatus( 404 );
        error = utl::f( "Post (post={1} topic={2} part={3}) not found",
          postId, topicId, partId );
        return;
      }
    }

    if( part.isPrivate() && topicId ) {
      /* an answer to private message */
      sql::Result r( sql());
      r.exec( utl::pf( "SELECT c.to_topic, c.from_topic, t.author AS ta,"
        " f.author AS fa, t.author2 AS ta2, f.author2 AS fa2 FROM correspondence AS c"
        " JOIN posts AS t ON c.to_topic=t.id JOIN posts AS f ON c.from_topic=f.id"
        " WHERE c.to_topic={1} OR c.from_topic={1}", topicId )).row();
      if( r.empty())
        throw std::runtime_error( "cannot find corresponding topic");
      author2 = r[ "ta" ].get<Id>();
      correspTopic = r[ "to_topic" ].get<Id>();
      if( author2 == user.id )
        author2 = r[ "ta2" ].get<Id>();
      if( correspTopic == topicId )
        correspTopic = r[ "from_topic" ].get<Id>();
    }

    if( form.author2.get<Id>()) form.author2.get( author2 );

    // DB 2 form
    if( part.rights.H()) {
      form.format.add( dta::ph::fHtml );
      if( partId == predefined::part::pages && isStartTopic())
        form.format.add( dta::ph::fTemplate );
    }
    form.abstract.setOrCmp( postId ? post.abstract() : Str());
    form.content .setOrCmp( postId ? post.content () : citation );
    form.format  .setOrCmp( postId ? post.format  () : Str());
    form.pinned  .setOrCmp( postId ? topic.pinned () : Str());
    form.author2 .setOrCmp( postId ? post.author2() : author2 );
    if( postId ) {
      form.title.setOrCmp ( post.title());
    } else {
      form.setTitle( topicId ? topic.title() : Str());
      form.title.changed = ! form.title.get().empty();
    }
    if( form.title.get().empty()) form.setAutofocus( form.title );
    else form.setAutofocus( form.content );

    if( part.isPrivate()) {
      // authorName2
      dta::UserResult correspondent( sql, author2 );
      authorName2 = correspondent.name();
    }

    // preview
    preview = form.preview.taken && form.preview;

    // headers
    Str topicTitle, topicAbstract, postTitle, postAbstract;
    if( topicId ) { topicTitle = topic.title(); topicAbstract = topic.abstract(); }
    if( preview ) {
      form.title.get( postTitle ); form.abstract.get( postAbstract );
      if( isStartTopic()) { topicTitle = postTitle; topicAbstract = postAbstract; }
    } else
      if( postId ) { postTitle = post.title(); postAbstract = post.abstract(); }
    headers.init( part.name(), part.description(), topicTitle, topicAbstract,
      postTitle, postAbstract, part.id() == predefined::part::pages );
    theTitle       = headers.postTitle();
    theDescription = headers.postDescr();

  } catch( const std::exception &e ) {
    CAUGHT_EXCEPTION( e ); theTitle = "Error";
  }
}

bool Post::validate() {
  bbcode::BbCodeInfo bbCodeInfo;
  ph::BbCodable( *this ).bbDecode( form.content, false, &bbCodeInfo, true/* dry run */ );
  if( bbCodeInfo.thereIsHtmlTag && ! part.rights.H()) {
    form.content.valid = false;
    form.content.message = tr("You do not have access to raw html code writing.");
    return false;
  }

  return BasePost::validate();
}

bool Post::save() {
  auto forbidden = [ this ]
  { response.setStatus( 403 ); error = "403 Forbidden"; return false; };

  try {

    if( part.isPrivate()) {
      if(  user.id <= predefined::user::anonymous
        || author2 <= predefined::user::anonymous
        || ! part.isPrivateOf( user.id )) return forbidden();
    } else
      author2 = user.id;

    if( ( form.format.get() == dta::ph::fHtml || form.format.get() == dta::ph::fTemplate )
      && ! part.rights.H()) return forbidden();

    // set Home Page
    if( form.submit && partId == predefined::part::pages && postId
      && isStartTopic() && form.setAsHome )
    { // set Home Page
      if( ! part.rights.M()) return forbidden();
      if( sql()( utl::pf( "UPDATE options SET value={1},modified=now()"
        " WHERE id='site.home' RETURNING id", postId )).row().empty()
      )
        sql( utl::pf( "INSERT INTO options(id,value)"
          " VALUES('site.home',{1})", postId ));
      cache.rise( Home::key());
    }

    if( preview ) {
      response.addHeader( "X-XSS-Protection", "0" ); // без этого заголовка почему-то не работает
      return false;
    }

    else if( form.submit && changed ) {
      if( postId ) {

        // updating of post (topic)
        if( post.canEdit( part, topic, user )) {
          sql::Transaction guard( sql());
          if( user.isModerator )
            topic.update( sql, topicId, 0, form.pinned.changed,
                               form.pinned, false );
          post.update( sql, postId, form.title, form.abstract,
                            form.content, form.format, author2 );
          dta::Users2rise users2rise;
          generateNotification( postId, true, users2rise );
          guard.commit();
          // rising cache
          users2rise.rise(); // users (notifications)
          if( form.pinned.changed ) /* rise all topics of the part */
            cache.rise( topic.topicsOfPartAllPagesCacheKey( partId ));
          else if
            (
              ( form.title.changed && topic.lastPost() == postId )
              || isStartTopic()
            )
              /* rise one page of topics of the part */ /* TODO to remove? */
              cache.rise( topic.topicsOfPartCacheKey());
          if( form.title.changed && part.lastMessage() == postId )
            /* rise all parts */ /* to remove? */
            cache.rise( part.allPartsCacheKey());
          /* rise one page of posts of the topic */
          cache.rise( post.postsOfTopicCacheKey());
          // end of rising cache
        } else return forbidden();
      } else { // ! postId ( post creating )
        sql::Transaction guard( sql());
        if( user.id == predefined::user::anonymous )
          createAnonymous();
        author = user.id;
        if( ! topicId ) { // topic creating
          if( ! part.rights.C()) return forbidden();
          if( part.isPrivate())
            part.createPrivate( sql, author );
          postId = topicId = topic.insert( sql, partId );
          post.insert( sql, postId, topicId, author, form.title,
                            form.abstract, form.content,
                            form.format, request.clientIp(), author2 );
          part.update( sql, partId, postId, true );
        } else { // post (not topic) creating
          if( ! canAnswer()) return forbidden();
          postId = topic.generateId( sql );
          post.insert( sql, postId, topicId, author, form.title,
                            form.abstract, form.content,
                            form.format, request.clientIp(), author2 );
          part.update( sql, partId, postId, false );
        }
        topic.update( sql, topicId, postId, user.isModerator, form.pinned,
                      true );

        dta::Users2rise users2rise;
        // for private message
        if( part.isPrivate()) {
          correspTopic = generateMessage( author2, correspTopic, form.title, form.abstract,
                                          form.content, form.format, author );
          if( isStartTopic())
            sql( utl::pf( "INSERT INTO correspondence(to_topic, from_topic) VALUES({1},{2})",
                          correspTopic, topicId ));
        }  else { // for public message
          sql( utl::pf( "UPDATE users SET posts_number=posts_number+1 WHERE id={1}",
                        author ));
          generateNotification( postId, false, users2rise );
        }

        guard.commit();
        users2rise.rise(); // users (notifications, posts counts)
        cache.rise( post.postsOfTopicAllPagesCacheKey( topicId ));
        cache.rise( topic.topicsOfPartAllPagesCacheKey( partId ));
        cache.rise( part.allPartsCacheKey());
        if( part.isPrivate()) {
          cache.rise( dta::UserResult::cacheKey( author2 ));
          cache.rise( dta::ph::PostsResult::postsOfTopicAllPagesCacheKey( correspTopic ));
          cache.rise( dta::ph::TopicsResult::topicsOfPartAllPagesCacheKey(
                                         dta::ph::PartsResult::getPrivateOf( author2 )));
        }
      }
    }
  } catch( const std::exception &e ) { CAUGHT_EXCEPTION( e ); return false; }
  response.redirect( urlOf<ph::Topic>( postId, topicId ) + utl::f( "#post{1}", postId ));
  return true;
}

void Post::generateNotification( Id messageId, bool doNotMail, dta::Users2rise &users2rise )
{
  dta::ph::PostsResult msg( sql );
  try {
    msg.init( messageId );
  } catch ( std::exception &e ) {
    throw std::runtime_error( "Post::generateNotification: can not generate notification for messageId="
      + n2s( messageId ) + "; " + e.what());
  }

  Id topicStarter = msg.author();
  if( messageId != msg.topicId()) {
    dta::ph::PostsResult startPost( sql ); startPost.init( msg.topicId());
    topicStarter = startPost.author();
  }

  sql( utl::pf( "DELETE FROM notifications WHERE post_id={1}", messageId ));

  bbcode::BbCodeInfo bbCodeInfo;
  Str html = ph::BbCodable( *this ).bbDecode( msg.content(), true, &bbCodeInfo );
  Page::generateNotification( topicStarter, messageId, doNotMail, users2rise,
    notificationAnswer, html );
  if( msg.format() == dta::ph::fBbcode ) {
    std::set<Id> quotedUsers;
    for( auto i: bbCodeInfo.quotedPosts ) {
      dta::ph::PostsResult post( sql ); post.init( i );
      quotedUsers.insert( post.author());
    }
    for( auto i: quotedUsers )
      Page::generateNotification( i, messageId, doNotMail, users2rise,
        notificationQuote, html );
  }
}

}} // ph, x2

