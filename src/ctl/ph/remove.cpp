/**
    \file ctl/ph/remove.cpp
    \brief controller for the removing of topic(s) or post(s)

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace ph {

// Removing
void Removing::crTbl( Str tblName, Str id, Str from ) {
  sql( "CREATE TEMP TABLE " + tblName +
  " (id INT PRIMARY KEY NOT NULL) WITHOUT OIDS ON COMMIT DROP" );
  sql( "INSERT INTO " + tblName + " SELECT " + id + " FROM " + from );
}

/** The method removes all posts of a part or of an author or one post(topic). */
void Removing::of( int part, int author, int post ) {
  if( predefined::part::pages == part ) return;
  if( author && predefined::user::isPredefined( author )) return;
  if( post && predefined::page::isPredefined( post )) return;

  try {
    if( part ) {
      crTbl( "deleted_topics", "id", utl::pf( "topics WHERE part_id={1}", part ));
      crTbl( "deleted_posts", "id", "posts WHERE topic_id IN (SELECT id"
        " FROM deleted_topics)" );
      crTbl( "updated_topics", "id", "deleted_topics" );
    } else if( author ) {
      crTbl( "deleted_posts", "id", utl::pf( "posts WHERE author={1}", author ));
      crTbl( "updated_topics", "topic_id", utl::pf( "posts WHERE author={1} GROUP"
        " BY topic_id", author ));
      crTbl( "deleted_topics", "topic_id", utl::pf( "posts WHERE author={1} AND"
        " id=topic_id GROUP BY topic_id", author ));
      sql( "INSERT INTO deleted_posts SELECT id FROM posts WHERE topic_id"
        " IN (SELECT id FROM deleted_topics) AND id NOT IN (SELECT id"
        " FROM deleted_posts)" );
    } else { // post or topic
      crTbl( "deleted_posts", "id", utl::pf( "posts WHERE id={1} OR topic_id={1}",
        post ));
      crTbl( "updated_topics", "topic_id", utl::pf( "posts WHERE id={1}",
        post ));
      crTbl( "deleted_topics", "topic_id", utl::pf( "posts WHERE id={1} AND"
        " id=topic_id ", post ));
    }
    crTbl( "updated_parts", "part_id", "topics WHERE id IN (SELECT"
      " id FROM updated_topics) GROUP BY part_id" );
    sql( "UPDATE parts SET last_message=NULL WHERE id IN (SELECT id FROM"
      " updated_parts)" );
    sql( "UPDATE topics SET last_post=NULL WHERE id IN (SELECT id FROM"
      " updated_topics)" );
    crTbl( "closed_topics", "topic_id", "corresponds(ARRAY(SELECT id"
      " FROM deleted_topics))" );
    sql( "UPDATE topics SET closed='C' WHERE id IN (SELECT id FROM"
      " closed_topics)" );
    crTbl( "updated_authors", "author", "posts WHERE id IN (SELECT id"
      " FROM deleted_posts) GROUP BY author" );
    crTbl( "updated_authors2","author2", "posts WHERE id IN (SELECT id"
      " FROM deleted_posts) GROUP BY author2" );
    sql( utl::pf( "WITH d AS(DELETE FROM posts WHERE id IN (SELECT id FROM"
      " deleted_posts) RETURNING *) INSERT INTO posts_log SELECT *,{1} FROM d",
      user ));
    sql( utl::pf( "WITH d AS(DELETE FROM topics WHERE id IN (SELECT id FROM"
      " deleted_topics) RETURNING *) INSERT INTO topics_log SELECT *,{1} FROM d",
      user ));
    sql( "SELECT COUNT(*) FROM recalc_users(ARRAY(SELECT id FROM updated_authors"
      " UNION (SELECT id FROM updated_authors2)))");
    sql( "SELECT COUNT(*) FROM recalc_topics(ARRAY(SELECT id FROM updated_topics))"
      );
    sql( "SELECT COUNT(*) FROM recalc_parts(ARRAY(SELECT id FROM updated_parts))"
      );
    sql::Result r = sql( "SELECT id FROM updated_authors UNION (SELECT id FROM"
      " updated_authors2)" );
    while( r.next())
      updatedAuthors.insert( r[ 0 ].get<int>());
    r = sql( "SELECT id FROM updated_topics UNION (SELECT id FROM closed_topics)" );
    while( r.next())
      updatedTopics.insert( r[ 0 ].get<int>());
    r = sql( "SELECT id FROM updated_parts" );
    while( r.next())
      updatedParts.insert( r[ 0 ].get<int>());
    sql( "DROP TABLE IF EXISTS deleted_posts, deleted_topics,"
    " updated_topics, updated_parts, closed_topics, updated_authors, updated_authors2"
      );
  } catch( const std::exception& e ) {
    LOG_ERROR( e.what());
    throw std::runtime_error( Str( "Remove::of() failed; " ) + e.what());
  }
}

void Removing::rise() {
  for( auto i: updatedAuthors )
    cache.rise( dta::UserResult::cacheKey( i ));
  if( ! updatedAuthors.empty())
    cache.rise( dta::ComplexUserResult::usersAllPagesCacheKey());

  for( auto i: updatedTopics ) {
    cache.rise( dta::ph::TopicsResult::cacheKey( i ));
    cache.rise( dta::ph::PostsResult::postsOfTopicAllPagesCacheKey( i ));
  }

  for( auto i: updatedParts )
    cache.rise( dta::ph::TopicsResult::topicsOfPartAllPagesCacheKey( i ));
  cache.rise( dta::ph::PartsResult::allPartsCacheKey());
}

// Remove

Remove::Remove( const Thread &thread )
  : Base( thread ), rm( sql, user.id ),
  part( sql ), topic( sql ), post( sql )
{
  if( ! url.args( url.tail, id )) throw AddressError();

  theTitle += " :: " + tr("Remove"); // will be shown in case of error
}

void Remove::load() {
  if( predefined::page::isPredefined( id )
      || ( msgBox() && ! approvedPressed())
    )
      { response.redirect( urlOf<ph::Phorum>()); return; }
  try {
    if( ! post.init( id ))
    { response.setStatus( 404 ); error = utl::f( "post id={1} not found", id ); };
  } catch( const std::exception &e ) { CAUGHT_EXCEPTION( e ); }
}

bool Remove::save() {
  if( approvedPressed()) try {
    topic.init( post.topicId());
    part.init ( topic.partId(), user );
    if( post.canRemove( part, topic, user )) {
      sql::Transaction guard( sql());
        Removing rm( sql, user.id ); rm.of( 0, 0, id );
      guard.commit();
      rm.rise();
    }
  } catch( const std::exception &e ) { CAUGHT_EXCEPTION( e ); return false; }
  if( post.isStartTopic())
    response.redirect( urlOf<ph::Part>( part.id()));
  else
    response.redirect( urlOf<ph::Topic>( 0, topic.id()));
  return true;
}

void Remove::render() {
  if( msgBox() || response.isErrorStatus()) Base::render();
  else {
      Str msg = utl::f( tr("The {1} ({2}) will be deleted."
        " Are you sure?"), ( post.isStartTopic() ? tr("topic") : tr("message")),
        id );
      int msgOptions = msgBoxYes | msgBoxNo | msgBoxIDoNotKnow | msgBoxLinkForFrom;
      if( post.isStartTopic())
        msgOptions |= msgBoxCancel | msgBoxWarning;
      else
        msgOptions |= msgBoxQuestion;
      renderMsgBox( msgOptions, msg, url( id ), request.referer());
  }
}

}} // ph, x2

