/**
    \file x2.cpp
    \brief main

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.

    \mainpage Phorum engine X2
    \section intro_sec Введение
    X2 представляет собой сайт (портал), написанный на c++, в основе которого лежит форум.
    Идея в том, что форум сам по себе - это сложная и могучая программа, уменьшая возможности
    которой, можно получить и блог и просто страницы или статьи с комментариями.
    \section site_sec Сайт
    Демо находится вот здесь <a href="https://x2f.herokuapp.com" target="_blank">https://x2f.herokuapp.com</a>
    \section negative_sec Отрицательные идентификаторы записей в базе данных
    В некоторых случаях для записей базы данных генерируются отрицательные значения,
    которые во всех случаях имеют особый смысл:
    \subsection negative_groups_subsec Отрицательные идентификаторы групп.
    На пользователей, входящих в группы с отрицательными идентификаторами накладываются
    ограничения.
    \n Для групп с идентификаторами в диапазоне от -1 до -100 включительно
    (соответствующая функция x1::predefined::group::bannedEverywhere( int group ))
    все входящие в них пользователи (кроме пользователя admin с идентификатором
    x1::predefined::user::admin и членов группы x1::predefined::group::admin) лишаются всех
    прав доступа. В частности группа с идентификатором x1::predefined::group::notActivated -
    это неактивированные пользователи, которые не подтвердили адрес электронной почты, с
    идентификатором x1::predefined::group::notApproved - это пользователи не одобренные
    администратором, а группа с идентификатором x1::predefined::group::banned - это забаненные
    пользователи.
    \n Для групп с идентификаторами в диапазоне от -101 и меньше (соответствующая функция
    x1::predefined::group::bannedInPart( int group )) накладываются ограничения на доступ в
    разделы, идентификаторы которых (разделов) равны по модулю, но противоположны по знаку
    соответствующим отрицательным идентификаторам групп. Такие пользователи получают только
    права безымянных пользователей.
    \subsection negative_users_subsec Отрицательные идентификаторы пользователей.
    Отрицательные идентификаторы пользователей - это для безымянных пользователей с уровнем
    доступа равным нулевому, как у пользователя x1::predefined::user::anonymous. В базе
    данных есть специальный генератор таких значений "users_id_seq_anonymous".
    \subsubsection pseudo_identifiers_subsubsec Псевдоидентификаторы
    Для анонимных пользователей с идентификатором x1::predefined::user::anonymous в
    x2::Visited::put() временно, без записи в базу данных, образуются и используются
    отрицательные идентификаторы вида (::Id_MIN + (unsigned short)), т.е. самые большие по
    модулю отрицательные идентификаторы; тест для таких псевдоидентификаторов
    ((pseudoId >> 16) == (Id_MIN >> 16)), см. функции pseudoIdFrom() и isPseudo() в файле
    visited.cpp.
    \subsection negative_parts_subsec Отрицательные идентификаторы разделов.
    Отрицательные идентификаторы разделов - это для разделов личной переписки и личных
    сообщений. Идентификатор личного раздела равен идентификатору пользователя по модулю,
    но со знаком минус.
    \subsection negative_posts_subsec Отрицательные идентификаторы постов.
    Не существуют в БД. Используются при цитировании всего поста в новый пост - при этом
    отрицательное значение обозначает обращенный идентикатор поста, предназначенного для
    цитирования (см. метод x2::ph::Post::Post(const Thread &thread) и
    x2::dta::ph::PostsResult::quoteId()).
    \section spec_names_sec Особенные имена в базе данных
    В некоторых случаях используются специальные имена, формат которых противоречит
    их же regex-ным правилам:
    \subsection spec_names_private_parts_subsec Особенные имена и описания личных разделов.
    В классе x2::dta::ph::PartsResult функциями x2::dta::ph::PartsResult::nameRegex() и
    x2::dta::ph::PartsResult::descriptionRegex() запрещены знаки табуляции в именах и
    описаниях. А при автоматической генерации таких имен и описаний функцией
    x2::dta::ph::PartsResult::name() специально добавляется знак табуляции.
    \subsection spec_names_users_subsec Особенные имена и почтовые ящики анонимных пользователей.
    В классе x2::dta::UserResult функциями x2::dta::UserResult::nameRegex() и
    x2::dta::UserResult::emailRegex() знак '/' (slash) запрещен. А при автоматической
    генерации таких имен и почтовых ящиков функцией
    x2::User::createAnonymous() специально добавляется слэш.
    \section cache_keys_sec Ключи кэширования.
    \subsection home_key_subsec Источник главной страницы.
    Ключ x2::Home::key() ("home") страницы используется и для идентификации источника
    главной страницы (см. класс x2::Home) в кэши.
    \subsection groups_cache_keys_subsec Ключ кэширования групп.
    Один ключ x2::dta::adm::GroupsResult::allGroupsCacheKey() вида "AllGroups".
    \subsection parts_cache_keys_subsec Ключи кэширования разделов.
    Два ключа, как надо для класса x2::sql::ComplexCachedResult:
    x2::dta::ph::PartsResult::cacheKey(Id id) и x2::dta::ph::PartsResult::cacheKey() const
    вида "part{id}" - ключ одного раздела и x2::dta::ph::PartsResult::allPartsCacheKey
    "allParts" - все разделы (без личных).
    \subsection users_cache_keys_subsec Ключи кэширования пользователей.
    Для пользователей применяются два разных и не совместимых класса: x2::dta::UserResult,
    применяемый во всех случаях, его ключ x2::dta::UserResult::cacheKey(Id id) имеет вид
    "user={id}", второй класс x2::dta::ComplexUserResult, применяется только для вывода
    списка пользователей классом x2::adm::Admin, у него все ключи, как положено классу
    x2::sql::ComplexCachedResult:
    \n x2::dta::ComplexUserResult::usersOfPageCacheKey( int page )
    и x2::dta::ComplexUserResult::usersOfPageCacheKey() const - для страницы пользователей,
    \n x2::dta::ComplexUserResult::cacheKey( Id id ) и он же x2::dta::ComplexUserResult::cacheKeyFormat()
    вида "user{id}" - для одного пользователя,
    \n и x2::dta::ComplexUserResult::usersAllPagesCacheKey()
    триггер для всех записей.
    \n Алгоритм использования простой - при изменении или удалении пользователя сбрасываеся
    кэшь ключем x2::dta::UserResult::cacheKey(Id id), и плюс во всех случаях, в том числе при
    создании нового пользователя, сбрасывается вся кэшь списка триггером
    x2::dta::ComplexUserResult::usersAllPagesCacheKey().
    \subsection groups_of_user_cache_keys_subsec Ключи кэширования групп пользователей.
    Два ключа: "groupsOfUser{id}" и x2::dta::GroupsOfUserResult::cacheKey( Id userId ) вида
    "ExtGroupsOfUser{id}". Оба сбрасываются автоматически триггером x2::dta::UserResult::cacheKey(Id id).
    \subsection group_access_cache_keys_subsec Ключ кэширования прав доступа групп к разделам.
    x2::dta::PartAccess::cacheKey() const и x2::dta::PartAccess::cacheKey( Id partId ) вида
    "accessOfPart{partId}", в классе для постоянного просмотра прав, и еще
    x2::dta::adm::PartAccess::cacheKey( Id partId ) вида "admAccessOfPart{partId}" в классе для
    редактирования прав доступа, все создаются с триггером и сбрасываются триггером
    x2::dta::adm::PartsResult::cacheKey().
    \subsection ban_group_cache_keys_subsec Ключи кэширования групп забаненных в блогах.
    определены в классе x2::dta::adm::PartsResult -
    x2::dta::adm::PartsResult::banGroupNameCacheKey( Id partId ) и
    x2::dta::adm::PartsResult::banGroupNameCacheKey() const вида "banGroupNameOfPart{partId}",
    создаются с триггером и сбрасываются триггером x2::dta::adm::PartsResult::cacheKey().
    \subsection notifications_of_user_cache_keys_subsec Ключ кэширования извещений для пользователя
    Один ключ "notificationsOfUser{id}" сбрасывается автоматически триггером
    x2::dta::UserResult::cacheKey(Id id).
    \subsection topics_cache_keys_subsec Ключи кэширования тем.
    В классе x2::dta::ph::TopicsResult списка тем все ключи, как положено классу
    x2::sql::ComplexCachedResult:
    \n x2::dta::ph::TopicsResult::topicsOfPartAllPagesCacheKey( Id partId )
    и x2::dta::ph::TopicsResult::topicsOfPartAllPagesCacheKey() const вида "topicsOfPart{partId}" - все
    страницы тем раздела,
    \n x2::dta::ph::TopicsResult::topicsOfPartCacheKey( Id partId, int page ) и
    x2::dta::ph::TopicsResult::topicsOfPartCacheKey() const вида "topicsOfPart{partId}ofPage{page}" -
    страница раздела,
    \n x2::dta::ph::TopicsResult::cacheKey( Id topicId ) и x2::dta::ph::TopicsResult::cacheKey() const
    и он же x2::dta::ph::TopicsResult::cacheKeyFormat() вида "topic{id}" - для одной темы.
    \subsection posts_cache_keys_subsec Ключи кэширования постов.
    В классе x2::dta::ph::PostsResult списка тем все ключи, как положено классу
    x2::sql::ComplexCachedResult:
    \n x2::dta::ph::PostsResult::postsOfTopicAllPagesCacheKey( Id topicId )
    и x2::dta::ph::PostsResult::postsOfTopicAllPagesCacheKey() const вида "postsOfTopic{topicId}" -
    все страницы постов темы,
    \n x2::dta::ph::PostsResult::postsOfTopicCacheKey( Id topicId, int page ) и
    x2::dta::ph::PostsResult::postsOfTopicCacheKey() const вида "postsOfTopic{topicId}ofPage{page}" -
    страница постов,
    \n x2::dta::ph::PostsResult::cacheKeyFormat() вида "post{id}" - для одного поста.
    \subsection lastPosts_cache_keys_subsec Ключ кэширования списка последних постов.
     Один ключ "lastPosts", x2::dta::ph::LastPostsResult::cacheKey(), сбрасывается
     автоматически триггером x2::dta::ph::PartsResult::allPartsCacheKey().
    \subsection cache_keys_subsec Список ключей кэширования
    <table>
    <tr><th>Файл<br>class::method</td><td>Ключ<br>Сущность</th></tr>
    <tr><td>views/pages.h       <br>x2::Home::key</td><td>"home"<br>Источник главной страницы</td></tr>
    <tr><td>data/adm/groups.h   <br>x2::dta::adm::GroupsResult::allGroupsCacheKey</td><td>"AllGroups"<br>Список групп</td></tr>
    <tr><td>data/ph/phorum.h    <br>x2::dta::ph::PartsResult::cacheKey</td><td>"part{id}"<br>Раздел</td></tr>
    <tr><td>data/ph/phorum.h    <br>x2::dta::ph::PartsResult::allPartsCacheKey</td><td>"allParts"<br>Все разделы</td></tr>
    <tr><td>data/user.h         <br>x2::dta::UserResult::cacheKey</td><td>"user={id}"<br>Пользователь</td></tr>
    <tr><td>data/user.h         <br>x2::dta::ComplexUserResult::usersOfPageCacheKey</td><td><br>Страница пользователей</td></tr>
    <tr><td>data/user.h         <br>x2::dta::ComplexUserResult::cacheKey</td><td><br>Пользователь в списке</td></tr>
    <tr><td>data/user.h         <br>x2::dta::ComplexUserResult::usersAllPagesCacheKey()</td><td>"allUsers"<br>Весь список пользователей</td></tr>
    <tr><td>content.cpp         <br></td><td>"groupsOfUser{id}"<br>Группа пользователей</td></tr>
    <tr><td>data/user.h         <br>x2::dta::GroupsOfUserResult::cacheKey</td><td>"ExtGroupsOfUser{id}"<br>Группа пользователей для редактирования пользователя</td></tr>
    <tr><td>data/base.h         <br>x2::dta::PartAccess::cacheKey</td><td>"accessOfPart{partId}"<br>Ключ кэширования прав доступа групп к разделам</td></tr>
    <tr><td>data/adm/part.h     <br>x2::dta::adm::PartAccess::cacheKey</td><td>"admAccessOfPart{partId}"<br>Ключ кэширования прав доступа групп к разделам</td></tr>
    <tr><td>data/adm/part.h     <br>x2::dta::adm::PartsResult::banGroupNameCacheKey</td><td>"banGroupNameOfPart{partId}"<br>Ключ кэширования групп забаненных в блогах</td></tr>
    <tr><td>content.cpp         <br></td><td>"notificationsOfUser{id}"<br>Извещения для пользователя</td></tr>
    <tr><td>data/ph/phorum.h    <br>x2::dta::ph::TopicsResult::topicsOfPartAllPagesCacheKey</td><td>"topicsOfPart{partId}"<br>Все страницы тем раздела</td></tr>
    <tr><td>data/ph/phorum.h    <br>x2::dta::ph::TopicsResult::topicsOfPartCacheKey</td><td>"topicsOfPart{partId}ofPage{page}"<br>Темы одной страницы раздела</td></tr>
    <tr><td>data/ph/phorum.h    <br>x2::dta::ph::TopicsResult::cacheKey</td><td>"topic{id}"<br>Тема</td></tr>
    <tr><td>data/ph/phorum.h    <br>x2::dta::ph::PostsResult::postsOfTopicAllPagesCacheKey</td><td>"postsOfTopic{topicId}"<br>Все посты данной темы</td></tr>
    <tr><td>data/ph/phorum.h    <br>x2::dta::ph::PostsResult::postsOfTopicCacheKey</td><td>"postsOfTopic{topicId}ofPage{page}"<br>Посты одной страницы темы</td></tr>
    <tr><td>data/ph/phorum.h    <br>x2::dta::ph::PostsResult::cacheKeyFormat()</td><td>"post{id}"<br>Пост</td></tr>
    <tr><td>data/ph/phorum.h    <br>x2::dta::ph::LastPostsResult::cacheKey()</td><td>"lastPosts"<br>Список последних постов</td></tr>
    </table>
    \section blogs_sec Авторские разделы (блоги)
    Любой админ (x1::predefined::user::admin или член группы x1::predefined::group::admin)
    может создать такой раздел, для этого нужно только вписать идентификатор пользователя
    в поле Ид Автора раздела.
    \subsection blogs_author_rights_subsec Права автора
    Автор получает все (и административные) права на раздел. Он (или админ) может
    в той же форме изменения раздела создать бан-группу; любой пользователь, член
    этой бан-группы лишается всех прав, низводится до безымянного пользователя.
    Права безымянных пользователей на раздел так же задаются автором раздела как и
    админом.
    \n Бан группа, записывается в таблицу groups базы данных, ее идентификатор равен
    идентификатору раздела по модулю и противоположен по знаку
    (см. \ref negative_groups_subsec).
*/

#include "x2.h"


int main( int argc, char **argv ) try {
  x2::cfg.load( argc, argv );
  LOG_INFO( "Started" );
  x2::Thread::run();
  return 0;
}
catch( const std::exception &e ) { LOG_FATAL( e.what()); return 1; }
catch( ... ) { LOG_FATAL( "unknown error" ); return 1; }

