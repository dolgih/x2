/**
    \file crypt.h
    \brief Cryptographic functions

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 {
namespace base64 {

Str encode( const Str& data, bool crlf = true, bool pad = true );
Str decode( const Str& data );

} // base64

namespace crypt {

// text only data
// returns aes256 and base64 encoded string
Str encode( Str data, const Str &key = cfg.crypt  );
// reverse of encode
Str decode( Str data, const Str &key = cfg.crypt  );

}} // crypt, x1

