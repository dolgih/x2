/**
    \file crypt.cpp
    \brief Cryptographic functions

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/


#include "x2.h"
#include "base64.hpp"
#include <openssl/aes.h>

namespace x2 {
namespace base64 {

Str encode(const Str& data, bool crlf, bool pad)
{
  Str v;

  // base64 encoded value will be 4/3 larger than original value
  v.reserve( (data.size() * 1.35));

  ::base64::encode(data.begin(), data.end(), std::back_inserter(v), crlf, pad);

  return v;
}

Str decode(const Str& data)
{
  Str v;

  // decoded value will be 3/4 smaller than encoded value
  v.reserve( (data.size() * 0.8));

  ::base64::decode(data.begin(), data.end(), std::back_inserter(v));

  return v;
}

} // base64

namespace crypt {

class Aes : AES_KEY {
public:
  void text( const Str &aKey, const Str &anIv, Str &aData, bool enc );
private:
  static void init( Str &str, size_t sz ) { str = Str( sz, '\0' ); }
  static void set( Str &str, const Str &val, size_t sz  ) {
    init( str, sz );
    memcpy( (char*)str.data(), val.data(), std::min( (size_t)val.size(), sz ));
  }
};

void Aes::text( const Str &aKey, const Str &anIv, Str &aData, bool enc ) {
  if( aData.empty()) return;
  Str key, iv, data;
  set( key, aKey, 32 ); set( iv, anIv, 32 );
  set( data, aData, ( ( aData.size() - 1 ) / 32 + 1 ) * 32 );
  int r;
  if( enc )
    r = AES_set_encrypt_key( (const unsigned char*)key.data(), 256, this );
  else
    r = AES_set_decrypt_key( (const unsigned char*)key.data(), 256, this );
  if( r ) throw std::runtime_error( "Aes error" );
  AES_cbc_encrypt( (const unsigned char*)data.data(), (unsigned char*)data.data(),
    data.size(), this, (unsigned char*)iv.data(), enc );
  if( enc ) aData = data;
  else aData = data.data();
}

// generates 2-byte iv
void genIv( Str &iv ) {
  iv.resize( sizeof (short)); *(short*)iv.data() = (short)time( 0 );
}

Str encode( Str data, const Str &key ) {
  Str iv; genIv( iv );
  Aes aes; aes.text( key, iv, data, true );
  return base64::encode( iv + data, false, false );
}

Str decode( Str data, const Str &key ) {
  data = base64::decode( data );
  if( data.size() < 32 + sizeof (short)) throw std::runtime_error( "crypt::decode error" );
  Str iv = data.substr( 0, sizeof (short));
  data = data.substr( sizeof (short));
  Aes aes; aes.text( key, iv, data, false );
  return data;
}

}} // crypt, x2

