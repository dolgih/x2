/**
    \file mail.cpp
    mail functions for x2 project

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2017 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include <string.h>
#include <curl/curl.h>
#include <stdexcept>
#include <string>
#include <vector>
#include "mail.h"

namespace x2 {
namespace mail {

Smtp::Smtp( std::string userName, std::string password,
            std::string mailServer )
  : curl( curl_easy_init()), res( CURLE_OK ), recipients( NULL ), t0( 0 )
{
  if( ! curl ) throw std::runtime_error( "mail: can not curl_easy_init()" );
  curl_easy_setopt( curl, CURLOPT_USERNAME, userName.data());
  curl_easy_setopt( curl, CURLOPT_PASSWORD, password.data());
  curl_easy_setopt( curl, CURLOPT_URL, mailServer.data());
}

Smtp& Smtp::addRecipient( std::string r ) {
  recipients = curl_slist_append( recipients, r.data());
  return *this;
}

static std::string createBoundary( std::string msg,
                                   std::string html )
{
  for( int i = 1; i < 100000; i ++ ) {
    char boundary[32];
    snprintf( boundary, sizeof boundary, "BOUNDARY%i", i );
    if( ! strcasestr( msg.data(), boundary ) && ! strcasestr( html.data(), boundary ))
      return boundary;
  }
  throw std::runtime_error( "mail: createBoundary() failed" );
}

static void checkStrings( std::vector<std::string> &text ) {
/* Функция проверяет вектор строк, нет ли строк длиннее maxSize,
   и если есть, то разбивает такие строки. */
  for( size_t i = 0; i < text.size(); i ++ ) {
    const short maxSize = 900;
    if( text[ i ].size() > maxSize ) { // найдена строка длиннее maxSize, разрезаем
      for( short j = maxSize / 2; j < maxSize; j ++ )
        if( ! strncmp( text[ i ].data() + j, "\r\n", 2 )) { // нашли разрыв строки
          text.insert( text.begin() + i, text[ i ].substr( 0, j + 2 ));
          text[ i + 1 ] = text[ i + 1 ].substr( j + 2 );
          goto nextString;
        }
      for( short j = maxSize / 2; j < maxSize; j ++ )
        if( isspace( text[ i ][ j ] )) { // нашли пробельный символ
          text.insert( text.begin() + i, text[ i ].substr( 0, j ) + "\r\n" );
          text[ i + 1 ] = text[ i + 1 ].substr( j + 1 );
          goto nextString;
        }
      // не нашли пробельный символ, так режем
      text.insert( text.begin() + i, text[ i ].substr( 0, maxSize ) + "\r\n" );
      text[ i + 1 ] = text[ i + 1 ].substr( maxSize );
    }
  nextString:;
  }
}

void Smtp::send( std::string from, std::string to, std::string subj, std::string msg,
                 std::string html )
{
  linesRead = 0;
  text.clear();
  t0 = time( 0 );
  curl_easy_setopt( curl, CURLOPT_MAIL_FROM, from.data());
  text.push_back( dateLine());
  text.push_back( "To: " + to + "\r\n" );
  text.push_back( "From: " + from + "(x2 Project Server)\r\n" );
  if( !ccLine().empty())
    text.push_back( ccLine());
  recipients = curl_slist_append( recipients, to.data());
  curl_easy_setopt( curl, CURLOPT_MAIL_RCPT, recipients );
  text.push_back( messageIdLine());
  text.push_back( "Subject: " + subj + "\r\n" );
  text.push_back( "MIME-Version: 1.0\r\n" );
  std::string
    cTransEnc = "Content-Transfer-Encoding: 8bit\r\n",
    textCType = cTransEnc +
                "Content-Type: text/plain; charset=\"UTF-8\"\r\n"
                "\r\n" /* empty line to divide headers from body, see RFC5322 */,
    htmlCType = cTransEnc +
                "Content-Type: text/html; charset=\"UTF-8\"\r\n"
                "\r\n" /* empty line to divide headers from body, see RFC5322 */;
  if( html.empty())
    text.push_back( textCType + msg + "\r\n" );
  else if( msg.empty())
    text.push_back( htmlCType + html + "\r\n" );
  else {
    std::string boundary = createBoundary( msg, html );
    text.push_back( "Content-Type: multipart/alternative; boundary=" + boundary + "\r\n"
                    "\r\n--" + boundary + "\r\n"
                    + textCType + msg + "\r\n"
                    "\r\n--" + boundary + "\r\n"
                    + htmlCType + html + "\r\n"
                    "\r\n--" + boundary + "--\r\n"
                    "\r\n" );
  }
  checkStrings( text );
  curl_easy_setopt( curl, CURLOPT_READFUNCTION, source );
  curl_easy_setopt( curl, CURLOPT_READDATA, this );
  curl_easy_setopt( curl, CURLOPT_UPLOAD, 1L );
  if( ( res = curl_easy_perform( curl )) != CURLE_OK ) {
    std::string errorMsg = "mail: curl_easy_perform() failed: ";
    errorMsg += curl_easy_strerror( CURLcode( res ));
    throw std::runtime_error( errorMsg.data());
  }
}

void Smtp::verbose( long act ) {
  /* Since the traffic will be encrypted, it is very useful to turn on debug
   * information within libcurl to see what is happening during the
   * transfer */
  curl_easy_setopt( curl, CURLOPT_VERBOSE, act );
}

void Smtp::verifyPeer( long act ) {
  /* If you want to connect to a site who isn't using a certificate that is
   * signed by one of the certs in the CA bundle you have, you can skip the
   * verification of the server's certificate. This makes the connection
   * A LOT LESS SECURE.
   *
   * If you have a CA cert for the server stored someplace else than in the
   * default bundle, then the CURLOPT_CAPATH option might come handy for
   * you. */
  curl_easy_setopt( curl, CURLOPT_SSL_VERIFYPEER, act );
}

void Smtp::verifyHost( long act ) {
  /* If the site you're connecting to uses a different host name that what
   * they have mentioned in their server certificate's commonName (or
   * subjectAltName) fields, libcurl will refuse to connect. You can skip
   * this check, but this will make the connection less secure. */
  curl_easy_setopt( curl, CURLOPT_SSL_VERIFYHOST, act );
}

void Smtp::useSslAll() {
  /* In this example, we'll start with a plain text connection, and upgrade
   * to Transport Layer Security (TLS) using the STARTTLS command. Be careful
   * of using CURLUSESSL_TRY here, because if TLS upgrade fails, the transfer
   * will continue anyway - see the security discussion in the libcurl
   * tutorial for more details. */
  if( ( res = curl_easy_setopt( curl, CURLOPT_USE_SSL, (long)CURLUSESSL_ALL)) != CURLE_OK ) {
    std::string errorMsg = "mail: Smtp::useSslAll() failed: ";
    errorMsg += curl_easy_strerror( CURLcode( res ));
    throw std::runtime_error( errorMsg.data());
  }
}

void Smtp::caInfo( std::string path /* "/path/to/certificate.pem" */ ) {
  /* If your server doesn't have a valid certificate, then you can disable
   * part of the Transport Layer Security protection by setting the
   * CURLOPT_SSL_VERIFYPEER and CURLOPT_SSL_VERIFYHOST options to 0 (false).
   *   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
   *   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
   * That is, in general, a bad idea. It is still better than sending your
   * authentication details in plain text though.
   * Instead, you should get the issuer certificate (or the host certificate
   * if the certificate is self-signed) and add it to the set of certificates
   * that are known to libcurl using CURLOPT_CAINFO and/or CURLOPT_CAPATH. See
   * docs/SSLCERTS for more information. */
  curl_easy_setopt(curl, CURLOPT_CAINFO, path.data());
}

Smtp::~Smtp() {
  if( curl ) {
    curl_easy_cleanup( curl );
    curl_slist_free_all( recipients );
  }
}

std::string Smtp::dateLine() const {
  char timestring[128];
  const char * timeformat = "Date: %d %b %y %H:%M:%S %Z\r\n";
  if( strftime( timestring, sizeof timestring - 1, timeformat, gmtime( &t0 )))
    return std::string( timestring );
  else
    throw std::runtime_error( "mail: strftime() failed" );
}

std::string Smtp::messageIdLine() const {
  int rnd;
#ifdef _WIN32
      FILETIME pentium_tsc; GetSystemTimeAsFileTime(&pentium_tsc);
      rnd = pentium_tsc.dwLowDateTime;
#else
      timeval tsc;
      gettimeofday(&tsc, 0);
      rnd = tsc.tv_usec;
#endif
  char msgId[128];
  snprintf( msgId, sizeof msgId, "%u-%llu", rnd, (unsigned long long)t0 );
  return std::string( "Message-ID: <" ) + msgId + "@rfcpedant.example.org>\r\n";
}

std::string Smtp::ccLine() const {
 if( ! recipients ) return std::string();
 std::string line = "Cc: ";
 for( curl_slist * it = recipients; it; it = it->next )
    line += std::string( it->data ) + ( it->next ? ",\r\n " /* space */ : "\r\n" );
 return line;
}

size_t Smtp::source( void *ptr, size_t size, size_t nmemb,
                     void *userp )
{
  Smtp &smtp = *static_cast<Smtp *>( userp );

  if( ! ( size = size * nmemb ) || smtp.text.size() <= smtp.linesRead ) return 0;

  std::string curr = smtp.text[ smtp.linesRead ++ ];
  if( size < curr.length()) throw std::runtime_error( "mail: can not source()" );
  memcpy( ptr, curr.data(), curr.length());
  return curr.length();
}

}}; // mail, x2

