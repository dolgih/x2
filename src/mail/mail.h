/**
    \file mail.h
    mail functions for x2 project.

    1) This class "Smtp" requires libcurl 7.20.0 or above.
    2) The sample of using see in file test.cpp.
    3) Call method Smtp::.useSslAll() right after the constructor for STARTTLS.
    4) For ssl, use address prefix (protocol) "smtps://" in server address, in third
       parameter of the constructor. For example  "smtps://mainserver.example.net".
    5) Set server port using address postfix ":n" in server address, in third
       parameter of the constructor. For example "smtp://mainserver.example.net:587"
       is address for the server with STARTTLS.

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2017 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

namespace x2 {
namespace mail {

class Smtp {
public:
  Smtp( std::string userName, std::string password, std::string mailServer );
  Smtp& addRecipient( std::string r );
  void send( std::string from, std::string to, std::string subj, std::string msg,
             std::string html = std::string());
  void verbose( long act = 1L );
  void verifyPeer( long act );
  void verifyHost( long act );
  void useSslAll();
  void caInfo( std::string path /* "/path/to/certificate.pem" */ );
  ~Smtp();
protected:
  void *curl;
  int res;
  struct curl_slist *recipients;
  time_t t0;
  std::vector<std::string> text;

  std::string dateLine() const;
  std::string messageIdLine() const;
  std::string ccLine() const;

  size_t linesRead;
  static size_t source( void *ptr, size_t size, size_t nmemb, void *userp );
};

}}; // mail, x2

