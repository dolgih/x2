/**
    \file test.cpp
    test for x2 mail

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2017 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include <string>
#include <iostream>
#include <vector>
#include "mail.h"

#define FROM    "<webmaster@volga.unaux.com>"
#define TO      "<probe@volga.unaux.com>"
#define CC      "<info@example.org>"

int main(void)
{
  try {
    x2::mail::Smtp smtp( "webmaster@volga.unaux.com", "tolataLevel", "smtps://smtp.yandex.com" );
    smtp.verbose();
//    smtp.useSslAll(); // STARTTLS
//    smtp.addRecipient( CC );
//    smtp.addRecipient( "anatolidolgih@ya.ru" );
//    smtp.addRecipient( "<dolguikh@outlook.com>" );
//    smtp.addRecipient( "<anatolidolgih@yandex.ua>" );
    smtp.send( FROM, TO, "SMTP example message",
      "The body of the message starts here.\r\n"
      "\r\n"
      "It could be a lot of lines, could be MIME encoded, whatever.\r\n"
      "Check RFC5322.\r\n" );
    std::cout << "OK\n";
  } catch ( const std::exception & e ) {
    std::cerr << e.what() << "\n";
  }
  return 0;
}
