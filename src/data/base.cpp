/**
    \file data/base.cpp
    \brief base classes for model data

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace dta {


void Rights::fetch( const sql::Result &rs ) {
  r = rs[ "r" ].get<Str>( "-" ) == "r";
  w = rs[ "w" ].get<Str>( "-" ) == "w";
  c = rs[ "c" ].get<Str>( "-" ) == "c";
  o = rs[ "o" ].get<Str>( "-" ) == "o";
  m = rs[ "m" ].get<Str>( "-" ) == "m";
  h = rs[ "h" ].get<Str>( "-" ) == "h";
  normalize();
}

// PartAccess

void PartAccess::getData( sql::PhSession &sql ) {
  if( ph::PartsResult::isPrivate( partId ))
    return;
  if( ! cache.fetch( cacheKey(), *this )) {
    sql::Result r = sql()( select());
    PartAccess p( 0 );
    for(;;) {
      Id partId;
      if( ! r.next() /* или все разделы закончились */ ||
        ( ( partId = r[ "part_id"].get<Id>()) != p.partId && p.partId ))  /* или предыдущий раздел */
      {
        if( ! p.empty()) {
          cache.store( p.cacheKey(), p, { ph::PartsResult::cacheKey( p.partId )});
          if( this->partId == p.partId )
            *this = p;
          p.clear();
        }
      }
      if( r.empty()) break;
      p.partId = partId;
      p.fetch( r );
    }
    // Запишем в кэшь пустые права для разделов, для которых в БД
    // в таблице "access_list" нет вообще записей.
    r = sql( "SELECT id FROM parts WHERE id>=0 AND id NOT IN"
             " (SELECT part_id FROM access_list GROUP BY part_id)" );
    while( r.next()) {
      PartAccess empty( r[ 0 ].get<Id>());
      cache.store( empty.cacheKey(), empty, { ph::PartsResult::cacheKey( empty.partId )});
    }
  }
}

void PartAccess::fetch( const sql::Result &rs ) {
  Rights rights; rights.fetch( rs );
  operator[] ( rs[ "id" ].get<Id>( predefined::group::anonymous )) = rights;
}

const char *PartAccess::select() {
  return "SELECT group_id as id, r, w, c, o, m, h, part_id"
  " FROM access_list ORDER BY part_id";
}

}} // dta, x2

