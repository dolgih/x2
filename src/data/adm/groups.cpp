/**
    \file data/adm/groups.cpp
    \brief groups editor model

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace dta { namespace adm {


// GroupsResult

GroupsResult::GroupsResult( sql::PhSession &sql )
  : sql::CachedResult( sql, allGroupsCacheKey(), select())
{ resultStructureKey = structureKey(); }

void GroupsResult::init() { execQuery(); }

Str GroupsResult::select() {
  return "SELECT * FROM groups WHERE id>=0 ORDER BY name FOR UPDATE OF groups NOWAIT";
}

void GroupsResult::insert( sql::PhSession &sql, const Str &name ) {
  sql( utl::pf( "INSERT INTO groups (name) VALUES ({1})", name ));
}

void GroupsResult::update( sql::PhSession &sql, Id id, const Str &name ) {
  sql( utl::pf( "UPDATE groups SET name={1}, modified=NOW() WHERE id={2}",
       name, id ));
}

void GroupsResult::remove( sql::PhSession &sql, Id id ) {
  if( predefined::group::isPredefined( id )) return;
  sql( utl::pf( "DELETE FROM groups WHERE id={1}", id ));
}

}}} // adm, dta, x2

