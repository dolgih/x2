/**
    \file data/adm/groups.h
    \brief groups editor model

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 { namespace dta { namespace adm {

/** all groups of user (group table) */
class GroupsResult : public sql::CachedResult {
public:
  GroupsResult( sql::PhSession &sql );
  void init();

  Id id()          const { return field( "id" ).get<Id>(); }
  Str name()       const { return field( "name" ).get(); }
  TimeT modified() const { return field( "modified" ).getTime(); }

  static const char *nameRegex() { return "^\\w(-|\\w|\\d){2,31}$"; }

  static Str select();
  static void insert( sql::PhSession &sql, const Str &name );
  static void update( sql::PhSession &sql, Id id, const Str &name );
  static void remove( sql::PhSession &sql, Id id );

  static Str allGroupsCacheKey() { return "AllGroups"; }

  typedef sql::Iterator<GroupsResult> Iterator;
  Iterator begin() { return Iterator( *this, 0 ); }
  Iterator end() { return Iterator( *this, recordCount()); }

private:
  static const char * structureKey() { return "GroupsStructureKey"; }
};

}}} // adm, dta, x2

