/**
    \file data/adm/part.h
    \brief models for the part editor

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 { namespace dta { namespace adm {

struct PartsResult : dta::ph::PartsResult {

  using dta::ph::PartsResult::PartsResult;

  static Id insert( sql::PhSession &sql, Id author, const Str &name,
               const Str &description, int weight );

  static void update( sql::PhSession &sql, Id id, Id author,
               const Str &name, const Str &description, int weight );

  static void remove( sql::PhSession &sql, Id id );

  // banned in the blogger's parts
  static Str selectBanGroup( Id partId );
  static void insertBanGroup( sql::PhSession &sql, Id partId, Str name );
  static void removeBanGroup( sql::PhSession &sql, Id partId );

  static Str banGroupNameCacheKey( Id partId ) { return "banGroupNameOfPart" + n2s( partId ); }
  const Str banGroupNameCacheKey() const { return banGroupNameCacheKey( id()); }

};

struct PartAccess : Rights { // access rights of the group to the part (access_list table)
  Id id /* group id */;
  Str name /* group name */, modified;

  void setAccess( Bools& v ) {
    r = v[0]; w = v[1]; c = v[2]; o = v[3]; m = v[4]; h = v[5];
    normalize();
  }
  bool hasRights() const { return R(); }
  void fetch( sql::Result &rs );

  static Str cacheKey( Id partId ) { return "admAccessOfPart" + n2s( partId ); }
  static Str select( Id partId );

  struct setCompare {
    bool operator() (const PartAccess& left, const PartAccess& right) const {
      return left.id < right.id;
    }
  };
};

}}} // adm, dta, x2

