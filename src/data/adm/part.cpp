/**
    \file data/adm/part.cpp
    \brief models for the part editor

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace dta { namespace adm {

// PartsResult

Id PartsResult::insert( sql::PhSession &sql, Id author, const Str &name,
  const Str &description, int weight )
{
  sql( utl::Pf( "INSERT INTO parts (author, name, description, weight)"
                " VALUES ({1},{2},{3},{4})" )
       ( author )( name )( description )( weight ));
  return sql.sequenceLast( "parts_id_seq" );
}

void PartsResult::update( sql::PhSession &sql, Id id, Id author,
  const Str &name, const Str &description, int weight )
{
  sql( utl::Pf( "UPDATE parts SET author={1}, name={2}, description={3},"
                " weight={4}, modified=NOW() WHERE id={5}" )
       ( author )( name )( description )( weight )( id ));
}

void PartsResult::remove( sql::PhSession &sql, Id id ) {
  if( predefined::part::isPredefined( id )) return;
  if( id )
    sql( utl::pf( "DELETE FROM parts WHERE id={1}", id ));
}

Str PartsResult::selectBanGroup( Id partId ) {
  return "SELECT name FROM groups WHERE id=" + n2s( - partId );
}

void PartsResult::insertBanGroup( sql::PhSession &sql, Id partId, Str name ) {
  if( sql( utl::pf( "UPDATE groups SET name={1} WHERE id={2} RETURNING id",
                    name, - partId )).row().empty()
  )
    sql( utl::pf( "INSERT INTO groups (id, name) VALUES ({1},{2})",
                  - partId, name ));
}

void PartsResult::removeBanGroup( sql::PhSession &sql, Id partId ) {
  sql( utl::pf( "DELETE FROM groups WHERE id={1}", - partId ));
}

// PartAccess

void PartAccess::fetch( sql::Result &rs ) {
  Rights::fetch( rs );
  id       = rs[ "id" ].get( Id());
  name     = rs[ "name" ].get( Str());
  modified = rs[ "modified" ].get( Str());
}

Str PartAccess::select( Id partId ) {
  return utl::pf( R"(SELECT groups.id AS id, groups.name as name,
al.r as r, al.w as w, al.c as c, al.o as o, al.m as m, al.h as h,
al.modified as modified FROM groups LEFT JOIN
(SELECT * FROM access_list WHERE part_id={1} FOR UPDATE OF access_list NOWAIT)
AS al ON groups.id=al.group_id WHERE groups.id>=0 ORDER BY groups.name
FOR UPDATE OF groups NOWAIT;)", partId );
}

}}} // adm, dta, x2

