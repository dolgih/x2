/**
    \file data/notifications.cpp
    \brief notifications model

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace dta {


NotificationsResult::NotificationsResult( sql::PhSession &sql, bool isPrivateMessages  )
: sql::Result( sql ), isPrivateMessages( isPrivateMessages ) { }

void NotificationsResult::init( Id userId ) {
  exec( select( userId, isPrivateMessages ));
}

Str NotificationsResult::select( Id userId, bool isPrivateMessages ) {
  Str what = isPrivateMessages ? "'P'" : "'A','Q'";
  return "SELECT n.post_id AS post_id, n.what AS what, p.title AS title,"
  " p.abstract AS abstract, p.created AS created, p.author AS author,"
  " u.name AS author_name FROM notifications AS n JOIN posts AS p ON n.post_id=p.id"
  " LEFT JOIN users AS u ON p.author=u.id"
  " WHERE user_id=" + n2s( userId ) + " AND n.what IN (" + what + ")"
  " ORDER BY n.post_id DESC" + pageLimit();
}

void NotificationsResult::insert( sql::PhSession &sql, Id userId, Id postId,
  char what )
{
  sql( utl::pf(
    "INSERT INTO notifications (user_id, post_id, what) VALUES ({1},{2},{3})",
    userId, postId, Str( 1, what )));
}

void NotificationsResult::remove( sql::PhSession &sql, Id userId, Id postId ) {
  sql( utl::pf("DELETE FROM notifications WHERE user_id={1} AND post_id={2}",
    userId, postId ));
}

void NotificationsResult::removeNotifications( sql::PhSession &sql,
  bool isPrivateMessages, Id userId )
{
  sql(
    utl::Pf( ( "WITH shown AS (SELECT post_id FROM notifications"
    " WHERE what" + Str( isPrivateMessages ? Str() : "!" ) + "='P'"
    " AND user_id={1} ORDER BY post_id DESC" + pageLimit() + ")"
    " DELETE FROM notifications USING shown WHERE"
    " notifications.post_id=shown.post_id AND user_id={1}" ))( userId )
  );
}

}} // dta, x2

