/**
    \file data/base.h
    \brief base classes for model data

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 { namespace dta {

/**
 Set of rights.
 Множество прав группы на раздел. Права чтения 'r', записи 'w'
 (включает 'r'), право создания ветки 'c' (включает 'r' и 'w'), право модерации
 собственной ветки 'o' (включает 'r', 'w' и 'c'), право модерации в разделе 'm'
 (включает все права, кроме 'h'), право писать прямо в формате html 'h'
 (включает 'r' и 'w'). Функции R(), W(), C(), O() M() H() учитывают включенные
 права (как 'r' включено в 'w')
*/
struct Rights : Serializable {
  bool
  r /** The read access */,
  w /** The write access, includes the read access */,
  c /** The topic creation access, includes the read and write accesses */,
  o /** The topic moderate access for the topic author, includes the 'r', 'w' and 'c' accesses */,
  m /** The moderate access, includes the 'r', 'w', 'c' and  'o' accesses */,
  h /** The writing plain html access, includes the read and write accesses */;

  Rights() : r( false ), w( false ), c( false ), o( false ), m( false ), h( false ) { }

  /** Adds the rights */
  void add( const Rights &sum ) {
    r = r||sum.r; w = w||sum.w; c = c||sum.c; o = o||sum.o; m = m||sum.m; h = h||sum.h;
    normalize();
  }
  /** Compares the rights */
  bool equalTo( const Rights &second ) const {
    Rights f = *this, s = second; f.normalize(); s.normalize();
    return f.r == s.r && f.w == s.w && f.c == s.c && f.o == s.o && f.m == s.m && f.h == s.h;
  }
  /** Cleanes from unnecessary fields */
  void normalize() {
    Rights n; n.h = H();
    if( (n.m = M())); else if( (n.o = O())); else if( (n.c = C()) || n.h ); else if( (n.w = W())); else n.r = R();
    *this = n;
  }

  bool R() /** The read access */ const { return r || W(); }
  bool W() /** The write access */ const  { return w || C() || H(); }
  bool C() /** The topic creation access */ const  { return c || O(); }
  bool O() /** The topic moderate access for topic author */ const  { return o || M(); }
  bool M() /** The moderate access */ const  { return m; }
  bool H() /** The writing plain html access */ const  { return h; }

  void fetch( const sql::Result &rs );
  void serialize( Archive &a ) override { a % r % w % c % o % m % h; }
};

// access rights to the part (access_list table)
struct PartAccess : std::map<Id/*group_id*/,Rights> {
  Id partId;
  PartAccess( Id partId ) : partId( partId ) { }
  void getData( sql::PhSession &sql ) /* gets the data from DB or cache */;
  void fetch( const sql::Result &rs );

  // Cache keys
  static Str cacheKey( Id partId ) { return "accessOfPart" + n2s( partId ); }
  Str cacheKey() const { return cacheKey( partId ); }

  static const char *select();
};

}} // dta, x2

