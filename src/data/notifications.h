/**
    \file data/notifications.h
    \brief notifications model

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 { namespace dta {

class NotificationsResult : public sql::Result {
public:
  Id postId()       const { return field( "post_id" )    .get<Id>(); }
  Str what()        const { return field( "what" )       .get(); }
  Str title()       const { return field( "title" )      .get(); }
  Str abstract()    const { return field( "abstract" )   .get(); }
  Id  author()      const { return field( "author" )     .get<Id>(); }
  Str authorName()  const { return field( "author_name" ).get(); }
  std::tm created() const { return field( "created" )    .get<std::tm>(); }

  NotificationsResult( sql::PhSession &sql, bool isPrivateMessages );
  void init( Id userId );

  static unsigned short limit() { return cfg.pageSize * 8; }
  static Str select( Id userId, bool isPrivateMessages );

  // modifications
  static void insert( sql::PhSession &sql, Id userId, Id postId, char what );
  static void remove( sql::PhSession &sql, Id userId, Id postId );
  static void removeNotifications( sql::PhSession &sql, bool isPrivateMessages,
    Id userId );

  typedef sql::Iterator<NotificationsResult> Iterator;
  Iterator begin() { return Iterator( *this, 0 ); }
  Iterator end() { return Iterator( *this, recordCount()); }

private:
  bool isPrivateMessages;

  static Str pageLimit() { return " LIMIT " + n2s( limit()); }
};

}} // dta, x2

