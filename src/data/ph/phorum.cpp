/**
    \file data/ph/phorum.cpp
    \brief Base model for all ph pages, and main ph page simultaneously

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace dta { namespace ph {


// PartsResult

PartsResult::PartsResult( sql::PhSession &sql )
  : sql::ComplexCachedResult( sql, resultStructureKey(),
    allPartsCacheKey(), cacheKeyFormat(), select())
{ }

bool PartsResult::init( Id partId ) {
  if( soughtId = partId, ! isPrivate()) { // запрос только не для личного раздела
    execQuery();
    try { row(); } catch( const sql::Error & ) { return false; }
  }
  return true;
}

bool PartsResult::init( Id partId, const User &user ) {
  if( ! init( partId )) return false;
  PartAccess access( soughtId );
  access.getData( session );
  getRights( user, access );
  return true;
}

void PartsResult::createPrivate( sql::PhSession &sql, Id userId ) {
#ifndef NDEBUG
  if( soughtId != getPrivateOf( userId ))
    throw std::runtime_error( "PartsResult::createPrivate: bad userId or the part not fit" );
  if( ! isPrivate())
    throw std::runtime_error( "PartsResult::createPrivate: not private part" );
#endif
  sql( utl::Pf( "INSERT INTO parts (id, author, name, description)"
    " SELECT {1},{2},{3},{4} WHERE NOT EXISTS (SELECT id FROM parts WHERE id = {1})" )
    ( getPrivateOf( userId ))( userId )( name())( description()));
}

const char *PartsResult::select() {
  return R"(SELECT p.id AS id, p.author AS author, p.name AS name, p.description AS description,
p.modified AS modified, p.weight AS weight, p.topics_number AS topics_number, p.posts_number AS posts_number,
p.last_message AS last_message, posts.title AS last_message_title, posts.author AS last_message_author,
posts.modified AS last_message_time, users.name AS last_message_author_name
FROM parts AS p LEFT JOIN posts ON p.last_message=posts.id LEFT JOIN users ON posts.author=users.id
WHERE p.id>0 ORDER BY p.weight, p.id FOR UPDATE OF p NOWAIT)";
}

void PartsResult::update(  sql::PhSession &sql, Id id, Id lastMessage, bool incTopicsNumber ) {
  Str setTopicsNumber;
  if( incTopicsNumber )
    setTopicsNumber = ",topics_number=topics_number+1";
  sql( utl::Pf(
    "UPDATE parts SET last_message={1},posts_number=posts_number+1" + setTopicsNumber +
    " WHERE id={2}" )( lastMessage )( id ));
}

void PartsResult::getPartsOfAuthor( Id anAuthor, std::set<Id> &parts ) {
  execQuery();
  while( next()) if( author() == anAuthor ) parts.insert( id());
}

void PartsResult::unseen( const User &user, std::set<Id> &ids ) {
  for( const auto &p: *this ) {
    dta::PartAccess access( id()); access.getData( session );
    getRights( user, access );
    if( ! rights.R())
      ids.insert( p.id());
  }
}

Str PartsResult::unseen( const std::set<Id> &ids ) {
  Str unseen( "0" );
  for( const auto u: ids ) unseen += ',' + n2s( u );
  return unseen;
}

Str PartsResult::unseen( const User &user ) {
  std::set<Id> ids; unseen( user, ids );
  return unseen( ids );
}

const char *PartsResult::resultStructureKey() { return "PartsResultStructureKey"; }

void PartsResult::getRights( const User &user, PartAccess &access  ) {
  rights = Rights();
  if( isPrivate()) {
    rights.m = isPrivateOf( user.id );
    return;
  }
  if( user.isAdmin || user.id == author()) {
    rights.m = rights.h = true;
    return;
  }
  bool theUserIsAnonymous = user.isAnonymous
    || user.isMemberOfGroup( - id());
  for( const auto &a: access )
    if( a.first == predefined::group::anonymous
      || ( ! theUserIsAnonymous && user.isMemberOfGroup( a.first ))
    )
      rights.add( a.second );
  if( user.isModerator )
    rights.m = true;
}

// TopicsResult

TopicsResult::TopicsResult( sql::PhSession &sql )
  : sql::ComplexCachedResult( sql, resultStructureKey(), Str(),
  cacheKeyFormat(), Str()) { }

void TopicsResult::init( Id partId, int page ) {
  key = topicsOfPartCacheKey( partId, page );
  query = select( 0, partId, page );
  execQuery();
}

bool TopicsResult::init( Id topicId ) {
  soughtId = topicId;
  key = utl::f( cacheKeyFormat(), soughtId );
  query = select( topicId );
  execQuery();
  try { row(); } catch( const sql::Error & ) { return false; }
  return true;
}

bool TopicsResult::updateKey() {
  if( next()) {
    key = topicsOfPartCacheKey();
    triggers.emplace_back( topicsOfPartAllPagesCacheKey());
    return true;
  } else
    return false;
}

Str TopicsResult::select( Id topicId, Id partId, int page ) {
  return utl::pf( R"(SELECT t.*, p.author AS author, p.title AS title,
p.abstract AS abstract, p.modified AS modified, u.name AS author_name,
l.title AS last_post_title, l.author AS last_post_author, l.modified AS last_post_time,
lu.name AS last_post_author_name FROM page_of_topics({1},{2},{3},{4}) AS t JOIN posts AS p
on t.id=p.id JOIN users AS u ON p.author=u.id LEFT JOIN posts AS l ON t.last_post=l.id
LEFT JOIN users AS lu ON l.author=lu.id ORDER BY topics_order(t.pinned,t.last_post)
FOR UPDATE OF p,u NOWAIT)", cfg.pageSize, topicId, partId, page );
}

Id TopicsResult::insert( sql::PhSession &sql, Id partId ) {
  sql()( utl::pf( "INSERT INTO topics (part_id) VALUES({1})", partId ));
  return sql.sequenceLast( "topics_id_seq" );
}

void TopicsResult::update( sql::PhSession &sql, Id id, Id lastPost, bool updatePinned,
  Str pinned, bool incPostsNumber, bool closed )
{
  Str sets;
  if( updatePinned )
    sets = utl::trim( pinned ).empty() ? "pinned=NULL," :
           utl::pf( "pinned={1},", pinned );
  if( incPostsNumber )
    sets += "posts_number=posts_number+1,";
  if( closed )
    sets += "closed='C',";
  if( lastPost )
    sets += "last_post=" + n2s( lastPost ) + ',';
  sql( "UPDATE topics SET " + sets +
    "modified=NOW() WHERE id=" + n2s( id ));
}

Id TopicsResult::generateId( sql::Session &sesion ) {
  return sesion( "SELECT nextval('topics_id_seq')" ).row()[ 0 ].get<Id>();
}

const char *TopicsResult::resultStructureKey() { return "TopicsResultStructureKey"; }

// PostsResult

const Str fText = "text", fHtml = "html", fBbcode = "bbcode",
  fMarkdown = "markdown", fTemplate = "template";

PostsResult::PostsResult( sql::PhSession &sql )
  : sql::ComplexCachedResult( sql, resultStructureKey(), Str(),
  cacheKeyFormat(), Str())
{ }

bool PostsResult::init( Id postId, Id topicId, int page ) {
  query = select( postId, topicId, page );
  if( ! ( soughtId = postId ))
    key = postsOfTopicCacheKey( topicId, page );
  return ! execQueryAll().empty();
}

bool PostsResult::init( Id postId ) {
  soughtId = postId;
  key = utl::f( cacheKeyFormat(), soughtId );
  query = select( postId );
  execQuery();
  try { row(); } catch( const sql::Error & ) { return false; }
  return true;
}

bool PostsResult::updateKey() {
  if( next()) {
    key = postsOfTopicCacheKey();
    triggers.emplace_back( postsOfTopicAllPagesCacheKey());
    // TODO triggers of authors
    return true;
  } else
    return false;
}

void PostsResult::updateQuery() {
  if( next()) query = select( 0, topicId(), page());
}

void PostsResult::addTriggersForOneRecord( Triggers &triggers ) const {
  ComplexCachedResult::addTriggersForOneRecord( triggers );
  triggers.emplace_back( UserResult::cacheKey( author()));
}

utl::Json PostsResult::authorProperties() const {
  utl::Json result; return result.parse( field( "author_properties").get());
}

Str PostsResult::select( Id postId, Id topicId, int page ) {
  Str whereTopic, wherePage, pageSize = n2s( cfg.pageSize );
  if( postId ) {
    whereTopic = utl::pf( "(SELECT topic_id FROM posts WHERE id={1})", postId ),
    wherePage = utl::pf( "(SELECT page FROM p WHERE id={1})", postId );
  } else {
    whereTopic = n2s( topicId );
    wherePage = n2s( page );
  }
  return "WITH p AS (SELECT id, topic_id, author, title, abstract, content, format,"
  " author2, created, modified, (ROW_NUMBER() OVER (ORDER BY id)-1)/" +
  pageSize + " AS page FROM posts WHERE topic_id=" +
  whereTopic + " ORDER BY id) SELECT p.*, u.name AS author_name, u.posts_number AS"
  " author_posts_number, u.properties AS author_properties, u2.name AS author2_name"
  " FROM p JOIN users AS u ON p.author=u.id JOIN users AS u2"
  " ON p.author2=u2.id JOIN posts ON p.id=posts.id WHERE page=" + wherePage +
  " ORDER BY p.id FOR UPDATE OF posts,u,u2 NOWAIT";
}

void PostsResult::insert( sql::PhSession &sql, Id id, Id topicId, Id author,
  Str title, Str abstract, Str content, Str format, Str ip, Id author2 )
{
  sql( utl::Pf( "INSERT INTO posts (id, topic_id, author, title, abstract, content,"
    " format, ip, author2) VALUES ({1},{2},{3},{4},{5},{6},{7},{8},{9})" )
    ( id )( topicId )( author )( title )( abstract )( content )( format )( ip )
    ( author2 ));
}

void PostsResult::update( sql::PhSession &sql, Id id, Str title, Str abstract,
  Str content, Str format, Id author2 )
{
  sql( utl::Pf( "UPDATE posts SET title={1}, abstract={2},"
    " content={3}, format={4}, author2={5}, modified=NOW() WHERE id={6}" )
    ( title )( abstract )( content )( format )( author2 )( id ));
}

const char *PostsResult::resultStructureKey() { return "PostsResultStructureKey"; }

// LastPostsResult

LastPostsResult::LastPostsResult( sql::PhSession &sql, const User &user )
  : sql::CachedResult( sql, cacheKey(), select( 0, cfg.lastPosts.maxNumber ),
                       { PartsResult::allPartsCacheKey() } )
  , user( user )
{ }

std::vector<Id> LastPostsResult::get( unsigned short number ) {
  if( number > cfg.lastPosts.maxNumber ) {
    number = cfg.lastPosts.maxNumber;
    LOG_ERROR( "number>cfg.lastPosts.maxNumber" );
  }
/**
  Алгоритм такой:
  \n В кэши мы держим x2::Cfg::LastPosts::maxNumber идентификаторов последних
  постов, из всех разделов, т.е. независимо от того, видимы они камому-либо
  пользователю или нет. Получаем множество невидимых текущему пользователю
  разделов методом x2:dta::PartsResult::unseen(const User &, std::set<Id> &),
  и выбирам в результат get только подходящие.
  \n Их может не хватить до \a number. Остальные получаем из БД, без кэши уже.
  Вот из-за этого последнего, значение x2::Cfg::LastPosts::maxNumber должно
  задаваться побольше (например 100, если \a number 10 или 20), иначе могут быть
  чрезмерные обращения к БД. Для контроля таких лишних обращений выполняется
  запись в лог сообщения "Extra DB traffic, optimization need?".
*/
  short idField = fieldNumber( "id" ), partIdField = fieldNumber( "part_id" );
  PartsResult parts( session ); parts.init();
  std::set<Id> unseen; parts.unseen( user, unseen );
  std::vector<Id> get; get.reserve( number );
  for( const auto &r: *this )
    if( get.size() < number ) {
      if( unseen.find( r.field( partIdField ).get<Id>()) == unseen.end())
        get.push_back( r.field( idField ).get<Id>());
      else
        continue;
    } else
      return get;
  // Здесь мы в переменной get получили иды постов из кэша,
  // количество которых может оказаться меньше number, остальные
  // надо получить из БД
  LOG_INFO( "Extra DB traffic, optimization need?" );
  sql::Result r( session ); r.exec( select( &unseen, number - get.size(), get.size()));
  while( r.next())
    if( get.size() < number ) {
      if( unseen.find( r.field( partIdField ).get<Id>()) == unseen.end())
        get.push_back( r.field( idField ).get<Id>());
      else
        continue;
    } else
      return get;
  return get;
}

Str LastPostsResult::select( std::set<Id> *unseenIds, short limit, short offset ) {
  Str unseen = "0";
  if( unseenIds ) {
    PartsResult parts( session ); parts.init();
    unseen = parts.unseen( *unseenIds );
  }
  // важно: здесь надо utl::f, а не utl::pf
  return utl::f( "SELECT posts.id, topics.part_id FROM posts JOIN topics"
    " ON posts.topic_id=topics.id JOIN parts ON topics.part_id=parts.id"
    " WHERE parts.id>0 AND parts.id NOT IN ({1}) ORDER BY posts.id DESC"
    " LIMIT {2} OFFSET {3} FOR UPDATE OF posts NOWAIT", unseen, limit, offset );
}

}}} // ph, dta, x2

