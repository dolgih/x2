/**
    \file data/ph/phorum.h
    \brief Data model for all ph pages

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 { namespace dta { namespace ph {

/**
  The Сlass describes the part (раздел) of the X2 Phorum.
  Данные хранятся в таблице базы данных "parts".
*/
struct PartsResult : sql::ComplexCachedResult {

/** В случае если soughtId<0, запись описывает не простой раздел, а раздел личной
    переписки пользователя с идентификатором -soughtId. */

  PartsResult( sql::PhSession &sql );
  void init() { execQuery(); }
  bool /* returns false if partId not found */ init( Id partId );
  /** Initialization with access rights simultaneously */
  bool /* returns false if partId not found */ init( Id partId, const User &user );

  static std::tm nullTm() { return { 0, 0, 0, 1, 0, 70, 0, 0, 0 }; }

  template<class T> T fld( const char* name, T priv = T(), T def = T()) const
  { if( isPrivate()) return priv; else return field( name ).get<T>( def ); }

  /** Identifier of the part */
  Id id()     const { return fld<Id>( "id", soughtId ); }
  /** Ид владельца раздела */
  Id author() const { return fld<Id>( "author", - soughtId, predefined::user::admin /* important */ ); }
  /** Название раздела */
  Str name() const { return fld<Str>( "name", "Private/" + n2s( - soughtId ) + "\t" ); }
  /** Краткое описание раздела */
  Str description() const { return fld<Str>( "description", "Private Messages of id/" + n2s( - soughtId ) + "\t" ); }
  /** Число от 1 до 1000, определяющее очередность вывода раздела.
      Чем меньше число, тем выше выводится раздел в списке разделов
      на странице */
  int weight() const { return fld<int>( "weight", 800, 800 /* default blogger weight */ ); }
  /** Таймстамп последнего изменения для полей id, weight,
      author, name, description */
  std::tm modified()  const { return fld<std::tm>( "modified", nullTm(), nullTm()); }
  /** Количество веток в разделе */
  int topicsNumber()      const { return fld<int>( "topics_number" ); }
  /** Максимальный номер страницы тем (количество страниц - 1) */
  int maxTopicsPage()     const { return ( topicsNumber() - 1 ) / cfg.pageSize; }
  /** Количество сообщений в разделе, для статистики */
  int postsNumber()       const { return fld<int>( "posts_number" ); }
  /** Ид последнего сообщения */
  Id lastMessage()        const { return fld<Id>( "last_message" ); }
  /** Ид автора последнего сообщения */
  Id lastMessageAuthor()  const { return fld<Id>( "last_message_author" ); }
  /** Автор последнего сообщения */
  Str lastMessageAuthorName() const { return fld<Str>( "last_message_author_name" ); }
  /** Заголовок последнего сообщения */
  Str lastMessageTitle()  const { return fld<Str>( "last_message_title" ); }
  /** Время последнего сообщения */
  TimeT lastMessageTime() const { return fld<TimeT>( "last_message_time" ); }

  /** Раздел является разделом личных сообщений */
  bool isPrivate() const { return isPrivate( soughtId ); }
  static bool isPrivate( Id partId ) { return partId < 0; }
  /** Возвращает идентификатор личного раздела пользователя
      с идентификатором userId */
  static Id getPrivateOf( Id userId ) { return ( - userId ); }
  /** Раздел является разделом личных сообщений пользователя userId */
  bool isPrivateOf( Id userId ) const { return id() == getPrivateOf( userId ); }
  /** Создает раздел личных сообщений пользователя userId с идентификатором -userId */
  void createPrivate( sql::PhSession &sql, Id userId );

  static const char *allPartsCacheKey() { return "allParts"; }
  static const char *cacheKeyFormat() { return "part{1}"; }
  static Str cacheKey( Id id ) { return utl::f( cacheKeyFormat(), id ); }
  Str cacheKey() const { return cacheKey( id()); }

  static const char *select();
  void update( sql::PhSession &sql, Id id, Id lastMessage, bool incTopicsNumber );

  /** Regex для контроля ввода названия раздела */
  static const char * nameRegex() { return "^[^\\t]{4,96}$"; }
  /** Regex для контроля ввода описания раздела */
  static const char * descriptionRegex() { return "^[^\\t]{6,128}$"; }

  /** Access rights */
  Rights rights;
  void getRights( const User &user, PartAccess &access );
  bool canEdit( const User &user ) const {
    return user.isAdmin || author() == user.id /* blogger */;
  }

  void getPartsOfAuthor( Id author, std::set<Id> &parts );
  /** В ids возвращает множество идентификаторов
    невидимых для данного пользователя разделов. */
  void unseen( const User &user, std::set<Id> &ids );
  /** Возвраенные методом unseen(const User &, std::set<Id> &) данные
    превращает в годную для SQL-запроса строку. */
  static Str unseen( const std::set<Id> &ids );
  /** Возвращает множество идентификаторов невидимых для данного пользователя
    разделов в виде годной для SQL-запроса строки. При помощи методов
    unseen(const User &, std::set<Id> &) и unseen(const std::set<Id> &). */
  Str unseen( const User &user );

  typedef sql::Iterator<PartsResult> Iterator;
  Iterator begin() { return Iterator( *this, 0 ); }
  Iterator end() { return Iterator( *this, recordCount()); }

private:
  static const char *resultStructureKey();

};

/**
  The Сlass describes the topic (тему, ветку) of the X2 Phorum.
  Данные хранятся в таблице базы данных "topics".
*/
struct TopicsResult : sql::ComplexCachedResult {

  TopicsResult( sql::PhSession &sql );
  void init( Id partId, int page );
  bool /* returns false if topicId not found */ init( Id topicId );
  bool updateKey() override;

  /** Identifier of the topic */
  Id id()                  const { return field( "id" ).get<Id>(); }
  /** Identifier of the part */
  Id partId()              const { return field( "part_id" ).get<Id>(); }
  /** Символ от 'A' до 'Z', определяющий очередность вывода
      прикрепленной темы. Чем раньше символ, тем выше выводится тема
      в списке тем раздела на странице, для неприкрепленной темы пустое */
  Str pinned()             const { return field( "pinned" ).get( Str()); }
  /** Ид последнего сообщения */
  Id lastPost()            const { return field( "last_post" ).get<Id>( 0 ); }
  /** Символ 'C' (латинская) для закрытой темы или '' (пустая строка)
      для открытой темы. */
  bool closed()            const { return field( "closed" ).get() == "C"; }
  /** Номер страницы списка тем раздела, нумерация с 0 */
  int page()               const { return field( "page" ).get<int>(); }
  /** Количество сообщений в теме, для статистики */
  int postsNumber()        const { return field( "posts_number" ).get<int>(); }
  /** Максимальный номер страницы постов (количество страниц - 1) */
  int maxPostsPage()       const { return ( postsNumber() - 1 ) / cfg.pageSize; }
  /** Таймстамп последнего изменения */
  TimeT modified()         const { return field( "modified" ).getTime(); }
  /** Ид автора темы */
  Id author()              const { return field( "author" ).get<Id>(); }
  /** Название темы */
  Str title()              const { return field( "title" ).get(); }
  /** Краткое описание темы */
  Str abstract()           const { return field( "abstract" ).get(); }
  /** Автор темы */
  Str authorName()         const { return field( "author_name" ).get(); }
  /** Ид автора последнего сообщения */
  Id lastPostAuthor()      const { return field( "last_post_author" ).get<Id>(); }
  /** Заголовок последнего сообщения */
  Str lastPostTitle()      const { return field( "last_post_title" ).get(); }
  /** Автор последнего сообщения */
  Str lastPostAuthorName() const { return field( "last_post_author_name" ).get(); }
  /** Время последнего сообщения */
  TimeT lastPostTime()     const { return field( "last_post_time" ).get( TimeT()); }


  static Str topicsOfPartAllPagesCacheKey( Id partId )
  { return "topicsOfPart" + n2s( partId ); }
  Str topicsOfPartAllPagesCacheKey() const { return topicsOfPartAllPagesCacheKey( partId()); }
  static Str topicsOfPartCacheKey( Id partId, int page )
  { return topicsOfPartAllPagesCacheKey( partId ) + "ofPage" + n2s( page ); }
  Str topicsOfPartCacheKey() const { return topicsOfPartCacheKey( partId(), page()); }
  static const char *cacheKeyFormat() { return "topic{1}"; }
  Str cacheKey() const { return cacheKey( id()); }
  static Str cacheKey( Id topicId ) { return utl::f( cacheKeyFormat(), topicId ); }

  static Str select( Id topicId, Id partId = 0, int page = 0 );
  static Id insert( sql::PhSession &sql, Id partId );
  static void update( sql::PhSession &sql, Id id, Id lastPost, bool updatePinned,
    Str pinned, bool incPostsNumber, bool closed = false );

  /** Regex для контроля ввода названия темы */
  static const char * titleRegex() { return "^.{6,256}$"; }
  /** Regex для контроля ввода буквы прикрепления */
  static const char * pinnedRegex() { return "^[A-Z]?$"; }

  static Id generateId( sql::Session &sql );

  bool canAnswer( const PartsResult &part ) const {
    return part.rights.W() && ! closed();
  }
  bool canClose( const PartsResult &part, const User &user ) const {
    if( part.isPrivate())
      return ! closed() && part.isPrivateOf( user.id );
    return part.rights.M() || ( part.rights.C() && author() == user.id );
  }
  bool canMoveToTrash( const PartsResult &part, const User &user ) const {
    if( part.isPrivate())
      return false;
    return part.rights.M() || ( part.rights.C() && author() == user.id );
  }

  typedef sql::Iterator<TopicsResult> Iterator;
  Iterator begin() { return Iterator( *this, 0 ); }
  Iterator end() { return Iterator( *this, recordCount()); }

private:
  static const char *resultStructureKey();

};

// Допустиые значения поля format таблицы posts
extern const Str fText, fHtml, fBbcode, fMarkdown, fTemplate;

/**
  The Сlass describes the post (статью) of the X2 Phorum.
  Данные хранятся в таблице базы данных "posts".
*/
struct PostsResult : sql::ComplexCachedResult {

  PostsResult( sql::PhSession &sql );
  bool init( Id postId, Id topicId, int page );
  bool /* returns false if postId not found */ init( Id postId );
  bool updateKey() override;
  void updateQuery() override;
  void addTriggersForOneRecord( Triggers &triggers ) const override;

  /** Identifier of the post */
  Id id()                    const { return field( "id" ).get<Id>(); }
  /** Identifier of the topic */
  Id topicId()               const { return field( "topic_id" ).get<Id>(); }
  /** Ид автора */
  Id author()                const { return field( "author" ).get<Id>(); }
  /** Различные свойства автора */
  utl::Json authorProperties() const;
  /** Ид последнего автора, изменявшего пост; а для личных сообщений это
      идентификатор адресата */
  Id author2()               const { return field( "author2" ).get<Id>(); }
  /** Количество сообщений в теме, для статистики */
  int authorPostsNumber()    const { return field( "author_posts_number" ).get<int>(); }
  /** Номер страницы списка тем раздела, нумерация с 0 */
  int page()                 const { return field( "page" ).get<int>(); }
  /** Название статьи */
  Str title()                const { return field( "title" ).get(); }
  /** Краткое описание статьи */
  Str abstract()             const { return field( "abstract" ).get(); }
  /** Краткое описание статьи */
  Str content()              const { return field( "content" ).get(); }
  /** Краткое описание статьи */
  Str format()               const { return field( "format" ).get(); }
  /** Таймстамп последнего изменения */
  TimeT created()      const { return field( "created" ).getTime(); }
  /** Таймстамп последнего изменения */
  TimeT modified()     const { return field( "modified" ).getTime(); }
  /** Автор статьи */
  Str authorName()           const { return field( "author_name" ).get(); }
  /** Последний автор статьи или адресат для личноно сообщения */
  Str authorName2()          const { return field( "author2_name" ).get(); }

  static Str postsOfTopicAllPagesCacheKey( Id topicId )
  { return "postsOfTopic" + n2s( topicId ); }
  Str postsOfTopicAllPagesCacheKey() const { return postsOfTopicAllPagesCacheKey( topicId()); }
  static Str postsOfTopicCacheKey( Id topicId, int page )
  { return postsOfTopicAllPagesCacheKey( topicId ) + "ofPage" + n2s( page ); }
  Str postsOfTopicCacheKey() const { return postsOfTopicCacheKey( topicId(), page()); }
  static const char *cacheKeyFormat() { return "post{1}"; }

  /** Regex для контроля ввода названия поста */
  static const char * titleRegex() { return TopicsResult::titleRegex(); }

  static Str select( Id postId, Id topicId = 0, int page = 0 );
  static void insert( sql::PhSession &sql, Id id, Id topicId, Id author, Str title,
    Str abstract, Str content, Str format, Str ip, Id author2 );
  static void update( sql::PhSession &sql, Id id, Str title, Str abstract,
    Str content, Str format, Id author2 );

  bool canEdit( const PartsResult &part, const TopicsResult &topic, const User &user ) const
  {
    if( empty() || part.isPrivate() || topic.closed())
      return false;
    return part.rights.M()
      || ( part.rights.O() && topic.author() == user.id )
      || ( part.rights.W() && author() == user.id );
  }
  bool canRemove( const PartsResult &part, const TopicsResult &topic, const User &user ) const
  {
    if( empty())
      return false;
    if( topic.closed() && id() != topicId()) // post in closed topic cannot be removed
      return false;
    if( part.isPrivate())
      return part.isPrivateOf( user.id );
    return part.rights.M()
      || ( part.rights.O() && topic.author() == user.id )
      || ( part.rights.W() && author() == user.id );
  }

  bool isStartTopic() const { return id() == topicId(); }

  bool isModified( Id partId ) const {
    if( empty()) return false;
    return partId > 0 && created() != modified();
  }

  // for citing in new post
  static Id quoteId( Id id ) { return -id; }

  typedef sql::Iterator<PostsResult> Iterator;
  Iterator begin() { return Iterator( *this, 0 ); }
  Iterator end() { return Iterator( *this, recordCount()); }

private:
  static const char *resultStructureKey();

};

/**
  The Сlass describes the last posts list visible to anonymous.
  Данные выбираются из таблице базы данных "posts".
*/
struct LastPostsResult : sql::CachedResult {

  LastPostsResult( sql::PhSession &sql, const User &user );
  void init() { execQuery(); }
  /** Возвращает вектор идентификаторов последних постов,
    видимых пользователем. */
  std::vector<Id> get( unsigned short number );

  static const char *cacheKey() { return "lastPosts"; }

private:
  const User &user;

  Str select( std::set<Id> *unseen, short limit, short offset = 0 );
};

}}} // ph, dta, x2

