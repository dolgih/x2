/**
    \file data/user.cpp
    \brief user model

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace dta {


UserResult::UserResult( sql::PhSession &sql )
: BaseUserResult( sql ) { resultStructureKey = structureKey(); }

UserResult::UserResult( sql::PhSession &sql, Id id )
: BaseUserResult( sql ) { resultStructureKey = structureKey(); findById( id ); }

UserResult &UserResult::findById( Id id ) {
  if( ! id ) {
    allProperties = utl::Json::createObj();
    return *this;
  }
  key = cacheKey( id );
  query = select( id );
  execQuery();
  if( empty()) {
    Str msg = "findById: user id not found: " + utl::id2s( id );
    LOG_ERROR( msg );
    throw ErrorNotFound( msg );
  }
  row();
  getProperties();
  return *this;
}

Str UserResult::columnList() {
  return " users.* ";
}

Str UserResult::select( Id id ) {
  return utl::pf( "SELECT " + columnList() + " FROM users"
  " WHERE id={1} FOR UPDATE OF users NOWAIT", id );
}

UserResult &UserResult::findBy
( const Str &what, const Str &value, const Str &sql ) {
  Id id = 0; Str cacheKey = what + '=' + value;
  if( cache.fetch( cacheKey, id ))
    return findById( id );
  sql::Transaction guard( session()); {
    exec( utl::pf( sql, value )).row();
    if( empty()) {
      Str msg = "findBy: " + what + " not found:" + value;
      LOG_ERROR( msg );
      throw ErrorNotFound( msg );
    }
    id = this->id();
    storeInCache( UserResult::cacheKey( id ));
    cache.store( cacheKey, id, { UserResult::cacheKey( id ) } );
  }; guard.commit();
  getProperties();
  return *this;
}

Id UserResult::insert( sql::PhSession &sql, Str name, Str email, Str phone, Str ip,
  Str password )
{
  sql( utl::Pf( "INSERT INTO users (name, email,"
                " phone, properties, ip,  password ) VALUES ({1},{2},{3},{4},{5},{6})" )
       ( name )( email )( phone )( allProperties.str())( ip )( password ));
  return sql.sequenceLast( "users_id_seq" );
}

Id UserResult::update( sql::PhSession &sql, Id id, Str name, Str email, Str phone,
  Str password, bool newId )
{
  Str s = "UPDATE users SET name={1}, email={2},"
    " phone={3}, properties={4}, modified=NOW()";
  if( newId ) s += ",id=nextval('users_id_seq')";
  if( ! password.empty()) s += ",password={6}";
  s += " WHERE id={5}";
  utl::Pf pf( s );
  pf( name )( email )( phone )( allProperties.str())( id )( password );
  sql( pf );
  return newId ? sql.sequenceLast( "users_id_seq" ) : id;
}

void UserResult::remove( sql::PhSession &sql, Id id ) {
  if( predefined::user::isPredefined( id )) return;
  sql( utl::pf( "DELETE from users WHERE id={1}", id ));
}

// ComplexUserResult

ComplexUserResult::ComplexUserResult( sql::PhSession &sql )
  : BaseUserResult( sql )
{
  resultStructureKey = structureKey();
  format = cacheKeyFormat();
}

bool ComplexUserResult::init( Id id, int page ) {
  query = select( id, page );
  if( ! ( soughtId = id ))
    key = usersOfPageCacheKey( page );
  return ! execQueryAll().empty();
}

bool ComplexUserResult::updateKey() {
  if( next()) {
    key = usersOfPageCacheKey();
    triggers.emplace_back( usersAllPagesCacheKey());
    return true;
  } else
    return false;
}

void ComplexUserResult::updateQuery() {
  if( next()) query = select( 0, page());
}

Str ComplexUserResult::select( Id id, int page ) {
  Str wherePage;
  if( id )
    wherePage = utl::pf( "(SELECT page FROM t WHERE id={1})", id );
  else
    wherePage = n2s( page );
  return "WITH t AS (SELECT id, (ROW_NUMBER() OVER (ORDER BY name ASC)-1)/"
  + n2s( cfg.pageSize * 5 ) + " AS page, (COUNT(*) OVER ()-1)/"
  + n2s( cfg.pageSize * 5 ) + " AS max_page FROM users ORDER BY name ASC)"
  " SELECT users.*, t.page, t.max_page FROM users JOIN t ON users.id=t.id"
  " WHERE page=" + wherePage + " ORDER BY name ASC FOR UPDATE OF users NOWAIT";
}

// GroupsOfUserResult

GroupsOfUserResult::GroupsOfUserResult( sql::PhSession &sql )
  : sql::CachedResult( sql )
{ }

void GroupsOfUserResult::init( Id userId ) {
  resultStructureKey = structureKey();
  key = cacheKey( userId );
  query = select( userId );
  triggers.emplace_back( UserResult::cacheKey( userId ));
  execQuery();
}

Str GroupsOfUserResult::select( Id userId ) {
  return utl::pf( R"(SELECT groups.id AS id, groups.name AS name, u2g.user_id AS user_id,
u2g.modified as modified, r.description, r.before FROM groups LEFT JOIN
(SELECT * FROM users2groups WHERE user_id={1} FOR UPDATE OF users2groups NOWAIT) AS u2g
ON groups.id=u2g.group_id LEFT JOIN restricted AS r
ON u2g.user_id=r.user_id AND u2g.group_id=r.group_id ORDER BY groups.name
FOR UPDATE OF groups NOWAIT)", userId );
}

}} // dta, x2

