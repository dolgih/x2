/**
    \file data/user.h
    \brief user model

    This file is a part of Phorum New Technology project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 { namespace dta {

template<class B, class O> struct BaseUserResult : B {
  using B::B;
  Id id()                const { return B::field( "id" ).B::template get<Id>(); }
  Str name()             const { return B::field( "name" ).get(); }
  Str password()         const { return B::field( "password" ).get(); }
  Str email()            const { return B::field( "email" ).get(); }
  Str phone()            const { return B::field( "phone" ).get(); }
  Str properties()       const { return B::field( "properties" ).get(); }
  Str postsNumber()      const { return B::field( "posts_number" ).get(); }
  std::tm lastActivity() const { return B::field( "last_activity" ).B::template get<std::tm>(); }
  std::tm created()      const { return B::field( "created" ).B::template get<std::tm>(); }
  std::tm modified()     const { return B::field( "modified" ).B::template get<std::tm>(); }
  // properties
  utl::Json allProperties;
  void getProperties() {
    Str p = properties();
    allProperties = utl::Json().parse( p.empty() ? R"({"human_name":""})" : p );
  }
#define DEFINEPROPERTY(n,t) Str n() const { return get( allProperties, t ); } \
  void n( const Str &v ) { if( v.empty()) allProperties.del( t ); \
                                  else allProperties.add( t, v ); }
#define DEFINEBOOLPROPERTY(n,t) bool n() const { return allProperties[ t ]; } \
  void n( bool v ) { allProperties.add( t, v ); }
  DEFINEPROPERTY(humanName,"human_name")
  DEFINEPROPERTY(webSite,"web_site")
  DEFINEPROPERTY(phone2,"phone2")
  DEFINEPROPERTY(avatar,"avatar")
  // preferences
  DEFINEPROPERTY(locale,"locale")
  DEFINEPROPERTY(theme,"theme")
  DEFINEBOOLPROPERTY(doNotTrackAnswers,"pref_do_not_track_answers")
  DEFINEBOOLPROPERTY(mailOnAnswer,"pref_mail_on_answer")
  DEFINEBOOLPROPERTY(doNotTrackQuotes,"pref_do_not_track_quotes")
  DEFINEBOOLPROPERTY(mailOnQuote,"pref_mail_on_quote")
#undef DEFINEPROPERTY
#undef DEFINEBOOLPROPERTY

  typedef sql::Iterator<O> Iterator;
  Iterator begin() { return Iterator( (O&)*this, 0 ); }
  Iterator end() { return Iterator( (O&)*this, this->recordCount()); }
protected:
  static Str get( const utl::Json &obj, const char *key ) {
    if( obj.empty()) return "--Error--";
    return obj[ key ];
  }
};

class UserResult : public BaseUserResult<sql::CachedResult,UserResult> {
public:
  struct ErrorNotFound : sql::Error {
    ErrorNotFound( const Str &msg ) : sql::Error( msg ) { }
  };

  static const char * nameRegex()      { return "^[A-Za-z]\\w{2,31}$"; }
  static const char * passwordRegex()  { return "^(.{6,32})?$"; }
  static const char * humanNameRegex() { return "^(.{3,128})$"; }
  static const char * emailRegex()     { return "^[^@\\s/\\:,;'\"<>]+@[^@\\s/\\:,;'\"<>]+$"; }
  static const char * webSiteRegex()   { return "^([^\\s\\,;'\"@]{5,128})?$"; }
  static const char * phoneRegex()     { return "^((\\d|\\(|\\)|-|\\+){5,16})?$"; }
  static const char * avatarRegex()    { return "^([^\\s\\,;'\"@]{5,192})?$"; }

  UserResult( sql::PhSession &sql );
  UserResult( sql::PhSession &sql, Id id );
  UserResult &findById( Id id );
  UserResult &findBy( const Str &what, const Str &value, const Str &sql );

  static Str cacheKey( Id id ) { return "user=" + n2s( id ); }

  static Str columnList();
  static Str select( Id id );

  // modifications
  Id insert( sql::PhSession &sql, Str name, Str email, Str phone, Str ip, Str password );
  Id update( sql::PhSession &sql, Id id, Str name, Str email, Str phone,
    Str password, bool newId );
  static void remove( sql::PhSession &sql, Id id );

private:
  static const char *structureKey() { return "userStructureKey"; }
};

struct Users2rise : std::set<Id> {
  void rise() const
  { for( const auto &id: *this ) cache.rise( UserResult::cacheKey( id )); }
};

/** Запрос на страницу пользователей,
    init с параметром Id также выполняет запрос целой страницы пользователей,
    той на которой находится пользователь Id. */
class ComplexUserResult : public BaseUserResult<sql::ComplexCachedResult,ComplexUserResult> {
public:
  ComplexUserResult( sql::PhSession &sql );
  bool init( Id id, int page );

  bool updateKey() override;
  void updateQuery() override;

  int page()    const { return field( "page" ).get<int>(); }
  int maxPage() const { return field( "max_page" ).get<int>(); }

  static const char *usersAllPagesCacheKey() { return "allUsers"; }
  static Str usersOfPageCacheKey( int page ) { return "usersOfPage" + n2s( page ); }
  Str usersOfPageCacheKey() const { return usersOfPageCacheKey( page()); }
  static const char * cacheKeyFormat() { return "user{1}"; }
  static Str cacheKey( Id id ) { return utl::f( cacheKeyFormat(), id ); }

  static Str select( Id id, int page );


private:
  static const char *structureKey() { return "complexUserStructureKey"; }
};

/** a group that user belongs to (users2groups table) */
class GroupsOfUserResult : public sql::CachedResult {
public:
  GroupsOfUserResult( sql::PhSession &sql );
  void init( Id userId );

  Id id()                const { return field( "id" ).get<Id>(); }
  Str name()             const { return field( "name" ).get(); }
  /** banned because of */
  Str description()      const { return field( "description" ).get( Str()); }
  TimeT modified()      const { return field( "modified" ).getTime( 0 ); }
  bool belongs()         const { return ! field( "user_id" ).isNull(); }
  std::tm before()       const { return field( "before" ).get<std::tm>(
    { 0, 0, 0, 1, 0, 200, 0, 0, 0 } ); }

  static Str select( Id userId );

  static Str cacheKey( Id userId ) { return utl::f( "ExtGroupsOfUser{1}", userId ); }

  typedef sql::Iterator<GroupsOfUserResult> Iterator;
  Iterator begin() { return Iterator( *this, 0 ); }
  Iterator end() { return Iterator( *this, recordCount()); }

private:
  static const char * structureKey() { return "ExtGroupsOfUserStructureKey"; }
};

}} // dta, x2

