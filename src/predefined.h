/**
    \file predefined.h
    various constants used in x2

    This file is a part of x2 project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#ifndef X2_PREDEFINED_H_
#define X2_PREDEFINED_H_

namespace x2 {
namespace predefined {

namespace user {
const int guest = -1, anonymous = 0,  admin = 1, system = 2;
inline bool isPredefined( int user ) { return user >= guest && user <= system ; }
} //user

namespace group {
const int minBannedEverywhere = -100,
  banned = -3, notApproved = -2, notActivated = -1, anonymous = 0,
  admin = 1, moderators = 3, users =  100;
inline bool isPredefined( int group ) {
  return ( group >= banned && group <= admin ) || group == moderators
    || group == users;
}
inline bool bannedInPart( int group ) { return group < minBannedEverywhere; }
inline bool bannedEverywhere( int group ) { return group >= minBannedEverywhere && group < 0; }
} //user

namespace part {
const int announcements = 300, news = 400, pages = 500, feedback = 600, bin = 700;
inline bool isPredefined( int part ) {
  return part == announcements || part == news || part == pages
      || part == feedback || part == bin;
}
} //part

namespace page {
const int main = 1, about = 200, contacts = 300, source = 400;
inline bool isPredefined( int page ) {
  return page == main || page == about || page == contacts || page == source;
}
} //page


}} // predefined, x2

#endif // X2_PREDEFINED_H_
