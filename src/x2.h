/**
    \file x2.h
    main x2 header

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

//#pragma once

#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include <string>
#include <sstream>
#include <map>
#include <exception>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <mutex>
#include <atomic>
#include <cxxabi.h>

#include <boost/property_tree/info_parser.hpp>
#include <boost/regex.hpp>
#include <boost/container/list.hpp>

#include <json-c/json.h>

#include <libxml/HTMLparser.h>
#include <libxml/xmlsave.h>

extern "C" {
  #include <mkdio.h>
}

#include "mail/mail.h"
#include "utl.h"

#ifdef X2FCGILIB
  #include "fastcgi/fastcgi.hpp"
  #include "fastcgi/x2fastcgi.h"
#else
  #include <fcgi_config.h>
  #include <fcgiapp.h>
  #include <fcgios.h>
#endif

#include "bbcode/bbcode.h"
#include "cfg/cfg.h"
#include "xss/xss.h"
#include "crypt/crypt.h"
#include "cache/cache.h"
#include "postgres/cachedSql.h"
#include "predefined.h"

#include "views/form.h"
#include "content.h"
#include "server.h"

#include "data/base.h"
#include "data/user.h"
#include "data/notifications.h"
#include "data/ph/phorum.h"
#include "data/adm/groups.h"
#include "data/adm/part.h"

#include "ctl/visited.h"

#include "views/pages.h"
#include "views/user.h"
#include "views/navigation.h"
#include "views/adm/admin.h"
#include "views/ph/phorum.h"

