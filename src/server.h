/**
    \file server.h
    \brief Various server classes

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 {


struct ServerError : std::runtime_error {
  int status;
  ServerError( int status, const Str &msg )
    : std::runtime_error( msg ), status( status ) { }
};

struct NotFoundError : ServerError {
  NotFoundError( const Str &msg ) : ServerError( 404, msg ) { }
};

struct AddressError : NotFoundError {
  AddressError() : NotFoundError( "Can not interpret the address line" ) { }
};

struct ForbiddenError : ServerError {
  ForbiddenError( const Str &msg ) : ServerError( 403, msg ) { }
};

struct Request : FCGX_Request {
  void clear() { output.clear(); output.str( Str()); posts.clear(); files.clear(); }

  void get( Str &str ) const;
  void put( const Str &str );

#ifdef X2FCGILIB
  using FCGX_Request::param;
  using FCGX_Request::isResponder;
  using FCGX_Request::contentLength;
#else
  Str param( const Str &name ) const {
    if( auto p = FCGX_GetParam( name.data(), envp )) return p; else return Str();
  }
  bool isResponder() const { return param( "FCGI_ROLE" ) == "RESPONDER"; }
  size_t contentLength() const { return utl::s2ul( param( "CONTENT_LENGTH" )); }
#endif

  Str method() const { return param( "REQUEST_METHOD" ); }
  bool isPost() const { return param( "REQUEST_METHOD" ) == "POST"; }
  bool isGet() const { return param( "REQUEST_METHOD" ) == "GET"; }
  Str host() const { return param( "HTTP_HOST" ); }
  Str hostAsPrefix() const { return proto() + "://" + host(); }
  Str ifModifiedSince() const { return param( "HTTP_IF_MODIFIED_SINCE" ); }
  Str userAgent() const { return param( "HTTP_USER_AGENT" ); }
  Str acceptLanguage() const { return param( "HTTP_ACCEPT_LANGUAGE" ); }
  Str referer() const { return param( "HTTP_REFERER" ); }
  Str cookie() const { return param( "HTTP_COOKIE" ); }
  Str docRoot() const; // "DOCUMENT_ROOT"
  Str contentType() const { return param( "CONTENT_TYPE" ); }
  Str serverName() const { return param( "SERVER_NAME" ); }
  Str serverAddr() const { return param( "SERVER_ADDR" ); }
  Str serverPort() const { return param( "SERVER_PORT" ); }
  Str remoteAddr() const { return param( "REMOTE_ADDR" ); }
  Str remotePort() const { return param( "REMOTE_PORT" ); }
  Str queryString() const { return param( "QUERY_STRING" ); }
  Str uri() const { return param( "REQUEST_URI" ); }
  Str scriptName() const { return param( "SCRIPT_NAME" ); }
  Str pathInfo() const { return param( "PATH_INFO" ); }
  Str xForwardedProto() const { return param( "HTTP_X_FORWARDED_PROTO" ); }
  Str xForwardedPort() const { return param( "HTTP_X_FORWARDED_PORT" ); }
  Str xRequestStart() const { return param( "HTTP_X_REQUEST_START" ); }
  Str xClientIp() const { return param( "HTTP_X_CLIENT_IP" ); }
  Str xForwardedFor() const { return param( "HTTP_X_FORWARDED_FOR" ); }
  Str xForwardedHost() const { return param( "HTTP_X_FORWARDED_HOST" ); }
  Str xForwardedServer() const { return param( "HTTP_X_FORWARDED_SERVER" ); }

  Str clientIp() const;
  Str clientPort() const;
  Str proto() /** "http" or "https" */ const;

  Str post( const Str &name ) const;

  void printEnvironment() const;
  void printPosts() const;

  bool parseForm();
  static Str urlDecode( const char *text, const char *end );
  bool parseJsonRpc( utl::Json &query ) const;

  mutable std::ostringstream output;
  typedef std::multimap<Str,Str> Posts; Posts posts;
  struct File { Str filename, contentType; }; std::map<Str,File> files;

private:
  bool parseFormUrlencoded();
  bool parseFormMultipart( Str boundary );
};

class Response {
public:
  Response() { clear(); }
  void clear() { theStatus = 200; setHtmlContentType(); theContentLength = 0; headers.clear(); }
  void setStatus( int status ) {
    if( statuses.find( status ) == statuses.end())
      throw std::runtime_error( utl::f( "Unknown Status: {1}", status ));
    theStatus = status;
  }
  int getStatus() const { return theStatus; }
  static Str header( const char *name, const Str &value )
    { return name + ( ": " + ( value + "\r\n" )); }
  Str status() const {
    return utl::f( "Status: {1} {2}\r\n", theStatus, ( statuses.find( theStatus ))->second );
  }
  void setContentType( const Str &type ) { theContentType = type; }
  void setTextContentType() { setContentType( "text/plain; charset=UTF-8" ); }
  void setHtmlContentType() { setContentType( "text/html; charset=UTF-8" ); }
  void setXmlContentType()  { setContentType( "text/xml; charset=UTF-8" ); }
  Str contentType() const { return header( "Content-type", theContentType ); }
  void setContentLength( size_t length ) { theContentLength = length; }
  Str contentLength() const
    { return header( "Content-Length", n2s( theContentLength )); }
  void addHeader( const char *name, const Str &val )
    { headers.emplace_back( header( name, val )); }
  void redirect( const Str &url ) { addHeader( "Location", url ); setStatus( 302 ); }
  bool isErrorStatus() const;
  Str errorPage( int status );

  static Str urlEncode( const Str &url );

  list<Str> headers;

private:
  int theStatus;
  Str theContentType;
  size_t theContentLength;

  typedef const std::map<int, const char*> Statuses;
  static Statuses statuses;
};


class Thread {
public:

  static void run();
  static void cleanup();

  ~Thread() { /*FCGX_Free( &request, 0 );*/ }

protected:
friend Page::Page( const Thread &thread );
friend Page::Demiurge Page::Dispatcher::findDemiurge( const Thread &thread ) const;
  Request request;
  mutable Response response;
  mutable Url url; // mutable for Page::Dispatcher::findDemiurge

private:
  static void killHandler( int s );
  [[ noreturn ]] void *doit();
  [[ noreturn ]] static void *staticDoit( Thread *o ) { /* return */ o->doit(); }
  void idle();

  pthread_t id = 0;

  static int socketId;
  static list<Thread> all;
  static std::atomic_uint threadsCount;
};

} // x2

