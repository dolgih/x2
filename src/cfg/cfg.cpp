/**
    \file cfg.cpp
    config classes

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace pt = boost::property_tree;


namespace x2 {

static void replaceWithEnv( Str &str ) {
  boost::regex re("(\\$\\{(\\w*)\\})");
  str = boost::regex_replace( str, re,
    [] ( const boost::smatch &match ) -> Str  {
      Str varName( match[2] );
      if( const char *varVal = getenv( varName.data()))
        return varVal;
      else {
        LOG_WARN( "Ph Cfg: environment variable not found: " + varName );
        return Str();
      }
    }
  , boost::match_any );
}

template<class T> T limited( T min, T max, T value ) {
  return std::max( min, std::min( max, value ));
}

void Cfg::load( int argc, char *argv[] ) {
  if( const char *lf = getenv( "X2_LOGGING_FILE" )) logging.file = lf;

  Str confFileName;
  if( const char *fn = getenv( "X2_CONFIG_INFO" )) confFileName = fn;
  for( int i = 1; i < ( argc - 1 ); i ++ )
    if( ! strcmp( argv[ i ], "-c" ) || ! strcmp( argv[ i ], "--config" ))
      confFileName = argv[ i + 1 ];
  if( confFileName.empty())
    throw std::runtime_error( "x2 config file not set" );
  std::ifstream confFile( confFileName );
  Str cfg( (std::istreambuf_iterator<char>( confFile )),
           std::istreambuf_iterator<char>());
  if( ! confFile.good())
    throw std::runtime_error( "cannot read config file: " + confFileName );

  { // modification time of the config
    struct stat st; if( stat( confFileName.data(), &st ))
      throw std::runtime_error( "cannot stat config file: " + confFileName );
    else
      modified = st.st_mtim.tv_sec;
  }

  replaceWithEnv( cfg );

  std::stringstream in( cfg );
  pt::ptree configTree; pt::read_info( in, configTree );
  pt::ptree emptyTree;
  const pt::ptree &tree = configTree.get_child( "x2", emptyTree );

#define VALUE( var, def ) var = tree.get( #var, def )
#define LIMITED( min, max, var, def ) var = limited( double( min ), double( max ), \
  tree.get( #var, double( def )))

  VALUE( title,       "X2 Site" );
  VALUE( description, "Super Power X2 Site" );
  VALUE( keywords,    "Super Power X2" );
  VALUE( root,        "/x2" );
  VALUE( docRoot,     "." );
  VALUE( media,       "/media" );
  VALUE( css,         "/css" );
  VALUE( js,          "/js" );
  VALUE( crypt,       "Secret" );
  LIMITED( 1, 1000, pageSize, 12 );

  // postgres
  VALUE( postgres.database,    "dbname=x2" );
  VALUE( postgres.defTSConfig, "english" );

  // logging
  Str llevel = tree.get( "logging.level", "warn" );
  logging.level = ( llevel == "debug" ? utl::LogDebug :
    ( llevel == "info" || llevel.empty() ? utl::LogInfo :
    ( llevel == "warning" || llevel == "warn" ? utl::LogWarn :
    ( llevel == "error" ? utl::LogError :
    ( llevel == "fatal" ? utl::LogFatal : utl::LogInvalid )))));
  if( logging.level == utl::LogInvalid ) {
    Str err = "invalid log level in " + confFileName;
    LOG_FATAL( err ); throw( std::runtime_error( err ));
  }
  if( logging.file.empty()) VALUE( logging.file, "x2.log" );
  VALUE( logging.append,    true );
  VALUE( logging.cacheCont, Str());

  // locales
  VALUE( localization.path,   "po" );
  VALUE( localization.domain, "x2" );
  for( const pt::ptree::value_type &v: tree.get_child( "localization.locales", emptyTree )) {
    Str id = v.second.data();
    // добавляем в локаль наш вспомогательный фасет и пустой фасет для чисел
    std::locale lWithCat( std::locale( std::locale( id.data()), new MessagesCatalog ));
    localization.locales[ id ] = lWithCat.combine<std::numpunct<char>>( std::locale());
    if( localization.locales.size() == 1 ) // first in list is default
      localization.defaultLocale = *localization.locales.begin();
  }
  // Make 'system default locale' system global
//  std::locale::global( localization.locales.defaultLocale.second );
  setlocale( LC_ALL, localization.defaultLocale.first.data());

  // themes
  Themes::Theme emptyTheme = themes.defaultTheme = { Str(), "sun3.png", "Sunflower.png" };
  for( const pt::ptree::value_type &v: tree.get_child( "themes", emptyTree )) {
    Themes::Theme theme = {
      v.second.get( "cssFile",   emptyTheme.cssFile ),
      v.second.get( "logoFile",  emptyTheme.logoFile ),
      v.second.get( "endOfMain", emptyTheme.endOfMain )
    };
    themes.allThemes[ v.first.data() ] = theme;
    if( themes.allThemes.size() == 1 ) // first in list is default
      themes.defaultTheme = themes.allThemes.begin()->second;
  }

  // Smtp
  VALUE( smtp.server,   "smtps://smtp.yandex.com" );
  VALUE( smtp.name,     "admin@yandex.com" );
  VALUE( smtp.password, "secret" );
  VALUE( smtp.from,     "<admin@yandex.com>" );
  VALUE( smtp.starttls, false );

  // admin
  VALUE( admin.policy.godMode, false );
  VALUE( admin.policy.validateEmail, true );
  VALUE( admin.policy.newUser.requiresApproval, false );
  for( const pt::ptree::value_type &v: tree.get_child( "admin.policy.newUser.notify", emptyTree ))
    admin.policy.newUser.notify.insert( v.first.data());
  VALUE( admin.sql.psqlCommand, "psql -c" );

  // cookie
  VALUE( cookie.name, "x2" );
  LIMITED( 60 /* a minute */, 31536000 /* 365 days */, cookie.maxAge, 604800 /* a week */ );

  // client
  VALUE( client.addressVar, "REMOTE_ADDR" );
  VALUE( client.portVar,    "REMOTE_PORT" );
  VALUE( client.protoVar,   "REQUEST_SCHEME" );
  VALUE( client.proto,      "https" );

  // server
  VALUE( server.socketPath, ":5000" );
  LIMITED( 1, 256, server.threadCount, 8 );
  LIMITED( 8192, 33554432, server.maxPostSize, 2097152 );
  LIMITED( 8192, 33554432, server.maxFile,     2097152 );

  // message
  LIMITED( 8192,  1048576, message.maxSize.user,      32768 );
  LIMITED( 32768, 2097152, message.maxSize.admin,     1048576 );
  LIMITED( 256,   32768,   message.maxSize.anonymous, 8192 );
  LIMITED( 64,    4096,    message.maxAbstract,       256 );

  // lastPosts
  LIMITED( 32, 1024, lastPosts.maxNumber, 128 );

  // avatar
  LIMITED( 8192, 2097152, avatar.maxSize, 65536 );

  // table
  LIMITED( 5, 1E4, table.pageSize, 100 );

  // cache
  LIMITED( 0,  1E6,  cache.maxNumber,  2000 );
  LIMITED( 1,  2048, cache.maxSum,     64 );
  LIMITED( 16, 1024, cache.maxKeySize, 64 );
  LIMITED( 1,  1000, cache.fill.tr.count, 500 );
  LIMITED( 1,  1E6,  cache.fill.tr.sleep, 2E5 );

  // activity
  LIMITED( 1,                  1000, activity.minTopics, 20 );
  LIMITED( activity.minTopics, 2000, activity.maxTopics, 100 );
  LIMITED( 1,                  1000, activity.minUsers,  20 );
  LIMITED( activity.minUsers,  2000, activity.maxUsers,  100 );
  LIMITED( 1, 3600 /* an hour */, activity.maxTimeInCache, 120 );
  LIMITED( 60, 1800 /* half an hour */, activity.breakTime, 300 );

  // xss
  for( const pt::ptree::value_type &v: tree.get_child( "xss.allow", emptyTree ))
    xss.allow.insert( v.first.data());
  for( const pt::ptree::value_type &v: tree.get_child( "xss.deny", emptyTree ))
    xss.deny.insert( v.first.data());
  for( const pt::ptree::value_type &v: tree.get_child( "xss.iframes", emptyTree ))
    xss.iframes.insert( v.first.data());

}

Cfg cfg;

const std::locale::id MessagesCatalog::id;

} // x2

