/**
    \file cfg.h
    config classes

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 {

extern class Cfg {
public:
  void load( int argc, char *argv[] );

  TimeT modified;

  Str title, description, keywords, root, docRoot, media, css, js, crypt;
  short pageSize;
  struct Postgres { Str database, defTSConfig; } postgres;
  struct Logging {
    utl::LogLevel level = utl::LogDebug;
    Str file;
    bool append = true;
    Str cacheCont;
  } logging;
  struct Localization {
    Str path, domain;
    std::map<Str,std::locale> locales;
    std::pair<Str,std::locale> defaultLocale;
  } localization;
  struct Themes {
    struct Theme { Str cssFile, logoFile, endOfMain; };
    std::map<Str,Theme> allThemes;
    Theme defaultTheme;
    const Theme &get( const Str &name ) const {
      auto theme = allThemes.find( name );
      if( theme == allThemes.end()) return defaultTheme;
      else return theme->second;
    }
  } themes;
  struct Smtp { Str server, name, password, from; bool starttls; } smtp;
  struct Admin {
    struct Policy {
      bool godMode, validateEmail;
      struct NewUser { bool requiresApproval; std::set<Str> notify; } newUser;
    } policy;
    struct Sql { Str psqlCommand; } sql;
  } admin;
  struct Cookie { Str name; unsigned maxAge; } cookie;
  struct Client { Str addressVar, portVar, protoVar, proto; } client;
  struct Server { short threadCount; Str socketPath; size_t maxPostSize, maxFile; } server;
  struct Message {
    struct MaxSize { size_t user, admin, anonymous; } maxSize;
    size_t maxAbstract;
  } message;
  struct LastPosts { unsigned short maxNumber; } lastPosts;
  struct Avatar { unsigned maxSize; } avatar;
  struct Table { unsigned short pageSize; } table;
  struct Cache {
    unsigned maxNumber, maxSum; unsigned short maxKeySize;
    struct Fill { struct Try { unsigned count, sleep; } tr; } fill;
  } cache;
  struct Activity {
    unsigned
      /// Миниальное количество тем, необходимое для записи в БД.
      minTopics,
      /// Максимальное количество тем в списке.
      maxTopics,
      /// Миниальное количество пользователей, необходимое для записи в БД.
      minUsers,
      /// Максимальное количество пользователей в списке.
      maxUsers,
      /// Время хранения без записи.
      /** В течение этого времени данные кэши могут находиться в памяти не
          будучи записанными в БД. По истечении этого времени данные принудительно
          записываются в БД, даже если их накопилось меньше минимально необходимых
          для записи (minUsers, minTopics). В секундах. */
      maxTimeInCache,
      /// Время разрыва.
      /** Если в течение заданного времени (в секундах) данный пользователь не
          заходил в тему, то после этого времени его посещение будет считаться
          еще одним посещением, и прибавится к количеству посещений этой темы. */
      breakTime;
  } activity;
  struct Xss {
    std::set<Str> allow, deny, // дополнительные разрешенные и запрещенные тэги
                      iframes; // допустимые адреса для тэгов iframe (Youtube и т.д.)
  } xss;

} cfg;

/** Вспомогательный фасет для хранения открытого каталога. */
struct MessagesCatalog : std::locale::facet {
  mutable std::messages<char>::catalog catalog;
  mutable bool opened = false;
  static const std::locale::id id;
};

} // x2

