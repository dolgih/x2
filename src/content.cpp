/**
    \file content.cpp
    \brief Various content classes

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Url

Url::Url( const Str &parent, const Str &key, const Str &tail )
  : parent( parent ), key( makeKey( key )), tail( tail )
{ }

bool Url::nextPart( Str path, Str &tail, Str &val ) {
  size_t pos = path.find( '/' );
  if( pos == Str::npos ) { tail = Str(); val = path; return true; }
  tail = path.substr( pos + 1 );
  val = path.substr( 0, pos );
  return true;
}

bool Url::nextPart( const Str &path, Str &tail, long &val ) {
  char *end;
  Str next; nextPart( path, tail, next );
  val = strtol( next.data(), &end, 10 );
  return ! *end;
}

bool Url::nextPart( const Str &path, Str &tail, int &val ) {
  long longVal; bool r = nextPart( path, tail, longVal );
  val = longVal; return r;
}

// User

User::User() : id( 0 ), hasMessages( false ), hasAnswers( false ),
  isAdmin( false ), isAnonymous( true ), isModerator( false ),
  theLocale( &cfg.localization.defaultLocale.second )
/** Не забыть, что конструируется анонимус, это используется в sitemap, например. */
{ /* makes the anonymous */ }

void User::init( Id anId, sql::PhSession &sql ) {
  id = anId;
  dta::UserResult userResult( sql, id );
  name = userResult.name();
  theme = userResult.theme();
  Triggers userTrigger = { userResult.cacheKey( id ) };
  // users2groups
  sql::CachedResult groups( sql, utl::f( "groupsOfUser{1}", id ),
    utl::pf( "SELECT group_id FROM users2groups WHERE user_id={1}"
             " FOR UPDATE OF users2groups NOWAIT", id ), userTrigger );
  groups.execQuery();
  while( groups.next())
    groupsOfUser.insert( groups[ 0 ].get<Id>());
  // notifications
  sql::CachedResult notificats( sql, utl::f( "notificationsOfUser{1}", id ),
    utl::pf( "SELECT post_id, what FROM notifications"
             " WHERE user_id={1} FOR UPDATE OF notifications NOWAIT", id ), userTrigger );
  notificats.execQuery();
  while( notificats.next()) {
    notifications.insert( notificats[ 0 ].get<Id>());
    if( notificats[ 1 ].get<Str>() == "P" ) hasMessages = true;
    else hasAnswers = true;
  }
  // set if anonymous
  isAnonymous = hasAnonymousId() || isMemberOfGroup( predefined::group::anonymous )
    || bannedEverywhere();
  // set if admin
  isAdmin = id == predefined::user::admin
    || ( isMemberOfGroup( predefined::group::admin ) && ! isAnonymous );
  // set if moderator
  isModerator = isAdmin
    || ( ! isAnonymous && isMemberOfGroup( predefined::group::moderators ));
}

void User::createAnonymous( sql::PhSession &sql ) {
  sql( "SELECT nextval('users_id_seq_anonymous')" ).row().fetch( 0, id );
  name = "guest/" + n2s( -id );
}

// end of User

// Cookie

Cookie::Cookie( const Request &request, Response &response )
  : request( request ), response( response ), id( 0 ), csrf( 0 ), changed( false )
{
  clear();
  Str cname = cfg.cookie.name + '=';
  Str c = request.cookie();
  size_t found = c.find( cname );
  if( found == Str::npos ) return;
  c = c.substr( found + cname.size());
  try { c = crypt::decode( c, cfg.crypt ); } catch ( const std::exception & ) { return; }
  char aLocale[ 32 ] = { 0 };
  if( 2 > sscanf( c.data(), "%li %hu %31s", &id, &csrf, aLocale )) return;
  locale = aLocale;
  changed = false;
}

void Cookie::clear()
{ set( predefined::user::anonymous, Str()); setCsrf(); }

void Cookie::set( Id anId, const Str &aLocale ) {
  if( id != anId )        { changed = true; id = anId; }
  if( locale != aLocale ) { changed = true; locale = aLocale; }
  setCsrf();
}

void Cookie::setCsrf() {
  changed = true;
/**
  Для создания нового csrf используем std::time с секундной дискретностью (а, не с
  микросекундной, как в tv_usec таймера, например) для борьбы с DDoS, потому что это
  значение используется для создания псевдоидентификаторов, см. \ref negative_users_subsec
*/
  csrf = std::time( 0 ); if( ! csrf ) csrf = 1;
}

void Cookie::set() {
  if( ! changed ) return;
  Str theCookie = crypt::encode( n2s( id ) + ' ' + getCsrf() + ' ' + locale, cfg.crypt );
  theCookie = cfg.cookie.name + '=' + theCookie
    + "; Max-Age=" + n2s( cfg.cookie.maxAge )
    + "; Expires=" + utl::htdate( std::time( 0 ) + cfg.cookie.maxAge )
    + "; Path=/; Version=1";
  response.addHeader( "Set-Cookie", theCookie );
}

// Page

Page::Page( const Thread &thread )
  : request( thread.request ), response( thread.response ),
  out( request.output ), cookie( thread.request, thread.response ),
  theTitle( cfg.title ), theDescription( cfg.description ),
  url( thread.url ), changed( false )
{
  if( ( user.id = cookie.getId())) {
    try { user.init( user.id, sql ); }
    catch ( const dta::UserResult::ErrorNotFound &e ) { cookie.clear(); }
    catch ( const std::exception &e ) { CAUGHT_EXCEPTION( e ); }
  }

  // activity
  visited.put( user.id, cookie.getCsrfUS(), timer.tv_sec );

  // locale
  Str locale = cookie.getLocale();
  if( ! locale.empty()) {
    auto found = cfg.localization.locales.find( locale );
    if( found != cfg.localization.locales.end())
      user.locale( found->second );
  }
  request.output.imbue( user.locale());
}

void Page::doGet() {
  load();
  switch( response.getStatus()) { case 302: case 304: break; default: render(); }
  cookie.set();
}

void Page::doPost() {
  // load posts data into forms
  for( Form *form: children ) {
    form->takePosts();
    if( form->csrf.get<Str>() != cookie.getCsrf()) {
      out << response.errorPage( 403 );
      LOG_ERROR( "CSRF checking failed" );
      return;
    }
  }
  load();
  switch( response.getStatus()) {
    case 302: case 304: break;
    default:
      if(  response.isErrorStatus() || ! validate()
        || response.isErrorStatus() || ! save()) render();
  }
  cookie.set();
}

bool Page::validate() {
  bool valid = true;
  for( Form *form: children ) {
    if( ! changed && form->changed()) changed = true;
    if( ! ( form->valid = form->validate())) valid = false;
  }
  return valid;
}

Str Page::tr( const char *text, const std::locale &locale ) {
  if( ! std::has_facet<MessagesCatalog>( locale )) return text;
  const MessagesCatalog &mc       = std::use_facet<MessagesCatalog>( locale );
  const std::messages<char> &msgs = std::use_facet<std::messages<char>>( locale );
  if( ! mc.opened ) {
    mc.opened = true;
    mc.catalog = msgs.open( cfg.localization.domain, locale, cfg.localization.path.data());
  }
  Str translated = msgs.get( mc.catalog, 0, 0, text );
  // TODO ? msgs.close( mc.catalog );
  return translated;
}

Str Page::t2s( const std::tm &tm, const char *format ) const
{ return utl::t2s( tm, format, user.locale()); }

Str Page::t2s( const TimeT &t, const char *format ) const
{ return utl::t2s( t, format, user.locale()); }

Str Page::xss( const Str &html ) { return xss::filter( html ); }

Str Page::markdown( const Str &input ) {
  int flags = 0x0004; // no_pants
  /// It is safe to const cast as mkd_string does not
  /// alter original string
  MMIOT *doc = mkd_string(const_cast<char *>(input.c_str()),input.size(),flags);
  if(!doc) {
    throw std::runtime_error("Failed to read document");
  }

  mkd_compile(doc,flags);

  char *content_ptr = 0;
  int content_size = 0;

  content_size = mkd_document(doc,&content_ptr);
  Str result(content_ptr,content_size);
  mkd_cleanup(doc);
  return result;
}

Str Page::bbcode( const Str &bbc, bool fullUrl ) const {
  return ph::BbCodable( *this ).bbDecode( bbc, fullUrl );
}

void Page::createAnonymous() {
  user.createAnonymous( sql());
  cookie.set( user.id, Str());
  Str password = user.name, email = user.name + "@nomail.nodomen",
      properties = "{\"avatar\":\"/media/stdAvatars/guest.jpg\"}";
  sql( utl::pf( "INSERT INTO users(id, name, password, email, properties)"
                " VALUES({1},{2},{3},{4},{5})",
  user.id, user.name, password, email, properties ));
}

void Page::mail( Str to, Str subj, Str msg, Str html ) {
/* std::cout << "mail:to:" << to << "\nsubj=" << subj << "\nmsg=" << msg
          << "\nhtml=" << html; */
  mail::Smtp mail( cfg.smtp.name, cfg.smtp.password, cfg.smtp.server );
  if( cfg.smtp.starttls ) mail.useSslAll();
  if( ! msg.empty())
    msg += "\r\n\r\n"
      "Thanks, X2 Phorum Engine\r\n";
  if( ! html.empty())
    html += "<br><p>"
      "Thanks, X2 Phorum Engine</p>";
  mail.send( cfg.smtp.from, to, subj, msg, html );
}

void Page::sendSysMsgAboutNewUser( Id to, Id newUser ) {
  generateMessage( to, 0, "We have a new user", Str(),
  "Hello Admin!\r\n------------\r\n"
  "We have got a new user, please click on the URL below to see his profile.\r\n"
  "\r\n<" + request.hostAsPrefix() + urlOf<UserProfile>( newUser ) +
  ">\r\n", dta::ph::fMarkdown, predefined::user::system );
}

/**
  \brief Generates a private message.
  Before the method call a transaction must be begun.
  \returns (may be new) topicId
*/
Id Page::generateMessage( Id toId, Id topicId, Str title, Str abstract,
                          Str content, Str format, Id fromId )
{
  dta::ph::PartsResult part( sql );
  part.init( part.getPrivateOf( toId ));
  part.createPrivate( sql, toId );

  Id messageId;
  if( topicId )
    messageId = dta::ph::TopicsResult::generateId( sql );
  else
    messageId = topicId = dta::ph::TopicsResult::insert( sql, part.id());
  dta::ph::PostsResult::insert( sql, messageId, topicId, fromId, title, abstract,
    content, format, request.clientIp(), toId );
  dta::ph::TopicsResult::update( sql, topicId, messageId, false, Str(), true,
    fromId == predefined::user::system );

  dta::Users2rise dummy;
  generateNotification( toId, messageId, false, dummy, notificationPrivate );

  return topicId;
}

Page::Page( const Page &page, const Url &url ) // "rewrite" constructor
  : request( page.request ), response( page.response ),
  out( page.out ), user( page.user ), cookie( page.cookie ), error( page.error ),
  theTitle( page.theTitle ), theDescription( page.theDescription ),
  url( url ), changed( false ), theMenu( page.theMenu ), timer( page.timer ),
  lastId( page.lastId )
{ }

void Page::caught( const std::exception& e, const char * module,
  const char * file, int line ) const
{
  if( response.getStatus() == 200 )
    response.setStatus( 500 );
  utl::log( utl::LogError,
    e.what() + ( " SCRIPT_NAME=" + request.scriptName() + " PATH_INFO=" + request.pathInfo()),
    module, file, line );
  error = e.what();
}

// void Page::render() and void Page::renderBody() are in views/base.cpp

// Page::Dispatcher

Page::Dispatcher::Dispatcher( const char *key, const Dispatcher &parent )
  : key( key ), parent( parent )
{ if( &parent != this ) parent.children[ key ] = this; }

void Page::Dispatcher::init() {
  if( &parent == this ) theUrl = Url( cfg.root );
  else { theUrl = Url( parent.url(), key ); }
  for( const auto child: children ) child.second->init();
}

Page::Demiurge Page::Dispatcher::findDemiurge( const Thread &thread ) const {
  Str key = thread.url.tail;
  thread.url.tail = Str();
  if( const char *tail = strpbrk( key.data(), "/&=?()[]{},;" )) {
    thread.url.tail = tail[ 0 ] == '/' ? tail + 1 : tail;
    key = key.substr( 0, tail - key.data());
  }
  thread.url.key = key;
  thread.url.parent = url();

  auto found = children.find( key );
  if( found == children.end()) {
    auto found = map.find( key );
    if( found == map.end()) {
      auto found = map.find( Str()); // last chance with the empty key
      if( found == map.end()) return 0; else {
        thread.url.tail = thread.url.key + '/' + thread.url.tail;
        thread.url.key = Str();
        return found->second;
      }
    } else return found->second;
  } else
    return found->second->findDemiurge( thread );
}

Page::Dispatcher::~Dispatcher() {
  while( ! children.empty()) delete children.begin()->second;
  if( &parent != this )
    parent.children.erase( key );
}

// end of Page::Dispatcher

Page::PageDispatcher::PageDispatcher() : Page::Dispatcher( 0, *this )
{
  map = {
    { Str(), Home::create<Home> }, dispatch<Home>(),
    dispatch<Login>(),        dispatch<Notifications>(),  dispatch<Search>(),
    dispatch<UserProfile>(),  dispatch<Activate>(),       dispatch<Rss>(),
    dispatch<Sitemap>(),      dispatch<ThePage>(),        dispatch<About>(),
    dispatch<Contacts>(),     dispatch<RpcJs>(),          dispatch<RpcServer>()
  };
}

Page::PageDispatcher Page::dispatcher;
const adm::Base::Dispatcher adm::Base::dispatcher( Page::dispatcher );
const ph::Base::Dispatcher ph::Base::dispatcher( Page::dispatcher );

// end of Page

// LastModified

bool LastModified::notModified( const Request &request, Response &response ) {
  response.addHeader( "Cache-Control", "no-cache" );
  Str lastModified = utl::htdate( t );
  response.addHeader( "Last-Modified", lastModified );
  if( request.ifModifiedSince() == lastModified ) {
    response.setStatus( 304 );
    return true;
  }
  return false;
}

bool LastModified::notModified( Page &page ) {
  if( page.user.id == predefined::user::anonymous )
    return notModified( page.request, page.response );
  else
    return false;
}

// end of LastModified

} // x2

