/**
    \file xss/xss.h
    \brief xss filter

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#pragma once

namespace x2 { namespace xss {

extern struct Filter {
  void init();
  Str operator() ( const Str &html, bool showTrash = true );

} filter;

}} // xss, x2

