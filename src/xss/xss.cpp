/**
    \file xss/xss.cpp
    \brief xss filter

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 { namespace xss {

std::set<Str> tags, iframes;

void Filter::init() {
  tags = { "text",           "ol",             "ul",             "li",
           "p",              "b",              "i",              "tt",
           "sub",            "sup",            "blockquote",     "strong",
           "em",             "h1",             "h2",             "h3",
           "h4",             "h5",             "h6",             "div",
           "span",           "code",           "pre",            "a",
           "hr",             "br",             "img",            "table",
           "tr",             "th",             "td",             "center",
           "section",        "main",           "article",        "header",
           "footer",         "iframe"
  };
  iframes = { "https://www.youtube.com/" };
  for( const auto &t: cfg.xss.allow )   tags.insert( t );
  for( const auto &t: cfg.xss.deny )    tags.erase( t );
  for( const auto &t: cfg.xss.iframes ) iframes.insert( t );
}

/** C++ wrapper for xmlBuffer and xmlSaveCtxt. */
struct XmlSave {
  XmlSave() {
    if( ! ( buf = xmlBufferCreate()))
      throw std::runtime_error( "can not create xml buffer" );
  }

  void clear() { xmlBufferEmpty( buf ); }

  void saveTree( xmlNodePtr cur ) {
    xmlSaveCtxtPtr ctx;
    if( ! ( ctx = xmlSaveToBuffer( buf, "UTF-8", XML_SAVE_NO_EMPTY )))
      throw std::runtime_error( "can not create save context" );
    if( xmlSaveTree( ctx, cur ) == -1 )
      throw std::runtime_error( "can not save tree" );
    if( xmlSaveClose( ctx ) == -1 )
      throw std::runtime_error( "can not close ctx" );
  }

  const xmlChar *get() const { return xmlBufferContent( buf ); }

  int length() const { return xmlBufferLength( buf ); }

  ~XmlSave() { xmlBufferFree( buf ); }

private:
  xmlBufferPtr buf;
};

#define toChar( s ) reinterpret_cast<const char*>( s )
#define toUChar( s ) reinterpret_cast<const unsigned char*>( s )

struct TheFilter : XmlSave {
  TheFilter( bool showTrash ) : showTrash( showTrash ) { }

  void removeNode( xmlNodePtr cur ) {
    if( showTrash ) {
      clear();
      saveTree( cur );
      xmlNodePtr newNode = xmlNewTextLen( get(), length());
      xmlAddNextSibling( cur, newNode );
    }
    xmlUnlinkNode( cur );
    xmlFreeNode( cur );
  }

  void doFilter( xmlNodePtr cur, int recursion ) {
    if( recursion >= 20 ) {
      removeNode( cur );
      return;
    }
    for( xmlAttrPtr attr = cur->properties; attr; ) { // remove all "on..." attributes
      xmlAttrPtr next = attr->next;
      if( ! strncmp( toChar( attr->name ), "on", 2 ))
        xmlRemoveProp( attr );
      attr = next;
    }
    for( cur = cur->children; cur; ) {
      xmlNodePtr next = cur->next;
      const auto f = tags.find( toChar( cur->name ));
      if( f == tags.end()) // remove
        removeNode( cur );
      else {
        bool nodeExists = true;
        if( ! strcmp( toChar( cur->name ), "iframe" )) // iframes addreses
          nodeExists = checkIframe( cur );
        if( nodeExists && cur->children )
          doFilter( cur, recursion + 1 );
      }
      cur = next;
    }
  }

  /** Returns false if the node has been eliminated. */
  bool checkIframe( xmlNodePtr cur ) {
    xmlAttrPtr src = xmlHasProp( cur, toUChar( "src" ));
    if( src && src->children && src->children->content ) {
      const char *addr = toChar( src->children->content );
      auto f = iframes.lower_bound( addr );
      if( f == iframes.begin() || ( f --, strncmp( f->data(), addr, f->size()))) {
        removeNode( cur );
        return false;
      }
    }
    return true;
  }

private:
  bool showTrash;
};

Str Filter::operator() ( const Str &html, bool showTrash ) {
  Str out;
  htmlDocPtr doc = 0;
  try {
    if( ! ( doc = htmlReadDoc( toUChar( html.data()),
        "", "UTF-8",
        HTML_PARSE_RECOVER | HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING |
        HTML_PARSE_NOBLANKS )))
      throw std::runtime_error( "xml parsing failed" );
    xmlNodePtr root = xmlDocGetRootElement( doc );

    // remove "html" and "body" tags
    for( root = root->children; root; root = root->next )
      if( ! strcmp( toChar( root->name ), "body" )) {
        xmlNodeSetName( root, toUChar( "div" ));
        xmlNewProp( root, toUChar( "xss" ), toUChar( "passed" )); // метка для красоты
        break;
      }

    TheFilter filter( showTrash );
    if( root ) {
      filter.doFilter( root, 0 );
      // for the tree saving we will use the same object, just clear it
      filter.clear(); filter.saveTree( root );
    }
    out.assign( toChar( filter.get()), filter.length());
  } catch( const std::exception &e ) {
    out = Str( "xml error: " ) + e.what();
    LOG_ERROR( out );
  }

  if( doc ) xmlFreeDoc( doc );
  return out;
}

Filter filter;

}} // xss, x2

