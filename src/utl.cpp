/**
    \file utl.cpp
    \brief Various utilities. See also utl.h

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"
#include <openssl/sha.h>

namespace x2 {
namespace utl {

// datetime

TimeT::TimeT( std::tm tm ) { t = mktime( &tm ); }

TimeT::operator std::tm() const { std::tm tm; localtime_r( &t, &tm ); return tm; }

std::tm TimeT::gmt() const { std::tm tm; gmtime_r( &t, &tm ); return tm; }

// T2s

std::ostream &T2s::out( std::ostream &stream ) const {
/* Вместо этого текста, дающего меньший бинарный код,
  const std::time_put<char,std::ostreambuf_iterator<char>> &tp =
    std::use_facet<std::time_put<char,std::ostreambuf_iterator<char>>>( stream.getloc());
   используем чуть более быстрый и короткий. */
  const struct Tp : std::time_put<char,std::ostreambuf_iterator<char>> { } tp;
  tp.put( std::ostreambuf_iterator<char>( stream ), stream, ' ', &time,
    format.data(), format.data() + format.size());
  return stream;
}

Str T2s::str( const std::locale &locale ) {
/* этот вариант правильный и красивый,
  std::ostringstream os; os.imbue( locale ); out( os );
  return os.str();
   но мы применим слегка уродливый, но чуть более быстрый.
*/
  struct Dummy : std::ios_base
    { Dummy( const std::locale &locale ) { imbue( locale ); } } dummy( locale );
  const struct Tp : std::time_put<char,std::back_insert_iterator<Str>> { } tp;
  Str out; tp.put( std::back_inserter( out ), dummy, ' ', &time,
                   format.data(), format.data() + format.size());
  return out;
}

// t2s

Str t2s( const std::tm &tm, const char *format, const std::locale &locale ) {
  return T2s( tm, format ).str( locale );
}

Str t2s( const TimeT &t, const char *format, const std::locale &loc ) {
  return t2s( (std::tm)t, format, loc );
}

// F

F::operator Str() const {
  Str r = theFormat;
  for( const auto &i: store ) r = replace( r, '{' + n2s( i.first + 1 ) + '}', i.second );
  return r;
}

// Pf

Pf &Pf::operator()( const Str &v ) {
  store[ store.size() ] = "'" + replace( v, "'", "''" ) + "'";
  return *this;
}

Pf &Pf::operator()( const std::tm &v ) {
  store[ store.size() ] = "'" + sql::formatTime( v ) + "'::timestamptz";
  return *this;
}

// string functions

// Returns a new string in which all occurrences of a specified string
// in the current instance are replaced with another specified string.
Str replace( Str s, Str pattern, Str repl ) {
  size_t index = 0;
  for(;;) {
    /* Locate the substring to replace. */
    index = s.find( pattern, index );
    if( index == Str::npos ) break;

    /* Make the replacement. */
    s.replace( index, pattern.length(), repl );

    /* Advance index forward so the next iteration doesn't pick it up as well. */
    index += repl.length();
  }
  return s;
}

namespace htEscConst { static const char lt[] = "&lt;",
                                         gt[] = "&gt;",
                                        amp[] = "&amp;",
                                       quot[] = "&quot;"; }

Str htEsc( Str data ) {
  Str out; unsigned i, len = data.size();
  out.reserve( len * 3 / 2 );
  for( i = 0; i < len; i ++ )
    switch( char c = data[ i ] ) {
      case '<': out += htEscConst::lt; break;
      case '>': out += htEscConst::gt; break;
      case '&': out += htEscConst::amp; break;
      case '"': out += htEscConst::quot; break;
      default: out += c;
    }
  return out;
}

Str htEsc2( Str data ) {
  Str out; unsigned i, len = data.size();
  out.reserve( len * 3 / 2 );
  for( i = 0; i < len; i ++ )
    switch( char c = data[ i ] ) {
      case '<': out += htEscConst::lt; break;
      case '>': out += htEscConst::gt; break;
      case '"': out += htEscConst::quot; break;
      case '&': {
        const char *d = &data[ i ] + 1;
        if(  strncmp( d, htEscConst::lt + 1,   sizeof htEscConst::lt - 2 )
          && strncmp( d, htEscConst::gt + 1,   sizeof htEscConst::gt - 2 )
          && strncmp( d, htEscConst::amp + 1,  sizeof htEscConst::amp - 2 )
          && strncmp( d, htEscConst::quot + 1, sizeof htEscConst::quot - 2 )
        ) {
          out += htEscConst::amp;
          break;
        }
      }
      default: out += c;
    }
  return out;
}

// logging

static void redir2() {
  int fd = open( cfg.logging.file.data(),
    O_CREAT | O_WRONLY | ( cfg.logging.append ? O_APPEND : O_TRUNC ), 0640 );
  if( fd == -1 ) {
    perror( ( "redir2: can not open log file = " + cfg.logging.file ).data());
    exit( 1 );
  }
  if( dup2( fd, 2 ) == -1 ) { perror( "redir2: dup2" ); exit( 1 ); }
  close( fd );
}

void log( LogLevel lvl, Str msg, const char *func, const char *file, int line ) {
  if( lvl < cfg.logging.level ) return;
  const char *level;
  switch( lvl ) {
    case LogDebug: level = "debug";   break;
    case LogInfo:  level = "info";    break;
    case LogWarn:  level = "warning"; break;
    case LogError: level = "error";   break;
    case LogFatal: level = "fatal";   break;
    default: level = "invalid log level";
  }
  std::time_t t = std::time( 0 ); std::tm tm; localtime_r( &t, &tm );
  {
    static std::mutex mutex; std::lock_guard<std::mutex> guard( mutex );
    static bool redirected2 = false;
    if( ! redirected2 && ! cfg.logging.file.empty()) {
      redir2(); redirected2 = true;
    };
    std::cerr << F( "{1}; {2}: {3}; ({4}, in {5}: {6})\n" )
            % t2s( tm ) % level % msg % func % file %  line;
    std::cerr.flush();
  }
}

Str sha1( Str s ) {
  Str md( 20, '\0' );
  SHA1( (const unsigned char*)s.data(), s.size(), (unsigned char*)md.data());
  Str r;
  for( int i = 0; i < 20; i ++ ) r += n2s( (unsigned char)md[ i ], "%2.2x" );
  return r;
}

SelfName::SelfName() {
  theName = Str( PATH_MAX + 1, '\0' );
  int length = readlink( "/proc/self/exe", (char*)theName.data(), PATH_MAX );
  if( -1 == length )
    throw std::runtime_error( "selfName: readlink error" );
  else theName.resize( length );
}
SelfName selfName;

Str getClassName( const std::type_info &ti ) {
  int status; char *demangled = abi::__cxa_demangle( ti.name(), 0, 0, &status );
  if( status ) throw std::runtime_error( "getClassName failed" );
  Str className( demangled ); free( demangled ); return className;
}

// date & time

Str htdate( std::tm *tm )
{
  static const char* wday[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
  static const char* monthn[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                 "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
  char result[80];

  sprintf(result, "%s, %02d %s %d %02d:%02d:%02d GMT",
    wday[tm->tm_wday], tm->tm_mday, monthn[tm->tm_mon], tm->tm_year + 1900,
    tm->tm_hour, tm->tm_min, tm->tm_sec);
  return result;
}

Str htdate( std::time_t t )
{
  struct ::tm tm;
  gmtime_r(&t, &tm);
  return htdate(&tm);
}

const char *dateRegex() {
  return "^(19|20)\\d\\d-((0[1-9]|1[012])-(0[1-9]|[12]\\d)|(0[13-9]|1[012])-30|(0[13578]|1[02])-31)$";
}

const char *dateFormat() { return "%Y-%m-%d"; }

// JsonIt

JsonIt::JsonIt( const json_object_iterator &from ) : json_object_iterator( from ) { }
bool JsonIt::operator == ( const JsonIt &right ) { return json_object_iter_equal( this, &right ); }
JsonIt &JsonIt::operator ++ () { json_object_iter_next( this ); return *this; }
JsonIt::Pair JsonIt::operator*() {
  Pair p; p.n = json_object_iter_peek_name( this );
  p.o = json_object_iter_peek_value( this );
  return p;
}
Json &JsonIt::Pair::val() { return /* Zorro */ *reinterpret_cast<Json*>( &o ); }

// Json

Json::Json( const Json &other ) { o = json_object_get( other.o ); }
const Json &Json::operator = ( const Json &other ) {
  json_object_put( o );
  o = json_object_get( other.o );
  return *this;
}
Json &Json::parse( const Str &str ) {
  json_object_put( o );
  o = json_tokener_parse( str.data());
  return *this;
}
Json::Json( bool v ) : o( json_object_new_boolean( v )) { }
Json::Json( int v ) : o( json_object_new_int64( v )) { }
Json::Json( long v ) : o( json_object_new_int64( v )) { }
Json::Json( double v ) : o( json_object_new_double( v )) { }
Json::Json( const char *str ) : o( json_object_new_string( str )) { }
Json::Json( const Str &str ) : o( json_object_new_string_len( str.data(), str.size())) { }
Json Json::createObj() { Json r; r.o = json_object_new_object(); return r; }
Json Json::createArray() { Json r; r.o = json_object_new_array(); return r; }
Json::Json( const JsonIt::Pair &pair ) { o = json_object_get( pair.o ); }
bool Json::empty() const { return json_object_is_type( o, json_type_null ); }
Json::operator bool() const { return  json_object_get_boolean( o ); }
Json::operator double() const { return  json_object_get_double( o ); }
Json::operator long() const { return  json_object_get_int( o ); }
Json::operator Str() const { return  Str( json_object_get_string( o ), json_object_get_string_len( o )); }
Json Json::operator[]( const char *key ) const {
  Json r;
  json_object_object_get_ex( o, key, &r.o );
  json_object_get( r.o );
  return r;
}
Str Json::str( bool pretty ) const {
  return json_object_to_json_string_ext
  ( o, pretty ? JSON_C_TO_STRING_PRETTY : JSON_C_TO_STRING_PLAIN );
}
Json &Json::add( const Str &key, const Json &el ) {
  json_object_get( el.o );
  json_object_object_add( o, key.data(), el.o );
  return *this;
}
Json &Json::del( const Str &key ) {
  json_object_object_del( o, key.data());
  return *this;
}
Json &Json::add( const Json &el ) {
  json_object_get( el.o );
  json_object_array_add( o, el.o );
  return *this;
}
Json Json::operator[] ( int idx ) const {
  Json r;
  r.o = json_object_array_get_idx( o, idx );
  json_object_get( r.o );
  return r;
}
int Json::arraySize() const { return json_object_array_length( o ); }

JsonIt Json::begin() { return json_object_iter_begin( o ); }
JsonIt Json::end() { return json_object_iter_end( o ); }
Json::~Json() { json_object_put( o ); }

/* use of json object
Json j = Json::createObj().add( "a", 44.44 ).add( "b", "qwe" );
j.add( "a", 33.333 );
for( auto j0: j ) {
  std::cout << j0.val().str() << '\n';
}*/

}} // utl, x2

