/**
    \file server.cpp
    \brief Various server classes

    This file is a part of x2 Phorum project.
    https://x2f.herokuapp.com/x2

    (c) 2015 anatoliDolguikh, dolguikh@outlook.com
    See License.txt for licensing information.
*/

#include "x2.h"

namespace x2 {

// Response

Response::Statuses Response::statuses = {
 { 100, "Continue" },                        { 101, "Switching Protocols" },
 { 200, "OK" },                              { 201, "Created" },
 { 202, "Accepted" },                        { 203, "Non-Authoritative Information" },
 { 204, "No Content" },                      { 205, "Reset Content" },
 { 206, "Partial Content" },                 { 300, "Multiple Choices" },
 { 301, "Moved Permanently" },               { 302, "Found" },
 { 303, "See Other" },                       { 304, "Not Modified" },
 { 305, "Use Proxy" },                       { 307, "Temporary Redirect" },
 { 400, "Bad Request" },                     { 401, "Unauthorized" },
 { 402, "Payment Required" },                { 403, "Forbidden" },
 { 404, "Not Found" },                       { 405, "Method Not Allowed" },
 { 406, "Not Acceptable" },                  { 407, "Proxy Authentication Required" },
 { 408, "Request Time-out" },                { 409, "Conflict" },
 { 410, "Gone" },                            { 411, "Length Required" },
 { 412, "Precondition Failed" },             { 413, "Request Entity Too Large" },
 { 414, "Request-URI Too Large" },           { 415, "Unsupported Media Type" },
 { 416, "Requested range not satisfiable" }, { 417, "Expectation Failed" },
 { 500, "Internal Server Error" },           { 501, "Not Implemented" },
 { 502, "Bad Gateway" },                     { 503, "Service Unavailable" },
 { 504, "Gateway Time-out" },                { 505, "HTTP Version not supported" }
};

bool Response::isErrorStatus() const {
  switch( getStatus()) {
    case 401: case 403: case 404: case 500: return true;
    default: return false;
  }
};

Str Response::errorPage( int status ) {
  setStatus( status );
  setHtmlContentType();
  return utl::f( R"(<!DOCTYPE html>
<h3 style="color:red">
<img alt="Thumb Down" src="data:image/gif;base64,R0lGODlhFAAQ
  AKEAAAAAAPXesx6Q/7+/vyH5BAEAAAMALAAAAAAUABAAAAI1nI+pB+2+WJh0tjhA3U+9HQCCAH
  ggJZLmGY5l8nHuenYwe2GPZmEe//IhgMJbqDjsIRnBRAEAOw==">
X2 Error :(</h3>
<h2>{1} {2}</h2>
)", theStatus, ( statuses.find( theStatus ))->second );
}

static char hexLookup( int n ) { return "0123456789abcdef"[(n & 0xF)]; }

Str Response::urlEncode( const Str &bytes ) {
  static const Str unsafeChars = " $&+,:;=?@'\"<>#%{}|\\^~[]`/";
  Str result;

  for (unsigned i = 0; i < bytes.size(); ++i) {
    unsigned char c = bytes[ i ];
    if( c <= 31 || c >= 127 || unsafeChars.find( c ) != Str::npos ) {
      result += '%';
      result += hexLookup( c >> 4 );
      result += hexLookup( c );
    } else
      result += (char)c;
  }

  return result;
}


// Request

void Request::get( Str &str ) const {
  size_t cLength = contentLength();
  if( cLength > cfg.server.maxPostSize ) throw ServerError( 413, "too big" );
#ifdef X2FCGILIB
  getStr( str );
#else
  str.resize( cLength );
  FCGX_GetStr( const_cast<char*>( str.data()), str.size(), in );
#endif
}

void Request::put( const Str &str ) {
  if( -1 ==
#ifdef X2FCGILIB
    putStr( str )
#else
    FCGX_PutStr( str.data(), str.size(), out )
#endif
    ) throw std::runtime_error( "Request::put()" );
}

Str Request::docRoot() const {
  Str dRoot = param( "DOCUMENT_ROOT" );
  if( dRoot.empty()) return cfg.docRoot;
  else return dRoot;
}

Str Request::clientIp() const { return param( cfg.client.addressVar.data()); }
Str Request::clientPort() const { return param( cfg.client.portVar.data()); }
Str Request::proto() /** "http" or "https" */ const {
  if( cfg.client.protoVar.empty())
    return cfg.client.proto;
  else
    return param( cfg.client.protoVar.data());
}

Str Request::post( const Str &name ) const {
  Str value;
  for( auto f = posts.lower_bound( name ); f != posts.end() && f->first == name; f ++ ) {
    if( value.empty()) value.assign( f->second );
    else {
     value.resize( value.size() + 1 );
     value.append( f->second );
    }
  }
  return value;
}

void Request::printEnvironment() const {
  output << "<ul>\n";
#ifdef X2FCGILIB
  for( auto &v: params())
    output << "<li>" << v.first << '=' << v.second << "</li>\n";
#else
  for( const char *const *e = envp; *e; ++ e )
    output << "<li>" << *e << "</li>\n";
#endif
  output << "</ul>\n";
}

void Request::printPosts() const {
  output << "<pre>\n";
  for( const auto &el: posts )
    output << el.first << '=' << el.second << '\n';
  output << "</pre>\n";
}

bool Request::parseForm() {
  if( ! isPost()) return false;
  Str ct = contentType(); // RFC2045

  static const char urlencoded[] = "application/x-www-form-urlencoded";
  if( ! strncasecmp( ct.data(), urlencoded, sizeof urlencoded - 1 ))
    return parseFormUrlencoded();

  static const char multipart[] = "multipart/form-data";
  if( ! strncasecmp( ct.data(), multipart, sizeof multipart - 1 ))
    return parseFormMultipart( ct.substr( sizeof multipart - 1 ));

  return false;
}

Str Request::urlDecode( const char *text, const char *end ) {
  Str result; unsigned length = end - text;

  for( unsigned i = 0; i < length; ++i ) {
    char c = text[i];

    if( c == '+' ) {
      result += ' ';
    } else if( c == '%' && i + 2 < length ) {
      Str h( text + i + 1, 2 );
      char *e = 0;
      int hval = std::strtol( h.c_str(), &e, 16 );

      if( *e == 0 ) {
        result += (char)hval;
        i += 2;
      } else
      // not a proper %XX with XX hexadecimal format
        result += c;
    } else
      result += c;
  }

  return result;
}

bool Request::parseJsonRpc( utl::Json &query ) const {
  if( isPost() && contentType() == "application/json" ) {
    Str input; get( input );
    if( ! input.empty()) {
      query.parse( input );
      return true;
    }
  }
  return false;
}

bool Request::parseFormUrlencoded() {
  Str input; get( input );
  if( input.empty()) return false;
  const char *begin = input.data(), *end = begin + input.size();

  const char *p;
  for( p = begin; p < end; ) {
    const char *e = std::find( p, end, '&' );
    const char *name_end = std::find( p, e, '=' );
    if( name_end == e || name_end == p )
      return false;
    Str name = urlDecode( p, name_end );
    Str value = urlDecode( name_end + 1, e );
    posts.insert( std::pair<Str,Str>( name, value ));
    p = e + 1;
  }

  return true;
}


// Thread

static void initSmiles() {
#define smileS( code, title, gettext /** Data for gettext */, file, size ) \
    bbcode::BbCodable::smiles[ code ] = \
    bbcode::Smile( title, cfg.media + "/stdSmiles/" file, size, size )
#define smile( code, title, gettext, file ) smileS( code, title, gettext, file, "1em" )
  smileS( ":SMILE:", "Smile", translate("Smile"), "smile.gif", "1.2em" );
  smile( ":ANGRY:", "Angry", translate("Angry"), "angry.gif" );
  smile( ":AVOID:", "Avoid", translate("Avoid"), "avoid.gif" );
  smile( ":CRY:", "Cry", translate("Cry"), "cry.gif" );
  smile( ":CUTE:", "Cute", translate("Cute"), "cute.gif" );
  smile( ":FLOWERS:", "Flowers", translate("Flowers"), "flowers.gif" );
  smile( ":HEART:", "Heart", translate("Heart"), "heart.gif" );
  smile( ":MISCHIEVOUS:", "Mischievous", translate("Mischievous"), "mischievous.gif" );
  smile( ":PUZZLED:", "Puzzled", translate("Puzzled"), "puzzled.gif" );
  smile( ":SAD:", "Sad", translate("Sad"), "sad.gif" );
  smile( ":SHY:", "Shy", translate("Shy"), "shy.gif" );
  smile( ":SUPRISED:", "Suprised", translate("Suprised"), "suprised.gif" );
  smile( ":TIRED:", "Tired", translate("Tired"), "tired.gif" );
  smile( ":WORRIED:", "Worried", translate("Worried"), "worried.gif" );
  smileS( ":SUPER:", "Super", translate("Super"), "super.gif", "1.4em" );
  smileS( ":BEWILDERED:", "Bewildered", translate("Bewildered"), "bewildered.gif", "1.2em" );
  smileS( ":LAUGH:", "Laugh", translate("Laugh"), "laugh.gif", "1.2em" );
  smile( ":READY:", "Ready", translate("Ready"), "ready.gif" );
  smile( ":OUT:", "Out", translate("Out"), "out.gif" );
  smileS( ":DRUNK:", "Drunk", translate("Drunk"), "drunk.gif", "1.4em" );
  smile( ":THREATENING:", "Threatening", translate("Threatening"), "threatening.gif" );
#undef smile
#undef smileS
}

void Thread::run() {
  cache.init();
  Page::dispatcher.init();
  xss::filter.init();
  initSmiles();

  signal( SIGTERM, killHandler );
  signal( SIGQUIT, killHandler );
  signal( SIGINT, killHandler );
  signal( SIGHUP, killHandler );

  FCGX_Init();
  LOG_DEBUG( "FCGI Lib is inited" );
  if( cfg.server.socketPath != "stdin" ) {
    socketId = FCGX_OpenSocket( cfg.server.socketPath.data(), 8 );
    if( socketId < 0 ) { LOG_FATAL( "Can not open socket" ); return; }
    LOG_INFO( "Socket is opened: " + cfg.server.socketPath );
  }

  for( int i = 1; i < cfg.server.threadCount; i ++ ) {
    all.emplace_back();
    if( pthread_create( &all.back().id, 0,
                        reinterpret_cast<void*(*)(void*)>( staticDoit ), &all.back()))
    {
      //all.erase( -- all.end());
      throw std::runtime_error( "Can not create thread" );
    }
  }

  all.emplace_back(); all.back().id = pthread_self(); all.back().doit();
}

void Thread::cleanup() {
  visited.clear();
  OS_LibShutdown();
  close( socketId );
  LOG_INFO( "Stopped" );
}

void Thread::killHandler( int s ) {
  LOG_INFO( utl::f( "'{1}' signaled", const_cast<const char *>( strsignal( s ))));
  if( s == SIGHUP ) return;
  FCGX_ShutdownPending();
  for( Thread &t: all ) {
    if( t.id == pthread_self()) {
      cleanup();
      exit( 0 );
    }
    if( ! pthread_cancel( t.id )) pthread_join( t.id, 0 );
  }
}

[[ noreturn ]] void *Thread::doit() {
  if( FCGX_InitRequest( &request, socketId, 0 ) != 0 ) {
    LOG_FATAL( "Can not init request" );
    raise( SIGTERM );
  }
  LOG_DEBUG( "FCGI Request is inited" );

  for(;;) {
    LOG_DEBUG( "Try to accept new FCGI request" );
    // pthread_setcanceltype( PTHREAD_CANCEL_ASYNCHRONOUS, 0 );
    /* Some platforms require accept() serialization, some don't..
    static pthread_mutex_t accept_mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_lock( &accept_mutex );  */
    int rc = FCGX_Accept_r( &request );
    /* pthread_mutex_unlock( &accept_mutex ); */
    // pthread_setcanceltype( PTHREAD_CANCEL_DEFERRED, 0 );

    if( rc < 0 ) {
      LOG_FATAL( "Can not accept new request" );
      raise( SIGTERM );
    }
    LOG_DEBUG( "FCGI request is accepted:" + request.uri());

    Str base = Page::dispatcher.url(), path = request.uri();
    while( path[ 0 ] == '/' && path[ 1 ] == '/' ) path = path.substr( 1 );
    if( path.substr( 0, base.size()) != base )
      LOG_INFO( "Path does not match config root, rewrited?" );
    url.tail = path.size() > base.size() ? path.substr( base.size() + 1 ) : Str();

    Page::Demiurge demiurge; Page *page = 0;
    try {
      if( ! ( demiurge = Page::dispatcher.findDemiurge( *this )))
        throw NotFoundError( "url not found; " + request.uri());
      page = demiurge( *this );

      // Json Rpc
      utl::Json query;
      if( request.parseJsonRpc( query )) { // json rpc
        response.setContentType( request.contentType());
        utl::Json answer; int error = page->jsonRpc( query, answer );
        utl::Json result = utl::Json::createObj().add( "result", answer );
        result.add( "error", error ? utl::Json( error ) : utl::Json());
        request.output << result.str();
      } else {
        if( request.parseForm()) // post
          page->doPost();
        else if( request.isGet()) // get
          page->doGet();
        else throw ServerError( 405, "posts and gets only" );
      }

    } catch( const ServerError &e ) {
      LOG_ERROR( e.what() + Str( "; uri=") + request.uri() + "; status=" + n2s( e.status ));
      request.output.str( Str());
      if( page )
        page->renderMsgBox( msgBoxOK | msgBoxError, response.errorPage( e.status ),
                            Page::urlOf<ph::Phorum>());
      else
        request.output << response.errorPage( e.status ) << e.what() << "\r\n";
    } catch( const std::exception &e ) {
      LOG_ERROR( e.what() + Str( "; uri=") + request.uri());
      request.output.str( Str()); request.output
        << response.errorPage( 500 ) << e.what() << "\r\n";
    } catch( ... ) {
      LOG_ERROR( "unknown error; uri=" + request.uri());
      request.output.str( Str()); request.output
        <<  response.errorPage( 500 ) << "unknown error\r\n";
    }

    delete page;

    // FCGX_SetExitStatus( response.getStatus(), request.in );
    request.output << '\n';
    response.setContentLength( request.output.str().size());
    Str headers = response.status()
                + response.contentType()
                + response.contentLength();
    for( const Str &header: response.headers ) headers += header;
    headers     += "\r\n";
    try {
      request.put( headers );
      request.put( request.output.str());
    } catch( const std::exception &e ) { LOG_ERROR( e.what()); };

    request.clear(); response.clear();

    FCGX_Finish_r( &request );

    idle();
  }

  /* return 0; */
}

void Thread::idle() {
  if( ( threadsCount ++ ) == 0 ) {
    visited.idle();
  }
  threadsCount --;
}

int Thread::socketId = 0;
list<Thread> Thread::all;
std::atomic_uint Thread::threadsCount( 0 );

} // x2

